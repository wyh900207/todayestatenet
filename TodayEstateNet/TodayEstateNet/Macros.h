//
//  Macros.h
//  TodayEstateNet
//
//  Created by  HomerLynn on 2018/12/11.
//  Copyright © 2018 TEN. All rights reserved.
//

#ifndef Macros_h
#define Macros_h

#define TENScreenWidth [UIScreen mainScreen].bounds.size.width
#define TENScreenHeight [UIScreen mainScreen].bounds.size.height
// 判断值是否为空, 为空返回 空字符串
#define DL_IS_NULL(text) [DLNull isEmptyString:text] ? @"" : text
#define DL_IS_NULL_WITH(text, defult) text ? [DLNull isEmptyString:text] : text
#define TEN_IS_LOGIN @"isLoginState"
//通知
#define TENChangeBtnName @"TENChangeBtnName"

#define kAccountId TEN_UD_VALUE(@"yyAccount.id")
#define kAvatarUrl TEN_UD_VALUE(@"yyUser.avatar")
#define kAccountType TEN_UD_VALUE(@"yyAccount.type")
#define kLoginState TEN_UD_BOOL(@"isLoginState")
#endif /* Macros_h */

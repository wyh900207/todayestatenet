//
//  TENBaseNetwork.m
//  TENBaseNetwork
//
//  Created by HomerLynn on 2018/9/9.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENBaseNetwork.h"
#import "SVProgressHUD.h"
#import "NSObject+Common.h"
#define TOKEN @"96e79218965eb72c92a549dd5a330112"

@implementation TENBaseNetwork

+ (instancetype)shareInstance {
    static TENBaseNetwork *network = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        network = [TENBaseNetwork new];
    });
    
    return network;
}

/*
+ (AFHTTPSessionManager *)shareInstance {
    static dispatch_once_t onceToken;
    static AFHTTPSessionManager  *manager = nil;
    dispatch_once(&onceToken, ^{
        manager = [[AFHTTPSessionManager alloc] init];
        manager.responseSerializer = [AFJSONResponseSerializer serializer];
        manager.requestSerializer = [AFJSONRequestSerializer serializer];
    });
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",nil];
    [manager.requestSerializer setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 60;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    
    return manager;
}

+ (AFHTTPSessionManager *)tokenShareInstance
{
    static dispatch_once_t onceToken;
    static AFHTTPSessionManager  *manager = nil;
    dispatch_once(&onceToken, ^{
        manager = [[AFHTTPSessionManager alloc]init];
        manager.responseSerializer = [AFHTTPResponseSerializer serializer];
        manager.requestSerializer = [AFHTTPRequestSerializer serializer];
    });
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",@"charset=utf-8",nil];
    [manager.requestSerializer setValue:@"application/json"forHTTPHeaderField:@"Content-Type"];
    [manager.requestSerializer setValue:TOKEN forHTTPHeaderField:@"Authorization"];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 60;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    
    return manager;
}

//post请求
+(void)post:(NSString *)method requestParams:(NSDictionary *)params success:(void(^)(NSURLSessionDataTask  *task, id responseObject))success failure:(void(^)(NSURLSessionDataTask  *task, id responseObject))failure orToken:(BOOL)isToken
{
    AFHTTPSessionManager  *session = nil;
    if (isToken == YES) {
        session = [TENBaseNetwork tokenShareInstance];
    }
    else
    {
        session = [TENBaseNetwork shareInstance];
    }
    session.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript",@"text/html",nil];
    [session.requestSerializer setValue:@"application/json;charset=UTF-8"forHTTPHeaderField:@"Content-Type"];
    [session.requestSerializer setValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Accept"];
    session.requestSerializer = [AFHTTPRequestSerializer serializer];
    session.requestSerializer.stringEncoding = NSUTF8StringEncoding;
    [session POST:method parameters:params progress:nil success:success failure:failure];
}

//get请求
+(void)get:(NSString *)method requestParams:(NSDictionary *)params success:(void(^)(NSURLSessionDataTask  *task, id responseObject))success failure:(void(^)(NSURLSessionDataTask  *task, id responseObject))failure orToken:(BOOL)isToken
{
    AFHTTPSessionManager  *session = nil;
    if (isToken == YES) {
        session = [TENBaseNetwork tokenShareInstance];
    }
    else
    {
        session = [TENBaseNetwork shareInstance];
    }
    session.requestSerializer = [AFHTTPRequestSerializer serializer];
    [session GET:method parameters:params progress:nil success:success failure:failure];
}
 */

- (void)get:(const NSString *)url params:(NSDictionary *)params success:(void (^)(NSDictionary *dict))success failure:(void (^)(NSError *error))failure {
    [self requestWith:@"GET" url:url params:params success:success failure:failure auth:NO hud:YES];
}

- (void)post:(const NSString *)url params:(NSDictionary *)params success:(void (^)(NSDictionary *dict))success failure:(void (^)(NSError *error))failure {
    [self requestWith:@"POST" url:url params:params success:success failure:failure auth:NO hud:YES];
}
- (void)post:(const NSString *)url params:(NSDictionary *)params success:(void (^)(NSDictionary *dict))success failure:(void (^)(NSError *error))failure Hud:(BOOL)isHud {
    [self requestWith:@"POST" url:url params:params success:success failure:failure auth:NO hud:isHud];
};
- (void)authGet:(const NSString *)url params:(NSDictionary *)params success:(void (^)(NSDictionary *dict))success failure:(void (^)(NSError *error))failure {
    [self requestWith:@"GET" url:url params:params success:success failure:failure auth:YES hud:YES];
}

- (void)authPost:(const NSString *)url params:(NSDictionary *)params success:(void (^)(NSDictionary *dict))success failure:(void (^)(NSError *error))failure {
    [self requestWith:@"POST" url:url params:params success:success failure:failure auth:YES hud:YES];
}

#pragma mark - 发起请求

- (void)requestWith:(NSString *)method url:(const NSString *)url params:(NSDictionary *)params success:(void (^)(NSDictionary *))success failure:(void (^)(NSError *))failure auth:(BOOL)isAuth hud:(BOOL)hud{
    if (hud) {
        [SVProgressHUD setDefaultStyle:SVProgressHUDStyleDark];
        [SVProgressHUD showWithStatus:@"请稍等..."];
    }
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager.requestSerializer willChangeValueForKey:@"timeoutInterval"];
    manager.requestSerializer.timeoutInterval = 8.f;
    [manager.requestSerializer didChangeValueForKey:@"timeoutInterval"];
    manager.securityPolicy = [AFSecurityPolicy policyWithPinningMode:AFSSLPinningModeNone];//不设置会报-1016或者会有编码问题
    manager.requestSerializer = [AFHTTPRequestSerializer serializer]; //不设置会报-1016或者会有编码问题
    manager.responseSerializer = [AFHTTPResponseSerializer serializer]; //不设置会报 error 3840 forHTTPHeaderField:@"Accept"];
    [manager.responseSerializer setAcceptableContentTypes:[NSSet setWithObjects:@"application/json",@"text/json", @"text/javascript",@"text/html",@"text/plain",nil]];
    NSMutableURLRequest *request = [[AFJSONRequestSerializer serializer] requestWithMethod:method URLString:(NSString *)url parameters:nil error:nil];
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json;charset=UTF-8" forHTTPHeaderField:@"Accept"];
    if (isAuth) {
        // TODO: 自己存储一下token, 手动把这里替换掉
        [request addValue:@"eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ0ZXN0MSIsImNyZWF0ZWQiOjE1MzcxOTgxMTQyNzQsImV4cCI6MTUzNzgwMjkxNH0.L36VPLV7lS9zqOOXeMvh8lrHVZocUTpAwFMKf8zyMbSDt30f2qINTdpDknnLAQIklfraoZuFoxqpYlifI4iKeA" forHTTPHeaderField:@"Authorization"];
    }
    NSString *jsonString;
    if (params) {
        jsonString = [TENBaseNetwork dictionaryToJson:params];
        NSData *body = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
        [request setHTTPBody:body];
    }
    
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
    [[manager dataTaskWithRequest:request completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        if (hud) {
            [SVProgressHUD dismiss];
        }
        if (error) {
            NSData *data = error.userInfo[@"com.alamofire.serialization.response.error.data"] ;
            NSString *errorStr = [[ NSString alloc ] initWithData:data encoding:NSUTF8StringEncoding];
            [NSObject showHudTipStr:@"请检查网络后重试"];
            NSLog(@"%@", errorStr);
            failure(error);
            return;
        }
        NSDictionary *dict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingMutableContainers error:nil];
        if (error) {
            failure(error);
        } else {
            success(dict);
        }
    }] resume];
#pragma clang diagnostic pop
}

#pragma mark - NSString to JSON

+ (NSString*)dictionaryToJson:(NSDictionary *)dic {
    NSError *parseError =nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:dic options:0 error:&parseError];
    return [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
}


@end

//
//  TENBaseNetwork.h
//  TENBaseNetwork
//
//  Created by HomerLynn on 2018/9/9.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>
@interface TENBaseNetwork : NSObject

+ (instancetype)shareInstance;

// 不需要token
- (void)get:(const NSString *)url params:(NSDictionary *)params success:(void (^)(NSDictionary *dict))success failure:(void (^)(NSError *error))failure;
- (void)post:(const NSString *)url params:(NSDictionary *)params success:(void (^)(NSDictionary *dict))success failure:(void (^)(NSError *error))failure;
- (void)post:(const NSString *)url params:(NSDictionary *)params success:(void (^)(NSDictionary *dict))success failure:(void (^)(NSError *error))failure Hud:(BOOL)isHud;
// 需要token
- (void)authGet:(const NSString *)url params:(NSDictionary *)params success:(void (^)(NSDictionary *dict))success failure:(void (^)(NSError *error))failure;
- (void)authPost:(const NSString *)url params:(NSDictionary *)params success:(void (^)(NSDictionary *dict))success failure:(void (^)(NSError *error))failure;

@end

//
//  NSString+URL.h
//  TENNetwork
//
//  Created by HomerLynn on 2018/9/22.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (URL)

@property (nonatomic, copy, readonly) NSString *url;

// 是否为有效的URL
- (BOOL)isActive;

@end

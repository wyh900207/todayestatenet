//
//  NSString+URL.m
//  TENNetwork
//
//  Created by HomerLynn on 2018/9/22.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "NSString+URL.h"
#import "TENNetwork.h"
@implementation NSString (URL)

- (NSString *)url {
    return [baseURL stringByAppendingPathComponent:self];
}

- (BOOL)isActive {
    if(self == nil) {
        return NO;
    }
    
    if ([self containsString:@"://"]) {
        return YES;
    } else {
        return NO;
    }
}

@end

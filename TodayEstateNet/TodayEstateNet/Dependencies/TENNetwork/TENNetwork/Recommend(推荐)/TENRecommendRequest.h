//
//  TENRecommendRequest.h
//  TENNetwork
//
//  Created by  HomerLynn on 2018/10/27.
//  Copyright © 2018 TEN. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TENRecommendRequest : NSObject
//推荐：活动
- (void)fetchActivityDataWithCityId:(NSString *)cityId  parameter:(NSDictionary *)parameter response:(void(^)(NSDictionary * responseObject))response failure:(void(^)(NSError *error))failure;
//推荐：楼盘
+ (void)fetchBuildingDataWithCityId:(NSString *)cityId  parameter:(NSDictionary *)parameter response:(void(^)(NSDictionary * responseObject))response failure:(void(^)(NSError *error))failure;
//推荐：头条
+ (void)fetchNewsDataWithCityId:(NSString *)cityId groupId:(NSString *)groupId parameter:(NSDictionary *)parameter response:(void (^)(NSDictionary *))response failure:(void (^)(NSError *))failure;

// 1. 本地新闻传cityId,
// 2. 精选 百科 爆料 传typeId 分别为109 110 111
// 3. 不传cityId 和 typeId 为全部
// 自定义: 本地 typeId = 112, 全部 = 113
+ (void)fetchNewsWithTypeId:(NSNumber *)typeId response:(void (^)(NSDictionary *))response failure:(void (^)(NSError *))failure;

@end


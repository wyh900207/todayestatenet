//
//  TENRecommendRequest.m
//  TENNetwork
//
//  Created by  HomerLynn on 2018/10/27.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENRecommendRequest.h"
#import "TENBaseNetwork.h"
#import "TENNetwork.h"

@implementation TENRecommendRequest
    
- (void)fetchActivityDataWithCityId:(NSString *)cityId  parameter:(NSDictionary *)parameter response:(void(^)(NSDictionary * responseObject))response failure:(void(^)(NSError *error))failure {
    NSDictionary *params = @{@"cityId": cityId,
                             @"pageNo": @1,
                             @"pageSize": @20,
                             };
    NSMutableDictionary *tem = [params mutableCopy];
    if (parameter) {
        for (NSString *key in parameter.allKeys) {
            [tem setValue:parameter[key] forKey:key];
        }
    }
    
    [[TENBaseNetwork shareInstance] post:kRecommendActivityURL.url params:tem success:^(NSDictionary *dict) {
        if (response) response(dict);
    } failure:^(NSError *error) {
        if (failure) failure(error);
    }];
}
+ (void)fetchBuildingDataWithCityId:(NSString *)cityId parameter:(NSDictionary *)parameter response:(void (^)(NSDictionary *))response failure:(void (^)(NSError *))failure{
    NSDictionary *params = @{@"cityId": cityId,
                             @"pageNo": @1,
                             @"pageSize": @20,
                             };
    NSMutableDictionary *tem = [params mutableCopy];
    if (parameter) {
        for (NSString *key in parameter.allKeys) {
            [tem setValue:parameter[key] forKey:key];
        }
    }
    
    [[TENBaseNetwork shareInstance] post:kRecommendBuildingURL.url params:tem success:^(NSDictionary *dict) {
        if (response) response(dict);
    } failure:^(NSError *error) {
        if (failure) failure(error);
    }];
}
//新闻
+ (void)fetchNewsDataWithCityId:(NSString *)cityId groupId:(NSString *)groupId parameter:(NSDictionary *)parameter response:(void (^)(NSDictionary *))response failure:(void (^)(NSError *))failure{
    NSDictionary *params = @{@"cityId": cityId,
                             @"pageNo": @1,
                             @"pageSize": @20,
                             @"buildingGroupId":groupId,
                             @"typeId":@"0"
                             };
    NSMutableDictionary *tem = [params mutableCopy];
    if (parameter) {
        for (NSString *key in parameter.allKeys) {
            [tem setValue:parameter[key] forKey:key];
        }
    }
    
    [[TENBaseNetwork shareInstance] post:kRecommendNewsURL.url params:tem success:^(NSDictionary *dict) {
        if (response) response(dict);
    } failure:^(NSError *error) {
        if (failure) failure(error);
    }];
}

+ (void)fetchNewsWithTypeId:(NSNumber *)typeId response:(void (^)(NSDictionary *))response failure:(void (^)(NSError *))failure {
    
    NSMutableDictionary *params = [@{} mutableCopy];
    
    NSString *type = nil;
    NSString *city = nil;
    
    switch (typeId.intValue) {
        case 112: {
            city = @"310100";
        }
            break;
        case 113: {
            type = nil;
        }
            break;
            
        default: {
            type = typeId.stringValue;
        }
            break;
    }
    
    params[@"pageNo"] = @1;
    params[@"pageSize"] = @20;
    // 本地新闻
    if (city && ![city isEqualToString:@""]) {
        params[@"cityId"] = city;
    }
    // 精选 百科 爆料 传typeId 分别为109 110 111
    if (type && ![type isEqualToString:@""]) {
        params[@"typeId"] = type;
    }
    
    [[TENBaseNetwork shareInstance] post:kRecommendNewsURL.url params:params success:^(NSDictionary *dict) {
        if (response) response(dict);
    } failure:^(NSError *error) {
        if (failure) failure(error);
    }];
}

@end

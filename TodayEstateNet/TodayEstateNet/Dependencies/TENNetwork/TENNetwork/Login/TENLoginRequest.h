//
//  TENLoginRequest.h
//  TENNetwork
//
//  Created by  HomerLynn on 2018/12/13.
//  Copyright © 2018 TEN. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TENLoginRequest : NSObject

+ (void)loginWithParams:(NSDictionary *)params success:(void (^)(NSDictionary *))success failure:(void (^)(NSError *))failure;
+ (void)fetchPictureMsgCoder:(void (^)(NSDictionary *))success failure:(void (^)(NSError *))failure;
+ (void)fetchSmsCode:(NSDictionary *)params success:(void (^)(NSDictionary *))success failure:(void (^)(NSError *))failure;
+ (void)bindingPhone:(NSDictionary *)params success:(void (^)(NSDictionary *))success failure:(void (^)(NSError *))failure;
//设置。更改支付密码
+ (void)setPWDWithParameters:(NSDictionary *)params success:(void (^)(NSDictionary *))success failure:(void (^)(NSError *))failure;

@end

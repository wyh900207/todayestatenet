//
//  TENLoginRequest.m
//  TENNetwork
//
//  Created by  HomerLynn on 2018/12/13.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENLoginRequest.h"
#import "TENBaseNetwork.h"
#import "TENNetwork.h"

@implementation TENLoginRequest

+ (void)loginWithParams:(NSDictionary *)params success:(void (^)(NSDictionary *))success failure:(void (^)(NSError *))failure {
    NSString *url = [TENNetwork urlWith:kWechaLoginURL];
    [[TENBaseNetwork shareInstance] post:url params:params success:^(NSDictionary *dict) {
        if (success) success(dict);
    } failure:^(NSError *error) {
        if (failure) failure(error);
    }];
}

+ (void)fetchPictureMsgCoder:(void (^)(NSDictionary *))success failure:(void (^)(NSError *))failure {
    NSString *url = [TENNetwork urlWith:kWechaLoginURL];
    [[TENBaseNetwork shareInstance] post:url params:nil success:^(NSDictionary *dict) {
        if (success) success(dict);
    } failure:^(NSError *error) {
        if (failure) failure(error);
    }];
}

+ (void)fetchSmsCode:(NSDictionary *)params success:(void (^)(NSDictionary *))success failure:(void (^)(NSError *))failure {
    NSString *url = [TENNetwork urlWith:kMessageCodeURL];
    [[TENBaseNetwork shareInstance] post:url params:params success:^(NSDictionary *dict) {
        if (success) success(dict);
    } failure:^(NSError *error) {
        if (failure) failure(error);
    }];
}

+ (void)bindingPhone:(NSDictionary *)params success:(void (^)(NSDictionary *))success failure:(void (^)(NSError *))failure {
    NSString *url = [TENNetwork urlWith:kBindingPhoneURL];
    [[TENBaseNetwork shareInstance] post:url params:params success:^(NSDictionary *dict) {
        if (success) success(dict);
    } failure:^(NSError *error) {
        if (failure) failure(error);
    }];
}

//设置。更改支付密码
+ (void)setPWDWithParameters:(NSDictionary *)params success:(void (^)(NSDictionary *))success failure:(void (^)(NSError *))failure{
    NSString *url = [TENNetwork urlWith:kSetPWDURL];
    [[TENBaseNetwork shareInstance] post:url params:params success:^(NSDictionary *dict) {
        if (success) success(dict);
    } failure:^(NSError *error) {
        if (failure) failure(error);
    }];
}

@end

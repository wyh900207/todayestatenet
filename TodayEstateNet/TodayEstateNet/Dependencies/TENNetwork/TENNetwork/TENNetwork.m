//
//  TENNetwork.m
//  TENNetwork
//
//  Created by HomerLynn on 2018/9/9.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENNetwork.h"

#pragma mark - 基地址

const NSString *baseURL = @"http://api.jrfw360.com";

#pragma mark - URL

// 认证
const NSString *kAuthenticationURL = @"/api/auth";

// -------------- 首页 ---------------
// 首页
const NSString *kHomePageURL = @"/api/index";
// 根据城市定位
const NSString *kLocationWithCityNameURL = @"/api/locate";
// 获取首页房型查询条件接口
const NSString *kRoomTypeDataURL = @"/api/buildingModuleTypes";
// 首页根据`CityID`或`CityName`获取内容
const NSString *kHomePageDataURL = @"/buildingGroup/index";
// 楼盘详情
const NSString *kHouseDetailURL = @"/buildingGroup/detail";
const NSString *kFavoriteURL = @"/buildGroupFavorite/addFavorite";
const NSString *kFavoriteListURL = @"/buildGroupFavorite/getFavorites";
const NSString *kSubscribeURL = @"/booking/building";
// 搜索城市
const NSString *kSearchByCityName = @"city/getCityByName";

// -------------- 推荐 ---------------

const NSString *kRecommendActivityURL = @"/activity/list";
const NSString *kRecommendBuildingURL = @"/buildingGroup/commendList";
const NSString *kRecommendNewsURL = @"/news/list";

// -------------- 找房 ---------------
//找房
const NSString *kSearchPageDataURL = @"/buildingGroup/findCondition";
const NSString *kSearchMapListURL = @"/buildingMap/getList";
//筛选
const NSString *kSearchFindURL = @"/buildingGroup/find";

// -------------- 登录 ---------------
// 微信登录
const NSString *kWechaLoginURL = @"/account/thirdLogin";
// 登录
const NSString *kUserLoginURL = @"/user/login";
// Picture code
const NSString *kPictureCodeURL = @"/code/getCode";
// Message code
const NSString *kMessageCodeURL = @"/account/getSmsCode";
// Binding phone and password
const NSString *kBindingPhoneURL = @"/account/bindPhone";

// -------------- 消息 ---------------
// Unread message count
const NSString *kMessageUnreadCountURL = @"/system/myMsn";
// Message list
const NSString *kMessageListURL = @"/system/getMsnList";
// Chat list
const NSString *kChatListURL = @"/group/getMyList";

// -------------- 消息 聊天 ---------------
const NSString *kOnlineQListURL = @"/group/getQuestionMsnList";
const NSString *kSendMsgURL = @"/group/sendMsn";

// -------------- 我的 ---------------
const NSString *kGetSharePicURL = @"/account/getSharePic";

// -------------- 钱包相关------------------
const NSString *kGetBalance = @"/payment/getMyList";
const NSString *kSetPWDURL = @"/account/savePayPassword";
const NSString *kRechargeURL = @"/payment/recharge";
const NSString *kZfbUnifiedOrderURL = @"/app/pay/zfbUnifiedorder";
const NSString *kCheckZfbOrderURL = @"/app/pay/zfbOrderQuery";
const NSString *kZfbWithdrawURL = @"/app/pay/zfbWithdraw";
const NSString *kGetAllOrderListURL = @"/app/pay/getMyOrder";
const NSString *kGetAlipayResultURL = @"/app/pay/zfbOrderQuery";
const NSString *kDeleteUnpayOrderURL = @"/app/pay/deleteMyOrder";
const NSString *kWxunifiedOrderURL = @"/app/pay/wxUnifiedorder";
const NSString *kWxCheckOrderURL = @"/app/pay/wxOrderQuery";

@implementation TENNetwork

+ (NSString *)urlWith:(const NSString *)urlKey {
    return [baseURL stringByAppendingPathComponent:urlKey];
//    return [NSString stringWithFormat:@"%@%@", baseURL, urlKey];
}

@end

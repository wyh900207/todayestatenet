//
//  TENMineRequest.h
//  HouseEconomics
//
//  Created by apple on 2019/1/8.
//  Copyright © 2019  HomerLynn. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TENMineRequest : NSObject

//获取我的分享图片
+ (void)fetchMineSharePicWithAccountId:(NSString *)accountId Success:(void (^)(NSDictionary *))success failure:(void (^)(NSError *))failure;

@end

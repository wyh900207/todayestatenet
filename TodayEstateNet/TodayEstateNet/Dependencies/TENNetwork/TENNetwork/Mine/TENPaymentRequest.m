//
//  TENPaymentRequest.m
//  HouseEconomics
//
//  Created by apple on 2019/2/22.
//  Copyright © 2019  HomerLynn. All rights reserved.
//

#import "TENPaymentRequest.h"
#import "TENBaseNetwork.h"
#import "TENNetwork.h"
@implementation TENPaymentRequest

//获取余额 和收支明细
+ (void)getBalanceAndListParameters:(NSDictionary *)params response:(void(^)(NSDictionary * responseObject))response failure:(void(^)(NSError *error))failure{
    [[TENBaseNetwork shareInstance] post:[TENNetwork urlWith:kGetBalance] params:params success:^(NSDictionary *dict) {
        if (response) response(dict);
    } failure:^(NSError *error) {
        if (failure) failure(error);
    }];
}

//充值
+ (void)rechargeWithParameters:(NSDictionary *)params response:(void(^)(NSDictionary * responseObject))response failure:(void(^)(NSError *error))failure{
    [[TENBaseNetwork shareInstance] post:[TENNetwork urlWith:kRechargeURL] params:params success:^(NSDictionary *dict) {
        if (response) response(dict);
    } failure:^(NSError *error) {
        if (failure) failure(error);
    }];
}

//支付宝统一下单
+ (void)zfbUnifiedOrderWithParameters:(NSDictionary *)params response:(void(^)(NSDictionary * responseObject))response failure:(void(^)(NSError *error))failure{
    [[TENBaseNetwork shareInstance] post:[TENNetwork urlWith:kZfbUnifiedOrderURL] params:params success:^(NSDictionary *dict) {
        if (response) response(dict);
    } failure:^(NSError *error) {
        if (failure) failure(error);
    }];
}

//查询支付宝订单
+ (void)checkZfbOrderWithParameters:(NSDictionary *)params response:(void(^)(NSDictionary * responseObject))response failure:(void(^)(NSError *error))failure{
    [[TENBaseNetwork shareInstance] post:[TENNetwork urlWith:kCheckZfbOrderURL] params:params success:^(NSDictionary *dict) {
        if (response) response(dict);
    } failure:^(NSError *error) {
        if (failure) failure(error);
    }];
}

//支付宝提现
+ (void)zfbWithdrawWithParameters:(NSDictionary *)params response:(void(^)(NSDictionary * responseObject))response failure:(void(^)(NSError *error))failure{
    [[TENBaseNetwork shareInstance] post:[TENNetwork urlWith:kZfbWithdrawURL] params:params success:^(NSDictionary *dict) {
        if (response) response(dict);
    } failure:^(NSError *error) {
        if (failure) failure(error);
    }];
}

//获取所有订单
+ (void)getAllOrderListsWithParameters:(NSDictionary *)params response:(void(^)(NSDictionary * responseObject))response failure:(void(^)(NSError *error))failure{
    [[TENBaseNetwork shareInstance] post:[TENNetwork urlWith:kGetAllOrderListURL] params:params success:^(NSDictionary *dict) {
        if (response) response(dict);
    } failure:^(NSError *error) {
        if (failure) failure(error);
    }];
}

//查询支付宝订单状态
+ (void)checkAlipayResultWithParameters:(NSDictionary *)params response:(void(^)(NSDictionary * responseObject))response failure:(void(^)(NSError *error))failure{
    [[TENBaseNetwork shareInstance] post:[TENNetwork urlWith:kGetAlipayResultURL] params:params success:^(NSDictionary *dict) {
        if (response) response(dict);
    } failure:^(NSError *error) {
        if (failure) failure(error);
    }];
}

//删除未支付订单
+ (void)deleteUnpayOrderWithParameters:(NSDictionary *)params response:(void(^)(NSDictionary * responseObject))response failure:(void(^)(NSError *error))failure{
    [[TENBaseNetwork shareInstance] post:[TENNetwork urlWith:kDeleteUnpayOrderURL] params:params success:^(NSDictionary *dict) {
        if (response) response(dict);
    } failure:^(NSError *error) {
        if (failure) failure(error);
    }];
}

//微信统一下单
+ (void)wxUnifiedOrderWithParameters:(NSDictionary *)params response:(void(^)(NSDictionary * responseObject))response failure:(void(^)(NSError *error))failure{
    [[TENBaseNetwork shareInstance] post:[TENNetwork urlWith:kWxunifiedOrderURL] params:params success:^(NSDictionary *dict) {
        if (response) response(dict);
    } failure:^(NSError *error) {
        if (failure) failure(error);
    }];
}
//查询微信订单状态
+ (void)checkWxResultWithParameters:(NSDictionary *)params response:(void(^)(NSDictionary * responseObject))response failure:(void(^)(NSError *error))failure{
    [[TENBaseNetwork shareInstance] post:[TENNetwork urlWith:kWxCheckOrderURL] params:params success:^(NSDictionary *dict) {
        if (response) response(dict);
    } failure:^(NSError *error) {
        if (failure) failure(error);
    }];
}
@end

//
//  TENPaymentRequest.h
//  HouseEconomics
//
//  Created by apple on 2019/2/22.
//  Copyright © 2019  HomerLynn. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface TENPaymentRequest : NSObject
//获取余额 和收支明细
+ (void)getBalanceAndListParameters:(NSDictionary *)params response:(void(^)(NSDictionary * responseObject))response failure:(void(^)(NSError *error))failure;
//充值
+ (void)rechargeWithParameters:(NSDictionary *)params response:(void(^)(NSDictionary * responseObject))response failure:(void(^)(NSError *error))failure;

//支付宝统一下单
+ (void)zfbUnifiedOrderWithParameters:(NSDictionary *)params response:(void(^)(NSDictionary * responseObject))response failure:(void(^)(NSError *error))failure;
//查询支付宝订单
+ (void)checkZfbOrderWithParameters:(NSDictionary *)params response:(void(^)(NSDictionary * responseObject))response failure:(void(^)(NSError *error))failure;
//支付宝提现
+ (void)zfbWithdrawWithParameters:(NSDictionary *)params response:(void(^)(NSDictionary * responseObject))response failure:(void(^)(NSError *error))failure;
//获取所有订单
+ (void)getAllOrderListsWithParameters:(NSDictionary *)params response:(void(^)(NSDictionary * responseObject))response failure:(void(^)(NSError *error))failure;
//查询支付宝订单状态
+ (void)checkAlipayResultWithParameters:(NSDictionary *)params response:(void(^)(NSDictionary * responseObject))response failure:(void(^)(NSError *error))failure;
//删除未支付订单
+ (void)deleteUnpayOrderWithParameters:(NSDictionary *)params response:(void(^)(NSDictionary * responseObject))response failure:(void(^)(NSError *error))failure;

//微信统一下单
+ (void)wxUnifiedOrderWithParameters:(NSDictionary *)params response:(void(^)(NSDictionary * responseObject))response failure:(void(^)(NSError *error))failure;
//查询微信订单状态
+ (void)checkWxResultWithParameters:(NSDictionary *)params response:(void(^)(NSDictionary * responseObject))response failure:(void(^)(NSError *error))failure;
@end

NS_ASSUME_NONNULL_END

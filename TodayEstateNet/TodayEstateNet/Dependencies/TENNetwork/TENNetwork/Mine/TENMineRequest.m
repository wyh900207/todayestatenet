//
//  TENMineRequest.m
//  HouseEconomics
//
//  Created by apple on 2019/1/8.
//  Copyright © 2019  HomerLynn. All rights reserved.
//

#import "TENMineRequest.h"
#import "TENBaseNetwork.h"
#import "TENNetwork.h"

@implementation TENMineRequest

//获取我的分享图片
+ (void)fetchMineSharePicWithAccountId:(NSString *)accountId Success:(void (^)(NSDictionary *))success failure:(void (^)(NSError *))failure{
    NSString *url = [TENNetwork urlWith:kGetSharePicURL];
    [[TENBaseNetwork shareInstance] post:url params:@{@"accountId":accountId} success:^(NSDictionary *dict) {
        if (success) success(dict);
    } failure:^(NSError *error) {
        if (failure) failure(error);
    }];
}

@end

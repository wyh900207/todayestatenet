//
//  TENNetwork.h
//  TENNetwork
//
//  Created by HomerLynn on 2018/9/9.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NSString+URL.h"
#import "TENAuthRequest.h"
#import "TENHomepageRequest.h"


typedef void (^FinishBlock)(id data);

@interface TENNetwork : NSObject

extern const NSString *baseURL;

// -------------- 首页 ---------------
    
extern const NSString *kAuthenticationURL;
extern const NSString *kHomePageURL;
extern const NSString *kLocationWithCityNameURL;
extern const NSString *kRoomTypeDataURL;
extern const NSString *kHomePageDataURL;
extern const NSString *kHouseDetailURL;
//楼盘详情
extern const NSString *kFavoriteURL;
extern const NSString *kFavoriteListURL;
extern const NSString *kSubscribeURL;
extern const NSString *kSearchByCityName;

// -------------- 找房 ---------------

extern const NSString *kSearchPageDataURL;
extern const NSString *kSearchFindURL;
extern const NSString *kSearchMapListURL;
// -------------- 消息 ---------------

extern const NSString *kMessageUnreadCountURL;
extern const NSString *kMessageListURL;
extern const NSString *kChatListURL;
// -------------- 消息 聊天 ---------------
extern const NSString *kOnlineQListURL;
extern const NSString *kSendMsgURL;
// -------------- 推荐 ---------------
    
extern const NSString *kRecommendActivityURL;
extern const NSString *kRecommendBuildingURL;
extern const NSString *kRecommendNewsURL;

// -------------- 登录 ---------------

extern const NSString *kWechaLoginURL;
extern const NSString *kUserLoginURL;
extern const NSString *kPictureCodeURL;
extern const NSString *kMessageCodeURL;
extern const NSString *kBindingPhoneURL;

// -------------- 我的 ---------------

extern const NSString *kGetSharePicURL;

// -------------- 钱包相关------------------
extern const NSString *kGetBalance;
extern const NSString *kSetPWDURL;
extern const NSString *kRechargeURL;
extern const NSString *kZfbUnifiedOrderURL;
extern const NSString *kCheckZfbOrderURL;
extern const NSString *kZfbWithdrawURL;
extern const NSString *kGetAllOrderListURL;
extern const NSString *kGetAlipayResultURL;
extern const NSString *kDeleteUnpayOrderURL;
extern const NSString *kWxunifiedOrderURL;
extern const NSString *kWxCheckOrderURL;
    
// 根据上边定义的key获取URL
+ (NSString *)urlWith:(const NSString *)urlKey;

@end

//
//  TENLocationRequest.h
//  TENNetwork
//
//  Created by HomerLynn on 2018/9/22.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TENLocationRequest : NSObject

+ (void)location:(NSString *)cityName response:(void(^)(NSDictionary * responseObject))response failure:(void(^)(NSError *error))failure;

@end

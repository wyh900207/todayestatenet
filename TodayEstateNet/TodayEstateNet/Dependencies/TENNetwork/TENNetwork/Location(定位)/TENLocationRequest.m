//
//  TENLocationRequest.m
//  TENNetwork
//
//  Created by HomerLynn on 2018/9/22.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENLocationRequest.h"
#import "TENBaseNetwork.h"
#import "TENNetwork.h"

@implementation TENLocationRequest

+ (void)location:(NSString *)cityName response:(void(^)(NSDictionary * responseObject))response failure:(void(^)(NSError *error))failure {
    NSDictionary *locationParams = @{@"cityName": cityName};
    [[TENBaseNetwork shareInstance] post:kLocationWithCityNameURL.url params:locationParams success:^(NSDictionary *dict) {
        if (response) response(dict);
    } failure:^(NSError *error) {
        if (failure) failure(error);
    }];
}


@end

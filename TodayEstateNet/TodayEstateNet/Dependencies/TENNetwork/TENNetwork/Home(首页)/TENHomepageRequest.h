//
//  TENHomepageRequest.h
//  TENNetwork
//
//  Created by HomerLynn on 2018/9/21.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TENHomepageRequest : NSObject

+ (void)fetchHomeControllerDataWithCityCode:(NSString *)cityCode page:(NSInteger)page response:(void(^)(NSDictionary * responseObject))response failure:(void(^)(NSError *error))failure;

+(void)requestHomeControllerDataWithCityName:(NSString *)cityName response:(void(^)(NSDictionary * responseObject))  response failure:(void(^)(NSError *error))failure;
//获取首页房型查询条件接口
+(void)requestHomeCotrollerRoomTypeData:(NSString *)parm response:(void(^)(NSDictionary * responseObject))  response failure:(void(^)(NSError *error))failure;
// 根据城市名获取首页内容
+ (void)fetchHomePageDataWithParameter:(NSDictionary *)params response:(void(^)(NSDictionary * responseObject))response failure:(void(^)(NSError *error))failure;
// 获取海报
+ (void)fetchHomePagePostViewDataWithResponse:(void(^)(NSDictionary * responseObject))response failure:(void(^)(NSError *error))failure;

@end

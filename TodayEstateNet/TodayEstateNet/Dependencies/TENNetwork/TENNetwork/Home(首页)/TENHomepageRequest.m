//
//  TENHomepageRequest.m
//  TENNetwork
//
//  Created by HomerLynn on 2018/9/21.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENHomepageRequest.h"
#import "TENBaseNetwork.h"
#import "TENNetwork.h"

@implementation TENHomepageRequest

+ (void)fetchHomeControllerDataWithCityCode:(NSString *)cityCode page:(NSInteger)page response:(void(^)(NSDictionary * responseObject))response failure:(void(^)(NSError *error))failure {
    NSDictionary *params = @{@"cityId": cityCode, @"page": @(page)};
    [[TENBaseNetwork shareInstance] post:kHomePageURL.url params:params success:^(NSDictionary *dict) {
        if (response) response(dict);
    } failure:^(NSError *error) {
        if (failure) failure(error);
    }];
}

//获取城市名字
+(void)requestHomeControllerDataWithCityName:(NSString *)cityName response:(void(^)(NSDictionary * responseObject))  response failure:(void(^)(NSError *error))failure {
    NSDictionary *locationParams = @{@"cityName": cityName};
    [[TENBaseNetwork shareInstance] post:[TENNetwork urlWith:kLocationWithCityNameURL] params:locationParams success:^(NSDictionary *dict) {
        if (response)  response(dict);
    } failure:^(NSError *error) {
        if (failure)  failure(error);
    }];
}

//获取首页房型查询条件接口
+(void)requestHomeCotrollerRoomTypeData:(NSString *)parm response:(void(^)(NSDictionary * responseObject))response failure:(void(^)(NSError *error))failure {
    [[TENBaseNetwork shareInstance] post:[TENNetwork urlWith:kRoomTypeDataURL] params:nil success:^(NSDictionary *dict) {
        if (response)  response(dict);
    } failure:^(NSError *error) {
        if (failure)  failure(error);
    }];
}

#pragma mark - 新版本接口
// 根据城市名获取首页内容
+ (void)fetchHomePageDataWithParameter:(NSDictionary *)params response:(void(^)(NSDictionary * responseObject))response failure:(void(^)(NSError *error))failure {
    //    NSDictionary *params = @{@"cityName": name,
    //                             @"pageNo": pageNo,
    //                             @"pageSize": pageSize
    //                             };
    [[TENBaseNetwork shareInstance] post:[TENNetwork urlWith:kHomePageDataURL] params:params success:^(NSDictionary *dict) {
        if (response) response(dict);
    } failure:^(NSError *error) {
        if (failure) failure(error);
    }];
}

@end

//
//  TENSearchPageRequest.m
//  TENNetwork
//
//  Created by 李冲 on 2018/11/1.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENSearchPageRequest.h"
#import "TENBaseNetwork.h"
#import "TENNetwork.h"
@implementation TENSearchPageRequest

+ (void)requestSearchPageDataWithParameters:(NSDictionary *)params success:(void(^)(NSDictionary * responseObject))success failure:(void(^)(NSError *error))failure{
    NSString *url = [TENNetwork urlWith:kSearchPageDataURL];
    [[TENBaseNetwork shareInstance] post:url params:params success:^(NSDictionary *dict) {
        if (success) success(dict);
    } failure:^(NSError *error) {
        if (failure) failure(error);
    }];
}
+(void)requestFindDataWithCondition:(NSDictionary *)parameter success:(void (^)(NSDictionary * _Nonnull))success failure:(void (^)(NSError * _Nonnull))failure{
    NSString *url = [TENNetwork urlWith:kSearchFindURL];
    NSDictionary *params = parameter;
    [[TENBaseNetwork shareInstance] post:url params:params success:^(NSDictionary *dict) {
        if (success) success(dict);
    } failure:^(NSError *error) {
        if (failure) failure(error);
    }];
}
+ (void)requestFindDataWithURL:(NSString *)url Condition:(NSDictionary *)parameter success:(void(^)(NSDictionary * responseObject))success failure:(void(^)(NSError *error))failure{
    NSString *URL = [TENNetwork urlWith:url];
    NSDictionary *params = parameter;
    [[TENBaseNetwork shareInstance] post:URL params:params success:^(NSDictionary *dict) {
        if (success) success(dict);
    } failure:^(NSError *error) {
        if (failure) failure(error);
    }];
}
+ (void)requestCityDataWithParameters:(NSDictionary *)params success:(void (^)(NSDictionary *))success failure:(void (^)(NSError *))failure {
    NSString *url = [TENNetwork urlWith:kSearchByCityName];
    [[TENBaseNetwork shareInstance] post:url params:params success:^(NSDictionary *dict) {
        if (success) success(dict);
    } failure:^(NSError *error) {
        if (failure) failure(error);
    }];
}

@end

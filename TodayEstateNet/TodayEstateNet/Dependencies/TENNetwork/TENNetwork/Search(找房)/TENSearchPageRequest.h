//
//  TENSearchPageRequest.h
//  TENNetwork
//
//  Created by 李冲 on 2018/11/1.
//  Copyright © 2018 TEN. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TENSearchPageRequest : NSObject

//请求找房页面数据
+ (void)requestSearchPageDataWithParameters:(NSDictionary *)params success:(void(^)(NSDictionary * responseObject))success failure:(void(^)(NSError *error))failure;
//根据条件筛选
+ (void)requestFindDataWithCondition:(NSDictionary *)parameter success:(void(^)(NSDictionary * responseObject))success failure:(void(^)(NSError *error))failure;
+ (void)requestFindDataWithURL:(NSString *)url Condition:(NSDictionary *)parameter success:(void(^)(NSDictionary * responseObject))success failure:(void(^)(NSError *error))failure;
+ (void)requestCityDataWithParameters:(NSDictionary *)params success:(void(^)(NSDictionary * responseObject))success failure:(void(^)(NSError *error))failure;

@end

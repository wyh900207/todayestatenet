//
//  TENMessageListRequest.m
//  TENNetwork
//
//  Created by  HomerLynn on 2019/1/14.
//  Copyright © 2019 TEN. All rights reserved.
//

#import "TENMessageListRequest.h"
#import "TENBaseNetwork.h"
#import "TENNetwork.h"

@implementation TENMessageListRequest

+ (void)fetchMessageListWithParams:(NSMutableDictionary *)otherParams success:(void (^)(NSDictionary *))success failure:(void (^)(NSError *))failure {
    NSString *url = [TENNetwork urlWith:kMessageListURL];
    otherParams[@"accountId"] = @"51";
    otherParams[@"pageSize"] = @20;
    [[TENBaseNetwork shareInstance] post:url params:otherParams success:^(NSDictionary *dict) {
        if (success) success(dict);
    } failure:^(NSError *error) {
        if (failure) failure(error);
    }];
}

+ (void)fetchUnreadCountWithSuccess:(void (^)(NSDictionary *))success failure:(void (^)(NSError *))failure {
    NSString *url = [TENNetwork urlWith:kMessageUnreadCountURL];
    NSDictionary *params = @{@"accountId": @"51"};
    [[TENBaseNetwork shareInstance] post:url params:params success:^(NSDictionary *dict) {
        if (success) success(dict);
    } failure:^(NSError *error) {
        if (failure) failure(error);
    }];
}


////获取在线咨询对话列表
+ (void)fetchOnlineQuestionListWithParameters:(NSDictionary *)params Success:(void (^)(NSDictionary *))success failure:(void (^)(NSError *))failure Hud:(BOOL)isHud{
    NSString *url = [TENNetwork urlWith:kOnlineQListURL];
    [[TENBaseNetwork shareInstance] post:url params:params success:^(NSDictionary *dict) {
        if (success) success(dict);
    } failure:^(NSError *error) {
        if (failure) failure(error);
    } Hud:isHud];
}
//发送文本消息
+ (void)sendTextMsgWithParameters:(NSDictionary *)params Success:(void (^)(NSDictionary *))success failure:(void (^)(NSError *))failure Hud:(BOOL)isHud{
    NSString *url = [TENNetwork urlWith:kSendMsgURL];
    [[TENBaseNetwork shareInstance] post:url params:params success:^(NSDictionary *dict) {
        if (success) success(dict);
    } failure:^(NSError *error) {
        if (failure) failure(error);
    } Hud:isHud];
}

+ (void)fetchChatListWithParams:(NSMutableDictionary *)params success:(void(^)(NSDictionary * responseObject))success failure:(void(^)(NSError *error))failure {
    NSString *url = [TENNetwork urlWith:kChatListURL];
    [[TENBaseNetwork shareInstance] post:url params:params success:^(NSDictionary *dict) {
        if (success) success(dict);
    } failure:^(NSError *error) {
        if (failure) failure(error);
    }];
}

@end

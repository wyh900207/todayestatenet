//
//  TENMessageListRequest.h
//  TENNetwork
//
//  Created by  HomerLynn on 2019/1/14.
//  Copyright © 2019 TEN. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TENMessageListRequest : NSObject

+ (void)fetchMessageListWithParams:(NSMutableDictionary *)otherParams success:(void(^)(NSDictionary * responseObject))success failure:(void(^)(NSError *error))failure;
+ (void)fetchUnreadCountWithSuccess:(void(^)(NSDictionary * responseObject))success failure:(void(^)(NSError *error))failure;


//获取在线咨询对话列表
+ (void)fetchOnlineQuestionListWithParameters:(NSDictionary *)params Success:(void (^)(NSDictionary *))success failure:(void (^)(NSError *))failure Hud:(BOOL)isHud;
//发送文本消息
+ (void)sendTextMsgWithParameters:(NSDictionary *)params Success:(void (^)(NSDictionary *))success failure:(void (^)(NSError *))failure Hud:(BOOL)isHud;

// 获取 消息 -> 群聊消息 列表
+ (void)fetchChatListWithParams:(NSMutableDictionary *)params success:(void(^)(NSDictionary * responseObject))success failure:(void(^)(NSError *error))failure;

@end

//
//  TENHouseDetailRequest.m
//  TENNetwork
//
//  Created by  HomerLynn on 2018/11/8.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENHouseDetailRequest.h"
#import "TENBaseNetwork.h"
#import "TENNetwork.h"

@implementation TENHouseDetailRequest

+ (void)fetchHouseDetailWithParameter:(NSDictionary *)params success:(void (^)(NSDictionary *))success failure:(void (^)(NSError *))failure {
    NSString *url = [TENNetwork urlWith:kHouseDetailURL];
    [[TENBaseNetwork shareInstance] post:url params:params success:^(NSDictionary *dict) {
        if (success) success(dict);
    } failure:^(NSError *error) {
        if (failure) failure(error);
    }];
}
//收藏
+ (void)favoriteInfoWithParamters:(NSDictionary *)params success:(void(^)(NSDictionary * responseObject))success failure:(void(^)(NSError *error))failure{
    NSString *url = [TENNetwork urlWith:kFavoriteURL];
//    NSDictionary *params = @{@"accountId": TEN_UD_VALUE(@"yyAccount.id"),
//                             @"targetType": @(targetType),
//                             @"targetId": @(targetId)
//                             };
    [[TENBaseNetwork shareInstance] post:url params:params success:^(NSDictionary *dict) {
        if (success) success(dict);
    } failure:^(NSError *error) {
        if (failure) failure(error);
    }];
}
//获取收藏列表
+ (void)loadFavoriteListWithParameters:(NSDictionary *)params success:(void(^)(NSDictionary * responseObject))success failure:(void(^)(NSError *error))failure{
    NSString *url = [TENNetwork urlWith:kFavoriteListURL];
//    NSDictionary *params = @{@"accountId": TEN_UD_VALUE(@"yyAccount.id"),
//                             @"targetType": @(targetType),
//                             @"pageNo": @(pageNum),
//                             @"pageSize":@(pageSize)
//                             };
    [[TENBaseNetwork shareInstance] post:url params:params success:^(NSDictionary *dict) {
        if (success) success(dict);
    } failure:^(NSError *error) {
        if (failure) failure(error);
    }];
}
//订阅
+ (void)subscribeWithParameters:(NSDictionary *)params success:(void(^)(NSDictionary *responseObject))success failure:(void(^)(NSError *error))failure{
    NSString *url = [TENNetwork urlWith:kSubscribeURL];
    [[TENBaseNetwork shareInstance] post:url params:params success:^(NSDictionary *dict) {
        if (success) success(dict);
    } failure:^(NSError *error) {
        if (failure) failure(error);
    }];
}

@end

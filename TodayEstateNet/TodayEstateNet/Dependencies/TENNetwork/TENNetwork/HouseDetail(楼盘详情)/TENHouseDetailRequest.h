//
//  TENHouseDetailRequest.h
//  TENNetwork
//
//  Created by  HomerLynn on 2018/11/8.
//  Copyright © 2018 TEN. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TENHouseDetailRequest : NSObject

+ (void)fetchHouseDetailWithParameter:(NSDictionary *)params success:(void (^)(NSDictionary *))success failure:(void (^)(NSError *))failure;
//收藏
+ (void)favoriteInfoWithParamters:(NSDictionary *)params success:(void(^)(NSDictionary * responseObject))success failure:(void(^)(NSError *error))failure;
//获取收藏列表
+ (void)loadFavoriteListWithParameters:(NSDictionary *)params success:(void(^)(NSDictionary * responseObject))success failure:(void(^)(NSError *error))failure;
//订阅
+ (void)subscribeWithParameters:(NSDictionary *)params success:(void(^)(NSDictionary *responseObject))success failure:(void(^)(NSError *error))failure;
@end

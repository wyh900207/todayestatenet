//
//  TENAuthRequest.h
//  TENNetwork
//
//  Created by HomerLynn on 2018/9/21.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TENAuthRequest : NSObject

+ (void)auth:(NSDictionary *)params;

@end

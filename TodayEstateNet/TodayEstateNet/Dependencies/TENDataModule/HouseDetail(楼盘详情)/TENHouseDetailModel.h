//
//  TENHouseDetailModel.h
//  TENDataModule
//
//  Created by  HomerLynn on 2018/11/7.
//  Copyright © 2018 TEN. All rights reserved.
//

#import <Foundation/Foundation.h>
@class TENBuildScoreModel, TENBuildTypeModel, TENNearBuildingModel, TENActivityModel, TENBuildingModelTypeModel, TENBuildingNewsModel, TENAttachmentModel, TENSamePriceModel, TENBuildingInfoModel;

@interface TENHouseDetailModel : NSObject

@property (nonatomic, strong) TENBuildScoreModel *buildScore;
@property (nonatomic, strong) TENActivityModel *activity;
@property (nonatomic, strong) TENBuildingNewsModel *buildingNews;
@property (nonatomic, strong) NSNumber *newsNum;
@property (nonatomic, assign) BOOL isFavorite;
@property (nonatomic, strong) NSNumber *buildingModelTypeSize;
@property (nonatomic, copy  ) NSString *accountRole;
@property (nonatomic, strong) NSMutableArray<TENSamePriceModel *> *samePriceList;
@property (nonatomic, strong) TENBuildingInfoModel *buildingInfo;
@property (nonatomic, strong) NSMutableArray<TENBuildTypeModel *> *buildTypeList;
@property (nonatomic, strong) NSMutableArray<TENNearBuildingModel *> *nearBuildingList;
@property (nonatomic, strong) NSMutableArray<TENBuildingModelTypeModel *> *buildingModelType;
@end



@interface TENBuildScoreModel : NSObject

@property (nonatomic, strong) NSNumber *ID;
@property (nonatomic, strong) NSNumber *buildingGroupId;
@property (nonatomic, strong) NSNumber *scoreNum;
@property (nonatomic, strong) NSNumber *priceScore;
@property (nonatomic, strong) NSNumber *sectionScore;
@property (nonatomic, strong) NSNumber *trafficScore;
@property (nonatomic, strong) NSNumber *matchingScore;
@property (nonatomic, strong) NSNumber *environmentScore;

@end



@interface TENBuildTypeModel : NSObject

@property (nonatomic, copy  ) NSString *area;
@property (nonatomic, copy  ) NSString *typeName;
@property (nonatomic, strong) NSNumber *typeId;
@property (nonatomic, strong) NSNumber *price;
@property (nonatomic, strong) NSNumber *totalPrice;

@end



@interface TENNearBuildingModel : NSObject

// 这里里边的字段 暂时 不确定
@property (nonatomic, copy  ) NSString *name;
@property (nonatomic, copy  ) NSString *logo;
@property (nonatomic, copy  ) NSString *tags;
@property (nonatomic, copy  ) NSString *address;
@property (nonatomic, copy  ) NSString *totalPrice;
@property (nonatomic, copy  ) NSString *buildingType;
@property (nonatomic, copy  ) NSString *saleType;
@property (nonatomic, copy  ) NSString *roomArea;
@property (nonatomic, copy  ) NSString *roomType;
@property (nonatomic, copy  ) NSString *newsTitle;
@property (nonatomic, copy  ) NSString *distance;
@property (nonatomic, copy  ) NSString *attachType;
@property (nonatomic, copy  ) NSString *provinceName;
@property (nonatomic, copy  ) NSString *cityName;
@property (nonatomic, copy  ) NSString *activityTypeName;
@property (nonatomic, copy  ) NSString *activityTypePic;
@property (nonatomic, copy  ) NSString *viewType;
@property (nonatomic, copy  ) NSString *firstRate;
@property (nonatomic, copy  ) NSString *estate;
@property (nonatomic, copy  ) NSString *attachment;
@property (nonatomic, copy  ) NSString *houseServiceIds;
@property (nonatomic, copy  ) NSString *beginSellDate;
@property (nonatomic, copy  ) NSString *beginSellDes;
@property (nonatomic, copy  ) NSString *dealTimeInfo;
@property (nonatomic, copy  ) NSString *bus;
@property (nonatomic, copy  ) NSString *market;
@property (nonatomic, copy  ) NSString *metro;
@property (nonatomic, copy  ) NSString *other;
@property (nonatomic, copy  ) NSString *panorama;
@property (nonatomic, copy  ) NSString *restaurant;
@property (nonatomic, copy  ) NSString *peripheryPic;
@property (nonatomic, strong) NSNumber *picNum;
@property (nonatomic, strong) NSNumber *newsId;
@property (nonatomic, strong) NSNumber *ID;
@property (nonatomic, strong) NSNumber *price;

@end



@interface TENActivityModel : NSObject

@property (nonatomic, copy  ) NSString *endDate;
@property (nonatomic, copy  ) NSString *type;
@property (nonatomic, copy  ) NSString *buildingGroupName;
@property (nonatomic, copy  ) NSString *title;
@property (nonatomic, copy  ) NSString *content;
@property (nonatomic, copy  ) NSString *picPath;
@property (nonatomic, copy  ) NSString *useRange;
@property (nonatomic, copy  ) NSString *service;
@property (nonatomic, copy  ) NSString *useMethod;
@property (nonatomic, copy  ) NSString *address;
@property (nonatomic, copy  ) NSString *beginSellDate;
@property (nonatomic, copy  ) NSString *builtUpArea;
@property (nonatomic, copy  ) NSString *price;
@property (nonatomic, strong) NSNumber *ID;
@property (nonatomic, strong) NSNumber *buildingGroupId;
@property (nonatomic, strong) NSNumber *joinNum;

@end



@interface TENBuildingModelTypeModel : NSObject

@property (nonatomic, copy  ) NSString *title;
@property (nonatomic, copy  ) NSString *modelTypeName;
@property (nonatomic, copy  ) NSString *picPath;
@property (nonatomic, copy  ) NSString *saleType;
@property (nonatomic, strong) NSNumber *ID;
@property (nonatomic, strong) NSNumber *buildingGroupId;
@property (nonatomic, strong) NSNumber *area;
@property (nonatomic, strong) NSNumber *price;
@property (nonatomic, strong) NSNumber *roomNum;

@end



@interface TENBuildingNewsModel : NSObject

@property (nonatomic, copy  ) NSString *content;
@property (nonatomic, copy  ) NSString *headline;
@property (nonatomic, copy  ) NSString *picPath;
@property (nonatomic, copy  ) NSString *resource;
@property (nonatomic, copy  ) NSString *createDateFormat;
@property (nonatomic, copy  ) NSString *zhaiyao;
@property (nonatomic, strong) NSNumber *ID;
@property (nonatomic, strong) NSNumber *readNum;

@end



@interface TENBuildingInfoModel : NSObject

@property (nonatomic, copy  ) NSString *name;
@property (nonatomic, copy  ) NSString *logo;
@property (nonatomic, copy  ) NSString *tags;
@property (nonatomic, copy  ) NSString *address;
@property (nonatomic, copy  ) NSString *totalPrice;
@property (nonatomic, copy  ) NSString *buildingType;
@property (nonatomic, copy  ) NSString *saleType;
@property (nonatomic, copy  ) NSString *roomArea;
@property (nonatomic, copy  ) NSString *roomType;
@property (nonatomic, copy  ) NSString *newsTitle;
@property (nonatomic, copy  ) NSString *distance;
@property (nonatomic, copy  ) NSString *provinceName;
@property (nonatomic, copy  ) NSString *cityName;
@property (nonatomic, copy  ) NSString *activityTypeName;
@property (nonatomic, copy  ) NSString *activityTypePic;
@property (nonatomic, copy  ) NSString *viewType;
@property (nonatomic, copy  ) NSString *estate;
@property (nonatomic, copy  ) NSString *houseServiceIds;
@property (nonatomic, copy  ) NSString *beginSellDate;
@property (nonatomic, copy  ) NSString *beginSellDes;
@property (nonatomic, copy  ) NSString *dealTimeInfo;
@property (nonatomic, copy  ) NSString *bus;
@property (nonatomic, copy  ) NSString *market;
@property (nonatomic, copy  ) NSString *metro;
@property (nonatomic, copy  ) NSString *other;
@property (nonatomic, copy  ) NSString *panorama;
@property (nonatomic, copy  ) NSString *restaurant;
@property (nonatomic, copy  ) NSString *peripheryPic;
@property (nonatomic, strong) NSNumber *ID;
@property (nonatomic, strong) NSNumber *price;
@property (nonatomic, strong) NSNumber *newsId;
@property (nonatomic, strong) NSNumber *attachType;
@property (nonatomic, strong) NSNumber *firstRate;
@property (nonatomic, strong) NSMutableArray<TENAttachmentModel *> *attachment;
@property (nonatomic, strong) NSNumber *picNum;

@end



@interface TENAttachmentModel : NSObject

@property (nonatomic, copy  ) NSString *peripheryPic;
@property (nonatomic, copy  ) NSString *picPath;
@property (nonatomic, copy  ) NSString *video;
@property (nonatomic, copy  ) NSString *url;
@property (nonatomic, strong) NSNumber *ID;
@property (nonatomic, strong) NSNumber *type;
@property (nonatomic, strong) NSNumber *sort;
@property (nonatomic, strong) NSNumber *buildingGroupId;
@property (nonatomic, strong) NSNumber *isShow;
@property (nonatomic, strong) NSNumber *isDelete;

@end



@interface TENSamePriceModel : NSObject

@property (nonatomic, copy  ) NSString *name;
@property (nonatomic, copy  ) NSString *logo;
@property (nonatomic, copy  ) NSString *tags;
@property (nonatomic, copy  ) NSString *address;
@property (nonatomic, copy  ) NSString *totalPrice;
@property (nonatomic, copy  ) NSString *buildingType;
@property (nonatomic, copy  ) NSString *saleType;
@property (nonatomic, copy  ) NSString *roomArea;
@property (nonatomic, copy  ) NSString *roomType;
@property (nonatomic, copy  ) NSString *newsTitle;
@property (nonatomic, copy  ) NSString *distance;
@property (nonatomic, copy  ) NSString *attachType;
@property (nonatomic, copy  ) NSString *provinceName;
@property (nonatomic, copy  ) NSString *cityName;
@property (nonatomic, copy  ) NSString *activityTypeName;
@property (nonatomic, copy  ) NSString *activityTypePic;
@property (nonatomic, copy  ) NSString *viewType;
@property (nonatomic, copy  ) NSString *firstRate;
@property (nonatomic, copy  ) NSString *estate;
@property (nonatomic, copy  ) NSString *attachment;
@property (nonatomic, copy  ) NSString *houseServiceIds;
@property (nonatomic, copy  ) NSString *beginSellDate;
@property (nonatomic, copy  ) NSString *beginSellDes;
@property (nonatomic, copy  ) NSString *dealTimeInfo;
@property (nonatomic, copy  ) NSString *bus;
@property (nonatomic, copy  ) NSString *market;
@property (nonatomic, copy  ) NSString *metro;
@property (nonatomic, copy  ) NSString *other;
@property (nonatomic, copy  ) NSString *panorama;
@property (nonatomic, copy  ) NSString *restaurant;
@property (nonatomic, copy  ) NSString *peripheryPic;
@property (nonatomic, strong) NSNumber *picNum;
@property (nonatomic, strong) NSNumber *newsId;
@property (nonatomic, strong) NSNumber *ID;
@property (nonatomic, strong) NSNumber *price;

@end

@interface TENSameDistrictModel : NSObject

@property (nonatomic, copy  ) NSString *name;
@property (nonatomic, copy  ) NSString *logo;
@property (nonatomic, copy  ) NSString *tags;
@property (nonatomic, copy  ) NSString *address;
@property (nonatomic, copy  ) NSString *totalPrice;
@property (nonatomic, copy  ) NSString *buildingType;
@property (nonatomic, copy  ) NSString *saleType;
@property (nonatomic, copy  ) NSString *roomArea;
@property (nonatomic, copy  ) NSString *roomType;
@property (nonatomic, copy  ) NSString *newsTitle;
@property (nonatomic, copy  ) NSString *distance;
@property (nonatomic, copy  ) NSString *attachType;
@property (nonatomic, copy  ) NSString *provinceName;
@property (nonatomic, copy  ) NSString *cityName;
@property (nonatomic, copy  ) NSString *activityTypeName;
@property (nonatomic, copy  ) NSString *activityTypePic;
@property (nonatomic, copy  ) NSString *viewType;
@property (nonatomic, copy  ) NSString *firstRate;
@property (nonatomic, copy  ) NSString *estate;
@property (nonatomic, copy  ) NSString *attachment;
@property (nonatomic, copy  ) NSString *houseServiceIds;
@property (nonatomic, copy  ) NSString *beginSellDate;
@property (nonatomic, copy  ) NSString *beginSellDes;
@property (nonatomic, copy  ) NSString *dealTimeInfo;
@property (nonatomic, copy  ) NSString *bus;
@property (nonatomic, copy  ) NSString *market;
@property (nonatomic, copy  ) NSString *metro;
@property (nonatomic, copy  ) NSString *other;
@property (nonatomic, copy  ) NSString *panorama;
@property (nonatomic, copy  ) NSString *restaurant;
@property (nonatomic, copy  ) NSString *peripheryPic;
@property (nonatomic, strong) NSNumber *picNum;
@property (nonatomic, strong) NSNumber *newsId;
@property (nonatomic, strong) NSNumber *ID;
@property (nonatomic, strong) NSNumber *price;

@end

//@interface TENNearPoiModel : BMKPointAnnotation
////待定
//@property (nonatomic, strong) NSNumber *price;
//
//@end


//
//  TENHouseDetailModel.m
//  TENDataModule
//
//  Created by  HomerLynn on 2018/11/7.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENHouseDetailModel.h"
#import "MJExtension.h"

//TENHouseDetailModel
@implementation TENHouseDetailModel

+ (NSDictionary *)mj_objectClassInArray {
    return @{@"buildTypeList": @"TENBuildTypeModel",
             @"nearBuildingList": @"TENNearBuildingModel",
             @"buildingModelType": @"TENBuildingModelTypeModel"
             };
}

@end

//TENBuildScoreModel
@implementation TENBuildScoreModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"id": @"ID"};
}

@end

//TENNearBuildingModel
@implementation TENNearBuildingModel

@end

//TENBuildTypeModel
@implementation TENBuildTypeModel

@end

//TENActivityModel
@implementation TENActivityModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"id": @"ID"};
}

@end

//TENBuildingModelTypeModel
@implementation TENBuildingModelTypeModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"id": @"ID"};
}

@end

//TENBuildingNewsModel
@implementation TENBuildingNewsModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"id": @"ID"};
}

@end

//TENAttachmentModel
@implementation TENAttachmentModel

@end

//TENSamePriceModel
@implementation TENSamePriceModel

@end

//TENBuildingInfoModel
@implementation TENBuildingInfoModel

+ (NSDictionary *)mj_objectClassInArray {
    return @{@"attachment": @"TENAttachmentModel"};
}
@end
@implementation TENSameDistrictModel

@end
//@implementation TENNearPoiModel
//
//@end


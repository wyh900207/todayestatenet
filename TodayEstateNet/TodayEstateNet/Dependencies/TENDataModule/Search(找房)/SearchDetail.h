//
//  SearchDetail.h
//  TENDataModule
//
//  Created by 李冲 on 2018/11/7.
//  Copyright © 2018 TEN. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface SearchDetail : NSObject
@property (nonatomic,copy) NSString *detailId;
@property (nonatomic,copy) NSString *name;
@property (nonatomic,copy) NSString *logo;
@property (nonatomic,copy) NSString *tags;
@property (nonatomic,copy) NSString *price;
@property (nonatomic,copy) NSString *address;
@property (nonatomic,copy) NSString *totalPrice;
@property (nonatomic,copy) NSString *buildingType;
@property (nonatomic,copy) NSString *saleType;

@property (nonatomic,copy) NSString *roomArea;
@property (nonatomic,copy) NSString *roomType;
@property (nonatomic,copy) NSString *newsId;
@property (nonatomic,copy) NSString *newsTitle;
@property (nonatomic,copy) NSString *distance;
@property (nonatomic,copy) NSString *attachType;
@property (nonatomic,copy) NSString *provinceName;
@property (nonatomic,copy) NSString *cityName;
@property (nonatomic,copy) NSString *activityTypeName;
@end

NS_ASSUME_NONNULL_END

//
//  DistrictList.h
//  TENDataModule
//
//  Created by 李冲 on 2018/11/1.
//  Copyright © 2018 TEN. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SearchData : NSObject

@property (nonatomic ,copy) NSString *name;
@property (nonatomic ,copy) NSString *type;
@property (nonatomic ,copy) NSString *value;
@property (nonatomic ,copy) NSString *searchId;
@property (nonatomic ,strong) NSMutableArray <SearchData *>*list;
@property (nonatomic ,strong) NSMutableArray <SearchData *>*child;
@property (nonatomic) BOOL isSelected;
@property (nonatomic) NSUInteger selectIndex;
@property (nonatomic) NSUInteger selectSection;
@end

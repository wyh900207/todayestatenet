//
//  DistrictList.m
//  TENDataModule
//
//  Created by 李冲 on 2018/11/1.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "SearchData.h"
#import "MJExtension/MJExtension.h"
@implementation SearchData

+(NSDictionary *)mj_objectClassInArray{
    return @{
             @"list" : @"SearchData",
             @"child" : @"SearchData"
             };
}
+ (NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{
             @"searchId" : @"id",
             };
}
@end

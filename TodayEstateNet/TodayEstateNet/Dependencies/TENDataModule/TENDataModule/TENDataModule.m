//
//  TENDataModule.m
//  TENDataModule
//
//  Created by HomerLynn on 2018/9/9.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENDataModule.h"

@implementation TENDataModule

+ (instancetype)dataModule {
    static TENDataModule *dataModule = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dataModule = [TENDataModule new];
    });
    return dataModule;
}

- (void)login:(NSString *)account psw:(NSString *)psw {
    NSLog(@"account is : %@, psw is : %@", account, psw);
}

@end

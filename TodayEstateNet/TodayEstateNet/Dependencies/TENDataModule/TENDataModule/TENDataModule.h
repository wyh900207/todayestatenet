//
//  TENDataModule.h
//  TENDataModule
//
//  Created by HomerLynn on 2018/9/9.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TENDataModule : NSObject

+ (instancetype)dataModule;

- (void)login:(NSString *)account psw:(NSString *)psw;

@end

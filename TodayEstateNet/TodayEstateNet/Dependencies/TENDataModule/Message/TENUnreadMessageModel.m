//
//  TENUnreadMessageModel.m
//  TENDataModule
//
//  Created by  HomerLynn on 2019/1/14.
//  Copyright © 2019 TEN. All rights reserved.
//

#import "TENUnreadMessageModel.h"
#import "MJExtension.h"

@implementation TENUnreadMessageModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{@"message_id" : @"id",
             @"xinMsnCount": @"newMsnCount"
             };
}

@end

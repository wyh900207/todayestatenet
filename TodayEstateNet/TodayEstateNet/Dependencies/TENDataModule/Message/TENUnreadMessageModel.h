//
//  TENUnreadMessageModel.h
//  TENDataModule
//
//  Created by  HomerLynn on 2019/1/14.
//  Copyright © 2019 TEN. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TENUnreadMessageModel : NSObject

@property (nonatomic, strong) NSNumber *accountId;
@property (nonatomic, strong) NSNumber *commendNum;
@property (nonatomic, strong) NSNumber *customDynamicNum;
@property (nonatomic, strong) NSNumber *dynamicCount;
@property (nonatomic, strong) NSNumber *followNum;
@property (nonatomic, strong) NSNumber *message_id;
@property (nonatomic, strong) NSNumber *msnNum;
@property (nonatomic, strong) NSNumber *xinMsnCount;
@property (nonatomic, strong) NSNumber *reportNum;
@property (nonatomic, strong) NSNumber *systemNum;

@end

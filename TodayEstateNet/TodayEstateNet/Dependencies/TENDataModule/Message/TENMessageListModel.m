//
//  TENMessageListModel.m
//  TENDataModule
//
//  Created by  HomerLynn on 2019/1/14.
//  Copyright © 2019 TEN. All rights reserved.
//

#import "TENMessageListModel.h"
#import "MJExtension.h"

@implementation TENMessageListModel

+ (NSDictionary *)mj_objectClassInArray {
    return @{@"content": @"TENMessageDetailModel"};
}

@end



@implementation TENMessageDetailModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{@"message_id" : @"id"};
}

@end

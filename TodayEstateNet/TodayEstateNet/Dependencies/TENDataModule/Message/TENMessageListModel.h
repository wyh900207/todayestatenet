//
//  TENMessageListModel.h
//  TENDataModule
//
//  Created by  HomerLynn on 2019/1/14.
//  Copyright © 2019 TEN. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TENMessageListModel : NSObject

@property (nonatomic, strong) NSMutableArray *content;

@end



@interface TENMessageDetailModel : NSObject

//account = "<null>";
//content = "\U8f7b\U8bed2019.01.01 10:00\U63d0\U9192\U6211\U8054\U7cfb\U5ba2\U6237";
//createId = "<null>";
//createTime = "<null>";
//id = 15;
//mediaId = "<null>";
//mediaType = 1;
//mediaUrl = "<null>";
//title = "\U5907\U5fd8-2019.01.01";
//type = "<null>";

@property (nonatomic, copy) NSString *account;
@property (nonatomic, copy) NSString *content;
@property (nonatomic, copy) NSString *createId;
@property (nonatomic, copy) NSString *createTime;
@property (nonatomic, copy) NSString *message_id;
@property (nonatomic, copy) NSString *mediaId;
@property (nonatomic, copy) NSString *mediaType;
@property (nonatomic, copy) NSString *mediaUrl;
@property (nonatomic, copy) NSString *title;
@property (nonatomic, copy) NSString *type;

@end

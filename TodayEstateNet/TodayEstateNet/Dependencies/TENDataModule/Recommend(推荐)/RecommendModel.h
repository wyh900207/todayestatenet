//
//  RecommendModel.h
//  TENDataModule
//
//  Created by 李冲 on 2018/11/9.
//  Copyright © 2018 TEN. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MJExtension.h"

@interface RecommendModel : NSObject

@end

//活动
@interface RecommendActivity :NSObject

@property (nonatomic, strong)   NSNumber *activityId;
@property (nonatomic, copy)     NSString *type;
@property (nonatomic, strong)   NSNumber *buildingGroupId;
@property (nonatomic, copy)     NSString *buildingGroupName;
@property (nonatomic, copy)     NSString *title;
@property (nonatomic, copy)     NSString *content;
@property (nonatomic, copy)     NSString *picPath;
@property (nonatomic, strong)   NSNumber *joinNum;
@property (nonatomic, copy)     NSString *useRange;

@property (nonatomic, copy)     NSString *service;
@property (nonatomic, copy)     NSString *useMethod;
@property (nonatomic, copy)     NSString *address;
@property (nonatomic, copy)     NSString *beginSellDate;
@property (nonatomic, copy)     NSString *builtUpArea;
@property (nonatomic, copy)     NSString *price;

@end



//楼盘
@interface RecommendBuilding : NSObject

@property (nonatomic, strong)   NSNumber *buildingId;
@property (nonatomic, copy)     NSString *name;
@property (nonatomic, copy)     NSString *logo;
@property (nonatomic, copy)     NSString *tags;
@property (nonatomic, strong)   NSNumber *price;
@property (nonatomic, copy)     NSString *address;
@property (nonatomic, copy)     NSString *totalPrice;
@property (nonatomic, copy)     NSString *buildingType;
@property (nonatomic, copy)     NSString *saleType;

@property (nonatomic, copy)     NSString *roomArea;
@property (nonatomic, copy)     NSString *roomType;
@property (nonatomic, strong)   NSNumber *newsId;
@property (nonatomic, copy)     NSString *distance;
@property (nonatomic, strong)   NSNumber *attachType;
@property (nonatomic, copy)     NSString *provinceName;

@property (nonatomic, copy)     NSString *cityName;
@property (nonatomic, copy)     NSString *activityTypeName;
@property (nonatomic, copy)     NSString *activityTypePic;
@property (nonatomic, copy)     NSString *viewType;
@property (nonatomic, strong)     NSNumber *firstRate;
@property (nonatomic, copy)     NSString *estate;
@property (nonatomic, strong)     NSNumber *picNum;

@property (nonatomic, copy)     NSString *houseServiceIds;
@property (nonatomic, copy)     NSString *beginSellDate;
@property (nonatomic, copy)     NSString *beginSellDes;
@property (nonatomic, copy)     NSString *dealTimeInfo;
@property (nonatomic, copy)     NSString *bus;
@property (nonatomic, copy)     NSString *market;
@property (nonatomic, copy)     NSString *metro;
@property (nonatomic, copy)     NSString *other;
@property (nonatomic, copy)     NSString *panorama;
@property (nonatomic, copy)     NSString *restaurant;
@property (nonatomic, copy)     NSString *peripheryPic;

@end



//推荐：新闻
@interface RecommendNews : NSObject

//content: null
//createDateFormat: "2018-12-24 15:54:23"
//headline: "买房攻略：如何选择一套心仪的房子？"
//id: 32
//picPath: "http://img.jrfw360.com/manage/1545638084898.png"
//readNum: 7
//resource: "今日房网"
//zhaiyao:

// 修改后
@property (nonatomic, copy)     NSString *content;
@property (nonatomic, copy)     NSString *newsId;
@property (nonatomic, copy)     NSString *createDateFormat;
@property (nonatomic, copy)     NSString *headline;
@property (nonatomic, copy)     NSString *picPath;
@property (nonatomic, copy)     NSString *readNum;
@property (nonatomic, copy)     NSString *resource;
@property (nonatomic, copy)     NSString *zhaiyao;
// 修改前的
@property (nonatomic, copy)     NSString *createTime;
@property (nonatomic, copy)     NSString *commentNum;
@property (nonatomic, copy)     NSString *peripheryPic;
@property (nonatomic, copy)     NSString *likeNum;
@property (nonatomic, copy)     NSString *keywords;
@property (nonatomic, copy)     NSString *buildingGroupId;

@end

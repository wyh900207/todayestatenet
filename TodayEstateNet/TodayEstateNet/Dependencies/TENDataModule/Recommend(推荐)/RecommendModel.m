//
//  RecommendModel.m
//  TENDataModule
//
//  Created by 李冲 on 2018/11/9.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "RecommendModel.h"
#import "MJExtension.h"
@implementation RecommendModel

@end
//活动
@implementation RecommendActivity

+ (NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{
             @"activityId" : @"id"
             };
}

@end
//楼盘
@implementation RecommendBuilding

+ (NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{
             @"buildingId" : @"id"
             };
}

@end
@implementation RecommendNews

+ (NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{
             @"newsId" : @"id"
             };
}

@end

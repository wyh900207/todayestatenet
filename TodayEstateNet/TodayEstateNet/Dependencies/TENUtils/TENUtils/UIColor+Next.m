//
//  UIColor+Next.m
//  TENUtils
//
//  Created by HomerLynn on 2018/9/27.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "UIColor+Next.h"
#import "TENColor.h"
#import "UIColor+Hex.h"
#import <objc/runtime.h>

@implementation UIColor (Next)

static char *CurrentColorKey = "CurrentColorKey";
static int CurrentColorIndex = 0;
static NSString *CurrentColor = @"#FF0000";

+ (UIColor *)defaultColor {
    UIColor *color = [UIColor colorWithHexString:TENRedColor];
    CurrentColorIndex += 1;
    
    return color;
}

- (UIColor *)next {
    switch (CurrentColorIndex % 5) {
        case 0:
            CurrentColor = @"FF0000";
            break;
        case 1:
            CurrentColor = @"00FF00";
            break;
        case 2:
            CurrentColor = @"0000FF";
            break;
        case 3:
            CurrentColor = @"0FF000";
            break;
            
        default:
            CurrentColor = @"000FF0";
            break;
    }
    CurrentColorIndex += 1;
    
    return [UIColor colorWithHexString:CurrentColor];
}

#pragma mark - Setter

- (void)setCurrentColor:(UIColor *)currentColor {
    objc_setAssociatedObject(self, CurrentColorKey, currentColor, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

#pragma mark - Getter

- (NSArray *)colors {
    NSArray *cols = @[];
    return cols;
}

@end

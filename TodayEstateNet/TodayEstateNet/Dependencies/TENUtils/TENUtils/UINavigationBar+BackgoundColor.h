//
//  UINavigationBar+BackgoundColor.h
//  CPS
//
//  Created by zhangsp on 2017/8/28.
//  Copyright © 2017年 zhangsp. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TENUtilsHeader.h"
@interface UINavigationBar (BackgoundColor)


-(void)DJSetBackgroundColor:(UIColor *)backgroundColor;
-(void)DJHideShadowImageOrNot:(BOOL)bHidden;

- (void)dj_setBackgroundColor:(UIColor *)backgroundColor;
- (void)dj_setElementsAlpha:(CGFloat)alpha;
- (void)dj_setTranslationY:(CGFloat)translationY;
- (void)dj_reset;

// 导航栏 从上到下渐变
-(void)dj_setGradientBackgroundColor:(UIColor *)backgroundColor;

//从左到右渐变
- (void)cc_setGradientBGcolor:(UIColor *)bgColor;
@end

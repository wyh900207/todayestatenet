//
//  NSObject+Common.h
//  BI
//
//  Created by zhangsp on 2017/3/31.
//  Copyright © 2017年 DJKJ. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Common)

+ (void)showHudTipStr:(NSString *)tipStr;
+ (instancetype)showHUDQueryStr:(NSString *)titleStr;
+ (NSUInteger)hideHUDQuery;

+ (BOOL)showError:(NSError *)error;

//时间戳
+ (NSString *)getTimeStrWithTimestamp:(NSString *)timestamp format:(NSString *)format;
//+ (void)hideAllHUDsForView:(UIView *)view animated:(BOOL)animated;
@end

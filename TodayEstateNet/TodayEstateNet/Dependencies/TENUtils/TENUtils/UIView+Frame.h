//
//  UIView+Frame.h
//  ComprehensiveDemo
//
//  Created by 杨时雨 on 16/8/3.
//  Copyright © 2016年 zwd. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (Frame)

// 避免与其他开发中冲突, 加前缀
@property CGFloat dj_width;
@property CGFloat dj_height;
@property CGFloat dj_x;
@property CGFloat dj_y;
@property CGFloat dj_maxX;
@property CGFloat dj_maxY;
@property CGFloat dj_centerX;
@property CGFloat dj_centerY;

+ (instancetype)dj_viewFromeXib;

@end

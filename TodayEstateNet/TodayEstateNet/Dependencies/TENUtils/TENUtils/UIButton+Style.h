//
//  UIButton+Style.h
//  TENUtils
//
//  Created by  HomerLynn on 2018/12/19.
//  Copyright © 2018 TEN. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, TENButtonEdgeInsetsStyle) {
    TENButtonEdgeInsetsStyleTop, // image在上，label在下
    TENButtonEdgeInsetsStyleLeft, // image在左，label在右
    TENButtonEdgeInsetsStyleBottom, // image在下，label在上
    TENButtonEdgeInsetsStyleRight // image在右，label在左
};

@interface UIButton (Style)

- (void)layoutButtonWithEdgeInsetsStyle:(TENButtonEdgeInsetsStyle)style imageTitleSpace:(CGFloat)space;

@end


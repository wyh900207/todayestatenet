//
//  UIView+Border.m
//  TENUtils
//
//  Created by  HomerLynn on 2018/11/6.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "UIView+Border.h"
#import <objc/runtime.h>

const char kTopLayer;
const char kLeftLayer;
const char kBottomLayer;
const char kRightLayer;
const char kRealBorderColor;

@implementation UIView (Border)

- (void)borderWith:(UIColor *)borderColor boderType:(HLBoderType)borderType {
    // 略取巧, 没有创建属性去保存颜色
    self.realBorderColor = borderColor;
    [self generateBoderLayerWith:borderType];
}

- (void)generateBoderLayerWith:(HLBoderType)type {
    //    // Top layer
    //    CALayer *topLayer = [CALayer layer];
    //    topLayer.frame = CGRectMake(0, 0, width, 1);
    //    topLayer.backgroundColor = self.layer.borderColor;
    //    // Left layer
    //    CALayer *leftLayer = [CALayer layer];
    //    leftLayer.frame = CGRectMake(0, 0, 1, height);
    //    leftLayer.backgroundColor = self.layer.borderColor;
    //    // Bottom layer
    //    CALayer *bottomLayer = [CALayer layer];
    //    bottomLayer.frame = CGRectMake(0, height - 1, width, 1);
    //    bottomLayer.backgroundColor = self.layer.borderColor;
    //    // Right layer
    //    CALayer *rightLayer = [CALayer layer];
    //    rightLayer.frame = CGRectMake(width - 1, 0, 1, height);
    //    rightLayer.backgroundColor = self.layer.borderColor;
    
    [self.layer addSublayer:self.topLayer];
    [self.layer addSublayer:self.leftLayer];
    [self.layer addSublayer:self.bottomLayer];
    [self.layer addSublayer:self.rightLayer];
    
    self.topLayer.hidden = YES;
    self.leftLayer.hidden = YES;
    self.bottomLayer.hidden = YES;
    self.rightLayer.hidden = YES;
    
    if (type & HLBoderTypeTop) [self drawBorderLayerWith:HLBoderTypeTop];
    if (type & HLBoderTypeLeft) [self drawBorderLayerWith:HLBoderTypeLeft];
    if (type & HLBoderTypeBottom) [self drawBorderLayerWith:HLBoderTypeBottom];
    if (type & HLBoderTypeRight) [self drawBorderLayerWith:HLBoderTypeRight];
}

- (void)drawBorderLayerWith:(HLBoderType)type {
    // 这里只处理 单个 type
    switch (type) {
        case HLBoderTypeTop: {
            self.topLayer.hidden = NO;
            self.leftLayer.hidden = YES;
            self.bottomLayer.hidden = YES;
            self.rightLayer.hidden = YES;
        }
            break;
        case HLBoderTypeLeft: {
            self.topLayer.hidden = YES;
            self.leftLayer.hidden = NO;
            self.bottomLayer.hidden = YES;
            self.rightLayer.hidden = YES;
        }
            break;
        case HLBoderTypeBottom: {
            self.topLayer.hidden = YES;
            self.leftLayer.hidden = YES;
            self.bottomLayer.hidden = NO;
            self.rightLayer.hidden = YES;
        }
            break;
        case HLBoderTypeRight: {
            self.topLayer.hidden = YES;
            self.leftLayer.hidden = YES;
            self.bottomLayer.hidden = YES;
            self.rightLayer.hidden = NO;
        }
            break;
        default: {
            self.topLayer.hidden = YES;
            self.leftLayer.hidden = YES;
            self.bottomLayer.hidden = YES;
            self.rightLayer.hidden = YES;
        }
            break;
    }
}

#pragma mark - Getter

- (CALayer *)topLayer {
    CALayer *layer = objc_getAssociatedObject(self, &kTopLayer);
    if (!layer) {
        CGFloat width = self.bounds.size.width;
        layer = [CALayer layer];
        layer.frame = CGRectMake(0, 0, width, 1);
        layer.backgroundColor = self.realBorderColor.CGColor;
    }
    return layer;
}

- (CALayer *)leftLayer {
    CALayer *layer = objc_getAssociatedObject(self, &kLeftLayer);
    if (!layer) {
        CGFloat height = self.bounds.size.height;
        layer = [CALayer layer];
        layer.frame = CGRectMake(0, 0, 1, height);
        layer.backgroundColor = self.realBorderColor.CGColor;
    }
    return layer;
}

- (CALayer *)bottomLayer {
    CALayer *layer = objc_getAssociatedObject(self, &kBottomLayer);
    if (!layer) {
        CGFloat width = self.bounds.size.width;
        CGFloat height = self.bounds.size.height;
        layer = [CALayer layer];
        layer.frame = CGRectMake(0, height - 1, width, 1);
        layer.backgroundColor = self.realBorderColor.CGColor;
    }
    return layer;
}

- (CALayer *)rightLayer {
    CALayer *layer = objc_getAssociatedObject(self, &kRightLayer);
    if (!layer) {
        CGFloat width = self.bounds.size.width;
        CGFloat height = self.bounds.size.height;
        layer = [CALayer layer];
        layer.frame = CGRectMake(width - 1, 0, 1, height);
        layer.backgroundColor = self.realBorderColor.CGColor;
    }
    return layer;
}

- (UIColor *)realBorderColor {
    return objc_getAssociatedObject(self, &kRealBorderColor);
}

#pragma mark - Setter

- (void)setTopLayer:(CALayer *)topLayer {
    objc_setAssociatedObject(self, &topLayer, topLayer, OBJC_ASSOCIATION_RETAIN);
}

- (void)setLeftLayer:(CALayer *)leftLayer {
    objc_setAssociatedObject(self, &leftLayer, leftLayer, OBJC_ASSOCIATION_RETAIN);
}

- (void)setBottomLayer:(CALayer *)bottomLayer {
    objc_setAssociatedObject(self, &bottomLayer, bottomLayer, OBJC_ASSOCIATION_RETAIN);
}

- (void)setRightLayer:(CALayer *)rightLayer {
    objc_setAssociatedObject(self, &rightLayer, rightLayer, OBJC_ASSOCIATION_RETAIN);
}

- (void)setRealBorderColor:(UIColor *)realBorderColor {
    objc_setAssociatedObject(self, &kRealBorderColor, realBorderColor, OBJC_ASSOCIATION_RETAIN);
}

@end

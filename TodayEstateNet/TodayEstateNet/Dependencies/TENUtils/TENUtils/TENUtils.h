//
//  TENUtils.h
//  TENUtils
//
//  Created by HomerLynn on 2018/9/9.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TENUtils : NSObject

+ (UIColor *)orangeColor;

@end

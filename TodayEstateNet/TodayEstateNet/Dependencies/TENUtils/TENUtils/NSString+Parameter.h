//
//  NSString+Parameter.h
//  TENUtils
//
//  Created by 李冲 on 2018/11/7.
//  Copyright © 2018 TEN. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (Parameter)
+(NSString *)parameterWithArray:(NSArray *)array;
@end

NS_ASSUME_NONNULL_END

//
//  UIColor+Next.h
//  TENUtils
//
//  Created by HomerLynn on 2018/9/27.
//  Copyright © 2018 TEN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (Next)

// 默认白色
+ (UIColor *)defaultColor;
- (UIColor *)next;

@end

//
//  UIView+Frame.m
//  ComprehensiveDemo
//
//  Created by 杨时雨 on 16/8/3.
//  Copyright © 2016年 zwd. All rights reserved.
//

#import "UIView+Frame.h"

@implementation UIView (Frame)

+ (instancetype)dj_viewFromeXib
{
    return [[NSBundle mainBundle] loadNibNamed:NSStringFromClass(self) owner:nil options:nil].firstObject;
}

- (void)setDj_width:(CGFloat)dj_width
{
    CGRect rect = self.frame;
    rect.size.width = dj_width;
    self.frame = rect;
}


-(CGFloat)dj_width
{
    return self.frame.size.width;
}

- (void)setDj_height:(CGFloat)dj_height
{
    CGRect rect = self.frame;
    rect.size.height = dj_height;
    self.frame = rect;
}

- (CGFloat)dj_height
{
    return self.frame.size.height;

}

- (void)setDj_x:(CGFloat)dj_x
{
    CGRect rect = self.frame;
    rect.origin.x = dj_x;
    self.frame = rect;
}

- (CGFloat)dj_x
{
    return self.frame.origin.x;

}


- (void)setDj_y:(CGFloat)dj_y
{
    CGRect rect = self.frame;
    rect.origin.y = dj_y;
    self.frame = rect;
}

-(CGFloat)dj_y
{
    return self.frame.origin.y;
    
}

- (CGFloat)dj_maxX{
    return CGRectGetMaxX(self.frame);
}

- (void)setDj_maxX:(CGFloat)dj_maxX{}


- (CGFloat)dj_maxY{
    return CGRectGetMaxY(self.frame);
}

- (void)setDj_maxY:(CGFloat)dj_maxY{}


- (void)setDj_centerX:(CGFloat)dj_centerX
{
    CGPoint center = self.center;
    center.x = dj_centerX;
    self.center = center;
}

- (CGFloat)dj_centerX
{
    return self.center.x;
}

- (void)setDj_centerY:(CGFloat)dj_centerY
{
    CGPoint center = self.center;
    center.y = dj_centerY;
    self.center = center;
}

- (CGFloat)dj_centerY
{
    return self.center.y;
}


@end

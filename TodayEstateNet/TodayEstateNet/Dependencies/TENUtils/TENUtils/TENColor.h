//
//  TENColor.h
//  TENUtils
//
//  Created by HomerLynn on 2018/9/11.
//  Copyright © 2018年 TEN. All rights reserved.
//

#ifndef TENColor_h
#define TENColor_h

static NSString *TENWhiteColor = @"FFFFFF";        // 白
static NSString *TENBlackColor = @"000000";        // 黑
static NSString *TENRedColor = @"FF0000";          // 红
static NSString *TENGreenColor = @"00FF00";        // 蓝
static NSString *TENBlueColor = @"0000FF";         // 绿
static NSString *TENGrayColor = @"8B8B90";         //灰

// 文本
static NSString *TENTextGrayColor = @"999999";     // 文字 灰色
static NSString *TENTextGrayColor6 = @"666666";    // 文字 灰色
static NSString *TENTextBlackColor3 = @"333333";   // 文字 黑色
static NSString *TENTextRedColor = @"D14E33";      // 文字 红色
static NSString *TENSpaceViewColor = @"D8D8D8";    // 分割线 灰色

static NSString *TENThemeBlueColor = @"3347D1";     // 主题蓝色

#endif /* TENColor_h */

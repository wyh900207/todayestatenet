//
//  UIView+Gradient.m
//  TENUtils
//
//  Created by  HomerLynn on 2018/12/11.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "UIView+Gradient.h"

@implementation UIView (Gradient)

- (void)gradientFromColor:(UIColor *)fromColor toColor:(UIColor *)toColor {
    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
    gradientLayer.frame = self.bounds;
    gradientLayer.startPoint = CGPointMake(0.5, 0);
    gradientLayer.endPoint = CGPointMake(1, 1);
    gradientLayer.colors = @[(__bridge id)fromColor.CGColor, (__bridge id)toColor.CGColor];
    gradientLayer.locations = @[@(0), @(1.0f)];
    self.layer.cornerRadius = 8;
    
    [self.layer insertSublayer:gradientLayer atIndex:0];
}

//将view转成图片
-(UIImage*)convertViewToImage:(UIView*)v{
    CGSize s = v.bounds.size;
    // 下面方法，第一个参数表示区域大小。第二个参数表示是否是非透明的。如果需  要显示半透明效果，需要传NO，否则传YES。第三个参数就是屏幕密度了
    UIGraphicsBeginImageContextWithOptions(s, YES, [UIScreen mainScreen].scale);
    [v.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage*image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}
@end

//
//  ImageLeftButton.h
//  CPS
//
//  Created by DJAPpple_4 on 2017/6/29.
//  Copyright © 2017年 zhangsp. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ImageLeftButton : UIButton

// 这里的左右是指label
- (instancetype)initWithLeft;
- (instancetype)initWithRight;

@end

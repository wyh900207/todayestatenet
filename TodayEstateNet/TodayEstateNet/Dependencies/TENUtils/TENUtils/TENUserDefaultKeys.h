//
//  TENUserDefaultKeys.h
//  TENUtils
//
//  Created by HomerLynn on 2018/10/21.
//  Copyright © 2018 TEN. All rights reserved.
//

#ifndef TENUserDefaultKeys_h
#define TENUserDefaultKeys_h



// 默认城市`name`
static NSString *kDisplayCityName = @"display-city-name";
// 默认街道`name`
static NSString *kSectionCode = @"display-section-name";
// 城市`code`
static NSString *kCityCode = @"city-code";
// 城市`name`
static NSString *kCityName = @"city-name";
// 城市`longitude`
static NSString *kCityLongitude = @"city-longitude";
// 城市`latitude`
static NSString *kCityLatitude = @"city-latitude";

#pragma mark - 用户

static NSString *kUserAvatar = @"yyUser.avatar";
static NSString *kUserAccountId = @"yyAccount.id";
static NSString *kUserRoles = @"yyAccount.roles";
static NSString *kUserCompanyId = @"companyId";
static NSString *kUserLoginToken = @"loginToken";
static NSString *kUserPhone = @"yyAccount.phone";
static NSString *kUserName = @"userName";
static NSString *kUserSharePic = @"sharePic";
static NSString *kUserId = @"yyUserId";

#endif /* TENUserDefaultKeys_h */

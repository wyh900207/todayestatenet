//
//  UIView+Gradient.h
//  TENUtils
//
//  Created by  HomerLynn on 2018/12/11.
//  Copyright © 2018 TEN. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIView (Gradient)

- (void)gradientFromColor:(UIColor *)fromColor toColor:(UIColor *)toColor;
//将view转成图片
-(UIImage*)convertViewToImage:(UIView*)v;
@end

NS_ASSUME_NONNULL_END

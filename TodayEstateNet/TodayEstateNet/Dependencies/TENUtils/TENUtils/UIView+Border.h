//
//  UIView+Border.h
//  TENUtils
//
//  Created by  HomerLynn on 2018/11/6.
//  Copyright © 2018 TEN. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_OPTIONS(NSUInteger, HLBoderType) {
    HLBoderTypeNone              = 0,
    HLBoderTypeTop               = 1 << 0,
    HLBoderTypeLeft              = 1 << 1,
    HLBoderTypeBottom            = 1 << 2,
    HLBoderTypeRight             = 1 << 3
};

@interface UIView (Border)

@property (nonatomic, strong) UIColor *realBorderColor;
@property (nonatomic, strong) CALayer *topLayer;
@property (nonatomic, strong) CALayer *leftLayer;
@property (nonatomic, strong) CALayer *bottomLayer;
@property (nonatomic, strong) CALayer *rightLayer;

- (void)borderWith:(UIColor *)borderColor boderType:(HLBoderType)borderType;

@end

NS_ASSUME_NONNULL_END

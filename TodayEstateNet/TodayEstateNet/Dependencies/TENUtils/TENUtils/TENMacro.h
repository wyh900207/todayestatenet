//
//  TENMacro.h
//  TENUtils
//
//  Created by HomerLynn on 2018/9/11.
//  Copyright © 2018年 TEN. All rights reserved.
//

#ifndef TENMacro_h
#define TENMacro_h

// 常用间距
static const float TENNavigationBarHeight = 44;         // 导航高度
static const float TENTabarHeight = 49;                 // Tabbar高度
static const float TENSpace10 = 10;                     // 10
static const float TENSpace15 = 15;                     // 15
static const float TENSpace16 = 16;                     // 16
static const float TENSpace20 = 20;                     // 20
static const float TENSpace30 = 30;                     // 30

#define TENScreenWidth [UIScreen mainScreen].bounds.size.width
#define TENScreenHeight [UIScreen mainScreen].bounds.size.height

// 字体大小
#define TENFont10 [UIFont systemFontOfSize:10]          // 10号
#define TENFont11 [UIFont systemFontOfSize:11]          // 11号
#define TENFont12 [UIFont systemFontOfSize:12]          // 12号
#define TENFont13 [UIFont systemFontOfSize:13]          // 13号
#define TENFont14 [UIFont systemFontOfSize:14]          // 14号
#define TENFont15 [UIFont systemFontOfSize:15]          // 15号
#define TENFont17 [UIFont systemFontOfSize:17]          // 17号
#define TENFont18 [UIFont systemFontOfSize:18]          // 18号
#define TENFont20 [UIFont systemFontOfSize:20]          // 20号
// 粗体
#define TENBoldFont13 [UIFont boldSystemFontOfSize:13]      // 13
#define TENBoldFont14 [UIFont boldSystemFontOfSize:14]      // 14
#define TENBoldFont15 [UIFont boldSystemFontOfSize:15]      // 15
#define TENBoldFont17 [UIFont boldSystemFontOfSize:17]      // 17
#define TENBoldFont20 [UIFont boldSystemFontOfSize:20]      // 20

// 图片
#define TENImage(name) [UIImage imageNamed:name]            // 只用字符串初始化图片

// 颜色
#define TENHexColor(colorString) [UIColor colorWithHexString:colorString]   // Hex color
#define TENHexClearColor [UIColor colorWithHexString:TENWhiteColor alpha:0]   // Hex color
#define LJColor(r, g, b, a) [UIColor colorWithRed:r green:g blue:b alpha:a]

/// RGB
#define  YMEC_COLOR(r,g,b)  [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
/// RGBA
#define YMEC_HEX_COLOR_A(c,a)   [UIColor colorWithRed:((c>>16)&0xFF)/255.0 green:((c>>8)&0xFF)/255.0 blue:(c&0xFF)/255.0 alpha:a]
/// RGB
#define YMEC_HEX_COLOR(c) YMEC_HEX_COLOR_A(c,1)

#define YMEC_COLOR_THEME YMEC_HEX_COLOR(0xffffff)       // 主题色
#define YMEC_COLOR_WHITE YMEC_HEX_COLOR(0xffffff)       // 白色
#define YMEC_COLOR_GRAY_3 YMEC_HEX_COLOR(0x333333)      // Gray3
#define YMEC_COLOR_GRAY_6 YMEC_HEX_COLOR(0x666666)      // Gray6
#define YMEC_COLOR_GRAY_9 YMEC_HEX_COLOR(0x999999)      // Gray9
#define YMEC_COLOR_FILL YMEC_HEX_COLOR(0xf4f4f4)        // Fill
#define YMEC_COLOR_SHARP YMEC_HEX_COLOR(0x148A89)       // Sharp
#define YMEC_COLOR_RED YMEC_HEX_COLOR(0xfc5454)         // 红色主题色
#define YMEC_COLOR_TEXT_GRAY YMEC_HEX_COLOR(0x5D6470)   // 文字深灰色
#define YMEC_COLOR_SHADOWCOLOR YMEC_HEX_COLOR(0xCBCBCB) // 阴影颜色
#define YMEC_COLOR_QGRAY YMEC_HEX_COLOR(0x8B8B90)       // 浅灰色
#define YMEC_COLOR_GRAY_C YMEC_HEX_COLOR(0xC0C0C0)      // 发布时间和浏览次数
#define YMEC_COLOR_GRAY_97 YMEC_HEX_COLOR(0x979797)     // 点赞, 评论, 转发
#define YMEC_COLOR_BLACK_4A YMEC_HEX_COLOR(0x4A4A4A)    // 发表内容
#define YMEC_COLOR_BLUE YMEC_HEX_COLOR(0x4E8CEE)        // 文字蓝色
#define YMEC_COLOR_SEPARATE YMEC_HEX_COLOR(0xE7E7E7)    // 分割线
#define YMEC_COLOR_TABLE_BACK YMEC_HEX_COLOR(0xF8F8F8)  // 表背景
#define YMEC_COLOR_BLACK YMEC_HEX_COLOR(0x000000)       // 纯黑色
#define YMEC_COLOR_YELLOW YMEC_HEX_COLOR(0xFFBB56)      // 文字黄色
#define YMEC_COLOR_CLEAR [UIColor clearColor]           // 透明
// weak 统一用这种方式修饰
// 使用方法:
//
// @weakify(实例对象)
// @strongtify(实例对象)
// 如果是在 Block 中, 在 block 外部是用 '@weakify()' 声明弱引用对象, 在 block 作用于内 声明 '@strongtify'
#define weakify(o) autoreleasepool{} __weak typeof(o) o##_weak = o;
#define strongtify(o) autoreleasepool{} __strong typeof(o) o = o##_weak;
#define WS(weakSelf)  __weak __typeof(&*self)weakSelf = self;

//字符串是否为空
//字符串是否为空
#define KSTRING_IS_EMPTY(str) ([str isKindOfClass:[NSNull class]] || str == nil || [str length] < 1 || [str isEqual:[NSNull null]] ? YES : NO )

// 打印
#ifdef DEBUG
# define TEN_LOG(format, ...) NSLog((@"\n[文件名:%s]" "\n[函数名:%s]" "\n[行号:%d]" format), __FILE__, __FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
# define TEN_LOG(...);
#endif

// NSUserDefaults
#define TEN_UD [NSUserDefaults standardUserDefaults]
#define TEN_UD_SYNC [[NSUserDefaults standardUserDefaults] synchronize];
#define TEN_UD_SET_VALUE(value, key) [[NSUserDefaults standardUserDefaults] setValue:value forKey:key];
#define TEN_UD_VALUE(key) [[NSUserDefaults standardUserDefaults] objectForKey:key]
#define TEN_UD_BOOL(key) [[NSUserDefaults standardUserDefaults] boolForKey:key]

//View 圆角和加边框
#define TENBorderAndRadius(View, Radius, Width, Color)\
\
[View.layer setCornerRadius:(Radius)];\
[View.layer setMasksToBounds:YES];\
[View.layer setBorderWidth:(Width)];\
[View.layer setBorderColor:[Color CGColor]];

// View 圆角
#define TENCornerRadius(View, Radius)\
\
[View.layer setCornerRadius:(Radius)];\
[View.layer setMasksToBounds:YES];

// 屏幕适配
#define kScreenSize [UIScreen mainScreen].bounds
#define kScreenHeight [UIScreen mainScreen].bounds.size.height
#define kScreenWidth [UIScreen mainScreen].bounds.size.width
#define kWidth(R) (R*kScreenWidth)/375.0
#define kHeight(R) (R*kScreenHeight)/667.0

// 系统适配
#define TEN_iOS_11_OR_LATER  ([[[UIDevice currentDevice] systemVersion] floatValue] >= 11)
#define TEN_iOS_10_OR_LATER  ([[[UIDevice currentDevice] systemVersion] floatValue] >= 10.0f)
#define TEN_iOS_9_OR_LATER   ([[[UIDevice currentDevice] systemVersion] floatValue] >= 9.0f)
#define TEN_iOS_8_OR_LATER   ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0f)

#define TEN_iOS11_Height (TEN_iOS_11_OR_LATER ? 0 : (-20))

// iPhoneX适配
//#define K_iPhoneX (kScreenWidth == 375.f && kScreenHeight == 812.f ? YES : NO)
#define K_iPhoneX ((kScreenWidth == 375.f && kScreenHeight == 812.f)||(kScreenWidth == 414.f && kScreenHeight == 896.f) ? YES : NO)

#define TENNavigationBarHeight (K_iPhoneX ? 88.f : 64.f)
#define TENStatusBarHeight (K_iPhoneX ? 44.f : 20.f)
#define TENTabbarHeight (K_iPhoneX ? 49.f : 49.f)
#define TENTabbarSafeBottomMargin (K_iPhoneX ? 34.f : 0.f)

// 是否是刘海屏
static inline BOOL is_iPhone_X() {
    BOOL result = NO;
    if (UIDevice.currentDevice.userInterfaceIdiom != UIUserInterfaceIdiomPhone) {
        //判断是否是手机
        return result;
    }
    if (@available(iOS 11.0, *)) {
        UIWindow *mainWindow = [[[UIApplication sharedApplication] delegate] window];
        if (mainWindow.safeAreaInsets.bottom > 0.0) {
            result = YES;
        }
    }
    return result;
}

/// 支付成功
#define YMEC_PAY_SUCCESS_NOTIFICATION @"paySuccessNotification"
#define YMEC_LOGIN_SUCCESS_NOTIFICATION @"loginSuccessNotification"
#define YMEC_LOGOUT_SUCCESS_NOTIFICATION @"logoutSuccessNotification"

//accountId
#define kAccountId TEN_UD_VALUE(@"yyAccount.id")
#define kAvatarUrl TEN_UD_VALUE(@"yyUser.avatar")
#define kRoles TEN_UD_VALUE(@"yyAccount.roles")
#define kCompanyId TEN_UD_VALUE(@"companyId")
#define kLoginToken TEN_UD_VALUE(@"loginToken")
#define TEN_USER_PHONE TEN_UD_VALUE(@"yyAccount.phone")
#define TEN_USER_NAME TEN_UD_VALUE(@"userName")
#define kSharePic TEN_UD_VALUE(@"sharePic")

// APP 版本
#define APP_VERSION ([[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"])


#endif /* TENMacro_h */

//
//  ImageLeftButton.m
//  CPS
//
//  Created by DJAPpple_4 on 2017/6/29.
//  Copyright © 2017年 zhangsp. All rights reserved.
//

#import "ImageLeftButton.h"
#import "UIView+Frame.h"
#define titleFont [UIFont systemFontOfSize:14]

@interface ImageLeftButton()

@property (nonatomic, assign) BOOL left;

@end

@implementation ImageLeftButton

- (instancetype)initWithLeft {
    if (self = [super init]) {
        self.left = YES;
    }
    return self;
}

- (instancetype)initWithRight {
    if (self = [super init]) {
        self.left = NO;
    }
    return self;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    // 图片在右边文字在左边
    if (self.left) {
        self.imageEdgeInsets = UIEdgeInsetsMake(0, self.titleLabel.dj_width + 3, 0, -(self.titleLabel.dj_width + 3));
        self.titleEdgeInsets = UIEdgeInsetsMake(0, -(self.imageView.dj_width + 3), 0, self.imageView.dj_width + 3);
    } else {
        self.imageEdgeInsets = UIEdgeInsetsMake(0, -(self.titleLabel.dj_width + 3), 0, (self.titleLabel.dj_width + 3));
        self.titleEdgeInsets = UIEdgeInsetsMake(0, (self.imageView.dj_width + 3), 0, -self.imageView.dj_width + 3);
    }
}

@end


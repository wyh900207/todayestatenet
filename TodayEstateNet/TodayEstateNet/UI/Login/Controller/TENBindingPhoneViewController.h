//
//  TENBindingPhoneViewController.h
//  TodayEstateNet
//
//  Created by  HomerLynn on 2018/12/10.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENBaseViewController.h"

@interface TENBindingPhoneViewController : TENBaseViewController

// Third platform ID. (For example wechat ID)
@property (nonatomic, strong) NSString *thirdPlatformId;
// Binding type. In order to decide it is `binding` or `reset password`.
// Binding -> @0, reset psw -> !@0.
// Default value is @0 (binding).
@property (nonatomic, strong) NSNumber *bindingType;
// Wechat third id.ss
@property (nonatomic, copy) NSString *thirdId;
// Login Token
@property (nonatomic, copy) NSString *loginToken;

@end

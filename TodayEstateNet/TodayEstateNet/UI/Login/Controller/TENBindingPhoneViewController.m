//
//  TENBindingPhoneViewController.m
//  TodayEstateNet
//
//  Created by  HomerLynn on 2018/12/10.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENBindingPhoneViewController.h"
//#import "TENUtilsHeader.h"
#import "TENLoginAccountView.h"
#import "DJCountdownButton.h"
#import "TENPictureCodeViewController.h"
#import "TENLoginRequest.h"
@interface TENBindingPhoneViewController ()

@property (nonatomic, strong) UIImageView *backgroundImageView;
@property (nonatomic, strong) UIImageView *closeImageView;
@property (nonatomic, strong) UIButton *closeButton;
@property (nonatomic, strong) UIImageView *loginIcon;
@property (nonatomic, strong) UILabel *loginTitle;
@property (nonatomic, strong) UILabel *protocolLabel;
@property (nonatomic, strong) TENLoginAccountView *loginAccountView;
@property (nonatomic, strong) UIButton *submitButton;
// Storage property
@property (nonatomic, copy) NSString *tempPhone;
@property (nonatomic, copy) NSString *tempMsgCode;

@end

@implementation TENBindingPhoneViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupSubviews];
}

#pragma mark - UI

- (void)setupSubviews {
    [self.view addSubview:self.backgroundImageView];
    [self.view addSubview:self.closeButton];
    [self.view addSubview:self.closeImageView];
    [self.view addSubview:self.loginIcon];
    [self.view addSubview:self.loginTitle];
    [self.view addSubview:self.protocolLabel];
    [self.view addSubview:self.loginAccountView];
    [self.view addSubview:self.submitButton];
    
    [self.view bringSubviewToFront:self.closeButton];
    
    [self.backgroundImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    [self.closeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0, *)) {
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop).offset(7);
        } else {
            make.top.equalTo(self.view).offset(7);
        }
        make.left.equalTo(self.view).offset(9);
        make.width.height.equalTo(@30);
    }];
    [self.closeImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.closeButton);
    }];
    [self.loginIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.top.equalTo(self.closeButton.mas_bottom).offset(53);
        make.width.height.equalTo(@72);
    }];
    [self.loginTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.top.equalTo(self.loginIcon.mas_bottom).offset(10);
        make.height.equalTo(@25);
    }];
    [self.protocolLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.height.equalTo(@17);
        if (@available(iOS 11.0, *)) {
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom).offset(-20);
        } else {
            make.bottom.equalTo(self.view).offset(-20);
        }
    }];
    [self.loginAccountView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.height.equalTo(@90);
        make.top.equalTo(self.loginTitle.mas_bottom).offset(20);
    }];
    [self.submitButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.protocolLabel.mas_top).offset(-75);
        make.centerX.equalTo(self.view);
        make.width.equalTo(@200);
        make.height.equalTo(@44);
    }];
    
    // Gradient color
    [self.submitButton layoutIfNeeded];
    [self.submitButton gradientFromColor:TENHexColor(@"3347D1") toColor:TENHexColor(@"5E6DE4")];
}

// Custom navigation bar
- (void)customNavigationBar {
    if (!self.bindingType || self.bindingType.integerValue == 0) {
        // Binding
        self.navigationItem.title = @"绑定手机号";
    }
    else {
        self.navigationItem.title = @"重置密码";
    }
}

#pragma mark - Private methods

// 关闭页面
- (void)close {
    [self dismissViewControllerAnimated:YES completion:nil];
}

// 确定按钮`Action`
- (void)commit {
    if (!self.tempPhone || [self.tempPhone isEqualToString:@""]) {
        [NSObject showHudTipStr:@"请输入手机号"];
        return;
    }
    if (!self.tempMsgCode || [self.tempMsgCode isEqualToString:@""]) {
        [NSObject showHudTipStr:@"请输入验证码"];
    }
    if (self.tempMsgCode.length != 4 && self.tempMsgCode.length != 6) {
        [NSObject showHudTipStr:@"请输入正确的验证码"];
    }
    
    if (!self.tempPhone) { [NSObject showHudTipStr:@"请输入手机号"]; return; }
    if (self.tempPhone.length != 11) { [NSObject showHudTipStr:@"请检查手机号码"]; return; }
    if (!self.tempMsgCode) { [NSObject showHudTipStr:@"请输入验证码"]; return; }
    
    NSDictionary * params = @{@"loginToken": self.loginToken,
                              @"verificationCode": self.tempMsgCode,
                              @"phone": self.tempPhone,
                              @"id": self.thirdId
                              };
    [TENLoginRequest bindingPhone:params success:^(NSDictionary *response) {
        TEN_LOG(@"%@", response);
        if ([response[@"code"] integerValue] == 1) {
            [TEN_UD setValue:self.tempPhone forKey:@""];
            //保存登录状态
            [[DLUserManager shareManager] loginSuccessWith:response];
            [TEN_UD setBool:YES forKey:TEN_IS_LOGIN];
            [self dismissViewControllerAnimated:YES completion:nil];
        }else{
            [NSObject showHudTipStr:response[@"msg"]];
        }
    } failure:^(NSError *error) {
        TEN_LOG(@"%@", error.description);
    }];
}

// Get `Picture Code`
- (void)presentPictureCodeViewController {
    if (!self.tempPhone) { [NSObject showHudTipStr:@"请输入手机号"]; return; }
    if (self.tempPhone.length != 11) { [NSObject showHudTipStr:@"请检查手机号码"]; return; }
    
    TENPictureCodeViewController *pictureCodeController = [TENPictureCodeViewController new];
    pictureCodeController.phone = self.tempPhone;
    //获取验证码成功，开始倒数
    @weakify(self);
    pictureCodeController.commitMsgCodeSuccessHandler = ^(BOOL isSuccess) {
        if (isSuccess) {
            @strongtify(self);
            [self countDown];
            [self.loginAccountView.pswTextField becomeFirstResponder];
        }
    };
    pictureCodeController.view.backgroundColor = [UIColor colorWithHexString:@"00000080"];// black & alpha = 0.5
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:pictureCodeController];
    nav.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    
    [self presentViewController:nav animated:YES completion:nil];
}

// Login 文本监听
- (void)textChangedWith:(UITextField *)textField text:(NSString *)text {
    if (textField.tag == 101) {
        // phone
        self.tempPhone = text;
    } else {
        // msg code
        self.tempMsgCode = text;
    }
}

// Count down
- (void)countDown {
    [self.loginAccountView.countDownButton onclickCountDown];
}

#pragma mark - Touch

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

#pragma mark - Getter

- (UIImageView *)backgroundImageView {
    if (!_backgroundImageView) {
        _backgroundImageView = [UIImageView new];
        _backgroundImageView.image = [UIImage imageNamed:@"login-background"];
        _backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _backgroundImageView;
}

- (UIImageView *)closeImageView {
    if (!_closeImageView) {
        _closeImageView = [UIImageView new];
                _closeImageView.image = [UIImage imageNamed:@"login-close"];
    }
    return _closeImageView;
}

- (UIButton *)closeButton {
    if (!_closeButton) {
        _closeButton = [UIButton new];
                [_closeButton addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    }
    return _closeButton;
}

- (UIImageView *)loginIcon {
    if (!_loginIcon) {
        _loginIcon = [UIImageView new];
        _loginIcon.image = [UIImage imageNamed:@"login-icon"];
    }
    return _loginIcon;
}

- (UILabel *)loginTitle {
    if (!_loginTitle) {
        _loginTitle = [UILabel new];
        _loginTitle.text = @"今日房网";
        _loginTitle.font = TENFont18;
        _loginTitle.textColor = TENHexColor(TENTextBlackColor3);
    }
    return _loginTitle;
}

- (UILabel *)protocolLabel {
    if (!_protocolLabel) {
        _protocolLabel = [UILabel new];
        _protocolLabel.text = @"登陆即同意 《今日房网软件及服务许可协议》";
        _protocolLabel.textColor = TENHexColor(TENTextGrayColor6);
        _protocolLabel.font = TENFont12;
    }
    return _protocolLabel;
}

- (TENLoginAccountView *)loginAccountView {
    if (!_loginAccountView) {
        _loginAccountView = [TENLoginAccountView new];
        @weakify(self);
        // `phone` or `psw` text changed handler
        _loginAccountView.textChangedHandler = ^(UITextField *textField, NSString *text) {
            @strongtify(self);
            [self textChangedWith:textField text:text];
        };
        // `count down button` touch up inside action handler
        _loginAccountView.countDownButtonTouchupInsideHandler = ^{
            // request for `picture msg code`
            @strongtify(self);
            //[self countDown];
            [self presentPictureCodeViewController];
        };
    }
    return _loginAccountView;
}

- (UIButton *)submitButton {
    if (!_submitButton) {
        _submitButton = [UIButton new];
        //_submitButton.backgroundColor = TENHexColor(@"3347D1");
        _submitButton.titleLabel.font = TENFont15;
        _submitButton.layer.cornerRadius = 8;
        _submitButton.layer.masksToBounds = YES;
        [_submitButton setTitle:@"确认" forState:0];
        [_submitButton addTarget:self action:@selector(commit) forControlEvents:UIControlEventTouchUpInside];
    }
    return _submitButton;
}

@end

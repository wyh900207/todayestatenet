//
//  TENPictureCodeViewController.h
//  TodayEstateNet
//
//  Created by  HomerLynn on 2018/12/21.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENBaseViewController.h"

@interface TENPictureCodeViewController : TENBaseViewController

@property (nonatomic, copy) NSString *phone;
@property (nonatomic, copy) void (^commitMsgCodeSuccessHandler)(BOOL isSuccess);

@end

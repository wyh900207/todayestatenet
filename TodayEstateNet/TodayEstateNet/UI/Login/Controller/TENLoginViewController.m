//
//  TENLoginViewController.m
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/11/7.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENLoginViewController.h"
#import "TENLoginRequest.h"
#import "TENBindingPhoneViewController.h"

@interface TENLoginViewController ()

@property (nonatomic, strong) UIImageView *backgroundImageView;
@property (nonatomic, strong) UIImageView *closeImageView;
@property (nonatomic, strong) UIButton *closeButton;
@property (nonatomic, strong) UIImageView *loginIcon;
@property (nonatomic, strong) UILabel *loginTitle;
@property (nonatomic, strong) UIButton *wechatButton;
@property (nonatomic, strong) UILabel *protocolLabel;

@end

@implementation TENLoginViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBar.hidden = YES;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupSubviews];
}

#pragma mark - UI

- (void)setupSubviews {
    [self.view addSubview:self.backgroundImageView];
    [self.view addSubview:self.closeButton];
    [self.view addSubview:self.closeImageView];
    [self.view addSubview:self.loginIcon];
    [self.view addSubview:self.loginTitle];
    [self.view addSubview:self.wechatButton];
    [self.view addSubview:self.protocolLabel];
    
    [self.view bringSubviewToFront:self.closeButton];
    
    [self.backgroundImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    [self.closeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0, *)) {
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop).offset(7);
        } else {
            make.top.equalTo(self.view).offset(7);
        }
        make.left.equalTo(self.view).offset(9);
        make.width.height.equalTo(@30);
    }];
    [self.closeImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.center.equalTo(self.closeButton);
    }];
    [self.loginIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.top.equalTo(self.closeButton.mas_bottom).offset(53);
        make.width.height.equalTo(@72);
    }];
    [self.loginTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.top.equalTo(self.loginIcon.mas_bottom).offset(10);
        make.height.equalTo(@25);
    }];
    [self.wechatButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.top.equalTo(self.loginTitle.mas_bottom).offset(36);
        make.width.equalTo(@200);
        make.height.equalTo(@43);
    }];
    [self.protocolLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.height.equalTo(@17);
        if (@available(iOS 11.0, *)) {
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom).offset(-20);
        } else {
            make.bottom.equalTo(self.view).offset(-20);
        }
    }];
}

#pragma mark - Private methods

// 关闭页面
- (void)close {
    [self dismissViewControllerAnimated:YES completion:nil];
}

// 微信登录
- (void)wechatLogin {
    TEN_LOG(@"Wecaht login");
    //[self dismissViewControllerAnimated:YES completion:nil];
    @weakify(self);
    // Fetch wechat user info.
    [ShareUserManager loginWithWechat:^(UMSocialUserInfoResponse *response) {
        TEN_LOG(@"%@", response);
        @strongtify(self);
        // 1. Fetch wechat user info successful.
        // 2. Query from server.
        [self loginWith:response];
    }];
}

// Query form server in order to binding phone or login.
- (void)loginWith:(UMSocialUserInfoResponse *)wechatInfo {
    NSMutableDictionary * params = [@{@"avatar": wechatInfo.iconurl,
                                      @"nickname": wechatInfo.name,
                                      @"openId": wechatInfo.openid,
                                      @"sex": [wechatInfo.gender isEqualToString:@"男"] ? @"1" : @"2",
                                      @"unionId": wechatInfo.unionId,
                                      @"appType":@"1"
                                      } mutableCopy];
    @weakify(self)
    [TENLoginRequest loginWithParams:params success:^(NSDictionary *response) {
        TEN_LOG(@"%@", response);
        @strongtify(self);
        if ([response[@"code"] integerValue] == 1) {
            NSNumber * isBindPhone = response[@"content"][@"isBindPhone"];
            if (isBindPhone.integerValue == 0) {//未绑定手机号
                NSString *accountId = response[@"content"][@"yyAccount"][@"id"];
                NSString *loginToken = response[@"content"][@"loginToken"];
                [self bindingPhoneWith:accountId loginToken:loginToken];
            } else {//已绑定
                [[DLUserManager shareManager] loginSuccessWith:response];
                [TEN_UD setBool:YES forKey:TEN_IS_LOGIN];
                [self dismissViewControllerAnimated:YES completion:nil];
            }
        }else{
            [NSObject showHudTipStr:response[@"msg"]];
        }
        
    } failure:^(NSError *error) {
        TEN_LOG(@"%@", error.description);
    }];
}

// Binding phone number.
- (void)bindingPhoneWith:(NSString *)accountId loginToken:(NSString *)loginToken {
    TENBindingPhoneViewController * bindingController = [TENBindingPhoneViewController new];
    bindingController.thirdId = accountId;
    bindingController.loginToken = loginToken;
    [self.navigationController pushViewController:bindingController animated:YES];
}


#pragma mark - Getter

- (UIImageView *)backgroundImageView {
    if (!_backgroundImageView) {
        _backgroundImageView = [UIImageView new];
        _backgroundImageView.image = [UIImage imageNamed:@"login-background"];
        _backgroundImageView.contentMode = UIViewContentModeScaleAspectFill;
    }
    return _backgroundImageView;
}

- (UIImageView *)closeImageView {
    if (!_closeImageView) {
        _closeImageView = [UIImageView new];
        _closeImageView.image = [UIImage imageNamed:@"login-close"];
    }
    return _closeImageView;
}

- (UIButton *)closeButton {
    if (!_closeButton) {
        _closeButton = [UIButton new];
        [_closeButton addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    }
    return _closeButton;
}

- (UIImageView *)loginIcon {
    if (!_loginIcon) {
        _loginIcon = [UIImageView new];
        _loginIcon.image = [UIImage imageNamed:@"login-icon"];
    }
    return _loginIcon;
}

- (UILabel *)loginTitle {
    if (!_loginTitle) {
        _loginTitle = [UILabel new];
        _loginTitle.text = @"今日房网";
        _loginTitle.font = TENFont18;
        _loginTitle.textColor = TENHexColor(TENTextBlackColor3);
    }
    return _loginTitle;
}

- (UIButton *)wechatButton {
    if (!_wechatButton) {
        _wechatButton = [UIButton new];
        [_wechatButton setBackgroundImage:[UIImage imageNamed:@"login-wechat"] forState:0];
        [_wechatButton addTarget:self action:@selector(wechatLogin) forControlEvents:UIControlEventTouchUpInside];
    }
    return _wechatButton;
}

- (UILabel *)protocolLabel {
    if (!_protocolLabel) {
        _protocolLabel = [UILabel new];
        _protocolLabel.text = @"登陆即同意 《今日房网软件及服务许可协议》";
        _protocolLabel.textColor = TENHexColor(TENTextGrayColor6);
        _protocolLabel.font = TENFont12;
    }
    return _protocolLabel;
}

@end

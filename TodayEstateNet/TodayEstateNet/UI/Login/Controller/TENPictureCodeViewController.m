//
//  TENPictureCodeViewController.m
//  TodayEstateNet
//
//  Created by  HomerLynn on 2018/12/21.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENPictureCodeViewController.h"
#import "TENLoginRequest.h"
#import "TENTextField.h"
#import "TENNetwork.h"

@interface TENPictureCodeViewController ()

@property (nonatomic, strong) UIView *alertContainerView;
@property (nonatomic, strong) UIView *topSeperateView;
@property (nonatomic, strong) UIView *codeContainerView;
@property (nonatomic, strong) UIWebView *webview;
@property (nonatomic, strong) UIView *bottomSeperateView;
@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UIButton *commitButton;
@property (nonatomic, strong) UIButton *refreshButton;
@property (nonatomic, strong) TENTextField *msgCodeTextField;
// Storage property
@property (nonatomic, copy  ) NSString *tempCode;

@end

@implementation TENPictureCodeViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = YES;
    [self reloadWebView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupSubviews];
    [self reloadWebView];
}

#pragma mark - UI

- (void)setupSubviews {
    [self.view addSubview:self.alertContainerView];
    
    
    [self.alertContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.width.equalTo(self.view).multipliedBy(295.0 / 375.0);
        //        make.height.equalTo(self.view).height.equalTo(@200);
        make.height.mas_equalTo(200);
        make.center.equalTo(self.view);
        
    }];
    
    [self.view addSubview:self.titleLabel];
    [self.view addSubview:self.topSeperateView];
    [self.view addSubview:self.codeContainerView];
    [self.view addSubview:self.webview];
    [self.view addSubview:self.msgCodeTextField];
    [self.view addSubview:self.bottomSeperateView];
    [self.view addSubview:self.commitButton];
    [self.view addSubview:self.refreshButton];
    
    CGFloat containerWidth = 295.0 / 375.0 * TENScreenWidth;
    
    [self.alertContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self.view);
        make.centerY.equalTo(self.view).offset(-50);
        make.width.equalTo(@(containerWidth));
    }];
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.alertContainerView);
        make.height.equalTo(@30);
    }];
    [self.topSeperateView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.alertContainerView);
        make.top.equalTo(self.titleLabel.mas_bottom);
        make.height.equalTo(@1);
    }];
    [self.codeContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.topSeperateView.mas_bottom);
        make.left.right.equalTo(self.alertContainerView);
        make.height.equalTo(@70);
    }];
    [self.webview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.codeContainerView);
        make.right.equalTo(self.codeContainerView).offset(-30);
        make.width.equalTo(@115);
        make.height.equalTo(@30);
    }];
    [self.msgCodeTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.codeContainerView);
        make.left.equalTo(self.codeContainerView).offset(25);
        make.right.equalTo(self.webview.mas_left).offset(-10);
        make.height.equalTo(self.webview);
    }];
    [self.bottomSeperateView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.alertContainerView);
        make.top.equalTo(self.codeContainerView.mas_bottom);
        make.height.equalTo(@1);
    }];
    [self.commitButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.bottomSeperateView.mas_bottom);
        make.left.right.bottom.equalTo(self.alertContainerView);
        make.height.equalTo(@50);
    }];
    [self.refreshButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.webview);
    }];
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [self dismiss];
}

#pragma mark - Private methods

- (void)commitMsgCode {
    // upload msg code, you will get an sms code.
    if (!self.tempCode || [self.tempCode isEqualToString:@""]) {
        [NSObject showHudTipStr:@"请输入验证码"];
        return;
    }
    NSDictionary *params = @{
                             @"phone": self.phone,
                             @"verificationCode": self.tempCode
                             };
    @weakify(self);
    [TENLoginRequest fetchSmsCode:params success:^(NSDictionary *response) {
        TEN_LOG(@"%@",response);
        @strongtify(self);
        NSNumber *code = response[@"code"];
        NSString *msg = response[@"msg"];
        if (code.integerValue == 1) {
            [NSObject showHudTipStr:@"验证码已发送"];
            self.commitMsgCodeSuccessHandler(YES);
            [self.view endEditing:YES];
            [self dismiss];
        } else {
            [NSObject showHudTipStr:msg];
            if (self.commitMsgCodeSuccessHandler) self.commitMsgCodeSuccessHandler(NO);
        }
        //重新加载图片
        [self reloadWebView];
        self.msgCodeTextField.text = @"";
        [self.msgCodeTextField becomeFirstResponder];
        
    } failure:^(NSError *error) {
        TEN_LOG(@"%@", error);
        self.commitMsgCodeSuccessHandler(NO);
    }];
}

- (void)reloadWebView {
    NSString *absoluteUrl = [TENNetwork urlWith:kPictureCodeURL];
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@", absoluteUrl, @"?phone=", self.phone];
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    [self.webview loadRequest:request];
}

- (void)dismiss {
    [self.view removeFromSuperview];
//    [self removeFromParentViewController];
//    [self.view endEditing:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Getter

- (UIView *)alertContainerView {
    if (!_alertContainerView) {
        _alertContainerView = [UIView new];
        _alertContainerView.backgroundColor = TENHexColor(TENWhiteColor);
        _alertContainerView.layer.cornerRadius = 3;
        _alertContainerView.layer.masksToBounds = YES;
    }
    return _alertContainerView;
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = TENFont12;
        _titleLabel.text = @"请输入图片验证码";
        _titleLabel.textColor = TENHexColor(@"131313");
        _titleLabel.textAlignment = NSTextAlignmentCenter;
    }
    return _titleLabel;
}

- (UIView *)topSeperateView {
    if (!_topSeperateView) {
        _topSeperateView = [UIView new];
        _topSeperateView.backgroundColor = TENHexColor(TENSpaceViewColor);
        _topSeperateView.alpha = 0.2;
    }
    return _topSeperateView;
}

- (UIView *)codeContainerView {
    if (!_codeContainerView) {
        _codeContainerView = [UIView new];
    }
    return _codeContainerView;
}

- (TENTextField *)msgCodeTextField {
    if (!_msgCodeTextField) {
        _msgCodeTextField = [TENTextField new];
        _msgCodeTextField.font = TENFont14;
        _msgCodeTextField.borderStyle = UITextBorderStyleRoundedRect;
        _msgCodeTextField.placeholder = @"请输入验证码";
        @weakify(self);
        _msgCodeTextField.textChangedHandler = ^(UITextField *textField, NSString *text) {
            @strongtify(self);
            self.tempCode = text;
        };
    }
    return _msgCodeTextField;
}

- (UIWebView *)webview {
    if (!_webview) {
        _webview = [UIWebView new];
    }
    return _webview;
}

- (UIView *)bottomSeperateView {
    if (!_bottomSeperateView) {
        _bottomSeperateView = [UIView new];
        _bottomSeperateView.backgroundColor = TENHexColor(TENSpaceViewColor);
        _bottomSeperateView.alpha = 0.2;
    }
    return _bottomSeperateView;
}

- (UIButton *)commitButton {
    if (!_commitButton) {
        _commitButton = [UIButton new];
        _commitButton.titleLabel.font = TENFont14;
        [_commitButton setTitle:@"确定" forState:0];
        [_commitButton setTitleColor:TENHexColor(@"3347D1") forState:0];
        [_commitButton addTarget:self action:@selector(commitMsgCode) forControlEvents:UIControlEventTouchUpInside];
    }
    return _commitButton;
}

- (UIButton *)refreshButton {
    if (!_refreshButton) {
        _refreshButton = [UIButton new];
        [_refreshButton addTarget:self action:@selector(reloadWebView) forControlEvents:UIControlEventTouchUpInside];
    }
    return _refreshButton;
}

@end

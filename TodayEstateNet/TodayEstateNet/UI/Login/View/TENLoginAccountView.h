//
//  TENLoginAccountView.h
//  TodayEstateNet
//
//  Created by  HomerLynn on 2018/12/10.
//  Copyright © 2018 TEN. All rights reserved.
//

#import <UIKit/UIKit.h>
@class DJCountdownButton;

@interface TENLoginAccountView : UIView
@property (nonatomic, strong) UITextField *pswTextField;
@property (nonatomic, strong) DJCountdownButton *countDownButton;

@property (nonatomic, copy) void (^textChangedHandler)(UITextField *textField, NSString *text);
@property (nonatomic, copy) void (^countDownButtonTouchupInsideHandler)(void);

@end

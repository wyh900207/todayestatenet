//
//  TENLoginAccountView.m
//  TodayEstateNet
//
//  Created by  HomerLynn on 2018/12/10.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENLoginAccountView.h"
#import "DJCountdownButton.h"

@interface TENLoginAccountView () <UITextFieldDelegate>

@property (nonatomic, strong) UIImageView *phoneIconImageView;
@property (nonatomic, strong) UIImageView *pswIconImageView;
@property (nonatomic, strong) UITextField *phoneTextField;
//@property (nonatomic, strong) UITextField *pswTextField;
@property (nonatomic, strong) UIView *phoneSeprateView;
@property (nonatomic, strong) UIView *pswSeprateView;

@end

@implementation TENLoginAccountView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setupSubviews];
    }
    return self;
}

#pragma mark - UI

- (void)setupSubviews {
    [self addSubview:self.phoneIconImageView];
    [self addSubview:self.pswIconImageView];
    [self addSubview:self.phoneTextField];
    [self addSubview:self.pswTextField];
    [self addSubview:self.phoneSeprateView];
    [self addSubview:self.pswSeprateView];
    [self addSubview:self.countDownButton];
    
    [self.phoneIconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self).multipliedBy(0.5);
        make.left.equalTo(self).offset(11);
        make.width.equalTo(@14);
        make.height.equalTo(@20);
    }];
    [self.pswIconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self).multipliedBy(1.5);
        make.left.equalTo(self).offset(9);
        make.width.equalTo(@18);
        make.height.equalTo(@20);
    }];
    [self.phoneTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.phoneIconImageView);
        make.left.equalTo(self.phoneIconImageView.mas_right).offset(8);
        make.right.equalTo(self).offset(-8);
        make.height.equalTo(@43);
    }];
    [self.pswTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.pswIconImageView);
        make.left.right.height.equalTo(self.phoneTextField);
    }];
    [self.phoneSeprateView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self).offset(-1);
        make.left.equalTo(self).offset(8);
        make.right.equalTo(self).offset(-8);
        make.height.equalTo(@1);
    }];
    [self.pswSeprateView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.height.equalTo(self.phoneSeprateView);
        make.bottom.equalTo(self);
    }];
    [self.countDownButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.equalTo(self.pswTextField);
        make.top.equalTo(self.phoneTextField.mas_bottom);
        make.bottom.equalTo(self.pswTextField);
        make.width.equalTo(@200);
    }];
}

#pragma mark - UITextFieldDelegate

- (void)textField:(UITextField *)textField {
    if (self.textChangedHandler) {
        self.textChangedHandler(textField, textField.text);
    }
}

#pragma mark - Getter

- (UIImageView *)phoneIconImageView {
    if (!_phoneIconImageView) {
        _phoneIconImageView = [UIImageView new];
        _phoneIconImageView.image = [UIImage imageNamed:@"login-phone-icon"];
    }
    return _phoneIconImageView;
}

- (UIImageView *)pswIconImageView {
    if (!_pswIconImageView) {
        _pswIconImageView = [UIImageView new];
        _pswIconImageView.image = [UIImage imageNamed:@"login-psw-icon"];
    }
    return _pswIconImageView;
}

- (UITextField *)phoneTextField {
    if (!_phoneTextField) {
        _phoneTextField = [UITextField new];
        _phoneTextField.tag = 101;
        _phoneTextField.placeholder = @"请输入手机号";
        [_phoneTextField addTarget:self action:@selector(textField:) forControlEvents:UIControlEventEditingChanged];
    }
    return _phoneTextField;
}

- (UITextField *)pswTextField {
    if (!_pswTextField) {
        _pswTextField = [UITextField new];
        _pswTextField.tag = 102;
        _pswTextField.placeholder = @"输入验证码";
        [_pswTextField addTarget:self action:@selector(textField:) forControlEvents:UIControlEventEditingChanged];
    }
    return _pswTextField;
}

- (UIView *)phoneSeprateView {
    if (!_phoneSeprateView) {
        _phoneSeprateView = [UIView new];
        _phoneSeprateView.backgroundColor = TENHexColor(TENTextGrayColor6);
    }
    return _phoneSeprateView;
}

- (UIView *)pswSeprateView {
    if (!_pswSeprateView) {
        _pswSeprateView = [UIView new];
        _pswSeprateView.backgroundColor = TENHexColor(TENTextGrayColor6);
    }
    return _pswSeprateView;
}

- (DJCountdownButton *)countDownButton {
    if (!_countDownButton) {
        _countDownButton = [DJCountdownButton new];
        _countDownButton.titleLabel.font = TENFont15;
        _countDownButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
        [_countDownButton setTitle:@"获取验证码" forState:UIControlStateNormal];
        [_countDownButton setTitleColor:[UIColor blueColor] forState:0];
        [_countDownButton addTarget:self action:@selector(clickedHandler) forControlEvents:UIControlEventTouchUpInside];
        //clickedHandlerh这个block是调用onclickCountDown后才会调
//        @weakify(self);
//        // `count down button` touch up inside ation handler
//        _countDownButton.clickedHandler = ^{
//            @strongtify(self);
//
//        };
    }
    return _countDownButton;
}
-(void)clickedHandler
{
    if (self.countDownButtonTouchupInsideHandler) self.countDownButtonTouchupInsideHandler();
}
@end

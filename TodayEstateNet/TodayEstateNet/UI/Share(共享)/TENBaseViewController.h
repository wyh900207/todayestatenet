//
//  TENBaseViewController.h
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/9/11.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MJRefresh/MJRefresh.h>
#import "NetReachabilityControl.h"

@interface TENBaseViewController : UIViewController
    
@property (weak, nonatomic) IBOutlet  UITableView    *baseTableView;
@property (strong,nonatomic) NetReachabilityControl *netControl;
// 用来解决齐刘海安全区域的问题
@property (nonatomic, strong) UIView *contentView; /**< 自定义内容容器 */

// 初始化`导航栏`样式
- (void)initNavigationBarStyle;

@end

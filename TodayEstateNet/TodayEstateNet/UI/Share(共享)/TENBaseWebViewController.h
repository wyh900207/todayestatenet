//
//  TENBaseWebViewController.h
//  TodayEstateNet
//
//  Created by  HomerLynn on 2019/1/2.
//  Copyright © 2019 TEN. All rights reserved.
//

#import "TENBaseViewController.h"
#import <WebKit/WebKit.h>

@interface TENBaseWebViewController : TENBaseViewController

@property (nonatomic, strong) WKWebView *webview;
@property (nonatomic, copy  ) NSString *uri;
@property (nonatomic, copy  ) NSString *userAgent;

// 快速打开一个`web controller`
+ (void)pushWith:(UINavigationController *)navigationController uri:(NSString *)uri hiddenBottom:(BOOL)hidden;

- (void)load;
- (void)loadWithPath:(NSString *)path;

@end

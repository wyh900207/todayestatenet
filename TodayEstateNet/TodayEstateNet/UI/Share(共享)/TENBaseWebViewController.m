//
//  TENBaseWebViewController.m
//  TodayEstateNet
//
//  Created by  HomerLynn on 2019/1/2.
//  Copyright © 2019 TEN. All rights reserved.
//

#import "TENBaseWebViewController.h"

@interface TENBaseWebViewController () <WKScriptMessageHandler, WKNavigationDelegate>

@property (nonatomic, copy) NSString *systemUserAgent;
@property (nonatomic, strong) WKWebViewConfiguration *configuration;

@end

@implementation TENBaseWebViewController

@synthesize userAgent = _userAgent;

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupSubviews];
    // 获取`title`
    [self.webview addObserver:self forKeyPath:@"title" options:NSKeyValueObservingOptionNew context:NULL];
}

- (void)setupSubviews {
    [self.view addSubview:self.webview];
    
    [self.webview mas_makeConstraints:^(MASConstraintMaker *make) {
        if (@available(iOS 11.0, *)) {
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
        } else {
            make.leading.trailing.equalTo(self.view);
            make.top.equalTo(self.mas_topLayoutGuide);
            make.bottom.equalTo(self.mas_bottomLayoutGuide);
        }
    }];
}

#pragma mark - KVO

-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:@"title"]){
        self.title = self.webview.title;
    }
}

- (void)dealloc {
    [self.webview removeObserver:self forKeyPath:@"title"];
}

#pragma mark - Public function

+ (void)pushWith:(UINavigationController *)navigationController uri:(NSString *)uri hiddenBottom:(BOOL)hidden {
    TENBaseWebViewController *webviewController = [TENBaseWebViewController new];
    webviewController.uri = uri;
    [webviewController load];
    webviewController.hidesBottomBarWhenPushed = hidden;
    [navigationController pushViewController:webviewController animated:YES];
}

- (void)load {
    NSURL *url = [NSURL URLWithString:self.uri];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.webview loadRequest:request];
}

- (void)loadWithPath:(NSString *)path {
    NSURL *url = [NSURL fileURLWithPath:path];//创建URL
    NSURLRequest *request = [NSURLRequest requestWithURL:url];//创建NSURLRequest
    [self.webview loadRequest:request];//加载
}

#pragma mark - Getter

- (WKWebView *)webview {
    if (!_webview) {
        _webview = [[WKWebView alloc] initWithFrame:CGRectZero configuration:self.configuration];
    }
    return _webview;
}

- (NSString *)userAgent {
    if (!_userAgent) {
        _userAgent = @"jrfwIOS";
    }
    return _userAgent;
}

- (WKWebViewConfiguration *)configuration {
    if (!_configuration) {
        _configuration = [WKWebViewConfiguration new];
        _configuration.userContentController = [WKUserContentController new];
        
        [_configuration.userContentController addScriptMessageHandler:self name:@"getUserInfo"];
        [_configuration.userContentController addScriptMessageHandler:self name:@"getDeviceInfo"];
        [_configuration.userContentController addScriptMessageHandler:self name:@"refreshLocation"];
        [_configuration.userContentController addScriptMessageHandler:self name:@"setShare"];
        [_configuration.userContentController addScriptMessageHandler:self name:@"openWebview"];
        [_configuration.userContentController addScriptMessageHandler:self name:@"back"];
        [_configuration.userContentController addScriptMessageHandler:self name:@"pictureBrowser"];
        
        WKPreferences *preferences = [WKPreferences new];
        preferences.javaScriptCanOpenWindowsAutomatically = YES;
        _configuration.preferences = preferences;
    }
    return _configuration;
}

#pragma mark - Setter

- (void)setUri:(NSString *)uri {
    _uri = uri;
}

- (void)setUserAgent:(NSString *)userAgent {
    _userAgent = userAgent;
    // 局部自定义`UserAgent`
    //self.webview.customUserAgent = [NSString stringWithFormat:@"%@;%@", userAgent, @"extra_user_agent"];
}

#pragma mark - WKScriptMessageHandler

- (void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message {
    //    NSArray *array = @[@"getUserInfo",
    //                       @"getDeviceInfo",
    //                       @"refreshLocation",
    //                       @"setShare",
    //                       @"openWebview",
    //                       @"back",
    //                       @"pictureBrowser"];
    //    if ([array containsObject:message.name]) {
    //
    //    }
    if ([message.name isEqualToString:@"getUserInfo"]) {
        [self getUserInfoWith:message.body];
    } else if ([message.name isEqualToString:@"getDeviceInfo"]) {
        [self getDeviceInfoWith:message.body];
    } else if ([message.name isEqualToString:@"refreshLocation"]) {
        [self refreshLocationWith:message.body];
    } else if ([message.name isEqualToString:@"setShare"]) {
        [self shareWith:message.body];
    } else if ([message.name isEqualToString:@"openWebview"]) {
        [self openWebviewWith:message.body];
    } else if ([message.name isEqualToString:@"back"]) {
        [self backWith:message.body];
    } else if ([message.name isEqualToString:@"close"]) {
        [self closeWith:message.body];
    } else if ([message.name isEqualToString:@"pictureBrowser"]) {
        [self pictureBrowserWith:message.body];
    }
}

#pragma mark - Private

// 获取用户信息
- (void)getUserInfoWith:(NSDictionary *)message {
    // 回调函数
    NSString *callback = message[@"callback"];
    // 是否强制登录
    NSNumber *force = message[@"force"];
    
    @weakify(self);
    if (force.boolValue) {
        [[DLUserManager shareManager] actionAfterLogin:^{
            @strongtify(self);
            // 回调参数
            NSDictionary *param = @{@"id": TEN_UD_VALUE(kUserId),
                                    @"phone": TEN_UD_VALUE(kUserPhone),
                                    @"userName": TEN_UD_VALUE(kUserName),
                                    @"avatar": TEN_UD_VALUE(kUserAvatar),
                                    @"loginToken": [[DLUserManager shareManager] loginToken],
                                    @"sharePic": TEN_UD_VALUE(kUserSharePic)
                                    };
            NSError *parseError = nil;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:param options:NSJSONWritingPrettyPrinted error:&parseError];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            NSString *js = [NSString stringWithFormat:@"%@(%@)", callback, jsonString];
            [self run:js];
        } currentController:self];
    } else {
        // 回调参数
        NSDictionary *param = @{@"id": TEN_UD_VALUE(kUserId),
                                @"phone": TEN_UD_VALUE(kUserPhone),
                                @"userName": TEN_UD_VALUE(kUserName),
                                @"avatar": TEN_UD_VALUE(kUserAvatar),
                                @"loginToken": [[DLUserManager shareManager] loginToken],
                                @"sharePic": TEN_UD_VALUE(kUserSharePic)
                                };
        NSError *parseError = nil;
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:param options:NSJSONWritingPrettyPrinted error:&parseError];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        NSString *js = [NSString stringWithFormat:@"%@(%@)", callback, jsonString];
        [self run:js];
    }
}

// 获取设备信息
- (void)getDeviceInfoWith:(NSDictionary *)message {
    // 回调函数
    NSString *callback = message[@"callback"];
    NSDictionary *param = @{@"version": APP_VERSION};
    NSError *parseError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:param options:NSJSONWritingPrettyPrinted error:&parseError];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *js = [NSString stringWithFormat:@"%@(%@)", callback, jsonString];
    [self run:js];
}

// 发起定位请求
- (void)refreshLocationWith:(NSDictionary *)message {
    @weakify(self);
    [[TENLocationManager share] locationJustOneceTimeWith:^(CLLocation *location, BMKLocationReGeocode *rgcData) {
        @strongtify(self);
        [self dealLocationInfo:rgcData location:location message:message];
    } faild:^(NSError *error) {
        TEN_LOG(@"%@", error.description);
    }];
    
}

// 调起分享面板
- (void)shareWith:(NSDictionary *)message {
    NSString *title = message[@"title"];
    NSString *imgUrl = message[@"imgUrl"];
    NSString *link = message[@"link"];
    NSString *desc = message[@"desc"];
    // TODO: - 调起分享
}

// 新建一个webview堆栈 打开H5
- (void)openWebviewWith:(NSDictionary *)message {
    NSString *deurlsc = message[@"url"];
    self.uri = deurlsc;
    [self load];
}

- (void)backWith:(NSDictionary *)message {
    // TODO: - 判断导航栈空间, 回退一次, 这里管他娘的, 直接退出
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)closeWith:(NSDictionary *)message {
    [self.navigationController popViewControllerAnimated:YES];
}

// 查看大图
- (void)pictureBrowserWith:(NSDictionary *)message {
    NSArray *images = message[@"images"];
    NSNumber *index = message[@"index"];
    // TODO: - 图片查看器
}

#pragma mark - Private

- (void)dealLocationInfo:(BMKLocationReGeocode *)rgcData location:(CLLocation *)location message:(NSDictionary *)message  {
    NSDictionary *param = @{@"cityId": @"3010100",
                            @"province": rgcData.province,
                            @"city": rgcData.city,
                            @"district": rgcData.district,
                            @"street": rgcData.street,
                            @"lat": @(location.coordinate.latitude),
                            @"lng": @(location.coordinate.longitude),
                            @"selectCityId": @"3010100",
                            @"selectCity": [TENLocationManager share].displayCityName
                            };
    // 回调函数
    NSString *callback = message[@"callback"];
    NSError *parseError = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:param options:NSJSONWritingPrettyPrinted error:&parseError];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    NSString *js = [NSString stringWithFormat:@"%@(%@)", callback, jsonString];
    [self run:js];
}

#pragma mark - Run JS

- (void)run:(NSString *)js {
    [self.webview evaluateJavaScript:js completionHandler:^(id _Nullable result, NSError * _Nullable error) {
        NSLog(@"%@----%@",result, error);
    }];
}

@end

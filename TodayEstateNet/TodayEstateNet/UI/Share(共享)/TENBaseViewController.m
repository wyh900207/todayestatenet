//
//  TENBaseViewController.m
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/9/11.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENBaseViewController.h"
#import "TENUtilsHeader.h"
#import <MJRefresh/MJRefresh.h>
#import "NetReachabilityControl.h"
@interface TENBaseViewController ()

@end

@implementation TENBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initNavigationBarStyle];
    self.view.backgroundColor = [UIColor colorWithHexString:@"FFFFFF"];
//    if (@available(iOS 11.0, *)) {
//        self.baseTableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
//    } else {
//        self.automaticallyAdjustsScrollViewInsets = NO;
//    }
//    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self.view addSubview:self.contentView];
    [self.contentView mas_makeConstraints:^(MASConstraintMaker *make) {
        // @available(iOS 11.0, *)  @available系统提供的写法, 指定系统版本.
        if (@available(iOS 11.0, *)) {
            // iOS11 安全区域的布局
            make.left.equalTo(self.view.mas_safeAreaLayoutGuideLeft);
            make.right.equalTo(self.view.mas_safeAreaLayoutGuideRight);
            make.top.equalTo(self.view.mas_safeAreaLayoutGuideTop);
            make.bottom.equalTo(self.view.mas_safeAreaLayoutGuideBottom);
        } else {
            // 首尾的约束
            make.leading.trailing.equalTo(self.view);
            make.top.equalTo(self.mas_topLayoutGuide);
            make.bottom.equalTo(self.mas_bottomLayoutGuide);
        }
    }];
    //有tablewview进行初始化
    if (self.baseTableView) {
        self.baseTableView.mj_header = [MJRefreshNormalHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshTableView)];
        
        self.baseTableView.mj_footer = [MJRefreshAutoNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(refreshTableView)];
    }
}

#pragma mark - Init navigation bar style

- (void)initNavigationBarStyle {
    // Custom tint color
    [self.navigationController.navigationBar DJSetBackgroundColor:[UIColor colorWithHexString:TENThemeBlueColor]];
    // Custom title text color
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: [UIColor whiteColor]};
}

#pragma mark  下拉刷新 上拉加载

-(void)refreshTableView{
    TEN_LOG(@"请在子类重写方法:-(void)refreshTableView");
}

-(void)tableViewStopRefreshing
{
    if ([self.baseTableView.mj_header isRefreshing]) {
        [self.baseTableView.mj_header endRefreshing];
    }else if ([self.baseTableView.mj_footer isRefreshing])
        [self.baseTableView.mj_header isRefreshing];
}
//开始刷新
-(void)tableBeginRefreshing
{
    [self.baseTableView.mj_header beginRefreshing];
    self.baseTableView.mj_footer.state = MJRefreshStateIdle;
}
    
#pragma mark - Statu bar style
    
- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}
    
#pragma mark - Getter
- (UIView *)contentView {
    if (!_contentView) {
        _contentView = [UIView new];
        _contentView.backgroundColor = [UIColor clearColor];
    }
    return _contentView;
}
#pragma mark -网络判断
-(BOOL)promptTheNetReachability
{
    __block BOOL isSuccess=YES;
    [self.netControl netWorkState:^(NSInteger netState) {
        switch (netState) {
            case 0:
            case -1:
            {
                isSuccess=NO;
                [self showAlertViewWithTitle:@"提示" message:@"当前网络不可用，请检查网络设置" style:1 cancelButtonTitle:@"取消" otherButtonTitle:@"去设置" firstActionBlock:^(UIAlertAction *action) {
                    
                } secondActionBlock:^(UIAlertAction *action) {
                    
                }];
            }
                break;
            default:
                break;
        }
    }];
    return isSuccess;
}
#pragma mark - 横屏切换

- (void)rotateCurrentScreen {
    if ([[UIDevice currentDevice] respondsToSelector:@selector(setOrientation:)]) {
        SEL selector = NSSelectorFromString(@"setOrientation:");
        
        if (UIDeviceOrientationIsLandscape([[UIDevice currentDevice] orientation])) {
            
            NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[UIDevice instanceMethodSignatureForSelector:selector]];
            [invocation setSelector:selector];
            [invocation setTarget:[UIDevice currentDevice]];
            int val = UIInterfaceOrientationPortrait;//
            [invocation setArgument:&val atIndex:2];
            [invocation invoke];
        }else{
            NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[UIDevice instanceMethodSignatureForSelector:selector]];
            [invocation setSelector:selector];
            [invocation setTarget:[UIDevice currentDevice]];
            int val = UIInterfaceOrientationLandscapeRight;//
            [invocation setArgument:&val atIndex:2];
            [invocation invoke];
        }
    }
}
#pragma mark - 系统的提示框
- (void)showAlertViewWithTitle:(NSString *)title message:(NSString *)message style:(UIAlertControllerStyle)style cancelButtonTitle:(NSString *)cancelButtonTitle otherButtonTitle:(NSString *)otherButtonTitle firstActionBlock:(void (^ __nullable)(UIAlertAction *action))firstHandler secondActionBlock:(void (^ __nullable)(UIAlertAction *action))secondHandler
{
    UIAlertController *alertVC = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:style];
    
    if (!KSTRING_IS_EMPTY(cancelButtonTitle)) {
        UIAlertAction *firstAction = [UIAlertAction actionWithTitle:cancelButtonTitle style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
            if (firstHandler) {
                firstHandler(action);
            }
        }];
        [alertVC addAction:firstAction];
    }
    
    if (!KSTRING_IS_EMPTY(otherButtonTitle)) {
        UIAlertAction *secondAction = [UIAlertAction actionWithTitle:otherButtonTitle style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            if (secondHandler) {
                secondHandler(action);
            }
        }];
        [alertVC addAction:secondAction];
    }
    [self presentViewController:alertVC animated:YES completion:nil];
}
- (void)setRightBtn:(NSString *)title  rightImage:(NSString *)imageName {
    UIBarButtonItem *imageBarItem = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:imageName] style:UIBarButtonItemStylePlain target:self action:@selector(rightBtnClick)];
    UIBarButtonItem *rightBarItem = [[UIBarButtonItem alloc] initWithTitle:title style:UIBarButtonItemStylePlain target:self action:@selector(rightBtnClick)];
    self.navigationItem.rightBarButtonItems = @[imageBarItem,rightBarItem];
}
#pragma mark   按钮处理
- (void)rightBtnClick {
    TEN_LOG(@"请在子类重写方法:-(void)rightBtnClick");
}
@end

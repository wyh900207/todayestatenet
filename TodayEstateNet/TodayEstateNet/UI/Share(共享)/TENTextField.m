//
//  TENTextField.m
//  TodayEstateNet
//
//  Created by  HomerLynn on 2018/12/27.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENTextField.h"

@implementation TENTextField

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self addTarget:self action:@selector(textField:) forControlEvents:UIControlEventEditingChanged];
    }
    return self;
}

// Text changed target action.

- (void)textField:(UITextField *)textField {
    if (self.textChangedHandler) {
        self.textChangedHandler(textField, textField.text);
    }
}

@end

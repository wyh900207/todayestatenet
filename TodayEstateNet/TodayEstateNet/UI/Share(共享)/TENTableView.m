//
//  TENTableView.m
//  TodayEstateNet
//
//  Created by 李冲 on 2018/10/31.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENTableView.h"
#import <MJRefresh/MJRefresh.h>
#import <YYKit/UIImage+YYAdd.h>

@implementation TENTableView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self ten_setup];
    }
    return self;
}
-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
       [self ten_setup];
    }
    return self;
}
-(instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style{
    self = [super initWithFrame:frame style:style];
    if (self) {
        [self ten_setup];
    }
    return self;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    [self ten_setup];
}

#pragma makr - 初始化操作

- (void)ten_setup {
    self.pageSize = 20;
    self.pageCount = 1;
}

#pragma mark - 懒加载

- (NSMutableArray *)dataSourceArray {
    if (!_dataSourceArray) {
        _dataSourceArray = [NSMutableArray array];
    }
    return _dataSourceArray;
}
-(void)setPageSize:(NSUInteger)pageSize{
    if (pageSize == 0) {
        _pageSize = 20;
    }else{
        _pageSize = pageSize;
    }
}
#pragma mark - Refresh

/// 添加MJRefresh
- (void)setRefreshType:(TENTableViewRefreshType)refreshType {
    _refreshType = refreshType;
    
    switch (refreshType) {
        case TENTableViewRefreshTypeHeader: {
            [self addRefreshForHeader];
        }
            break;
        case TENTableViewRefreshTypeFooter: {
            [self addRefreshForFooter];
        }
            break;
        case TENTableViewRefreshTypeBoth: {
            [self addRefreshForHeader];
            [self addRefreshForFooter];
        }
            break;
        case TENTableViewRefreshTypeNone: {
            self.mj_header = nil;
            self.mj_footer = nil;
        }
            break;
    }
}

// 添加下拉刷新
- (void)addRefreshForHeader {
    MJRefreshGifHeader *gifHeader = [MJRefreshGifHeader headerWithRefreshingTarget:self refreshingAction:@selector(refreshForHeader)];
    gifHeader.gifView.image = [UIImage imageWithSmallGIFData:[NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"loading" ofType:@"gif"]] scale:[UIScreen mainScreen].scale];
    gifHeader.lastUpdatedTimeLabel.hidden = YES;
    gifHeader.stateLabel.hidden = YES;
    self.mj_header = gifHeader;
}

- (void)refreshForHeader {
    self.pageCount = 1;
    if ([self.refreshDelegate respondsToSelector:@selector(headerWillRefresh)]) {
        [self.refreshDelegate headerWillRefresh];
    }
    [self.mj_footer setRefreshingTarget:self refreshingAction:@selector(refreshForFooter)];
    [self.mj_footer resetNoMoreData];
}

/// 添加下拉加载
- (void)addRefreshForFooter {
    MJRefreshBackNormalFooter *foot = [MJRefreshBackNormalFooter footerWithRefreshingTarget:self refreshingAction:@selector(refreshForFooter)];
    [foot setTitle:@"亲,已经到底咯!" forState:MJRefreshStateNoMoreData];
    self.mj_footer = foot;
}

- (void)refreshForFooter {
    /*
     static dispatch_once_t onceToken;
     dispatch_once(&onceToken, ^{
     self.pageCount = 1;
     });
     self.pageCount++;
     */
    if ([self.refreshDelegate respondsToSelector:@selector(footerWillRefresh)]) {
        [self.refreshDelegate footerWillRefresh];
    }
}

/// 结束下拉刷新
- (void)endHeaderRefresh {
    [self.mj_header endRefreshing];
    NSLog(@"%lu",(unsigned long)self.pageSize);
    if (self.dataSourceArray.count % self.pageSize != 0) {
        [self.mj_footer endRefreshingWithNoMoreData];
    } else {
        [self.mj_footer resetNoMoreData];
    }
    self.pageCount = 2;
}

/// 结束上拉加载
- (void)endFooterRefresh {
    
    if (self.dataSourceArray.count % self.pageSize != 0) {
        [self.mj_footer endRefreshingWithNoMoreData];
    } else {
        [self.mj_footer resetNoMoreData];
    }
    self.pageCount += 1;
}

/// 以错误结束刷新
- (void)endRefreshWithError {
    [self.mj_header endRefreshing];
    [self.mj_footer endRefreshing];
}

- (void)endHeaderRefreshWith:(NSArray *)array {
    [self.dataSourceArray removeAllObjects];
    [self.dataSourceArray addObjectsFromArray:array];
    [self endHeaderRefresh];
}

- (void)endFooterRefreshWith:(NSArray *)array {
    [self.dataSourceArray addObjectsFromArray:array];
    if (array.count % self.pageSize != 0 || array.count == 0) {
        [self.mj_footer endRefreshingWithNoMoreData];
    } else {
        [self.mj_footer resetNoMoreData];
    }
    
    if (array.count == 0) return;
    self.pageCount += 1;
}

@end

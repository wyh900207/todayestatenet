//
//  TENTextField.h
//  TodayEstateNet
//
//  Created by  HomerLynn on 2018/12/27.
//  Copyright © 2018 TEN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TENTextField : UITextField

@property (nonatomic, strong) void (^textChangedHandler)(UITextField *textField, NSString *text);

@end

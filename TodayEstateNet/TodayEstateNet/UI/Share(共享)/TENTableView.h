//
//  TENTableView.h
//  TodayEstateNet
//
//  Created by 李冲 on 2018/10/31.
//  Copyright © 2018 TEN. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSUInteger, TENTableViewRefreshType) {
    TENTableViewRefreshTypeNone = 0,
    TENTableViewRefreshTypeHeader,
    TENTableViewRefreshTypeFooter,
    TENTableViewRefreshTypeBoth
};

@protocol YMECTableViewRefreshProtocol <NSObject>

@optional
- (void)headerWillRefresh;  /// 下拉刷新
- (void)footerWillRefresh;  /// 上拉加载

@end

@interface TENTableView : UITableView

@property (nonatomic, assign) NSUInteger pageSize;
@property (nonatomic, assign) NSUInteger pageCount;
@property (nonatomic, strong) NSMutableArray *dataSourceArray;

@property (nonatomic, assign) TENTableViewRefreshType refreshType;
@property (nonatomic, weak  ) id<YMECTableViewRefreshProtocol> refreshDelegate;

- (void)endHeaderRefresh;       /// 结束下拉刷新
- (void)endFooterRefresh;       /// 结束上拉加载
- (void)endRefreshWithError;    /// 以错误结束刷新
- (void)endHeaderRefreshWith:(NSArray *)array;
- (void)endFooterRefreshWith:(NSArray *)array;

@end

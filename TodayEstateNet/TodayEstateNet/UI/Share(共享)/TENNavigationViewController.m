//
//  TENNavigationViewController.m
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/9/11.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENNavigationViewController.h"

@interface TENNavigationViewController ()<UINavigationControllerDelegate>

@end

@implementation TENNavigationViewController


- (void)viewDidLoad {
    [super viewDidLoad];
    
//    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
//    [button setImage:[UIImage imageNamed:@"ten_navigation_back"] forState:0];
//
//    UIBarButtonItem *back = [[UIBarButtonItem alloc] initWithCustomView:button];
//
//    self.navigationItem.backBarButtonItem = back;
}
// 统一设置push
- (void)pushViewController:(UIViewController *)viewController animated:(BOOL)animated
{
    // 设置返回按钮, 只有非根控制器ten_navigation_back
    if (self.childViewControllers.count > 0) {// 非根控制器
       viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithImage:[[UIImage imageNamed:@"ten_navigation_back"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain target:self action:@selector(back)];
        viewController.hidesBottomBarWhenPushed = YES;
    }
    [super pushViewController:viewController animated:animated];
}
- (void)back
{
    [self popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

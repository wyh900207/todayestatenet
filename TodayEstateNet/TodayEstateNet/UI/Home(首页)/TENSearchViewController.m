//
//  TENSearchViewController.m
//  TodayEstateNet
//
//  Created by Admin on 2018/9/18.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENSearchViewController.h"
#import "TENUtilsHeader.h"
#import "TENSearchCell.h"
#import "TENHotSearchCell.h"
#import "TENTagView.h"
static NSString *const kTagsTableCellReuseIdentifier = @"TagsTableCell";
@interface TENSearchViewController ()<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate,TENHotSearchCellDelegate>

@property (nonatomic,strong)UISearchBar *searchBar;
@property (nonatomic,strong)UITableView *tableView;
@property (nonatomic, strong)TENHotSearchCell  *searchCell;
@property (nonatomic, strong) NSArray *colors;
@end

@implementation TENSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
       make.edges.equalTo(self.view);
    }];
    self.colors = @[@"#efeff4"];
    [self customNav];
}
#pragma mark 懒加载
-(UISearchBar  *)searchBar{
    if (!_searchBar) {
        _searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0,TENScreenWidth -100, 30)];
        _searchBar.backgroundColor = [UIColor whiteColor];
        _searchBar.delegate = self;
        _searchBar.layer.cornerRadius = 15;
        _searchBar.placeholder = @"搜索";
        UITextField *searchField = [self.searchBar valueForKey:@"searchField"];
        if (searchField) {
            searchField.layer.cornerRadius = 15.0f;
            searchField.layer.masksToBounds = YES;
            // 设置放大镜
            UIImage *image = [UIImage imageNamed: @"tabbar_search"];
            UIImageView *iView = [[UIImageView alloc] initWithImage:image];
            iView.frame = CGRectMake(0, 0, image.size.width , image.size.height);
            searchField.leftView = iView;
        }
    }
    return _searchBar;
}
#pragma mark 自定义导航
-(void)customNav
{
    [self.navigationController.navigationBar DJSetBackgroundColor:[UIColor colorWithHexString:TENThemeBlueColor]];
    //返回
    ImageLeftButton *leftButton = [[ImageLeftButton alloc] initWithRight];
    leftButton.frame = CGRectMake(0, 0, 30, 44);
    [leftButton setImage:nil forState:UIControlStateNormal];
    [leftButton setTitle:@"X" forState:0];
    [leftButton setTitleColor:[UIColor colorWithHexString:TENWhiteColor] forState:0];
    [leftButton addTarget:self action:@selector(backNav:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    
    //搜索
    UIBarButtonItem *buttonItem=[[UIBarButtonItem alloc]initWithTitle:@"搜索" style:UIBarButtonItemStylePlain target:self action:@selector(search:)];
    buttonItem.tintColor = [UIColor whiteColor];
    self.navigationItem.rightBarButtonItem=buttonItem;
    
    UIView  *searchView = [[UIView alloc]initWithFrame:CGRectMake(0, 0,TENScreenWidth - 100, 30)];
    searchView.layer.cornerRadius = 15;
    searchView.clipsToBounds = YES;
    [searchView addSubview:self.searchBar];
    self.navigationItem.titleView = searchView;
}
#pragma mark 返回
-(void)backNav:(UIButton *)button
{
    [self.navigationController popViewControllerAnimated:YES];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = YES;
}
#pragma mark 搜索处理
-(void)search:(UIButton *)button
{
    NSLog(@"------搜索");
}
-(UITableView *)tableView{
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.backgroundColor = TENHexColor(TENWhiteColor);
        _tableView.separatorColor = TENHexClearColor;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        //TENHotSearchCell
        [_tableView registerNib:[UINib nibWithNibName:@"TENSearchCell" bundle:nil] forCellReuseIdentifier:@"searchCell"];
        [_tableView registerNib:[UINib nibWithNibName:NSStringFromClass([TENHotSearchCell class]) bundle:nil] forCellReuseIdentifier:kTagsTableCellReuseIdentifier];
    }
    return _tableView;
}
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 1;
    }
    return 5;
}
#pragma mark UITableViewDataSource
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        TENHotSearchCell  *cell = [tableView dequeueReusableCellWithIdentifier:kTagsTableCellReuseIdentifier forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellEditingStyleNone;
        [self configureCell:cell atIndexPath:indexPath];
        return cell;
    }
    TENSearchCell *searchCell = [tableView dequeueReusableCellWithIdentifier:@"searchCell" forIndexPath:indexPath];
    return searchCell;
}
-(void)configureCell: (TENHotSearchCell *)cell atIndexPath: (NSIndexPath *)indexPath
{
    cell.tagView.preferredMaxLayoutWidth = [UIScreen mainScreen].bounds.size.width;
    cell.tagView.padding = UIEdgeInsetsMake(5, 10, 5, 5);
    cell.tagView.interitemSpacing = 10;
    cell.tagView.lineSpacing = 10;
    cell.tagView.didTapTagAtIndex = ^(NSUInteger index){
        NSLog(@"Tap：index:%lu",(unsigned long)index);
    };
    [cell.tagView removeAllTags];
    NSMutableArray  *data = [NSMutableArray arrayWithArray:@[@"绿地长岛", @"学区房", @"双联别墅", @"地铁周边", @"上海周边",@"2",@"1",@"好世凤翔苑", @"独栋别墅",@"lol",@"吃鸡小分队",@"sssss",@"啦啦啦",@"这是个bug",@"........"]];
    //Add Tags
    [data enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        TENTag *tag = [TENTag tagWithText:obj];
        tag.textColor = [UIColor grayColor];
        tag.fontSize = 12;
        tag.padding = UIEdgeInsetsMake(3, 3, 3, 3);
        tag.bgColor = TENHexColor(self.colors[idx % self.colors.count]);
        tag.cornerRadius = 5;
        tag.enable = YES;
        [cell.tagView addTag:tag];
    }];
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        static TENHotSearchCell *cell = nil;
        static dispatch_once_t onceToken;
        dispatch_once(&onceToken, ^{
            cell = [tableView dequeueReusableCellWithIdentifier: kTagsTableCellReuseIdentifier];
        });
        [self configureCell: cell atIndexPath: indexPath];
        NSLog(@"高度:%f",[cell.contentView systemLayoutSizeFittingSize: UILayoutFittingCompressedSize].height + 1);
        return [cell.contentView systemLayoutSizeFittingSize: UILayoutFittingCompressedSize].height + 1-15;
    }
    return 44;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section==1) {
        return 30;
    }
    return 40;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 1;
}
-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    if (section == 0) {
        UIView  *view1 = [UIView new];
        UILabel  *label1 = [[UILabel alloc]initWithFrame:CGRectMake(8, 10, TENScreenWidth - 5, 30)];
        label1.text = @"最近热搜";
        label1.textColor = [UIColor blackColor];
        label1.textAlignment = 0;
        [view1 addSubview:label1];
        return view1;
    }
    UIView  *view2 = [UIView new];
    UILabel  *label2 = [[UILabel alloc]initWithFrame:CGRectMake(8, 5, TENScreenWidth - 200, 20)];
    label2.text = @"历史记录";
    label2.textColor = [UIColor blackColor];
    label2.textAlignment = 0;
    [view2 addSubview:label2];
    
    UIButton  *btn = [UIButton new];
    btn.frame = CGRectMake(TENScreenWidth - 80, 5, 60, 20);
    [btn setTitle:@"全部删除" forState:0];
    [btn setTitleColor:[UIColor grayColor] forState:0];
    btn.titleLabel.font = [UIFont systemFontOfSize:14];
    [view2 addSubview:btn];
    
    return view2;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
@end

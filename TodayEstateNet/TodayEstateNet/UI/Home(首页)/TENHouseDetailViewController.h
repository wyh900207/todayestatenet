//
//  TENHouseDetailViewController.h
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/11/4.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENBaseViewController.h"

@interface TENHouseDetailViewController : TENBaseViewController

@property (nonatomic, strong) NSNumber *houseId;

@property (nonatomic, strong) NSString *lat;
@property (nonatomic, strong) NSString *lng;
@end

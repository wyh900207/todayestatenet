//
//  TENMapPoiViewController.h
//  HouseEconomics
//
//  Created by apple on 2018/12/29.
//  Copyright © 2018  HomerLynn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TENBaseViewController.h"
NS_ASSUME_NONNULL_BEGIN

@interface TENMapPoiViewController : TENBaseViewController
@property (nonatomic, strong) NSString *lat;
@property (nonatomic, strong) NSString *lng;
@end

NS_ASSUME_NONNULL_END

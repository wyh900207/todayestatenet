//
//  TENOfferHouseViewController.m
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/10/30.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENOfferHouseViewController.h"
#import "TENUtilsHeader.h"
#import "TENOfferHouseDescriptionView.h"
#import "TENOfferHouseDetailView.h"

@interface TENOfferHouseViewController ()

@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) UIImageView *topImageView;        // 顶部图片
@property (nonatomic, strong) UILabel *houseTitleLabel;         // 标题
@property (nonatomic, strong) UILabel *participateLabel;        // 参与的人
@property (nonatomic, strong) UILabel *lastTimeLabel;           // 剩余时间
@property (nonatomic, strong) TENOfferHouseDescriptionView *descriptionView; // 流程与详情
@property (nonatomic, strong) TENOfferHouseDetailView *detailView;           // 楼盘详情
@property (nonatomic, strong) UIButton *moreButton;             // 更多详情按钮

@end

@implementation TENOfferHouseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupSubviews];
}
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = YES;
}
- (void)setupSubviews {
    [self.view addSubview:self.scrollView];
    [self.scrollView addSubview:self.topImageView];
    [self.scrollView addSubview:self.houseTitleLabel];
    [self.scrollView addSubview:self.participateLabel];
    [self.scrollView addSubview:self.lastTimeLabel];
    [self.scrollView addSubview:self.descriptionView];
    [self.scrollView addSubview:self.detailView];
    [self.scrollView addSubview:self.moreButton];
    
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
        make.bottom.equalTo(self.moreButton);
    }];
    [self.topImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.scrollView);
        make.left.right.equalTo(self.view);
        make.height.equalTo(self.topImageView.mas_width).multipliedBy(225.0/375);
    }];
    [self.houseTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.topImageView.mas_bottom).offset(10);
        make.left.equalTo(self.view).offset(8);
        make.right.equalTo(self.view).offset(-8);
    }];
    [self.participateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.houseTitleLabel.mas_bottom).offset(15);
        make.left.right.equalTo(self.houseTitleLabel);
        make.height.equalTo(@20);
    }];
    [self.lastTimeLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.participateLabel.mas_bottom);
        make.left.right.equalTo(self.participateLabel);
        make.height.equalTo(@20);
    }];
    [self.descriptionView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.lastTimeLabel.mas_bottom).offset(10);
        make.left.right.equalTo(self.view);
    }];
    [self.detailView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.descriptionView.mas_bottom).offset(10);
        make.left.right.equalTo(self.view);
    }];
    [self.moreButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.detailView.mas_bottom);
        make.centerX.equalTo(self.detailView);
        make.width.equalTo(@70);
        make.height.equalTo(@37);
    }];
    
    [self.scrollView layoutIfNeeded];
}

#pragma mark - Private methods

- (void)moreButtonHandler {
    TEN_LOG(@"%s", __func__);
}

#pragma mark - Getter

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [UIScrollView new];
    }
    return _scrollView;
}

- (UIImageView *)topImageView {
    if (!_topImageView) {
        _topImageView = [UIImageView new];
        _topImageView.backgroundColor = [UIColor orangeColor];
    }
    return _topImageView;
}

- (UILabel *)houseTitleLabel {
    if (!_houseTitleLabel) {
        _houseTitleLabel = [UILabel new];
        _houseTitleLabel.textAlignment = NSTextAlignmentLeft;
        _houseTitleLabel.lineBreakMode = NSLineBreakByCharWrapping;
        _houseTitleLabel.numberOfLines = 0;
        _houseTitleLabel.font = TENFont20;
        _houseTitleLabel.text = @"海伦堡·观澜 [林海公路精公小户大作建筑面积约60-125M²]";
    }
    return _houseTitleLabel;
}

- (UILabel *)participateLabel {
    if (!_participateLabel) {
        _participateLabel = [UILabel new];
        _participateLabel.font = TENFont14;
        _participateLabel.textColor = TENHexColor(TENTextGrayColor);
        _participateLabel.text = @"已有109289人参与";
    }
    return _participateLabel;
}

- (UILabel *)lastTimeLabel {
    if (!_lastTimeLabel) {
        _lastTimeLabel = [UILabel new];
        _lastTimeLabel.font = TENFont14;
        _lastTimeLabel.textColor = TENHexColor(TENTextGrayColor);
        _lastTimeLabel.text = @"剩余时间：10天 10小时 10分钟";
    }
    return _lastTimeLabel;
}

- (TENOfferHouseDescriptionView *)descriptionView {
    if (!_descriptionView) {
        _descriptionView = [TENOfferHouseDescriptionView new];
    }
    return _descriptionView;
}

- (TENOfferHouseDetailView *)detailView {
    if (!_detailView) {
        _detailView = [TENOfferHouseDetailView new];
    }
    return _detailView;
}

- (UIButton *)moreButton {
    if (!_moreButton) {
        _moreButton = [UIButton new];
        _moreButton.titleLabel.font = TENFont12;
        [_moreButton setTitle:@"更多详情" forState:UIControlStateNormal];
        [_moreButton setTitleColor:TENHexColor(@"D14E33") forState:UIControlStateNormal];
        [_moreButton addTarget: self action:@selector(moreButtonHandler) forControlEvents:UIControlEventTouchUpInside];
    }
    return _moreButton;
}

@end

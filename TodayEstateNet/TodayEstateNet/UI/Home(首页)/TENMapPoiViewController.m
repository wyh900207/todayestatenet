//
//  TENMapPoiViewController.m
//  HouseEconomics
//
//  Created by apple on 2018/12/29.
//  Copyright © 2018  HomerLynn. All rights reserved.
//

#import "TENMapPoiViewController.h"
#import <BaiduMapAPI_Search/BMKSearchComponent.h>
#import <BaiduMapAPI_Location/BMKLocationComponent.h>
#import "TENPoiBottomView.h"
#import "BaiduMapAPI_Utils/BMKUtilsComponent.h"
#import "TENHouseDetailModel.h"
#import "TENNearPoiModel.h"
@interface TENMapPoiViewController ()<BMKMapViewDelegate,BMKPoiSearchDelegate,BottomItemClickedDelegate>

@property (nonatomic, strong) BMKMapView * mapView;
@property (nonatomic, strong) TENPoiBottomView * bottomView;
@property (nonatomic, strong) BMKPoiSearch * poiSearch;
@property (nonatomic, strong) BMKNearbySearchOption * searchOption;
@property (nonatomic, strong) NSMutableArray * mapAnnotationArray;
@end

@implementation TENMapPoiViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupSubviews];
}
- (void)setupSubviews{
    self.title = @"周边详情";
    self.contentView.backgroundColor = [UIColor cyanColor];
    [self.contentView addSubview:self.bottomView];
    [self.contentView addSubview:self.mapView];
    
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self.contentView);
        make.height.mas_equalTo(49);
    }];
    [self.mapView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.contentView);
        make.bottom.equalTo(self.bottomView.mas_top);
    }];
}
- (BMKAnnotationView *)mapView:(BMKMapView *)mapView viewForAnnotation:(id<BMKAnnotation>)annotation{
    if (![annotation isKindOfClass:[TENNearPoiModel class]]) {//楼盘标注点
        BMKPinAnnotationView *newAnnotationView = [[BMKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"myAnnotation"];
        newAnnotationView.annotation = annotation;
        newAnnotationView.image = [UIImage imageNamed:@"paopao.png"];
        [newAnnotationView setSelected:YES animated:YES];
        return newAnnotationView;
    }else{
        BMKPinAnnotationView *poiAnnotationView = [[BMKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"poiAnnotation"];
        if (poiAnnotationView == nil) {
            poiAnnotationView = [[BMKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"poiAnnotation"];
        }
        poiAnnotationView.annotation = annotation;
        poiAnnotationView.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@_annotation",self.searchOption.keyword]];
        poiAnnotationView.canShowCallout = YES;
        return poiAnnotationView;
    }
    return nil;
    
}

#pragma mark-------------BottomItemClickedDelegate
- (void)bottomItemClickedWithKey:(NSString *)key{
    self.searchOption.keyword = key;
    [self.poiSearch poiSearchNearBy:self.searchOption];
}
#pragma mark-------------BMKPoiSearchDelegate
- (void)onGetPoiResult:(BMKPoiSearch *)searcher result:(BMKPoiResult *)poiResult errorCode:(BMKSearchErrorCode)errorCode{//检索回调
    if (errorCode == BMK_OPEN_NO_ERROR) {
        //检索成功，返回poi数组，生成标注
        NSLog(@"检索成功，返回poi数组，生成标注 %@",poiResult);
        [self creatAnnotationWithdata:poiResult.poiInfoList];
    }
}
- (void)creatAnnotationWithdata:(NSArray<BMKPoiInfo *> *)dataArray{
    [self.mapView removeAnnotations:self.mapAnnotationArray];
    if (self.mapAnnotationArray.count > 0) {
        [self.mapAnnotationArray removeAllObjects];
    }
    for (int i = 0; i < dataArray.count; i++) {
        TENNearPoiModel *annotation = [[TENNearPoiModel alloc] init];
        BMKPoiInfo *poiInfo = dataArray[i];
        annotation.title = poiInfo.name;
        annotation.coordinate = poiInfo.pt;
        BMKMapPoint a = BMKMapPointForCoordinate(CLLocationCoordinate2DMake(self.mapView.centerCoordinate.latitude, self.mapView.centerCoordinate.longitude));
        BMKMapPoint b = BMKMapPointForCoordinate(CLLocationCoordinate2DMake(poiInfo.pt.latitude, poiInfo.pt.longitude));
        CLLocationDistance distance = BMKMetersBetweenMapPoints(a,b);
        annotation.subtitle = [NSString stringWithFormat:@"距小区直线距离%.0f米",distance];
        [self.mapAnnotationArray addObject:annotation];
    }
    [self.mapView addAnnotations:self.mapAnnotationArray];
    
}
- (BMKMapView *)mapView{
    if (!_mapView) {
        _mapView = [[BMKMapView alloc] initWithFrame:CGRectZero];
        _mapView.centerCoordinate = CLLocationCoordinate2DMake([self.lat doubleValue],[self.lng doubleValue]);
        _mapView.zoomLevel = 15;
        _mapView.delegate = self;
        BMKPointAnnotation *annotation = [[BMKPointAnnotation alloc] init];
        annotation.coordinate = self.mapView.centerCoordinate;
        [_mapView addAnnotation:annotation];
    }
    return _mapView;
}
- (TENPoiBottomView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[TENPoiBottomView alloc] initWithFrame:CGRectZero];
        _bottomView.backgroundColor = [UIColor whiteColor];
        _bottomView.delegate = self;
    }
    return _bottomView;
}

- (BMKPoiSearch *)poiSearch{
    if (!_poiSearch) {
        _poiSearch = [[BMKPoiSearch alloc] init];
        _poiSearch.delegate = self;
    }
    return _poiSearch;
}
- (BMKNearbySearchOption *)searchOption{
    if (!_searchOption) {
        _searchOption = [[BMKNearbySearchOption alloc] init];
        _searchOption.location = CLLocationCoordinate2DMake([self.lat doubleValue],[self.lng doubleValue]);
        _searchOption.radius = 3000;
    }
    return _searchOption;
}
- (NSMutableArray *)mapAnnotationArray{
    if (!_mapAnnotationArray) {
        _mapAnnotationArray = [NSMutableArray array];
    }
    return _mapAnnotationArray;
}
@end

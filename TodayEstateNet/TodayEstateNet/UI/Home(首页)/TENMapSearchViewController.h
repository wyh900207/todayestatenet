//
//  TENMapSearchViewController.h
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/9/11.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENBaseViewController.h"

@interface TENMapSearchViewController : TENBaseViewController
/*
 *  定位城市id
 */
@property (nonatomic, strong) NSString *locationCityID;

@end

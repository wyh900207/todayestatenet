//
//  TENHomepageActivityCollectionViewCell.m
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/10/27.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENHomepageActivityCollectionViewCell.h"
#import "TENUtilsHeader.h"

@implementation TENHomepageActivityCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

#pragma mark - Setter

- (void)setActivityObject:(NSDictionary *)activityObject {
    _activityObject = activityObject;
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:activityObject[@"picPath"]]];
}

@end

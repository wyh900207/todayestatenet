//
//  TENHomeIconsCell.m
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/9/11.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENHomeIconsCell.h"
#import "TENUtilsHeader.h"
#import "TENHomeIconCollectionViewCell.h"

@interface TENHomeIconsCell () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UICollectionView *iconCellectionView;

@end

@implementation TENHomeIconsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.iconCellectionView.delegate = self;
    self.iconCellectionView.dataSource = self;
    [self.iconCellectionView registerNib:[UINib nibWithNibName:@"TENHomeIconCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"iconCollectionCell"];
}

#pragma mark - UICollectionViewDelegate, UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.iconObjectes.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    TENHomeIconCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"iconCollectionCell" forIndexPath:indexPath];
    // 适配数据icon数据
    cell.iconObject = self.iconObjectes[indexPath.row];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.clickItemHandler) {
        self.clickItemHandler(indexPath, self.iconObjectes[indexPath.row]);
    }
}
    
#pragma mark - Setter
    
- (void)setIconObjectes:(NSArray *)iconObjectes {
    _iconObjectes = iconObjectes;
    [self.iconCellectionView reloadData];
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat width = TENScreenWidth * 0.2 - 1;
    CGFloat height = width;
    
    return CGSizeMake(width, height);
}
    
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}
    
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}

@end

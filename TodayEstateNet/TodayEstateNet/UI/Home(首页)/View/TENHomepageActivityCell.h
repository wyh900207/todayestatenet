//
//  TENHomepageActivityCell.h
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/10/27.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENTableViewCell.h"

@protocol ActivityItemClickDelegate<NSObject>

- (void)activityItemClicked:(NSInteger)index;

@end

@interface TENHomepageActivityCell : TENTableViewCell

@property (nonatomic, strong) NSArray *activityObjects;
@property (weak, nonatomic) IBOutlet UIImageView *iconImage;
@property (nonatomic, assign) id<ActivityItemClickDelegate>delegate;

@end

//
//  TENHomeNormalSallingCell.h
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/9/11.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENTableViewCell.h"
#import "SearchDetail.h"
#import "RecommendModel.h"
@interface TENHomeNormalSallingCell : TENTableViewCell

@property (nonatomic, strong) NSDictionary *dataObject;
@property (nonatomic, strong) SearchDetail *data;
@property (nonatomic, strong) RecommendBuilding *buildingData;

@end

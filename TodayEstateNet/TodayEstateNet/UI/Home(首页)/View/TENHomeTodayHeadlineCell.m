//
//  TENHomeTodayHeadlineCell.m
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/9/16.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENHomeTodayHeadlineCell.h"
#import "TENUtilsHeader.h"
#import "TENAdserScrollview.h"

@interface TENHomeTodayHeadlineCell ()<TENAdserScrollviewDelegate>

@property (nonatomic, strong) NSMutableArray *images;

@end

@implementation TENHomeTodayHeadlineCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupSubviews];
}

- (void)setupSubviews {
//    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:@"https://ss0.baidu.com/7Po3dSag_xI4khGko9WTAnF6hhy/image/h%3D300/sign=0f602b24b7a1cd111ab674208913c8b0/b219ebc4b74543a9759a35d813178a82b9011425.jpg"]];
    
    
    [self.contentView addSubview:self.adsersScrollview];
    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 0, 0);
    [self.adsersScrollview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView).with.insets(padding);
    }];
    
}

- (TENAdserScrollview *)adsersScrollview {
    if (_adsersScrollview==nil) {
        _adsersScrollview = [[TENAdserScrollview alloc]initWithFrame:CGRectZero];
//          NSArray *images = @[@"https://ss1.baidu.com/9vo3dSag_xI4khGko9WTAnF6hhy/image/h%3D300/sign=3892481d2c3fb80e13d167d706d02ffb/4034970a304e251facb1a0d4aa86c9177f3e5353.jpg",@"https://ss0.baidu.com/7Po3dSag_xI4khGko9WTAnF6hhy/image/h%3D300/sign=0f602b24b7a1cd111ab674208913c8b0/b219ebc4b74543a9759a35d813178a82b9011425.jpg",@"https://ss3.baidu.com/9fo3dSag_xI4khGko9WTAnF6hhy/image/h%3D300/sign=4c15db25bb003af352bada60052bc619/b58f8c5494eef01f40ef23e9edfe9925bc317d26.jpg"];
//        _adsersScrollview.signImages = images;
        _adsersScrollview.delegate = self;
    }
    return _adsersScrollview;
}

#pragma mark  TENAdserScrollviewDelegate

- (void)advertScrollView:(TENAdserScrollview *)advertScrollView didSelectedItemAtIndex:(NSInteger)index {
    TEN_LOG(@"点击了第:%ld个",(long)index);
    // TODO: - 点击了 今日头条
}

#pragma mark  假数据  测试用

- (NSMutableArray *)images {
    if (!_images) {
        _images = [NSMutableArray array];
    }
    return _images;
}

#pragma mark - Setter

- (void)setNews:(NSMutableArray *)news {
    _news = news;
    
    // images
    NSMutableArray *images = [NSMutableArray array];
    // titles
    NSMutableArray *titles = [NSMutableArray array];
    // content text
    NSMutableArray *contents = [NSMutableArray array];
    // date & auther
    NSMutableArray *dateAndAuthers = [NSMutableArray array];
    
    for (NSDictionary *object in news) {
        NSString *imageName = object[@"picPath"];
        NSString *title = object[@"headline"];
        NSString *content = object[@"zhaiyao"];
        NSString *date = object[@"createDateFormat"];
        NSString *auther = object[@"resource"];
        NSString *dateAndAuther = [NSString stringWithFormat:@"%@ %@", date, auther];
        [images addObject:imageName];
        [titles addObject:title];
        [contents addObject:content];
        [dateAndAuthers addObject:dateAndAuther];
    }
    
    // Icon iamges
//    self.adsersScrollview.signImages = images;
//    self.adsersScrollview.titles = titles;
//    self.adsersScrollview.contentTextArray = contents;
    
    self.adsersScrollview.datasourceObjects = news;
}

@end

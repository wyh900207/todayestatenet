//
//  TENTag.m
//  TodayEstateNet
//
//  Created by DJAPPLE3-ysy on 2018/12/21.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENTag.h"
static const CGFloat kDefaultFontSize = 13.0;
@implementation TENTag
- (instancetype)init {
    self = [super init];
    if (self) {
        _fontSize = kDefaultFontSize;
        _textColor = [UIColor blackColor];
        _bgColor = [UIColor whiteColor];
        _enable = YES;
    }
    return self;
}
- (instancetype)initWithText: (NSString *)text {
    self = [self init];
    if (self) {
        _text = text;
    }
    return self;
}

+ (instancetype)tagWithText: (NSString *)text {
    return [[self alloc] initWithText: text];
}

@end

//
//  TENTagView.h
//  TodayEstateNet
//
//  Created by DJAPPLE3-ysy on 2018/12/21.
//  Copyright © 2018 TEN. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TENTag.h"
NS_ASSUME_NONNULL_BEGIN

@interface TENTagView : UIView
@property (assign, nonatomic) UIEdgeInsets padding;
@property (assign, nonatomic) CGFloat lineSpacing;
@property (assign, nonatomic) CGFloat interitemSpacing;
@property (assign, nonatomic) CGFloat preferredMaxLayoutWidth;
@property (assign, nonatomic) BOOL singleLine;
@property (copy, nonatomic, nullable) void (^didTapTagAtIndex)(NSUInteger index);
#pragma mark - Public

- (void)addTag: (TENTag *)tag;
- (void)removeAllTags;
@end

NS_ASSUME_NONNULL_END

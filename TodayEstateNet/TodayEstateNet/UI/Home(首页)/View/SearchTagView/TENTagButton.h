//
//  TENTagButton.h
//  TodayEstateNet
//
//  Created by DJAPPLE3-ysy on 2018/12/21.
//  Copyright © 2018 TEN. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@class TENTag;
@interface TENTagButton : UIButton


+ (nonnull instancetype)buttonWithTag: (nonnull TENTag *)tag;

@end

NS_ASSUME_NONNULL_END

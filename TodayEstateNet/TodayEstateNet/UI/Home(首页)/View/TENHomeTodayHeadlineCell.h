//
//  TENHomeTodayHeadlineCell.h
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/9/16.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TENAdserScrollview.h"

@interface TENHomeTodayHeadlineCell : UITableViewCell

@property (nonatomic, weak  ) IBOutlet UIImageView *iconImageView;
// 数据源对象
@property (nonatomic, strong) NSMutableArray *news;
// 跑马灯效果`ScrollView`j
@property (nonatomic, strong) TENAdserScrollview *adsersScrollview;

@end

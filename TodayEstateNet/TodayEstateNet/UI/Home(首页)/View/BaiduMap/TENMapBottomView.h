//
//  TENMapBottomView.h
//  HouseEconomics
//
//  Created by apple on 2018/12/28.
//  Copyright © 2018  HomerLynn. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol TENBottomViewSelectedDelegate <NSObject>

- (void)switchToBuildingDetailVCWithId:(NSString *)houseId;

@end
@interface TENMapBottomView : UIView
@property (nonatomic, assign) id <TENBottomViewSelectedDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITableView *bottomTableView;
+ (instancetype)TENMapBottomView;
- (void)reloadDataWithData:(NSDictionary *)dataDic;
@end

NS_ASSUME_NONNULL_END

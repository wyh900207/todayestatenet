//
//  TENAnnotationView2.h
//  HouseEconomics
//
//  Created by apple on 2018/12/24.
//  Copyright © 2018  HomerLynn. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TENAnnotationView2 : BMKAnnotationView
@property (nonatomic, strong) UIImageView *portraitImageView;

@property (nonatomic, copy) NSString *name;

@property (nonatomic, strong) UIImage *portrait;

@property (nonatomic, assign) BOOL canNotSelect;// == yes 则不允许选中，默认允许选中
@end

NS_ASSUME_NONNULL_END

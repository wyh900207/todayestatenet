//
//  DistrictMapModel.m
//  HouseEconomics
//
//  Created by apple on 2018/12/25.
//  Copyright © 2018  HomerLynn. All rights reserved.
//

#import "DistrictMapModel.h"

@implementation DistrictMapModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"ID": @"id"};
}
@end

//
//  DistrictMapModel.h
//  HouseEconomics
//
//  Created by apple on 2018/12/25.
//  Copyright © 2018  HomerLynn. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DistrictMapModel : BMKPointAnnotation
//{
//    "id": 3,
//    "name": "东鼎",
//    "logo": "http://img.jrfw360.com/manage/1540020195936.jpg",
//    "tags": "团购优惠-景区房-完美装修-合适婚房-立即看房",
//    "price": 45000,
//    "address": "上海市松江区东鼎名人府邸",
//    "totalPrice": "680万起",
//    "buildingType": "商铺-住宅",
//    "saleType": "待售",
//    "roomArea": "100-300㎡",
//    "roomType": "住宅 高层",
//    "newsId": null,
//    "newsTitle": null,
//    "distance": "0",
//    "attachType": null,
//    "provinceName": "上海",
//    "cityName": "上海市",
//    "activityTypeName": null,
//    "activityTypePic": null,
//    "viewType": null,
//    "firstRate": null,
//    "estate": null,
//    "attachment": null,
//    "picNum": null,
//    "houseServiceIds": null,
//    "beginSellDate": null,
//    "beginSellDes": null,
//    "dealTimeInfo": null,
//    "bus": null,
//    "market": null,
//    "metro": null,
//    "other": null,
//    "panorama": null,
//    "restaurant": null,
//    "peripheryPic": null,
//    "lng": "121.21457513588706",
//    "lat": "31.044059796643685",
//    "readNum": null
//},
/**<#注释#>*/
@property (nonatomic, copy  ) NSString *name;
@property (nonatomic, copy  ) NSString *logo;
@property (nonatomic, copy  ) NSString *tags;
@property (nonatomic, copy  ) NSString *address;
@property (nonatomic, copy  ) NSString *totalPrice;
@property (nonatomic, copy  ) NSString *buildingType;
@property (nonatomic, copy  ) NSString *saleType;
@property (nonatomic, copy  ) NSString *roomArea;
@property (nonatomic, copy  ) NSString *roomType;
@property (nonatomic, copy  ) NSString *newsTitle;
@property (nonatomic, copy  ) NSString *distance;
@property (nonatomic, copy  ) NSString *attachType;
@property (nonatomic, copy  ) NSString *provinceName;
@property (nonatomic, copy  ) NSString *cityName;
@property (nonatomic, copy  ) NSString *activityTypeName;
@property (nonatomic, copy  ) NSString *activityTypePic;
@property (nonatomic, copy  ) NSString *viewType;

@property (nonatomic, copy  ) NSString *estate;
@property (nonatomic, copy  ) NSString *attachment;
@property (nonatomic, copy  ) NSString *houseServiceIds;
@property (nonatomic, copy  ) NSString *beginSellDate;
@property (nonatomic, copy  ) NSString *beginSellDes;
@property (nonatomic, copy  ) NSString *dealTimeInfo;
@property (nonatomic, copy  ) NSString *bus;
@property (nonatomic, copy  ) NSString *market;
@property (nonatomic, copy  ) NSString *metro;
@property (nonatomic, copy  ) NSString *other;
@property (nonatomic, copy  ) NSString *panorama;
@property (nonatomic, copy  ) NSString *restaurant;
@property (nonatomic, copy  ) NSString *peripheryPic;
@property (nonatomic, strong) NSNumber *picNum;
@property (nonatomic, strong) NSNumber *newsId;
@property (nonatomic, strong) NSNumber *ID;
@property (nonatomic, strong) NSNumber *price;

@property (nonatomic, strong) NSString * cityId;

@property (nonatomic, strong) NSString * districtId;


@property (nonatomic, strong) NSString * lat;
@property (nonatomic, strong) NSString * lineId;
@property (nonatomic, strong) NSString * lng;
@property (nonatomic, strong) NSString * moduleType;
@property (nonatomic, strong) NSString * moreIds;
@property (nonatomic, strong) NSString * orderBy;
@property (nonatomic, assign) NSInteger pageNo;
@property (nonatomic, assign) NSInteger pageSize;
@property (nonatomic, strong) NSString * stepIds;
@property (nonatomic, strong) NSString * streetIds;

@end

//
//  MapSearchModel.h
//  HouseEconomics
//
//  Created by apple on 2018/12/24.
//  Copyright © 2018  HomerLynn. All rights reserved.
//

#import <Foundation/Foundation.h>
NS_ASSUME_NONNULL_BEGIN

@interface MapSearchModel : BMKPointAnnotation
/**楼盘个数*/
@property (nonatomic, strong) NSString *countNum;
/**区域id*/
@property (nonatomic, strong) NSString * districtId;
/**区域名*/
@property (nonatomic, strong) NSString * districtName;
/**维度*/
@property (nonatomic, strong) NSString * latitude;
/**经度*/
@property (nonatomic, strong) NSString * longitude;
@end

NS_ASSUME_NONNULL_END

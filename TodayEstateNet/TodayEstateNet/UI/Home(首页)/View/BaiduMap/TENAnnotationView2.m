//
//  TENAnnotationView2.m
//  HouseEconomics
//
//  Created by apple on 2018/12/24.
//  Copyright © 2018  HomerLynn. All rights reserved.
//

#import "TENAnnotationView2.h"
@interface TENAnnotationView2()
@property (nonatomic, strong) UILabel *nameLabel;
@end
@implementation TENAnnotationView2
- (void)setName:(NSString *)name
{
    self.nameLabel.text = name;
}

- (void)setPortrait:(UIImage *)portrait
{
    self.portraitImageView.image = portrait;
}
- (void)setSelected:(BOOL)selected
{
    [self setSelected:selected animated:NO];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated{
    if (self.canNotSelect) {
        return;
    }
    if (selected) {
        self.portraitImageView.image = [UIImage imageNamed:@"loc_on"];
    }else{
        self.portraitImageView.image = [UIImage imageNamed:@"loc_off"];
    }
}
- (id)initWithAnnotation:(id<BMKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self) {
        self.bounds = CGRectMake(0, 0, 80, 64);
        self.backgroundColor = [UIColor clearColor];
        self.layer.cornerRadius = 5;
        self.layer.masksToBounds = YES;
        self.portraitImageView = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 80, 74)];
        self.portraitImageView.image = [UIImage imageNamed:@"loc_off"];
        [self addSubview:self.portraitImageView];
        
        /* Create name label. */
        self.nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 80, 64)];
        self.nameLabel.backgroundColor  = [UIColor clearColor];
        self.nameLabel.textAlignment    = NSTextAlignmentCenter;
        self.nameLabel.numberOfLines    = 2;
        self.nameLabel.textColor        = [UIColor whiteColor];
        self.nameLabel.font             = [UIFont systemFontOfSize:14.f];
        
        [self addSubview:self.nameLabel];
    }
    return self;
}

@end

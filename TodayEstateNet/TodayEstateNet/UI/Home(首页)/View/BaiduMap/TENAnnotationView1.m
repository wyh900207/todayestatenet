//
//  TENAnnotationView1.m
//  HouseEconomics
//
//  Created by apple on 2018/12/24.
//  Copyright © 2018  HomerLynn. All rights reserved.
//

#import "TENAnnotationView1.h"
@interface TENAnnotationView1()
@property (nonatomic, strong) UILabel *nameLabel;
@end
@implementation TENAnnotationView1

- (id)initWithAnnotation:(id<BMKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self) {
        self.bounds = CGRectMake(0.f, 0.f, 90, 90);
        self.layer.masksToBounds = YES;
        self.layer.cornerRadius = 45;
        
        self.backgroundColor = [UIColor colorWithRed:84/255.0 green:167/255.0 blue:70/255.0 alpha:1];
        /* Create name label. */
        self.nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0.f, 0.f, 90, 90)];
        self.nameLabel.backgroundColor  = [UIColor clearColor];
        self.nameLabel.textAlignment    = NSTextAlignmentCenter;
        self.nameLabel.numberOfLines    = 2;
        self.nameLabel.textColor        = [UIColor whiteColor];
        self.nameLabel.font             = [UIFont systemFontOfSize:13.f];
        
        [self addSubview:self.nameLabel];
    }
    return self;
}
- (void)setName:(NSString *)name{
    self.nameLabel.text = name;
}
@end

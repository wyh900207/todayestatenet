//
//  TENMapBottomView.m
//  HouseEconomics
//
//  Created by apple on 2018/12/28.
//  Copyright © 2018  HomerLynn. All rights reserved.
//

#import "TENMapBottomView.h"
#import "TENHomeNormalSallingCell.h"


@interface TENMapBottomView()<UITableViewDelegate,UITableViewDataSource>
/**<#注释#>*/
@property (nonatomic, strong) NSDictionary * dataDic;
@end
@implementation TENMapBottomView
+ (instancetype)TENMapBottomView {
    return [[[NSBundle mainBundle]loadNibNamed:NSStringFromClass(self) owner:nil options:nil] firstObject];
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setup];
    }
    return self;
}
- (void)awakeFromNib
{
    [super awakeFromNib];
    [self setup];
}

- (void)setup {
    _bottomTableView.delegate = self;
    _bottomTableView.dataSource = self;
    [_bottomTableView registerNib:[UINib nibWithNibName:@"TENHomeNormalSallingCell" bundle:nil] forCellReuseIdentifier:@"TENHomeNormalSallingCell"];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TENHomeNormalSallingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TENHomeNormalSallingCell"];
    cell.dataObject = self.dataDic;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (_delegate && [_delegate respondsToSelector:@selector(switchToBuildingDetailVCWithId:)]) {
        [_delegate switchToBuildingDetailVCWithId:self.dataDic[@"id"]];
    }
}
- (void)reloadDataWithData:(NSDictionary *)dataDic{
    self.dataDic = dataDic;
    [self.bottomTableView reloadData];
}
@end

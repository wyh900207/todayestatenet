//
//  TENAnnotationView1.h
//  HouseEconomics
//
//  Created by apple on 2018/12/24.
//  Copyright © 2018  HomerLynn. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TENAnnotationView1 : BMKAnnotationView
/**text*/
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *districtId;

@end

NS_ASSUME_NONNULL_END

//
//  如遇到问题或有更好方案，请通过以下方式进行联系
//      QQ：1357127436
//      Email：kingsic@126.com
//      GitHub：https://github.com/kingsic/SGAdvertScrollView.git
//
//  SGAdvertScrollView.h
//  Version 1.2.5
//
//  Created by kingsic on 17/3/8.
//  Copyright © 2017年 kingsic. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TENAdserScrollview;

typedef enum : NSUInteger {
    /// 一行文字滚动样式
    TENAdserScrollviewStyleNormal,
    /// 二行文字滚动样式
    TENAdserScrollviewStyleMore,
} TENAdserScrollviewStyle;

@protocol TENAdserScrollviewDelegate <NSObject>
/// delegate
- (void)advertScrollView:(TENAdserScrollview *)advertScrollView didSelectedItemAtIndex:(NSInteger)index;

@end

@interface TENAdserScrollview : UIView

/// SGAdvertScrollViewDelegate
@property (nonatomic, weak) id<TENAdserScrollviewDelegate> delegate;
/// 默认 SGAdvertScrollViewStyleNormal 样式
@property (nonatomic, assign) TENAdserScrollviewStyle advertScrollViewStyle;
/// 滚动时间间隔，默认为3s
@property (nonatomic, assign) CFTimeInterval scrollTimeInterval;
/// datasource objects
@property (nonatomic, strong) NSArray *datasourceObjects;

@end

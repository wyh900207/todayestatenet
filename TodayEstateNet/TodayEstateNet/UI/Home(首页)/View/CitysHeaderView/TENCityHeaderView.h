//
//  TENCityHeaderView.h
//  TodayEstateNet
//
//  Created by Admin on 2018/9/19.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TENCityHeaderView : UITableViewHeaderFooterView

@property (nonatomic, strong) UILabel *titleLabel;

@end

//
//  TENHomeSectionHeaderView.h
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/9/15.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TENHomeSectionHeaderView : UIView

@property (nonatomic, strong) UILabel *titlLabel;
@property (nonatomic, strong) UIButton *moreButton;
@property (nonatomic, strong) UILabel *rightLabel;
@end

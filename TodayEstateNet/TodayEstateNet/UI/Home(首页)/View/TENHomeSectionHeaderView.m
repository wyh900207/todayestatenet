//
//  TENHomeSectionHeaderView.m
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/9/15.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENHomeSectionHeaderView.h"
//#import "TENUtilsHeader.h"

@interface TENHomeSectionHeaderView ()

@property (nonatomic, strong) UIImageView *leftBlueLineView;


@end

@implementation TENHomeSectionHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setupSubviews];
    }
    return self;
}

- (void)setupSubviews {
    [self addSubview:self.leftBlueLineView];
    [self addSubview:self.moreButton];
    [self addSubview:self.titlLabel];
    [self addSubview:self.rightLabel];
    
    [self.leftBlueLineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(8);
        make.centerY.equalTo(self);
        make.height.equalTo(@21);
        make.width.equalTo((@4));
    }];
    [self.moreButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.top.bottom.right.equalTo(self);
        make.width.equalTo(@50);
    }];
    [self.titlLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.left.equalTo(self.leftBlueLineView.mas_right).offset(10);
        make.width.mas_equalTo(100);
    }];
    [self.rightLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.right.equalTo(self.moreButton.mas_left);
        make.left.equalTo(self.titlLabel.mas_right);
    }];
    
}

#pragma mark - Getter

- (UIImageView *)leftBlueLineView {
    if (!_leftBlueLineView) {
        _leftBlueLineView = [UIImageView new];
        _leftBlueLineView.image = TENImage(@"home_blue_vertical_line");
    }
    return _leftBlueLineView;
}

- (UILabel *)titlLabel {
    if (!_titlLabel) {
        _titlLabel = [UILabel new];
        _titlLabel.textColor = TENHexColor(TENTextBlackColor3);
        _titlLabel.font = TENFont20;
    }
    return _titlLabel;
}
- (UILabel *)rightLabel{
    if (!_rightLabel) {
        _rightLabel = [UILabel new];
        _rightLabel.textColor = [UIColor colorWithHexString:@"666666"];
        _rightLabel.font = [UIFont systemFontOfSize:15];
        _rightLabel.textAlignment = NSTextAlignmentRight;
    }
    return _rightLabel;
}
- (UIButton *)moreButton {
    if (!_moreButton) {
        _moreButton = [ImageLeftButton new];
        _moreButton.titleLabel.font = TENFont15;
        [_moreButton setTitle:@"更多" forState:UIControlStateNormal];
        [_moreButton setTitleColor:TENHexColor(TENTextGrayColor6) forState:UIControlStateNormal];
    }
    return _moreButton;
}

@end

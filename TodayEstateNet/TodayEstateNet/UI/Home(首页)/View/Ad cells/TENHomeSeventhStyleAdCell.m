//
//  TENHomeSeventhStyleAdCell.m
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/9/11.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENHomeSeventhStyleAdCell.h"
#import "TENUtilsHeader.h"

@implementation TENHomeSeventhStyleAdCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setAdvertisementImageObjects:(NSArray *)advertisementImageObjects {
    _advertisementImageObjects = advertisementImageObjects;
    [self.button1st sd_setImageWithURL:[NSURL URLWithString:advertisementImageObjects[0][@"picPath"]] forState:UIControlStateNormal];
    [self.button2nd sd_setImageWithURL:[NSURL URLWithString:advertisementImageObjects[1][@"picPath"]] forState:UIControlStateNormal];
    [self.button3rd sd_setImageWithURL:[NSURL URLWithString:advertisementImageObjects[2][@"picPath"]] forState:UIControlStateNormal];
    [self.button4th sd_setImageWithURL:[NSURL URLWithString:advertisementImageObjects[3][@"picPath"]] forState:UIControlStateNormal];
}

@end

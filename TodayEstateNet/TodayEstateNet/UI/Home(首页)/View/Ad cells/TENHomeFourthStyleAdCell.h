//
//  TENHomeFourthStyleAdCell.h
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/9/11.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENTableViewCell.h"

@interface TENHomeFourthStyleAdCell : TENTableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView1st;
@property (weak, nonatomic) IBOutlet UIImageView *imageView2rd;
@property (weak, nonatomic) IBOutlet UIImageView *imageView3rd;
// Data
@property (nonatomic, strong) NSArray *advertisementImageObjects;

@end

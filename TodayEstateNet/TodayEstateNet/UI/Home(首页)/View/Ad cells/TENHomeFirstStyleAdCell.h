//
//  TENHomeFirstStyleAdCell.h
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/9/11.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENTableViewCell.h"

@interface TENHomeFirstStyleAdCell : TENTableViewCell

// UI
@property (weak, nonatomic) IBOutlet UIImageView *imageView1st;
// 1st 图片
@property (nonatomic, strong) NSArray *advertisementImageObjects;

@end

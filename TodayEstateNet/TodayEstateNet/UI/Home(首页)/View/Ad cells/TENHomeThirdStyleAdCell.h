//
//  TENHomeThirdStyleAdCell.h
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/9/11.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENTableViewCell.h"

@interface TENHomeThirdStyleAdCell : TENTableViewCell
    
@property (nonatomic, strong) NSArray *advertisementImageObjects;

@end

//
//  TENHomeSecondStyleAdCell.m
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/9/11.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENHomeSecondStyleAdCell.h"
#import "TENUtilsHeader.h"

@implementation TENHomeSecondStyleAdCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setAdvertisementImageObjects:(NSArray *)advertisementImageObjects {
    _advertisementImageObjects = advertisementImageObjects;
    [self.imageView1st sd_setImageWithURL:[NSURL URLWithString:advertisementImageObjects[0][@"picPath"]]];
    [self.imageView2rd sd_setImageWithURL:[NSURL URLWithString:advertisementImageObjects[1][@"picPath"]]];
}

@end

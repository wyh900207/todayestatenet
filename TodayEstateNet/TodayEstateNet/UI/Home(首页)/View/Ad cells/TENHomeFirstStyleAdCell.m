//
//  TENHomeFirstStyleAdCell.m
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/9/11.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENHomeFirstStyleAdCell.h"
#import "TENUtilsHeader.h"

@implementation TENHomeFirstStyleAdCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setAdvertisementImageObjects:(NSArray *)advertisementImageObjects {
    _advertisementImageObjects = advertisementImageObjects;
    [self.imageView1st sd_setImageWithURL:[NSURL URLWithString:advertisementImageObjects.firstObject[@"picPath"]]];
}

@end

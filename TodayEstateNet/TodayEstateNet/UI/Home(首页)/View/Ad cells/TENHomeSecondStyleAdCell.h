//
//  TENHomeSecondStyleAdCell.h
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/9/11.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENTableViewCell.h"

@interface TENHomeSecondStyleAdCell : TENTableViewCell

// UI
@property (weak, nonatomic) IBOutlet UIImageView *imageView1st;
@property (weak, nonatomic) IBOutlet UIImageView *imageView2rd;
// Data
@property (nonatomic, strong) NSArray *advertisementImageObjects;

@end

//
//  TENHomeFourthStyleAdCell.m
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/9/11.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENHomeFourthStyleAdCell.h"
#import "TENUtilsHeader.h"

@implementation TENHomeFourthStyleAdCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setAdvertisementImageObjects:(NSArray *)advertisementImageObjects {
    _advertisementImageObjects = advertisementImageObjects;
    [self.imageView1st sd_setImageWithURL:[NSURL URLWithString:advertisementImageObjects[0][@"imageUrl"]]];
    [self.imageView2rd sd_setImageWithURL:[NSURL URLWithString:advertisementImageObjects[1][@"imageUrl"]]];
    [self.imageView3rd sd_setImageWithURL:[NSURL URLWithString:advertisementImageObjects[2][@"imageUrl"]]];
}

@end

//
//  TENHomeSeventhStyleAdCell.h
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/9/11.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENTableViewCell.h"

@interface TENHomeSeventhStyleAdCell : TENTableViewCell

@property (weak, nonatomic) IBOutlet UIButton *button1st;
@property (weak, nonatomic) IBOutlet UIButton *button2nd;
@property (weak, nonatomic) IBOutlet UIButton *button3rd;
@property (weak, nonatomic) IBOutlet UIButton *button4th;
// Data
@property (nonatomic, strong) NSArray *advertisementImageObjects;

@end

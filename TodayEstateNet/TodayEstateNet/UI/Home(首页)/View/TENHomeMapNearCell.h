//
//  TENHomeMapNearCell.h
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/9/16.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol NearBuildingBtnClickDelegate <NSObject>
- (void)nearBuildingSearchBtnClicked;
@end
@interface TENHomeMapNearCell : UITableViewCell
/**<#注释#>*/
@property (nonatomic, strong) NSMutableDictionary * nearBuildingDic;
@property (nonatomic, assign) id<NearBuildingBtnClickDelegate> delegate;
@end

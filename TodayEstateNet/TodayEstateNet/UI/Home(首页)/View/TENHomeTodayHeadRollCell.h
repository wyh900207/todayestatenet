//
//  TENHomeTodayHeadRollCell.h
//  TodayEstateNet
//
//  Created by DJAPPLE3-ysy on 2018/12/18.
//  Copyright © 2018 TEN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TENHomeTodayHeadRollCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *leftImage;
@property (weak, nonatomic) IBOutlet UILabel *titleText;
@property (weak, nonatomic) IBOutlet UILabel *conText;
@property (weak, nonatomic) IBOutlet UILabel *bottomText;

@end



//
//  TENHomeMapNearCell.m
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/9/16.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENHomeMapNearCell.h"
#import "HLTagsView.h"
@interface TENHomeMapNearCell()
@property (weak, nonatomic) IBOutlet UILabel *buildingNameLabel;
@property (weak, nonatomic) IBOutlet HLTagsView *tagsView;
@property (weak, nonatomic) IBOutlet UILabel *buildingTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet HLTagsView *discountTagView;
@property (weak, nonatomic) IBOutlet UILabel *distanceLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalPriceLabel;
@property (weak, nonatomic) IBOutlet UIButton *searchHouseBtn;

@end
@implementation TENHomeMapNearCell

- (void)setNearBuildingDic:(NSMutableDictionary *)nearBuildingDic{
    _nearBuildingDic = nearBuildingDic;
    self.buildingNameLabel.text = nearBuildingDic[@"name"];
    NSString *tempStr = nearBuildingDic[@"tags"];
    self.discountTagView.tags = [tempStr componentsSeparatedByString:@"-"];
    NSString *tempStr1 = nearBuildingDic[@"roomType"];
    self.tagsView.tags = [tempStr1 componentsSeparatedByString:@" "];
    self.priceLabel.text = [NSString stringWithFormat:@"%.1f万元/㎡",[nearBuildingDic[@"price"] floatValue]/10000];
    self.totalPriceLabel.text = nearBuildingDic[@"totalPrice"];
    self.distanceLabel.text = [NSString stringWithFormat:@"距您 %@",nearBuildingDic[@"distance"]];
    self.buildingTypeLabel.text = nearBuildingDic[@"roomArea"];
    
    
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}
//地图找房按钮点击
- (IBAction)searchHouseBtnClicked:(id)sender {
    [_delegate nearBuildingSearchBtnClicked];
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

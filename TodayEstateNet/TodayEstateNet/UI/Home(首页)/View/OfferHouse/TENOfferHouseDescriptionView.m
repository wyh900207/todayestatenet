//
//  TENOfferHouseDescriptionView.m
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/10/31.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENOfferHouseDescriptionView.h"
#import "TENUtilsHeader.h"

@interface TENOfferHouseDescriptionView ()

@property (nonatomic, strong) UIView *spaceView1st;             // 第一条分割线
@property (nonatomic, strong) UILabel *descriptionTitleLabel;   // 流程与详情Title
@property (nonatomic, strong) UIView *spaceView2nd;             // 第二条分割线
@property (nonatomic, strong) UILabel *activityDetailTitleLabel;// 活动详情Title
@property (nonatomic, strong) UILabel *activityDetailLabel;     // 活动详情
@property (nonatomic, strong) UILabel *activityAreaTitleLabel;  // 适用范围Title
@property (nonatomic, strong) UILabel *activityAreaLabel;       // 适用范围
@property (nonatomic, strong) UILabel *useOpinionTitelLabel;    // 使用方法Title
@property (nonatomic, strong) UILabel *useOpinionLabel;         // 使用方法
@property (nonatomic, strong) UILabel *promiseTitleLabel;       // 服务承诺Title
@property (nonatomic, strong) UILabel *promiseLabel;            // 服务承诺

@end

@implementation TENOfferHouseDescriptionView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setupSubviews];
    }
    return self;
}

#pragma mark - UI

- (void)setupSubviews {
    [self addSubview:self.spaceView1st];
    [self addSubview:self.descriptionTitleLabel];
    [self addSubview:self.spaceView2nd];
    [self addSubview:self.activityDetailTitleLabel];
    [self addSubview:self.activityDetailLabel];
    [self addSubview:self.activityAreaTitleLabel];
    [self addSubview:self.activityAreaLabel];
    [self addSubview:self.useOpinionTitelLabel];
    [self addSubview:self.useOpinionLabel];
    [self addSubview:self.promiseTitleLabel];
    [self addSubview:self.promiseLabel];
    
    [self.spaceView1st mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self);
        make.height.equalTo(@0.5);
    }];
    [self.descriptionTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.spaceView1st.mas_bottom).offset(23);
        make.left.equalTo(self).offset(8);
        make.right.equalTo(self).offset(-8);
        make.height.equalTo(@21);
    }];
    [self.spaceView2nd mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.descriptionTitleLabel.mas_bottom).offset(5);
        make.left.equalTo(self.descriptionTitleLabel);
        make.right.equalTo(self);
        make.height.equalTo(@0.5);
    }];
    [self.activityDetailTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.spaceView2nd.mas_bottom).offset(10);
        make.left.equalTo(self.spaceView2nd);
        make.width.equalTo(@70);
        make.height.equalTo(@20);
    }];
    [self.activityDetailLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.activityDetailTitleLabel);
        make.left.equalTo(self.activityDetailTitleLabel.mas_right).offset(8);
        make.right.equalTo(self).offset(-8);
    }];
    [self.activityAreaTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.activityDetailLabel.mas_bottom).offset(4);
        make.left.right.equalTo(self.activityDetailTitleLabel);
    }];
    [self.activityAreaLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.activityAreaTitleLabel);
        make.left.right.equalTo(self.activityDetailLabel);
    }];
    [self.useOpinionTitelLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.activityAreaLabel.mas_bottom).offset(4);
        make.left.right.equalTo(self.activityAreaTitleLabel);
    }];
    [self.useOpinionLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.useOpinionTitelLabel);
        make.left.right.equalTo(self.activityAreaLabel);
    }];
    [self.promiseTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.useOpinionLabel.mas_bottom).offset(4);
        make.left.right.equalTo(self.useOpinionTitelLabel);
    }];
    [self.promiseLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.promiseTitleLabel);
        make.left.right.equalTo(self.useOpinionLabel);
        make.bottom.equalTo(self);
    }];
}

#pragma mark - Getter

- (UIView *)spaceView1st {
    if (!_spaceView1st) {
        _spaceView1st = [UIView new];
        _spaceView1st.backgroundColor = TENHexColor(TENSpaceViewColor);
    }
    return _spaceView1st;
}

- (UILabel *)descriptionTitleLabel {
    if (!_descriptionTitleLabel) {
        _descriptionTitleLabel = [UILabel new];
        _descriptionTitleLabel.font = TENFont15;
        _descriptionTitleLabel.textColor = TENHexColor(TENTextBlackColor3);
        _descriptionTitleLabel.text = @"流程与详情";
    }
    return _descriptionTitleLabel;
}

- (UIView *)spaceView2nd {
    if (!_spaceView2nd) {
        _spaceView2nd = [UIView new];
        _spaceView2nd.backgroundColor = TENHexColor(TENSpaceViewColor);
    }
    return _spaceView2nd;
}

- (UILabel *)activityDetailTitleLabel {
    if (!_activityDetailTitleLabel) {
        _activityDetailTitleLabel = [UILabel new];
        _activityDetailTitleLabel.text =  @"活动详情:";
        _activityDetailTitleLabel.font = TENFont14;
        _activityDetailTitleLabel.textColor = TENHexColor(TENTextGrayColor);
    }
    return _activityDetailTitleLabel;
}

- (UILabel *)activityDetailLabel {
    if (!_activityDetailLabel) {
        _activityDetailLabel = [UILabel new];
        _activityDetailLabel.textColor = TENHexColor(TENTextBlackColor3);
        _activityDetailLabel.font = TENFont14;
        _activityDetailLabel.numberOfLines = 0;
        _activityDetailLabel.lineBreakMode = NSLineBreakByCharWrapping;
        _activityDetailLabel.text = @"浦江南 建面60-125㎡精工小户大作";
    }
    return _activityDetailLabel;
}

- (UILabel *)activityAreaTitleLabel {
    if (!_activityAreaTitleLabel) {
        _activityAreaTitleLabel = [UILabel new];
        _activityAreaTitleLabel.text =  @"适用范围:";
        _activityAreaTitleLabel.font = TENFont14;
        _activityAreaTitleLabel.textColor = TENHexColor(TENTextGrayColor);
    }
    return _activityAreaTitleLabel;
}

- (UILabel *)activityAreaLabel {
    if (!_activityAreaLabel) {
        _activityAreaLabel = [UILabel new];
        _activityAreaLabel.textColor = TENHexColor(TENTextBlackColor3);
        _activityAreaLabel.font = TENFont14;
        _activityAreaLabel.numberOfLines = 0;
        _activityAreaLabel.lineBreakMode = NSLineBreakByCharWrapping;
        _activityAreaLabel.text = @"适用现主推的120㎡、70㎡、90㎡、70㎡、90㎡户型";
    }
    return _activityAreaLabel;
}

- (UILabel *)useOpinionTitelLabel {
    if (!_useOpinionTitelLabel) {
        _useOpinionTitelLabel = [UILabel new];
        _useOpinionTitelLabel.text =  @"使用方法:";
        _useOpinionTitelLabel.font = TENFont14;
        _useOpinionTitelLabel.textColor = TENHexColor(TENTextGrayColor);
    }
    return _useOpinionTitelLabel;
}

- (UILabel *)useOpinionLabel {
    if (!_useOpinionLabel) {
        _useOpinionLabel = [UILabel new];
        _useOpinionLabel.textColor = TENHexColor(TENTextBlackColor3);
        _useOpinionLabel.font = TENFont14;
        _useOpinionLabel.numberOfLines = 0;
        _useOpinionLabel.lineBreakMode = NSLineBreakByCharWrapping;
        _useOpinionLabel.text = @"团购报名短信有效期至2018.02.10";
    }
    return _useOpinionLabel;
}

- (UILabel *)promiseTitleLabel {
    if (!_promiseTitleLabel) {
        _promiseTitleLabel = [UILabel new];
        _promiseTitleLabel.text =  @"服务承诺:";
        _promiseTitleLabel.font = TENFont14;
        _promiseTitleLabel.textColor = TENHexColor(TENTextGrayColor);
    }
    return _promiseTitleLabel;
}

- (UILabel *)promiseLabel {
    if (!_promiseLabel) {
        _promiseLabel = [UILabel new];
        _promiseLabel.textColor = TENHexColor(TENTextBlackColor3);
        _promiseLabel.font = TENFont14;
        _promiseLabel.numberOfLines = 0;
        _promiseLabel.lineBreakMode = NSLineBreakByCharWrapping;
        _promiseLabel.text = @"参加免费专场看房或自行前往售楼处，签署购参加免费专场看房或自行前往售楼处，签署购";
    }
    return _promiseLabel;
}

@end

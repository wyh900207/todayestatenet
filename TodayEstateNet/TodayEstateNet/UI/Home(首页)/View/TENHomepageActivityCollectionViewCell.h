//
//  TENHomepageActivityCollectionViewCell.h
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/10/27.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENCollectionViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface TENHomepageActivityCollectionViewCell : TENCollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (nonatomic, strong) NSDictionary *activityObject;

@end

NS_ASSUME_NONNULL_END

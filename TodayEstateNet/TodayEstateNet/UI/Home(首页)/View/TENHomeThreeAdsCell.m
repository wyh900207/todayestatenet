//
//  TENHomeThreeAdsCell.m
//  TodayEstateNet
//
//  Created by HomerLynn on 2019/2/20.
//  Copyright © 2019 TEN. All rights reserved.
//

#import "TENHomeThreeAdsCell.h"
#import "HLTagsView.h"

@interface TENHomeThreeAdsCell ()

@property (nonatomic, strong) UILabel *titleLabel;
@property (nonatomic, strong) UILabel *areaLabel;
@property (nonatomic, strong) UILabel *priceLabel;
@property (nonatomic, strong) UILabel *subtitleLabel;
@property (nonatomic, strong) UIButton *firstButton;
@property (nonatomic, strong) UIButton *secondButton;
@property (nonatomic, strong) UIButton *thirdButton;
@property (nonatomic, strong) UILabel *saleStateLabel;
@property (nonatomic, strong) UILabel *locationLabel;
@property (nonatomic, strong) HLTagsView *tagsView;
@property (nonatomic, strong) UIView *speView;

@end

@implementation TENHomeThreeAdsCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setupSubviews];
    }
    return self;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

# pragma mark - Init

- (void)setupSubviews {
    [self.contentView addSubview:self.titleLabel];
    [self.contentView addSubview:self.areaLabel];
    [self.contentView addSubview:self.priceLabel];
    [self.contentView addSubview:self.subtitleLabel];
    [self.contentView addSubview:self.firstButton];
    [self.contentView addSubview:self.secondButton];
    [self.contentView addSubview:self.thirdButton];
    [self.contentView addSubview:self.saleStateLabel];
    [self.contentView addSubview:self.locationLabel];
    [self.contentView addSubview:self.tagsView];
    [self.contentView addSubview:self.speView];
    
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(10);
        make.left.equalTo(self.contentView).offset(8);
        make.width.greaterThanOrEqualTo(@20);
        make.height.equalTo(@20);
    }];
    [self.areaLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.titleLabel);
        make.height.equalTo(@14);
        make.left.equalTo(self.titleLabel.mas_right).offset(5);
        make.width.greaterThanOrEqualTo(@20);
    }];
    [self.priceLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self.titleLabel);
        make.right.equalTo(self.contentView).offset(-8);
        make.height.equalTo(@14);
        make.left.equalTo(self.areaLabel.mas_right).offset(5);
    }];
    [self.subtitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.titleLabel.mas_bottom).offset(3);
        make.left.equalTo(self.titleLabel);
        make.right.equalTo(self.priceLabel);
        make.height.equalTo(@17);
    }];
    [self.firstButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.subtitleLabel.mas_bottom).offset(5);
        make.left.equalTo(self.titleLabel);
        make.height.mas_equalTo(self.firstButton.mas_width).multipliedBy(69 / 115.0);
    }];
    [self.secondButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.firstButton.mas_right).offset(7);
        make.top.height.width.equalTo(self.firstButton);
    }];
    [self.thirdButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.secondButton.mas_right).offset(7);
        make.right.equalTo(self.contentView).offset(-8);
        make.top.height.width.equalTo(self.secondButton);
    }];
    [self.saleStateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.firstButton).offset(2);
        make.left.equalTo(self.firstButton).offset(2);
        make.height.equalTo(@14);
        make.width.equalTo(@30);
    }];
    [self.locationLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.titleLabel);
        make.top.equalTo(self.firstButton.mas_bottom).offset(8);
        make.height.equalTo(@14);
        make.width.greaterThanOrEqualTo(@160);
    }];
    [self.tagsView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self.locationLabel);
        make.right.equalTo(self.priceLabel);
        make.left.equalTo(self.locationLabel.mas_right);
        make.height.equalTo(@20);
    }];
    [self.speView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self.contentView);
        make.height.equalTo(@1);
        make.top.equalTo(self.locationLabel.mas_bottom).offset(10);
    }];
}

# pragma mark - Getter & Setter

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.font = TENFont20;
        _titleLabel.textColor = TENHexColor(TENTextBlackColor3);
        _titleLabel.text = @"嘉善·神盘";
    }
    return _titleLabel;
}

- (UILabel *)areaLabel {
    if (!_areaLabel) {
        _areaLabel = [UILabel new];
        _areaLabel.font = TENFont14;
        _areaLabel.textColor = TENHexColor(TENTextGrayColor);
        _areaLabel.text = @"92-106㎡";
    }
    return _areaLabel;
}

- (UILabel *)priceLabel {
    if (!_priceLabel) {
        _priceLabel = [UILabel new];
        _priceLabel.font = TENFont17;
        _priceLabel.textColor = TENHexColor(@"E4805E");
        _priceLabel.text = @"1.5万元/㎡";
    }
    return _priceLabel;
}

- (UILabel *)subtitleLabel {
    if (!_subtitleLabel) {
        _subtitleLabel = [UILabel new];
        _subtitleLabel.font = TENFont14;
        _subtitleLabel.textColor = TENHexColor(TENTextGrayColor);
        _subtitleLabel.text = @"大盘点，高级楼盘大甩卖，不容错过，大盘点，高级楼盘大甩卖，不容错过，大盘点，高级楼盘大甩卖，不容错过";
    }
    return _subtitleLabel;
}

- (UIButton *)firstButton {
    if (!_firstButton) {
        _firstButton = [UIButton new];
        _firstButton.backgroundColor = [UIColor orangeColor];
    }
    return _firstButton;
}

- (UIButton *)secondButton {
    if (!_secondButton) {
        _secondButton = [UIButton new];
        _secondButton.backgroundColor = [UIColor orangeColor];
    }
    return _secondButton;
}

- (UIButton *)thirdButton {
    if (!_thirdButton) {
        _thirdButton = [UIButton new];
        _thirdButton.backgroundColor = [UIColor orangeColor];
    }
    return _thirdButton;
}

- (UILabel *)saleStateLabel {
    if (!_saleStateLabel) {
        _saleStateLabel = [UILabel new];
        _saleStateLabel.font = TENFont14;
        _saleStateLabel.text = @"代售";
        _saleStateLabel.backgroundColor = TENHexColor(@"E4805E");
        _saleStateLabel.textColor = TENHexColor(TENWhiteColor);
    }
    return _saleStateLabel;
}

- (UILabel *)locationLabel {
    if (!_locationLabel) {
        _locationLabel = [UILabel new];
        _locationLabel.font = TENFont14;
        _locationLabel.textColor = TENHexColor(TENTextGrayColor);
        _locationLabel.text = @"浙江省 嘉善县 | 3000km";
    }
    return _locationLabel;
}

- (HLTagsView *)tagsView {
    if (!_tagsView) {
        _tagsView = [HLTagsView new];
    }
    return _tagsView;
}

- (UIView *)speView {
    if (!_speView) {
        _speView = [UIView new];
        _speView.backgroundColor = TENHexColor(TENSpaceViewColor);
    }
    return _speView;
}

@end

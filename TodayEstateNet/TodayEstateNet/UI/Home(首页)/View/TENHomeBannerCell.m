//
//  TENHomeBannerCell.m
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/9/11.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENHomeBannerCell.h"
#import "TENUtilsHeader.h"
#import <SDCycleScrollView/SDCycleScrollView.h>

@interface TENHomeBannerCell () <SDCycleScrollViewDelegate>

@property (nonatomic, strong) SDCycleScrollView *bannerView;
@property (nonatomic, strong) NSMutableArray *images;

@end

@implementation TENHomeBannerCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self.contentView addSubview:self.bannerView];
    [self.bannerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
        make.width.equalTo(self.bannerView.mas_height).multipliedBy(2.5);
    }];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - SDCycleScrollViewDelegate

- (void)cycleScrollView:(SDCycleScrollView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index {
    
}

- (void)setAdsObjects:(NSArray *)adsObjects {
    _adsObjects = adsObjects;
    // 配置Banner图片数组
    [self.images removeAllObjects];
    for (int i = 0; i < adsObjects.count; i++) {
        NSDictionary *object = adsObjects[i];
        [self.images addObject:object[@"picPath"]];
    }
    self.bannerView.imageURLStringsGroup = self.images;
}

#pragma mark - Getter

- (UIView *)bannerView {
    if (!_bannerView) {
        _bannerView = [SDCycleScrollView cycleScrollViewWithFrame:CGRectZero delegate:self placeholderImage:TENImage(@"hello")];
        _bannerView.currentPageDotColor = TENHexColor(TENThemeBlueColor);
        _bannerView.pageDotColor = TENHexColor(TENWhiteColor);
        NSArray *images = @[@"https://ss3.baidu.com/9fo3dSag_xI4khGko9WTAnF6hhy/image/h%3D300/sign=4c15db25bb003af352bada60052bc619/b58f8c5494eef01f40ef23e9edfe9925bc317d26.jpg", @"https://ss1.baidu.com/9vo3dSag_xI4khGko9WTAnF6hhy/image/h%3D300/sign=3892481d2c3fb80e13d167d706d02ffb/4034970a304e251facb1a0d4aa86c9177f3e5353.jpg"];
        _bannerView.imageURLStringsGroup = images;
    }
    return _bannerView;
}

- (NSMutableArray *)images {
    if (!_images) {
        _images = [NSMutableArray array];
    }
    return _images;
}

@end

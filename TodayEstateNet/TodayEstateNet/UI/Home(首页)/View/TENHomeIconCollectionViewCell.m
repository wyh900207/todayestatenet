//
//  TENHomeIconCollectionViewCell.m
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/9/13.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENHomeIconCollectionViewCell.h"
#import "TENUtilsHeader.h"

@implementation TENHomeIconCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setIconObject:(NSDictionary *)iconObject {
    _iconObject = iconObject;
    
    // 图片地址
    NSURL *iconURL = [NSURL URLWithString:iconObject[@"iconUrl"]];
    // 标题
    NSString *title = iconObject[@"iconName"];
    
    [self.iconImageView sd_setImageWithURL:iconURL];
    self.iconTitleLabel.text = title;
}
    
- (void)setItemObject:(NSDictionary *)itemObject {
    _itemObject = itemObject;
    // icon
    self.iconImageView.image = TENImage(itemObject[@"iconUrl"]);
    // 标题
    self.iconTitleLabel.text = itemObject[@"iconName"];
}

@end

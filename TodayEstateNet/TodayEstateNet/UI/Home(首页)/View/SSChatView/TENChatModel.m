//
//  TENChatModel.m
//  HouseEconomics
//
//  Created by apple on 2019/1/15.
//  Copyright © 2019  HomerLynn. All rights reserved.
//

#import "TENChatModel.h"

@implementation TENChatModel
+ (NSDictionary *)mj_replacedKeyFromPropertyName{
    return @{
             @"chatId" : @"id",
             @"descriptionStr" : @"description"
             };
}
@end

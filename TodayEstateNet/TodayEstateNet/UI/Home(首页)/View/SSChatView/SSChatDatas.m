//
//  SSChatDatas.m
//  SSChatView
//
//  Created by soldoros on 2018/9/25.
//  Copyright © 2018年 soldoros. All rights reserved.
//


#import "SSChatDatas.h"
#import "TENMessageListRequest.h"
#import "YMECAliyunOSS.h"
#import "OSSConstants.h"
#define headerImg1  @""
#define headerImg2  @""
#define headerImg3  @""

@implementation SSChatDatas

//获取单聊的初始会话 数据均该由服务器处理生成 这里demo写死
+(NSMutableArray *)LoadingMessagesStartWithChat:(NSString *)sessionId GroupId:(NSString *)groupId PageNo:(NSInteger)pageNo PageSize:(NSInteger)pageSize{
    
    return nil;
    
}



//获取群聊的初始会话
+(NSMutableArray *)LoadingMessagesStartWithGroupChat:(NSString *)sessionId{
    
    return nil;
}


//处理接收的消息数组
+(NSMutableArray *)receiveMessages:(NSArray *)messages{
    
    NSMutableArray *array = [NSMutableArray new];
    for(NSDictionary *dic in messages){
        SSChatMessagelLayout *layout = [SSChatDatas getMessageWithDic:dic];
        [array addObject:layout];
    }
    return array;
}

//接受一条消息
+(SSChatMessagelLayout *)receiveMessage:(NSDictionary *)dic{
    return [SSChatDatas getMessageWithDic:dic];
}

//消息内容生成消息模型
+(SSChatMessagelLayout *)getMessageWithDic:(NSDictionary *)dic{
    
    SSChatMessage *message = [SSChatMessage new];
   
    SSChatMessageType messageType = (SSChatMessageType)[dic[@"targetType"] integerValue];
    SSChatMessageFrom messageFrom = (SSChatMessageFrom)[dic[@"accountId"] integerValue];
    
    if(messageFrom == [kAccountId integerValue]){
        message.messageFrom = SSChatMessageFromMe;
        message.backImgString = @"icon_qipao1";
    }else{
        message.messageFrom = SSChatMessageFromOther;
        message.backImgString = @"icon_qipao2";
    }
    
    
    message.sessionId    = [NSString stringWithFormat:@"%ld",(long)dic[@"groupId"]];
    message.sendError    = NO;
    message.headerImgurl = dic[@"avatar"];
    message.messageId    = dic[@"id"];
    message.textColor    = SSChatTextColor;
    message.messageType  = messageType;
    
    //判断时间是否展示
//    NSString *tempStr = [NSTimer getTimeStrWithTimestamp:[NSString stringWithFormat:@"%ld",(long)dic[@"createDate"]] format:@"YYYY-MM-dd HH:mm:ss"];
//    message.messageTime = [NSTimer getChatTimeStr2:(long)dic[@"createDate"]];
//    NSUserDefaults *user = [NSUserDefaults standardUserDefaults];
//    NSString *tempDate = [user valueForKey:message.sessionId];
//    if(tempDate.length == 0){
//        [user setValue:[dic[@"createDate"] stringValue] forKey:message.sessionId];
//        message.showTime = YES;
//    }else{
//        [message showTimeWithLastShowTime:[user valueForKey:message.sessionId] currentTime:[dic[@"createDate"] stringValue]];
//        if(message.showTime){
//            [user setValue:[dic[@"createDate"] stringValue] forKey:message.sessionId];
//        }
//    }
    
    //判断消息类型
    if(message.messageType == SSChatMessageTypeText){
        
        message.cellString   = SSChatTextCellId;
        message.textString = dic[@"content"];
    }else if (message.messageType == SSChatMessageTypeImage){
        message.cellString   = SSChatImageCellId;
        
        if([dic[@"picPath"] isKindOfClass:NSClassFromString(@"NSString")]){
            message.imageString = dic[@"picPath"];
        }else{
            message.image = dic[@"picPath"];
        }
    }
    else if (message.messageType == SSChatMessageTypeVoice){

        message.cellString   = SSChatVoiceCellId;
        message.voice = dic[@"picPath"];
        if([dic[@"picPath"] isKindOfClass:NSClassFromString(@"NSString")]){
            message.voiceRemotePath = dic[@"picPath"];
        }else{
            message.voice = dic[@"picPath"];
        }
        message.voiceDuration = [dic[@"times"] integerValue];
        message.voiceTime = [NSString stringWithFormat:@"%@″ ",dic[@"times"]];

        message.voiceImg = [UIImage imageNamed:@"chat_animation_white3"];
        message.voiceImgs =
        @[[UIImage imageNamed:@"chat_animation_white1"],
          [UIImage imageNamed:@"chat_animation_white2"],
          [UIImage imageNamed:@"chat_animation_white3"]];

        if(messageFrom == SSChatMessageFromOther){

            message.voiceImg = [UIImage imageNamed:@"chat_animation3"];
            message.voiceImgs =
            @[[UIImage imageNamed:@"chat_animation1"],
              [UIImage imageNamed:@"chat_animation2"],
              [UIImage imageNamed:@"chat_animation3"]];
        }

    }
//    else if (message.messageType == SSChatMessageTypeMap){
//        message.cellString = SSChatMapCellId;
//        message.latitude = [dic[@"lat"] doubleValue];
//        message.longitude = [dic[@"lon"] doubleValue];
//        message.addressString = dic[@"address"];
//
//    }else if (message.messageType == SSChatMessageTypeVideo){
//        message.cellString = SSChatVideoCellId;
//        message.videoLocalPath = dic[@"videoLocalPath"];
//        message.videoImage = [UIImage getImage:message.videoLocalPath];
//    }
//
    SSChatMessagelLayout *layout = [[SSChatMessagelLayout alloc]initWithMessage:message];
    return layout;
    
}




//发送一条消息
+(void)sendMessage:(NSDictionary *)dict messageType:(SSChatMessageType)messageType messageBlock:(MessageBlock)messageBlock{
   
    NSMutableDictionary *messageDic = [NSMutableDictionary dictionaryWithDictionary:dict];
    
    switch (messageType) {
        case SSChatMessageTypeText:{
            [TENMessageListRequest sendTextMsgWithParameters:messageDic Success:^(NSDictionary * _Nonnull response) {
                TEN_LOG(@"%@",response);
            } failure:^(NSError * _Nonnull error) {
                TEN_LOG(@"%@",error);
            } Hud:NO];
            
        }
            break;
        case SSChatMessageTypeImage:{
            NSData *imageData = UIImageJPEGRepresentation(messageDic[@"picPath"], 0.5);
            //用日期给文件命名
            NSString *fileName = [self setupFileName];
            NSLog(@"%@",fileName);
            
            //上传图片
            NSString *objectKey = [NSString stringWithFormat:@"user/chat%@.jpg",fileName];
            [[YMECAliyunOSS sharedInstance] setupEnviroment];
            [[YMECAliyunOSS sharedInstance] uploadObjectAsyncWithData:imageData withObjectKey:objectKey success:^{
                TEN_LOG(@"================上传图片成功============");

                [messageDic setValue:[NSString stringWithFormat:@"%@/%@",imageEndPoint,objectKey] forKey:@"picPath"];
                [TENMessageListRequest sendTextMsgWithParameters:messageDic Success:^(NSDictionary * _Nonnull response) {
                    TEN_LOG(@"%@",response);
                } failure:^(NSError * _Nonnull error) {
                    TEN_LOG(@"%@",error);
                } Hud:NO];
            }];
        }
            break;
        case SSChatMessageTypeVoice:{
            NSString *fileName = [self setupFileName];
            //上传录音
            NSString *objectKey = [NSString stringWithFormat:@"user/chat%@.mp3",fileName];
            [[YMECAliyunOSS sharedInstance] setupEnviroment];
            [[YMECAliyunOSS sharedInstance] uploadObjectAsyncWithData:messageDic[@"picPath"] withObjectKey:objectKey success:^{
                TEN_LOG(@"================上传录音成功============");
                
                [messageDic setValue:[NSString stringWithFormat:@"%@/%@",audioEndPoint,objectKey] forKey:@"picPath"];
                [TENMessageListRequest sendTextMsgWithParameters:messageDic Success:^(NSDictionary * _Nonnull response) {
                    TEN_LOG(@"发送录音成功-=-=--=%@",response);
                } failure:^(NSError * _Nonnull error) {
                    TEN_LOG(@"%@",error);
                } Hud:NO];
            }];
        }
            break;
        case SSChatMessageTypeVideo:{
//            [messageDic setObject:TEN_UD_VALUE(@"yyAccount.id") forKey:@"accountId"];
//            [messageDic setValue:@(messageType) forKey:@"targetType"];
//            [messageDic setValue:groupId forKey:@"groupId"];
        }
            break;
        case SSChatMessageTypeBuilding:{
//            [messageDic setObject:TEN_UD_VALUE(@"yyAccount.id") forKey:@"accountId"];
//            [messageDic setValue:@(messageType) forKey:@"targetType"];
//            [messageDic setValue:groupId forKey:@"groupId"];
        }
            break;
        case SSChatMessageTypeLocation:{
//            [messageDic setObject:TEN_UD_VALUE(@"yyAccount.id") forKey:@"accountId"];
//            [messageDic setValue:@(messageType) forKey:@"targetType"];
//            [messageDic setValue:groupId forKey:@"groupId"];
        }
            break;
        default:
            break;
    }
    
    SSChatMessagelLayout *layout = [SSChatDatas getMessageWithDic:messageDic];
    NSProgress *pre = [[NSProgress alloc]init];
    
    messageBlock(layout,nil,pre);
}

//用日期给文件命名
+ (NSString *)setupFileName{
    NSDateFormatter *formatter = [[NSDateFormatter alloc]init];
    formatter.dateFormat = @"yyyyMMddHHmmss";
    NSString *str = [formatter stringFromDate:[NSDate date]];
    return [NSString stringWithFormat:@"%@",str];
}
@end

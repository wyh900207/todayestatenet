//
//  TENChatModel.h
//  HouseEconomics
//
//  Created by apple on 2019/1/15.
//  Copyright © 2019  HomerLynn. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TENChatModel : NSObject

@property(nonatomic, strong) NSString *accountId;
@property(nonatomic, strong) NSString *address;
@property(nonatomic, strong) NSString *avatar;
@property(nonatomic, strong) NSString *content;
@property(nonatomic, strong) NSString *createDate;
@property(nonatomic, strong) NSString *descriptionStr;
@property(nonatomic, strong) NSString *groupId;
@property(nonatomic, strong) NSString *chatId;
@property(nonatomic, strong) NSString *picPath;
@property(nonatomic, strong) NSString *targetId;
@property(nonatomic, strong) NSString *targetType;
@property(nonatomic, strong) NSString *userName;

@end

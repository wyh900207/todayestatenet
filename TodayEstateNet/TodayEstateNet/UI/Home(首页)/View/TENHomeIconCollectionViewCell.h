//
//  TENHomeIconCollectionViewCell.h
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/9/13.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TENHomeIconCollectionViewCell : UICollectionViewCell

// UI
@property (weak, nonatomic) IBOutlet UILabel *iconTitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
// Data
@property (nonatomic, strong) NSDictionary *iconObject;
@property (nonatomic, strong) NSDictionary *itemObject;

@end

//
//  TENHomeIconsCell.h
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/9/11.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENTableViewCell.h"

@interface TENHomeIconsCell : TENTableViewCell

@property (nonatomic, strong) NSArray *iconObjectes;
// 点击回调
@property (nonatomic, copy  ) void (^clickItemHandler)(NSIndexPath *indexPath, NSDictionary *object);

@end

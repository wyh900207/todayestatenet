//
//  TENAroundInfoCell.m
//  HouseEconomics
//
//  Created by apple on 2018/12/26.
//  Copyright © 2018  HomerLynn. All rights reserved.
//

#import "TENAroundInfoCell.h"

@implementation TENAroundInfoCell
- (void)setBuildingInfoModel:(TENBuildingInfoModel *)buildingInfoModel{
    _buildingInfoModel = buildingInfoModel;
    self.subwayLabel.text = !KSTRING_IS_EMPTY(buildingInfoModel.metro) ? buildingInfoModel.metro : @"无";
    self.busLabel.text = !KSTRING_IS_EMPTY(buildingInfoModel.bus) ? buildingInfoModel.bus : @"无";
    self.eatLabel.text = !KSTRING_IS_EMPTY(buildingInfoModel.estate) ? buildingInfoModel.estate : @"无";
    self.mallLabel.text = !KSTRING_IS_EMPTY(buildingInfoModel.market) ? buildingInfoModel.market : @"无";
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

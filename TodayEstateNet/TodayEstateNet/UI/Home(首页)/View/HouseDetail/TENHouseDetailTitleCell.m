//
//  TENHouseDetailTitleCell.m
//  TodayEstateNet
//
//  Created by  HomerLynn on 2018/11/7.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENHouseDetailTitleCell.h"

@implementation TENHouseDetailTitleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupSubviews];
}

#pragma mark - UI

- (void)setupSubviews {
    // config
    self.sallingStateLabel.layer.cornerRadius = 2;
    self.sallingStateLabel.layer.masksToBounds = YES;
}

@end

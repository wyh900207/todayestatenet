//
//  TENNoticeView.m
//  HouseEconomics
//
//  Created by apple on 2019/1/7.
//  Copyright © 2019  HomerLynn. All rights reserved.
//

#import "TENNoticeView.h"
#import "TENHouseDetailRequest.h"
@interface TENNoticeView()
@property (nonatomic, copy) NSMutableDictionary *bookingDic;
@end
@implementation TENNoticeView
- (void)awakeFromNib{
    [super awakeFromNib];
    [self setupSubviews];
}
- (void)layoutSubviews{
    [super layoutSubviews];
    if (self.bookingDic.count > 0) {
        [self setImageWithDataDic:self.bookingDic];
    }
}
- (void)setDataDic:(NSDictionary *)dataDic{
    _dataDic = dataDic;
    [self.discountBtn setImage:[UIImage imageNamed:[dataDic[@"discount"] integerValue] == 1 ? @"search_selected" : @"search_deselected"] forState:normal];
    [self.openBtn setImage:[UIImage imageNamed:[dataDic[@"sale"] integerValue] == 1 ? @"search_selected" : @"search_deselected"] forState:normal];
    [self.subscribeBtn setImage:[UIImage imageNamed:[dataDic[@"news"] integerValue] == 1 ? @"search_selected" : @"search_deselected"] forState:normal];
    self.discountBtn.selected = [dataDic[@"discount"] integerValue] == 1 ? YES : NO;
    self.openBtn.selected = [dataDic[@"sale"] integerValue] == 1 ? YES : NO;
    self.subscribeBtn.selected = [dataDic[@"news"] integerValue] == 1 ? YES : NO;
    [self.bookingDic setValuesForKeysWithDictionary:dataDic];
}
- (void)setImageWithDataDic:(NSMutableDictionary *)dataDic{
    if (dataDic.count > 0) {
        [self.discountBtn setImage:[UIImage imageNamed:[dataDic[@"discount"] integerValue] == 1 ? @"search_selected" : @"search_deselected"] forState:normal];
        [self.openBtn setImage:[UIImage imageNamed:[dataDic[@"sale"] integerValue] == 1 ? @"search_selected" : @"search_deselected"] forState:normal];
        [self.subscribeBtn setImage:[UIImage imageNamed:[dataDic[@"news"] integerValue] == 1 ? @"search_selected" : @"search_deselected"] forState:normal];
        self.discountBtn.selected = [dataDic[@"discount"] integerValue] == 1 ? YES : NO;
        self.openBtn.selected = [dataDic[@"sale"] integerValue] == 1 ? YES : NO;
        self.subscribeBtn.selected = [dataDic[@"news"] integerValue] == 1 ? YES : NO;
    }
}
- (void)setupSubviews{
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissView)];
    self.bgView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    [self.bgView addGestureRecognizer:tap];
}
- (void)dismissView{
    [self removeFromSuperview];
}
- (IBAction)btnClicked:(UIButton *)sender {
    
    switch (sender.tag) {
        case 11://优惠
        {
            self.discountBtn.selected = !self.discountBtn.selected;
            [self.discountBtn setImage:[UIImage imageNamed:self.discountBtn.selected ? @"search_selected" : @"search_deselected"] forState:normal];
        }
            break;
        case 12://开盘
        {
            self.openBtn.selected = !self.openBtn.selected;
            [self.openBtn setImage:[UIImage imageNamed:self.openBtn.selected ? @"search_selected" : @"search_deselected"] forState:normal];
        }
            break;
        case 13://订阅
        {
            self.subscribeBtn.selected = !self.subscribeBtn.selected;
            [self.subscribeBtn setImage:[UIImage imageNamed:self.subscribeBtn.selected ? @"search_selected" : @"search_deselected"] forState:normal];
        }
            break;
        case 14://取消
        {
            [self removeFromSuperview];
        }
            break;
        case 15://确定
        {
            NSDictionary *parameters = @{
                                         @"accountId"   :   TEN_UD_VALUE(@"yyAccount.id"),
                                         @"discount"    :   @(self.discountBtn.selected),
                                         @"news"        :   @(self.subscribeBtn.selected),
                                         @"sale"        :   @(self.openBtn.selected),
                                         @"buildGroupId":   self.houseId
                                         
                                         
                                         };
            [TENHouseDetailRequest subscribeWithParameters:parameters success:^(NSDictionary *responseObject) {
                TEN_LOG(@"订阅信息---%@",responseObject);
                if ([responseObject[@"code"] integerValue] == 1) {
                    [NSObject showHudTipStr:@"操作成功"];
                }
                //保存修改后的状态
                self.bookingDic[@"discount"] = @(self.discountBtn.selected);
                self.bookingDic[@"sale"] = @(self.openBtn.selected);
                self.bookingDic[@"news"] = @(self.subscribeBtn.selected);
                [[NSNotificationCenter defaultCenter] postNotificationName:TENChangeBtnName object:nil userInfo:@{@"isSubscribe":@([self.bookingDic[@"news"] boolValue])}];
                
                [self removeFromSuperview];
            } failure:^(NSError *error) {
                TEN_LOG(@"%@",error.description);
            }];
        }
            break;
            
            
        default:
            break;
    }
}


- (NSMutableDictionary *)bookingDic{
    if (!_bookingDic) {
        _bookingDic = [NSMutableDictionary dictionary];
    }
    return _bookingDic;
}
@end

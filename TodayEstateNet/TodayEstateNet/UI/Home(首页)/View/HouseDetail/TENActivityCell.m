//
//  TENActivityCell.m
//  HouseEconomics
//
//  Created by apple on 2018/12/26.
//  Copyright © 2018  HomerLynn. All rights reserved.
//

#import "TENActivityCell.h"

@implementation TENActivityCell
- (void)setActivityModel:(TENActivityModel *)activityModel{
    _activityModel = activityModel;
    self.activityNameLabel.text = activityModel.buildingGroupName;
    self.activityTypeLabel.text = [NSString stringWithFormat:@"[%@]",activityModel.type];
    self.activityLabel.text = activityModel.service;
    [self.buildingImgView sd_setImageWithURL:[NSURL URLWithString:activityModel.picPath]];
    self.sloganLabel.text = activityModel.title;
    self.joinLabel.text = [NSString stringWithFormat:@"已有%@人参与",activityModel.joinNum.stringValue];
    self.timeLeftLabel.text = [TimeChangeTools regularTimeFormatterWithTime:activityModel.endDate];
}




- (void)awakeFromNib {
    [super awakeFromNib];
    _activityLabel.layer.cornerRadius = 2;
    _activityLabel.layer.borderWidth = 1;
    _activityLabel.layer.borderColor = [UIColor colorWithHexString:@"E45ECA"].CGColor;
    _activityLabel.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

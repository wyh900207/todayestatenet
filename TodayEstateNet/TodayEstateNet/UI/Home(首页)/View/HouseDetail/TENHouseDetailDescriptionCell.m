//
//  TENHouseDetailDescriptionCell.m
//  TodayEstateNet
//
//  Created by  HomerLynn on 2018/11/7.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENHouseDetailDescriptionCell.h"
#import "TENBaseWebViewController.h"
@implementation TENHouseDetailDescriptionCell
- (void)setBuildingInfoModel:(TENBuildingInfoModel *)buildingInfoModel{
    _buildingInfoModel = buildingInfoModel;
    self.openTimeLabel.text = [TimeChangeTools regularTimeFormatterWithTime:buildingInfoModel.beginSellDate];
    self.deliverTimeLabel.text = buildingInfoModel.dealTimeInfo;
    self.houseTypeLabel.text = buildingInfoModel.roomType;
    self.saleAddressLabel.text = buildingInfoModel.address;
    NSArray *array = [buildingInfoModel.tags componentsSeparatedByString:@"-"];
    self.tagsView.tags = array;
}




- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  TENBuildingDynamicCell.m
//  HouseEconomics
//
//  Created by apple on 2018/12/26.
//  Copyright © 2018  HomerLynn. All rights reserved.
//

#import "TENBuildingDynamicCell.h"

@implementation TENBuildingDynamicCell
- (void)setBuildingNewsModel:(TENBuildingNewsModel *)buildingNewsModel{
    _buildingNewsModel = buildingNewsModel;
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:buildingNewsModel.picPath]];
    self.contactLabel.text = buildingNewsModel.zhaiyao;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  TENCommentCell.h
//  HouseEconomics
//
//  Created by apple on 2018/12/27.
//  Copyright © 2018  HomerLynn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TENHouseDetailModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface TENCommentCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *priceScoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *sectionScoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *trafficScoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *matchingScoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *environmentScoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *averageScoreLabel;
@property (weak, nonatomic) IBOutlet UIStackView *priceView;
@property (weak, nonatomic) IBOutlet UIStackView *sectionView;
@property (weak, nonatomic) IBOutlet UIStackView *trafficView;
@property (weak, nonatomic) IBOutlet UIStackView *matchingView;
@property (weak, nonatomic) IBOutlet UIStackView *environmentView;
@property (nonatomic, strong) TENBuildScoreModel *buildScoreModel;
@end

NS_ASSUME_NONNULL_END

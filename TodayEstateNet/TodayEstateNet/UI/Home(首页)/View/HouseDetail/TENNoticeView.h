//
//  TENNoticeView.h
//  HouseEconomics
//
//  Created by apple on 2019/1/7.
//  Copyright © 2019  HomerLynn. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol NoticeViewBtnDelegate<NSObject>
- (void)noticeViewBtnClicked:(UIButton *)btn;
@end
NS_ASSUME_NONNULL_BEGIN

@interface TENNoticeView : UIView
@property (weak, nonatomic) IBOutlet UIButton *discountBtn;
@property (weak, nonatomic) IBOutlet UIButton *openBtn;
@property (weak, nonatomic) IBOutlet UIButton *subscribeBtn;
@property (weak, nonatomic) IBOutlet UIButton *cancelBtn;
@property (weak, nonatomic) IBOutlet UIButton *sureBtn;
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (nonatomic ,assign) id<NoticeViewBtnDelegate> delegate;
@property (nonatomic, strong)NSString *houseId;
@property (nonatomic, strong)NSDictionary *dataDic;
- (void)setImageWithDataDic:(NSMutableDictionary *)dataDic;
@end

NS_ASSUME_NONNULL_END

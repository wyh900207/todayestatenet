//
//  TENLookHouseServiceCell.h
//  HouseEconomics
//
//  Created by apple on 2018/12/26.
//  Copyright © 2018  HomerLynn. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TENLookHouseServiceCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *serviceBtn;

@end

NS_ASSUME_NONNULL_END

//
//  TENHouseDetailAveragePriceCell.h
//  TodayEstateNet
//
//  Created by  HomerLynn on 2018/11/7.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENTableViewCell.h"
#import "TENHouseDetailModel.h"

@interface TENHouseDetailAveragePriceCell : TENTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *averagePriceTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *averagePriceLabel;

@property (weak, nonatomic) IBOutlet UILabel *totalPriceTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalPriceLabel;

@property (weak, nonatomic) IBOutlet UILabel *areaTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *areaLabel;

@property (nonatomic, strong) TENBuildTypeModel *averageModel;
@property (nonatomic, assign) NSInteger row;

@end

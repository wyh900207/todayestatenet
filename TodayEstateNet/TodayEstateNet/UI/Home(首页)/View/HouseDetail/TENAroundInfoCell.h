//
//  TENAroundInfoCell.h
//  HouseEconomics
//
//  Created by apple on 2018/12/26.
//  Copyright © 2018  HomerLynn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TENHouseDetailModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface TENAroundInfoCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *areaInfoImgView;
@property (weak, nonatomic) IBOutlet UILabel *subwayLabel;
@property (weak, nonatomic) IBOutlet UILabel *busLabel;
@property (weak, nonatomic) IBOutlet UILabel *eatLabel;
@property (weak, nonatomic) IBOutlet UILabel *mallLabel;
@property(nonatomic, strong) TENBuildingInfoModel *buildingInfoModel;
@end

NS_ASSUME_NONNULL_END

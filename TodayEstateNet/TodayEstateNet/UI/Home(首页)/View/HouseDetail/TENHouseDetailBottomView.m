//
//  TENHouseDetailBottomView2.m
//  HouseEconomics
//
//  Created by apple on 2019/1/4.
//  Copyright © 2019  HomerLynn. All rights reserved.
//

#import "TENHouseDetailBottomView.h"

@implementation TENHouseDetailBottomView
- (void)willMoveToWindow:(UIWindow *)newWindow{// Will be removed from window, similar to -viewDidUnload.
    if (newWindow == nil) {//移除通知
        [[NSNotificationCenter defaultCenter] removeObserver:self name:TENChangeBtnName object:nil];
    }
}
- (void)didMoveToWindow {
    if (self.window) {
        // Added to a window, similar to -viewDidLoad.
        // Subscribe to notifications here.
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(changeBtnNameWith:) name:TENChangeBtnName object:nil];
    }
}
- (void)changeBtnNameWith:(NSNotification *)noti {
    if (noti.userInfo.allKeys.count == 3) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.favoriteBtn setTitle:[noti.userInfo[@"isFavorite"] integerValue] == 0 ? @"收藏" : @"已收藏" forState:normal];
            [self.favoriteBtn setImage:[UIImage imageNamed:[noti.userInfo[@"isFavorite"] integerValue] == 0 ? @"houseDetail_favorite" : @"houseDetail_favorite_selected"] forState:normal];
        });
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.subscribeBtn setTitle:[noti.userInfo[@"isSubscribe"] integerValue] == 0 ? @"订阅" : @"已订阅" forState:normal];
    });
}
- (IBAction)buttonHandler:(UIButton *)sender {
   
    TEN_LOG(@"%ld",sender.tag);
    [_delegate bottomBtnClickedWith:sender];
}

@end

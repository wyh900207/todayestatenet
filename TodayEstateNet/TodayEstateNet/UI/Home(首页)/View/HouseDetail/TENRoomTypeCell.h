//
//  TENRoomTypeCell.h
//  HouseEconomics
//
//  Created by apple on 2018/12/26.
//  Copyright © 2018  HomerLynn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TENHouseDetailModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface TENRoomTypeCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *roomImgView;
@property (weak, nonatomic) IBOutlet UILabel *roomTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *roomSizeLabel;
@property (weak, nonatomic) IBOutlet UILabel *roomLeftLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *roomFunctionLabel;
@property (weak, nonatomic) IBOutlet UILabel *saleTypeLabel;
@property(nonatomic, strong) TENBuildTypeModel *buildTypeModel;
@end

NS_ASSUME_NONNULL_END

//
//  TENPoiBottomView.h
//  HouseEconomics
//
//  Created by apple on 2019/1/2.
//  Copyright © 2019  HomerLynn. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol BottomItemClickedDelegate <NSObject>

- (void)bottomItemClickedWithKey:(NSString *)key;

@end
@interface TENPoiBottomView : UIView
@property (nonatomic, assign) id<BottomItemClickedDelegate>delegate;
@end

NS_ASSUME_NONNULL_END

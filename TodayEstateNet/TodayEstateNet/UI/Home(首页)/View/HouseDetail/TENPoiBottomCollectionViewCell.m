//
//  TENPoiBottomCollectionViewCell.m
//  HouseEconomics
//
//  Created by apple on 2019/1/2.
//  Copyright © 2019  HomerLynn. All rights reserved.
//

#import "TENPoiBottomCollectionViewCell.h"
@interface TENPoiBottomCollectionViewCell()

@end
@implementation TENPoiBottomCollectionViewCell
- (void)setDataDic:(NSDictionary *)dataDic{
    _dataDic = dataDic;
    [self.iconImgView setImage:[UIImage imageNamed:dataDic[@"iconImage"]]];
    self.iconNameLabel.text = dataDic[@"iconName"];
    self.iconNameLabel.textColor = [UIColor colorWithHexString:@"333333"];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

@end

//
//  TENRoomTypeCell.m
//  HouseEconomics
//
//  Created by apple on 2018/12/26.
//  Copyright © 2018  HomerLynn. All rights reserved.
//

#import "TENRoomTypeCell.h"

@implementation TENRoomTypeCell
- (void)setBuildTypeModel:(TENBuildTypeModel *)buildTypeModel{
    _buildTypeModel = buildTypeModel;
    self.roomSizeLabel.text = [NSString stringWithFormat:@"%@㎡",buildTypeModel.area];
    self.roomFunctionLabel.text = buildTypeModel.typeName;
    self.priceLabel.text = [NSString stringWithFormat:@"%@万元起",buildTypeModel.totalPrice];
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  TENHouseDetailBannerCell.h
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/11/4.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENTableViewCell.h"
#import <SDCycleScrollView/SDCycleScrollView.h>

@interface TENHouseDetailBannerCell : TENTableViewCell

@property (weak, nonatomic) IBOutlet SDCycleScrollView *bannerView;

@end

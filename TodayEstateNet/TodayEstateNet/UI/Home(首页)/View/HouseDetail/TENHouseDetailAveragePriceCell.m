//
//  TENHouseDetailAveragePriceCell.m
//  TodayEstateNet
//
//  Created by  HomerLynn on 2018/11/7.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENHouseDetailAveragePriceCell.h"
#import "TENUtilsHeader.h"

@implementation TENHouseDetailAveragePriceCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setAverageModel:(TENBuildTypeModel *)averageModel {
    _averageModel = averageModel;
    
    self.averagePriceTitleLabel.text = @"住宅均价";
    self.averagePriceLabel.text = [NSString stringWithFormat:@"%@万元/㎡", averageModel.price.stringValue];
    self.totalPriceTitleLabel.text = [NSString stringWithFormat:@"%@总价", averageModel.typeName];
    self.totalPriceLabel.text = [NSString stringWithFormat:@"%@万元", averageModel.totalPrice.stringValue];
    self.areaTitleLabel.text = @"建筑面积";
    self.areaLabel.text = [NSString stringWithFormat:@"%@㎡", averageModel.area];
}

- (void)setRow:(NSInteger)row {
    _row = row;
    UIColor *color;
    if (row % 3 == 0) {
        color = TENHexColor(@"3347D1");
    } else if (row % 3 == 1) {
        color = TENHexColor(@"D14E33");
    } else {
        color = TENHexColor(@"D19B33");
    }
    self.averagePriceLabel.textColor = color;
    self.totalPriceLabel.textColor = color;
    self.areaLabel.textColor = color;
}

@end

//
//  TENHouseDetailTitleCell.h
//  TodayEstateNet
//
//  Created by  HomerLynn on 2018/11/7.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENTableViewCell.h"
#import "HLTagsView.h"

@interface TENHouseDetailTitleCell : TENTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet HLTagsView *tagsView;
@property (weak, nonatomic) IBOutlet UIButton *sallingStateLabel;

@end

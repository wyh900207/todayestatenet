//
//  TENHouseDetailBottomView2.h
//  HouseEconomics
//
//  Created by apple on 2019/1/4.
//  Copyright © 2019  HomerLynn. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol BottomBtnClickedDelegate<NSObject>
- (void)bottomBtnClickedWith:(UIButton *)btn;
@end
NS_ASSUME_NONNULL_BEGIN

@interface TENHouseDetailBottomView : UIView
@property (weak, nonatomic) IBOutlet UIButton *wechatShareBtn;//微信分享

@property (weak, nonatomic) IBOutlet UIButton *subscribeBtn;//订阅
@property (weak, nonatomic) IBOutlet UIButton *favoriteBtn;//收藏
@property (weak, nonatomic) IBOutlet UIButton *serviceBtn;//在线咨询
@property (weak, nonatomic) IBOutlet UIButton *reportBtn;//我要看房  vip：报备客户
@property (nonatomic, assign) id <BottomBtnClickedDelegate> delegate;

@end

NS_ASSUME_NONNULL_END

//
//  TENBuildingDynamicCell.h
//  HouseEconomics
//
//  Created by apple on 2018/12/26.
//  Copyright © 2018  HomerLynn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TENHouseDetailModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface TENBuildingDynamicCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *contactLabel;
@property (weak, nonatomic) IBOutlet UIButton *readAllBtn;
@property (weak, nonatomic) IBOutlet UIButton *subscribeBtn;
@property (nonatomic, strong) TENBuildingNewsModel *buildingNewsModel;
@end

NS_ASSUME_NONNULL_END

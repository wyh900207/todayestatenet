//
//  TENPoiBottomCollectionViewCell.h
//  HouseEconomics
//
//  Created by apple on 2019/1/2.
//  Copyright © 2019  HomerLynn. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TENPoiBottomCollectionViewCell : UICollectionViewCell
@property (nonatomic, strong) NSDictionary *dataDic;
@property (weak, nonatomic) IBOutlet UIImageView *iconImgView;
@property (weak, nonatomic) IBOutlet UILabel *iconNameLabel;
@end

NS_ASSUME_NONNULL_END

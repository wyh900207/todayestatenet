//
//  TENHouseDetailDescriptionCell.h
//  TodayEstateNet
//
//  Created by  HomerLynn on 2018/11/7.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENTableViewCell.h"
#import "HLTagsView.h"
#import "TENHouseDetailModel.h"
@interface TENHouseDetailDescriptionCell : TENTableViewCell

@property (nonatomic, assign, getter=isOpen) BOOL open;

@property (weak, nonatomic) IBOutlet UILabel *openTimeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *activityTypeImgView;
@property (weak, nonatomic) IBOutlet UILabel *deliverTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *houseTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *saleAddressLabel;
@property (weak, nonatomic) IBOutlet HLTagsView *tagsView;
@property (weak, nonatomic) IBOutlet UIButton *moreInfoButton;
@property (weak, nonatomic) IBOutlet UIButton *discountBtn;
@property (weak, nonatomic) IBOutlet UIButton *openBtn;
@property (strong,nonatomic) TENBuildingInfoModel *buildingInfoModel;
@end

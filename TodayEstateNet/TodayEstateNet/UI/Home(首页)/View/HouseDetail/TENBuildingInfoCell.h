//
//  TENBuildingInfoCell.h
//  HouseEconomics
//
//  Created by apple on 2018/12/26.
//  Copyright © 2018  HomerLynn. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TENBuildingInfoCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *infoImgView;

@end

NS_ASSUME_NONNULL_END

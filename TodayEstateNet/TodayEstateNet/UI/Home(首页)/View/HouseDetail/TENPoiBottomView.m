//
//  TENPoiBottomView.m
//  HouseEconomics
//
//  Created by apple on 2019/1/2.
//  Copyright © 2019  HomerLynn. All rights reserved.
//

#import "TENPoiBottomView.h"
#import "TENPoiBottomCollectionViewCell.h"
@interface TENPoiBottomView()<UICollectionViewDelegate,UICollectionViewDataSource,BottomItemClickedDelegate>

@property (nonatomic, strong) UICollectionView * collectionView;
@property (nonatomic, strong) NSArray *iconArray;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) NSMutableDictionary *cellDic;
@end
@implementation TENPoiBottomView
- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupSubviews];
    }
    return self;
}
- (void)setupSubviews {
    [self addSubview:self.collectionView];
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.iconArray.count;
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    //防止cell复用
    NSString *identifier = [self.cellDic objectForKey:[NSString stringWithFormat:@"%@", indexPath]];
    if (identifier == nil) {
        identifier = [NSString stringWithFormat:@"TENPoiBottomCollectionViewCell%@", [NSString stringWithFormat:@"%@", indexPath]];
        [self.cellDic setValue:identifier forKey:[NSString stringWithFormat:@"%@", indexPath]];
        [self.collectionView registerNib:[UINib nibWithNibName:@"TENPoiBottomCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:identifier];
    }

    TENPoiBottomCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    
    cell.dataDic = self.iconArray[indexPath.row];
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    TENPoiBottomCollectionViewCell *cell = (TENPoiBottomCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [self updateCellStatus:cell indexPath:indexPath selected:YES];
    [_delegate bottomItemClickedWithKey:cell.iconNameLabel.text];
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{
    TENPoiBottomCollectionViewCell *cell = (TENPoiBottomCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
    [self updateCellStatus:cell indexPath:indexPath selected:NO];
}
- (void)updateCellStatus:(TENPoiBottomCollectionViewCell *)cell indexPath:(NSIndexPath *)indexPath selected:(BOOL)selected{
    cell.iconImgView.image = selected ? [UIImage imageNamed:[NSString stringWithFormat:@"%@_selected",self.iconArray[indexPath.row][@"iconImage"]]] : [UIImage imageNamed:[NSString stringWithFormat:@"%@",self.iconArray[indexPath.row][@"iconImage"]]];
    cell.iconNameLabel.textColor = selected ? [UIColor colorWithHexString:TENThemeBlueColor] : [UIColor colorWithHexString:@"333333"];
}

- (UICollectionView *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout *flowLayout = [[UICollectionViewFlowLayout alloc] init];
        flowLayout.itemSize = CGSizeMake(TENScreenWidth/6.5,48);
        flowLayout.minimumLineSpacing = 0;
        flowLayout.minimumInteritemSpacing = 0;
        flowLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
//        flowLayout.sectionInset = UIEdgeInsetsMake(35, 0, 0, 0);
        _collectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, TENScreenWidth, 49) collectionViewLayout:flowLayout];
        _collectionView.showsHorizontalScrollIndicator = NO;
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.backgroundColor = [UIColor whiteColor];
    }
    return _collectionView;
}
- (NSArray *)iconArray{
    if (!_iconArray) {
        _iconArray = @[@{@"iconImage":@"公交",@"iconName":@"公交"},@{@"iconImage":@"地铁",@"iconName":@"地铁"},@{@"iconImage":@"学校",@"iconName":@"学校"},@{@"iconImage":@"楼盘",@"iconName":@"楼盘"},@{@"iconImage":@"医院",@"iconName":@"医院"},@{@"iconImage":@"银行",@"iconName":@"银行"},@{@"iconImage":@"购物",@"iconName":@"购物"},@{@"iconImage":@"健身",@"iconName":@"健身"},@{@"iconImage":@"餐饮",@"iconName":@"餐饮"}];
    }
    return _iconArray;
}

- (NSMutableDictionary *)cellDic{
    if (!_cellDic) {
        _cellDic = [NSMutableDictionary dictionary];
    }
    return _cellDic;
}
@end

//
//  TENCommentCell.m
//  HouseEconomics
//
//  Created by apple on 2018/12/27.
//  Copyright © 2018  HomerLynn. All rights reserved.
//

#import "TENCommentCell.h"

@implementation TENCommentCell
- (void)setBuildScoreModel:(TENBuildScoreModel *)buildScoreModel{
    _buildScoreModel = buildScoreModel;
    self.averageScoreLabel.text = [NSString stringWithFormat:@"%@分",buildScoreModel.scoreNum];
    self.priceScoreLabel.text = [NSString stringWithFormat:@"%@分",buildScoreModel.priceScore];
    self.sectionScoreLabel.text = [NSString stringWithFormat:@"%@分",buildScoreModel.sectionScore];
    self.trafficScoreLabel.text = [NSString stringWithFormat:@"%@分",buildScoreModel.trafficScore];
    self.matchingScoreLabel.text = [NSString stringWithFormat:@"%@分",buildScoreModel.matchingScore];
    self.environmentScoreLabel.text = [NSString stringWithFormat:@"%@分",buildScoreModel.environmentScore];
    for (int i = 0; i < buildScoreModel.priceScore.integerValue; i++) {
        UIImageView *imageView = self.priceView.arrangedSubviews[i];
        imageView.image = [UIImage imageNamed:@"score_hl"];
    }
    for (int i = 0; i < buildScoreModel.sectionScore.integerValue; i++) {
        UIImageView *imageView = self.sectionView.arrangedSubviews[i];
        imageView.image = [UIImage imageNamed:@"score_hl"];
    }
    for (int i = 0; i < buildScoreModel.trafficScore.integerValue; i++) {
        UIImageView *imageView = self.trafficView.arrangedSubviews[i];
        imageView.image = [UIImage imageNamed:@"score_hl"];
    }
    for (int i = 0; i < buildScoreModel.matchingScore.integerValue; i++) {
        UIImageView *imageView = self.matchingView.arrangedSubviews[i];
        imageView.image = [UIImage imageNamed:@"score_hl"];
    }
    for (int i = 0; i < buildScoreModel.environmentScore.integerValue; i++) {
        UIImageView *imageView = self.environmentView.arrangedSubviews[i];
        imageView.image = [UIImage imageNamed:@"score_hl"];
    }
}
- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

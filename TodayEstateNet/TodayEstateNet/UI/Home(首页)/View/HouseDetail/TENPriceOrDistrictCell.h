//
//  TENPriceOrDistrictCell.h
//  HouseEconomics
//
//  Created by apple on 2018/12/27.
//  Copyright © 2018  HomerLynn. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TENHouseDetailModel.h"
NS_ASSUME_NONNULL_BEGIN

@interface TENPriceOrDistrictCell : UITableViewCell


@property (weak, nonatomic) IBOutlet UIButton *samePriceBtn;
@property (weak, nonatomic) IBOutlet UIButton *sameDistrictBtn;
@property (weak, nonatomic) IBOutlet UITableView *sameTableView;

@property (nonatomic, strong) NSArray *samePriceDataArray;
@property (nonatomic, strong) NSArray *sameDistictDataArray;
@end

NS_ASSUME_NONNULL_END

//
//  TENPriceOrDistrictCell.m
//  HouseEconomics
//
//  Created by apple on 2018/12/27.
//  Copyright © 2018  HomerLynn. All rights reserved.
//

#import "TENPriceOrDistrictCell.h"
#import "TENHomeNormalSallingCell.h"

@interface TENPriceOrDistrictCell()<UITableViewDelegate,UITableViewDataSource>
/**<#注释#>*/
@property (nonatomic, assign) BOOL samePriceSelected;
@end
@implementation TENPriceOrDistrictCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupSubviews];

}
- (void)setupSubviews {
    [self.sameTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"noDataCell"];
    [self.sameTableView registerNib:[UINib nibWithNibName:@"TENHomeNormalSallingCell" bundle:nil] forCellReuseIdentifier:@"TENHomeNormalSallingCell"];
    self.sameTableView.delegate = self;
    self.sameTableView.dataSource = self;
    self.sameTableView.rowHeight = UITableViewAutomaticDimension;
    self.samePriceSelected = YES;
    self.sameTableView.estimatedRowHeight = 133;
    self.sameTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    }
#pragma mark-------------UITableViewDelegate -datasource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (self.samePriceSelected) {
        return self.samePriceDataArray.count > 0 ? self.samePriceDataArray.count : 1;
    }else{
        return self.sameDistictDataArray.count > 0 ? self.sameDistictDataArray.count : 1;
    }
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TENHomeNormalSallingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TENHomeNormalSallingCell"];
    if (self.samePriceSelected) {
        if (self.samePriceDataArray.count > 0) {
            if (!cell) {
                cell = [[TENHomeNormalSallingCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TENHomeNormalSallingCell"];
            }
            cell.dataObject = self.samePriceDataArray[indexPath.row];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }else{
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"noDataCell"];
            cell.textLabel.text = @"暂无数据!";
            cell.textLabel.textAlignment = NSTextAlignmentCenter;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
        
    }else{
        if (self.sameDistictDataArray.count > 0) {
            if (!cell) {
                cell = [[TENHomeNormalSallingCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"TENHomeNormalSallingCell"];
            }
            cell.dataObject = [self.sameDistictDataArray[indexPath.row] mj_keyValues];
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }else{
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"noDataCell"];
            cell.textLabel.text = @"暂无数据!";
            cell.textLabel.textAlignment = NSTextAlignmentCenter;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
        
    }
    
    
    return nil;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:[NSIndexPath indexPathWithIndex:indexPath.row] animated:YES];
}

- (IBAction)samePriceBtnClicked:(UIButton *)sender {
    sender.selected = self.sameDistrictBtn.selected;
    self.samePriceSelected = !sender.selected;
//    if (self.samePriceDataArray.count == 0) {
//        self.sameTableView.hidden = YES;
//    }else{
//        self.sameTableView.hidden = NO;
//    }
    [self.samePriceBtn setTitleColor:[UIColor colorWithHexString:@"3347D1"] forState:normal];
    [self.sameDistrictBtn setTitleColor:[UIColor colorWithHexString:@"333333"] forState:normal];
    [self.sameTableView reloadData];
    
}
- (IBAction)sameDistrictBtnClicked:(UIButton *)sender {
    sender.selected = self.samePriceBtn.selected;
    self.samePriceSelected = sender.selected;
//    if (self.sameDistictDataArray.count == 0) {
//        self.sameTableView.hidden = YES;
//    }else{
//        self.sameTableView.hidden = NO;
//    }
    [self.sameDistrictBtn setTitleColor:[UIColor colorWithHexString:@"3347D1"] forState:normal];
    [self.samePriceBtn setTitleColor:[UIColor colorWithHexString:@"333333"] forState:normal];
    [self.sameTableView reloadData];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

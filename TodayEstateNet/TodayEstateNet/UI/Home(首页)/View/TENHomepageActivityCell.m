//
//  TENHomepageActivityCell.m
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/10/27.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENHomepageActivityCell.h"
#import "TENUtilsHeader.h"
#import "TENHomepageActivityCollectionViewCell.h"
#import <iCarousel/iCarousel.h>

@interface TENHomepageActivityCell () <iCarouselDelegate, iCarouselDataSource>

@property (weak, nonatomic) IBOutlet UICollectionView *activityCollectionView;
@property (nonatomic, strong) UIView *selectView;
@property (nonatomic, strong) iCarousel *carousel;

@end

@implementation TENHomepageActivityCell

#pragma mark - Life cycle

- (void)awakeFromNib {
    [super awakeFromNib];
//    self.activityCollectionView.showsHorizontalScrollIndicator = NO;
//    self.activityCollectionView.delegate = self;
//    self.activityCollectionView.dataSource = self;
//    [self.activityCollectionView registerNib:[UINib nibWithNibName:@"TENHomepageActivityCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"TENHomepageActivityCollectionViewCell"];
    [self.iconImage sd_setImageWithURL:[NSURL URLWithString:@"https://ss0.baidu.com/7Po3dSag_xI4khGko9WTAnF6hhy/image/h%3D300/sign=0f602b24b7a1cd111ab674208913c8b0/b219ebc4b74543a9759a35d813178a82b9011425.jpg"]];
    self.iconImage.hidden = YES;
    
    [self.contentView addSubview:self.carousel];
    UIEdgeInsets padding = UIEdgeInsetsMake(0, 0, 0, 0);
    [self.carousel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self).with.insets(padding);
    }];
    
//    [self.contentView addSubview:self.carousel];
//    [self.carousel mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.equalTo(self).offset(5);
//        make.left.equalTo(self).offset(8);
//        make.right.equalTo(self).offset(-8);
//        make.bottom.equalTo(self).offset(-5);
//    }];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - UI

#pragma mark - UICollectionViewDelegate, UICollectionViewDataSource

//- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
//    return self.activityObjects.count;
//}
//
//- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
//    TENHomepageActivityCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TENHomepageActivityCollectionViewCell" forIndexPath:indexPath];
//    cell.activityObject = self.activityObjects[indexPath.row];
//
//    return cell;
//}

#pragma mark - iCarouselDelegate, iCarouselDataSource

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel {
    return self.activityObjects.count;
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view {
    if (view == nil) {
        UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 160, 90)];
        [button sd_setImageWithURL:[NSURL URLWithString:self.activityObjects[index][@"picPath"]] forState:0];
        button.tag = index;
        [button addTarget:self action:@selector(didSelectCell:) forControlEvents:UIControlEventTouchUpInside];
        view = button;
    }
    
    return view;
}
-(void)didSelectCell:(UIButton *)item {
       //NSLog(@"Tapped view number: %ld", (long)item.tag);
    if (self.delegate && [self.delegate respondsToSelector:@selector(activityItemClicked:)]) {
        [self.delegate activityItemClicked:item.tag];
    }
}
- (CATransform3D)carousel:(iCarousel *)carousel itemTransformForOffset:(CGFloat)offset baseTransform:(CATransform3D)transform {
    static CGFloat max_sacle = 1.0f;
    static CGFloat min_scale = 1.f;
    if (offset <= 1 && offset >= -1) {
        float tempScale = offset < 0 ? 1+offset : 1-offset;
        float slope = (max_sacle - min_scale) / 1;
        
        CGFloat scale = min_scale + slope*tempScale;
        
        transform = CATransform3DScale(transform, scale, scale, 1);
    } else {
        transform = CATransform3DScale(transform, min_scale, min_scale, 1);
    }
    
    return CATransform3DTranslate(transform, offset * self.carousel.itemWidth * 1.01, 0.0, 0.0);
}
- (void)carousel:(iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index{
    TEN_LOG(@"点击-=-=-=-=-%ld",index);
}
- (CGFloat)carousel:(iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value {
    switch (option)
    {
        case iCarouselOptionWrap:
        {
            return YES;
        }
        case iCarouselOptionSpacing:
        {
            return value * 1.05;
        }
        case iCarouselOptionFadeMax:
        {
            if (self.carousel.type == iCarouselTypeCustom)
            {
                return 0.0;
            }
            return value;
        }
        case iCarouselOptionShowBackfaces:
        case iCarouselOptionRadius:
        case iCarouselOptionAngle:
        case iCarouselOptionArc:
        case iCarouselOptionTilt:
        case iCarouselOptionCount:
        case iCarouselOptionFadeMin:
        case iCarouselOptionFadeMinAlpha:
        case iCarouselOptionFadeRange:
        case iCarouselOptionOffsetMultiplier:
        case iCarouselOptionVisibleItems:
        {
            return value;
        }
    }
}

#pragma mark - Setter

- (void)setActivityObjects:(NSArray *)activityObjects {
    _activityObjects = activityObjects;
    //[self.activityCollectionView reloadData];
    [self.carousel reloadData];
}

#pragma mark - UICollectionViewDelegateFlowLayout

//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
//    CGFloat width = (200 / 375.0) * TENScreenWidth;
//    CGFloat height = (120 / 200.0) * width;
//
//    return CGSizeMake(width, height);
//}

#pragma mark - Getter

- (iCarousel *)carousel {
    if (!_carousel) {
        _carousel = [iCarousel new];
        _carousel.delegate = self;
        _carousel.dataSource = self;
        _carousel.scrollSpeed = 0.5;
        //iCarouselTypeCustom
        _carousel.type = iCarouselTypeLinear;
        _carousel.autoscroll = -0.4;
    }
    return _carousel;
}



@end

//
//  TENAdserScrollview.m
//  TodayEstateNet
//
//  Created by DJAPPLE3-ysy on 2018/12/18.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENAdserScrollview.h"
#import "TENUtilsHeader.h"
#import "TENHomeTodayHeadRollCell.h"

@interface TENAdserScrollview () <UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, strong) UICollectionViewFlowLayout *flowLayout;
@property (nonatomic, strong) UICollectionView *collectionView;
@property (nonatomic, strong) NSTimer *timer;
@property (nonatomic, strong) NSArray *titleArr;
@property (nonatomic, strong) NSArray *imageArr;
@property (nonatomic, strong) NSArray *bottomImageArr;
@property (nonatomic, strong) NSArray *bottomTitleArr;
@property (nonatomic, strong) NSArray *contentTextArr;
@property (nonatomic, strong) NSArray *dateAndAutherArr;

@end

@implementation TENAdserScrollview

static NSInteger const advertScrollViewMaxSections = 90;

- (void)awakeFromNib {
    [super awakeFromNib];
    [self initialization];
    [self setupSubviews];
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
//        self.backgroundColor = [UIColor blueColor];
        [self initialization];
        [self setupSubviews];
    }
    return self;
}

- (void)willMoveToSuperview:(UIView *)newSuperview {
    if (!newSuperview) {
        [self removeTimer];
    }
}

- (void)dealloc {
    _collectionView.delegate = nil;
    _collectionView.dataSource = nil;
}

- (void)initialization {
    _scrollTimeInterval = 2.0;
    
    [self addTimer];
//    _advertScrollViewStyle = TENAdserScrollviewStyleNormal;
}

- (void)setupSubviews {
    
    [self addSubview:self.collectionView];
}

- (UICollectionView *)collectionView {
    if (!_collectionView) {
        _flowLayout = [[UICollectionViewFlowLayout alloc] init];
        _flowLayout.minimumLineSpacing = 0;
        
        _collectionView = [[UICollectionView alloc] initWithFrame:self.bounds collectionViewLayout:_flowLayout];
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        _collectionView.scrollsToTop = NO;
        _collectionView.scrollEnabled = NO;
        _collectionView.pagingEnabled = YES;
        _collectionView.showsVerticalScrollIndicator = NO;
        _collectionView.backgroundColor = [UIColor clearColor];
        [_collectionView registerNib:[UINib nibWithNibName:@"TENHomeTodayHeadRollCell" bundle:nil] forCellWithReuseIdentifier:@"roll"];
    }
    return _collectionView;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    _flowLayout.itemSize = CGSizeMake(self.frame.size.width, self.frame.size.height);
    _collectionView.frame = self.bounds;
    
    if (self.imageArr.count > 1) {
        [self defaultSelectedScetion];
    }
}

- (void)defaultSelectedScetion {
    [self.collectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:0 inSection:0.5 * advertScrollViewMaxSections] atScrollPosition:UICollectionViewScrollPositionBottom animated:NO];
}

#pragma mark - - - UICollectionView 的 dataSource、delegate方法

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return advertScrollViewMaxSections;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.datasourceObjects.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    TENHomeTodayHeadRollCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"roll" forIndexPath:indexPath];
    
    NSDictionary *object = self.datasourceObjects[indexPath.item];
    
    NSString *topImagePath = object[@"picPath"];
    NSString *title = object[@"headline"];
    NSString *contentText = object[@"zhaiyao"];
    NSString *date = object[@"createDateFormat"];
    NSString *auther = object[@"resource"];
    NSString *dateAndAuther = [NSString stringWithFormat:@"%@ %@", date, auther];

    if ([topImagePath hasPrefix:@"http"]) {
        [cell.leftImage sd_setImageWithURL:[NSURL URLWithString:topImagePath]];
    } else {
        cell.leftImage.image = [UIImage imageNamed:topImagePath];
    }
    
    cell.titleText.text = title;
    cell.conText.text = contentText;
    cell.bottomText.text = dateAndAuther;
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.delegate && [self.delegate respondsToSelector:@selector(advertScrollView:didSelectedItemAtIndex:)]) {
        [self.delegate advertScrollView:self didSelectedItemAtIndex:indexPath.item];
    }
}

#pragma mark - - - NSTimer

- (void)addTimer {
    [self removeTimer];
    
    self.timer = [NSTimer timerWithTimeInterval:self.scrollTimeInterval target:self selector:@selector(beginUpdateUI) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
}

- (void)removeTimer {
    [_timer invalidate];
    _timer = nil;
}

- (void)beginUpdateUI {
    if (self.datasourceObjects.count == 0) return;
    
    // 1、当前正在展示的位置
    NSIndexPath *currentIndexPath = [[self.collectionView indexPathsForVisibleItems] lastObject];

    // 马上显示回最中间那组的数据
    NSIndexPath *resetCurrentIndexPath = [NSIndexPath indexPathForItem:currentIndexPath.item inSection:0.5 * advertScrollViewMaxSections];
    [self.collectionView scrollToItemAtIndexPath:resetCurrentIndexPath atScrollPosition:UICollectionViewScrollPositionBottom animated:NO];

    // 2、计算出下一个需要展示的位置
    NSInteger nextItem = resetCurrentIndexPath.item + 1;
    NSInteger nextSection = resetCurrentIndexPath.section;
    if (nextItem == self.datasourceObjects.count) {
        nextItem = 0;
        nextSection++;
    }

    NSIndexPath *nextIndexPath = [NSIndexPath indexPathForItem:nextItem inSection:nextSection];

    // 3、通过动画滚动到下一个位置
    [self.collectionView scrollToItemAtIndexPath:nextIndexPath atScrollPosition:UICollectionViewScrollPositionBottom animated:YES];
}

- (void)setDatasourceObjects:(NSArray *)datasourceObjects {
    _datasourceObjects = datasourceObjects;
    if (datasourceObjects.count > 1) {
        [self addTimer];
    } else {
        [self removeTimer];
    }

    [self.collectionView reloadData];
}

@end

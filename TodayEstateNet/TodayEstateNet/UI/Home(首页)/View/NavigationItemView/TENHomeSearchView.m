//
//  TENHomeSearchView.m
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/9/11.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENHomeSearchView.h"
#import "TENUtilsHeader.h"

@interface TENHomeSearchView()

@property (nonatomic, strong) UIImageView *searchIconImageView;
@property (nonatomic, strong) UIButton *searchContext;

@end

@implementation TENHomeSearchView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setupSubviews];
    }
    return self;
}

- (void)setupSubviews {
    [self addSubview:self.searchIconImageView];
    [self addSubview:self.searchContext];
    
    [self.searchIconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.equalTo(self);
        make.left.equalTo(self).offset(5);
        make.width.height.equalTo(@(TENSpace15));
    }];
    [self.searchContext mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(self);
        make.left.equalTo(self.searchIconImageView.mas_right).offset(5);
        make.right.lessThanOrEqualTo(self).offset(-5).priorityLow();
        make.width.greaterThanOrEqualTo(@200);
    }];
}

#pragma mark - Private Action

- (void)clickSearchContextButton:(UIButton *)button {
    if (self.searchHandle) {
        self.searchHandle();
    }
}

#pragma mark - Getter

- (UIImageView *)searchIconImageView {
    if (!_searchIconImageView) {
        _searchIconImageView = [UIImageView new];
        _searchIconImageView.image = TENImage(@"tabbar_search");
    }
    return _searchIconImageView;
}

- (UIButton *)searchContext {
    if (!_searchContext) {
        _searchContext = [UIButton new];
        _searchContext.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
        _searchContext.titleLabel.font = TENFont13;
        [_searchContext setTitleColor:[UIColor colorWithHexString:TENTextGrayColor] forState:UIControlStateNormal];
        [_searchContext setTitle:@"Today's Network" forState:UIControlStateNormal];
        [_searchContext addTarget:self action:@selector(clickSearchContextButton:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _searchContext;
}

@end

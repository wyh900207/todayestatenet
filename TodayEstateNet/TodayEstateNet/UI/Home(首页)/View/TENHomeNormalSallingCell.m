//
//  TENHomeNormalSallingCell.m
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/9/11.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENHomeNormalSallingCell.h"
#import "TENUtilsHeader.h"
#import "HLTagsView.h"

@interface TENHomeNormalSallingCell ()

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *sallingTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *houseSizeInfoLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalPriceLabel;
@property (weak, nonatomic) IBOutlet UILabel *saleTypeLabel;
@property (weak, nonatomic) IBOutlet HLTagsView *tagsView;
@property (weak, nonatomic) IBOutlet UIImageView *watermarkImageView;
@property (weak, nonatomic) IBOutlet UIImageView *attachTypeImageView;//展示类型 1 图片 2视频 3全景
@property (weak, nonatomic) IBOutlet UILabel *newsLabel;
@property (weak, nonatomic) IBOutlet UILabel *dongtai;
@property (weak, nonatomic) IBOutlet UILabel *rightDongtai;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomConstraint;

@end

@implementation TENHomeNormalSallingCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    //[self.iconImageView sd_setImageWithURL:[NSURL URLWithString:@"https://ss0.baidu.com/7Po3dSag_xI4khGko9WTAnF6hhy/image/h%3D300/sign=0f602b24b7a1cd111ab674208913c8b0/b219ebc4b74543a9759a35d813178a82b9011425.jpg"]];
}
//推荐：楼盘
- (void)setBuildingData:(RecommendBuilding *)buildingData{
    _buildingData = buildingData;
    // 销售状态
    NSString *saleType = buildingData.saleType;
    // Icon
    NSURL *url = [NSURL URLWithString:buildingData.logo];
    // 标题
    NSString *name = buildingData.name;
    // 户型
    NSString *size = [NSString stringWithFormat:@"%@ | %@", buildingData.roomType, buildingData.roomArea];
    // 地区 & 距离
    NSString *address = [NSString stringWithFormat:@"%@ %@ | %@", buildingData
                         .provinceName, buildingData.cityName,buildingData.distance];
    // 单价
    NSString *price = [NSString stringWithFormat: @"%@/㎡", buildingData.price];
    // 总价
    NSString *totalPrice = buildingData.totalPrice;
    // tags
    NSString *tags = buildingData.activityTypeName;
    if (tags && tags.length > 0) {
        self.tagsView.tags = @[tags];
    }
    
    [self.iconImageView sd_setImageWithURL:url];
    self.sallingTitleLabel.text = name;
    self.houseSizeInfoLabel.text = size;
    self.addressLabel.text = address;
    self.priceLabel.text = price;
    self.totalPriceLabel.text = totalPrice;
    self.saleTypeLabel.text = saleType;
    
    self.saleTypeLabel.layer.cornerRadius = 2;
    self.saleTypeLabel.layer.masksToBounds = YES;
    if ([saleType isEqualToString:@"在售"]) {
        self.saleTypeLabel.backgroundColor = TENHexColor(@"D14E33"); //[UIColor colorWithHexString:@"D14E33"];
    } else {
        self.saleTypeLabel.backgroundColor = TENHexColor(@"999999"); //[UIColor colorWithHexString:@"999999"];
    }
}
//找房
- (void)setData:(SearchDetail *)data{
    _data = data;
    // 销售状态
    NSString *saleType = data.saleType;
    // Icon
    NSURL *url = [NSURL URLWithString:data.logo];
    // 标题
    NSString *name = data.name;
    // 户型
    NSString *size = [NSString stringWithFormat:@"%@ | %@", data.roomType, data.roomArea];
    // 地区 & 距离
    NSString *address = [NSString stringWithFormat:@"%@ %@ | %@", data
                         .provinceName, data.cityName,data.distance];
    // 单价
    NSString *price = [NSString stringWithFormat: @"%@/㎡", data.price];
    // 总价
    NSString *totalPrice = data.totalPrice;
    // tags
//    NSString *tags = data.activityTypeName;
//    if (![tags isKindOfClass:[NSNull class]] && tags.length > 0) {
//        self.tagsView.tags = @[tags];
//    }
    NSString *tags = data.tags;
    if (!KSTRING_IS_EMPTY(tags)) {
        NSArray *array = [tags componentsSeparatedByString:@"-"];
        TEN_LOG(@"%@", array);
        self.tagsView.tags = array;
    }
    
    [self.iconImageView sd_setImageWithURL:url];
    self.sallingTitleLabel.text = name;
    self.houseSizeInfoLabel.text = size;
    self.addressLabel.text = address;
    self.priceLabel.text = price;
    self.totalPriceLabel.text = totalPrice;
    self.saleTypeLabel.text = saleType;
    
    self.saleTypeLabel.layer.cornerRadius = 2;
    self.saleTypeLabel.layer.masksToBounds = YES;
    if ([saleType isEqualToString:@"在售"]) {
        self.saleTypeLabel.backgroundColor = TENHexColor(@"D14E33"); //[UIColor colorWithHexString:@"D14E33"];
    } else {
        self.saleTypeLabel.backgroundColor = TENHexColor(@"999999"); //[UIColor colorWithHexString:@"999999"];
    }
    
    [self layoutIfNeeded];
}
- (void)setDataObject:(NSDictionary *)dataObject {
    _dataObject = dataObject;
    // 销售状态
    NSString *saleType = dataObject[@"saleType"];
    // Icon
    NSURL *url = [NSURL URLWithString:dataObject[@"logo"]];
    // 标题
    NSString *name = dataObject[@"name"];
    // 户型
    NSString *size = [NSString stringWithFormat:@"%@ | %@", dataObject[@"roomType"], dataObject[@"roomArea"]];
    // 地区 & 距离
    NSString *address = [NSString stringWithFormat:@"%@ %@ | %@", dataObject[@"provinceName"], dataObject[@"cityName"], dataObject[@"distance"]];
    // 单价
    NSString *price = [NSString stringWithFormat: @"%@/㎡", dataObject[@"price"]];
    // 总价
    NSString *totalPrice = dataObject[@"totalPrice"];
    // tags
    NSString *tags = dataObject[@"tags"];
    // 水印
    NSString *watermark = dataObject[@"activityTypePic"];
    
//    if (![tags isKindOfClass:[NSNull class]] && tags.length > 0) {
//        self.tagsView.tags = @[tags];
//    }
    
    if (!KSTRING_IS_EMPTY(tags)) {
        NSArray *array = [tags componentsSeparatedByString:@"-"];
        TEN_LOG(@"%@", array);
        self.tagsView.tags = array;
    }
    
    [self.iconImageView sd_setImageWithURL:url];
    self.sallingTitleLabel.text = name;
    self.houseSizeInfoLabel.text = size;
    self.addressLabel.text = address;
    self.priceLabel.text = price;
    self.totalPriceLabel.text = totalPrice;
    self.saleTypeLabel.text = saleType;
    
    self.saleTypeLabel.layer.cornerRadius = 2;
    self.saleTypeLabel.layer.masksToBounds = YES;
    // 动态
    BOOL isDongTaiEmpty = KSTRING_IS_EMPTY(dataObject[@"newsTitle"]);
    self.newsLabel.hidden = isDongTaiEmpty;
    self.dongtai.hidden = isDongTaiEmpty;
    self.rightDongtai.hidden = isDongTaiEmpty;
    if (isDongTaiEmpty) {
        //self.newsLabel.text = @"给个动态吧, 别什么都给<null>, 会不会自己判断一下";
        self.bottomConstraint.constant = 10;
    } else {
        self.newsLabel.text = dataObject[@"newsTitle"];
        self.bottomConstraint.constant = 40;
    }
    [self.watermarkImageView sd_setImageWithURL:[NSURL URLWithString:watermark]];
    if ([saleType isEqualToString:@"在售"]) {
        self.saleTypeLabel.backgroundColor = TENHexColor(@"D14E33"); //[UIColor colorWithHexString:@"D14E33"];
    } else {
        self.saleTypeLabel.backgroundColor = TENHexColor(@"999999"); //[UIColor colorWithHexString:@"999999"];
    }
    // 全景
    if (![dataObject[@"attachType"] isKindOfClass:[NSNull class]]) {
        
        if ([dataObject[@"attachType"] integerValue] == 2) {
            self.attachTypeImageView.image = [UIImage imageNamed:@"home_video"];
        }else if ([dataObject[@"attachType"] integerValue] == 3){
            self.attachTypeImageView.image = [UIImage imageNamed:@"home_panorama"];
        }else{
            self.attachTypeImageView.image = [UIImage imageNamed:@""];
        }
    }else{
        self.attachTypeImageView.image = [UIImage imageNamed:@""];
    }
    
    [self layoutIfNeeded];
}

@end

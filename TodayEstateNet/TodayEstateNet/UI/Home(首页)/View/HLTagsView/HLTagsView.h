//
//  HLTagsView.h
//  HLTagsView
//
//  Created by  HomerLynn on 2018/10/8.
//  Copyright © 2018  HomerLynn. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HLTagsView : UIView

@property (nonatomic, strong) NSArray *tags;

@end

//
//  HLTagsView.m
//  HLTagsView
//
//  Created by  HomerLynn on 2018/10/8.
//  Copyright © 2018  HomerLynn. All rights reserved.
//

#import "HLTagsView.h"
#import "TENUtilsHeader.h"

@interface HLTagsView ()

@property (nonatomic, strong) NSMutableArray<UILabel *> *tagButtons;

@end

@implementation HLTagsView

- (void)setTags:(NSArray *)tags {
    _tags = tags;
    static BOOL shouldBreak = NO;
    [self.tagButtons enumerateObjectsUsingBlock:^(UILabel * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj removeFromSuperview];
    }];
    [self.tagButtons removeAllObjects];
    for (int i = 0; i < tags.count; i++) {
        @weakify(self)
        [self calculateCurrentWidthWith:i response:^(BOOL couldAddToSuperView, CGRect frame) {
            @strongtify(self)
            if (couldAddToSuperView) {
                UILabel *label = [UILabel new];
                label.tag = i;
                label.textAlignment = NSTextAlignmentCenter;
                label.font = [UIFont systemFontOfSize:10];
                label.layer.cornerRadius = 3;
                label.layer.masksToBounds = YES;
                label.layer.borderColor = TENHexColor(TENTextGrayColor).CGColor;
                label.layer.borderWidth = 1;
                label.text = self.tags[i];
                label.textColor = TENHexColor(TENTextGrayColor);
                [self.tagButtons addObject:label];
                label.frame = frame;
                [self addSubview:label];
            } else {
                shouldBreak = YES;
            }
        }];
        if (shouldBreak) {
            break;
        }
    }
}

- (void)calculateCurrentWidthWith:(int)index response:(void(^)(BOOL couldAddToSuperView, CGRect frame))response {
    CGFloat previousMaxX = index == 0 ? 0 : CGRectGetMaxX(self.tagButtons[index - 1].frame) + 5;
    NSString *tagString = self.tags[index];
    CGFloat tagWidth = [tagString sizeWithAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:10]}].width;
    CGFloat nextMaxX = previousMaxX + 15 + tagWidth;
    if (nextMaxX <= self.bounds.size.width) {
        CGRect rect = CGRectMake(previousMaxX, 0, tagWidth + 10, self.frame.size.height);
        response(YES, rect);
    } else {
        response(NO, CGRectZero);
    }
}

#pragma mark - Getter

- (NSMutableArray<UILabel *> *)tagButtons {
    if (!_tagButtons) {
        _tagButtons = [@[] mutableCopy];
    }
    return _tagButtons;
}

@end

//
//  TENSearchLabelCell.m
//  TodayEstateNet
//
//  Created by Admin on 2018/9/18.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENSearchLabelCell.h"
CGFloat heightForCell = 35;
@implementation TENSearchLabelCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.clipsToBounds = YES;
    self.layer.cornerRadius = 6;
}
- (void)setKeyword:(NSString *)keyword {
    _keyword = keyword;
    _titleLabel.text = _keyword;
    [self layoutIfNeeded];
    [self updateConstraintsIfNeeded];
}
- (CGSize)sizeForCell {
    //宽度加 heightForCell 为了两边圆角。
    return CGSizeMake([_titleLabel sizeThatFits:CGSizeMake(MAXFLOAT, MAXFLOAT)].width + heightForCell/2, heightForCell);
}
@end

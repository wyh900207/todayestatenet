//
//  TENHotSearchCell.m
//  TodayEstateNet
//
//  Created by Admin on 2018/9/18.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENHotSearchCell.h"
#import "UICollectionViewLeftAlignedLayout.h"
#import "TENSearchLabelCell.h"
#import "TENUtilsHeader.h"
@interface TENHotSearchCell ()
@property (nonatomic, strong)TENSearchLabelCell   *cellId;
@end
@implementation TENHotSearchCell

+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath{
    static NSString *reuseId = @"cell2";
    TENHotSearchCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseId];
    
    if (!cell) {
       
        cell = [[TENHotSearchCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:reuseId];
       
    }
    return cell;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
//         _tagsLabelFramesM = [NSMutableArray new];
//        [self initView];
    }
    return self;
}
-(CGFloat)cellHeight
{
    NSValue *frameValue = self.tagsLabelFramesM.lastObject;
    CGRect frame = [frameValue CGRectValue];
    CGFloat maxY = CGRectGetMaxY(frame);
    return 75 + maxY;
}
-(void)setData:(NSMutableArray *)data
{
    _data = data;
    
}
-(void)initView
{
    
    self.userInteractionEnabled = YES;
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc]init];
    layout.minimumLineSpacing = 0;
    //  CGRectMake(0, 0, TENScreenWidth, 100)
    self.collectionView = [[UICollectionView alloc]initWithFrame:CGRectMake(0, 0, TENScreenWidth, [self cellHeight])  collectionViewLayout:layout];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    self.collectionView.userInteractionEnabled = YES;
    self.collectionView.showsHorizontalScrollIndicator = NO ;
    self.collectionView.scrollEnabled = YES;
    self.collectionView.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:self.collectionView];
    
    [self.collectionView registerNib:[UINib nibWithNibName:@"TENSearchLabelCell" bundle:nil] forCellWithReuseIdentifier:@"TENSearchLabelCell"];
}
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return _data.count;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 5;
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    return CGSizeZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeMake(0, 10);
}
- ( UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    TENSearchLabelCell  *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TENSearchLabelCell" forIndexPath:indexPath];
    cell.keyword = _data[indexPath.row];
    
    return cell;
}
#pragma mark  上左下右
- (UIEdgeInsets) collectionView:(UICollectionView *)collectionView
                         layout:(UICollectionViewLayout *)collectionViewLayout
         insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(2.0f, 5.0f, 2.0f, 5.0f);
}
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (_cell == nil) {
        _cell = [[NSBundle mainBundle]loadNibNamed:@"TENSearchLabelCell" owner:nil options:nil][0];
    }
    _cell.keyword = _data[indexPath.row];
    NSLog(@"%f-----%f",[_cell sizeForCell].width,[_cell sizeForCell].height);
    return [_cell sizeForCell];
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didSelectLabel:andSelectText:)]) {
        [self.delegate didSelectLabel:indexPath.row andSelectText:_data[indexPath.row]];
    }
}
@end

//
//  TENHotSearchCell.h
//  TodayEstateNet
//
//  Created by Admin on 2018/9/18.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENTableViewCell.h"
#import "TENSearchLabelCell.h"
@class  TENTagView;
@protocol TENHotSearchCellDelegate <NSObject>

-(void)didSelectLabel:(NSInteger)indexItem  andSelectText:(NSString *)text;

@end


@interface TENHotSearchCell : TENTableViewCell<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet TENTagView *tagView;

@property(nonatomic,strong) UICollectionView *collectionView;
@property (nonatomic, strong) TENSearchLabelCell *cell;
@property (nonatomic, strong) NSMutableArray *data;
/** 标签数组对应label的frame 数组 */
@property (nonatomic, strong) NSMutableArray<NSValue *> *tagsLabelFramesM;
@property (nonatomic, weak)id<TENHotSearchCellDelegate>delegate;
+ (instancetype)cellWithTableView:(UITableView *)tableView indexPath:(NSIndexPath *)indexPath;
-(CGFloat)cellHeight;
@end

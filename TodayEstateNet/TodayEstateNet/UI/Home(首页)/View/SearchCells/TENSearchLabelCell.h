//
//  TENSearchLabelCell.h
//  TodayEstateNet
//
//  Created by Admin on 2018/9/18.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TENSearchLabelCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (nonatomic, copy) NSString *keyword;
- (CGSize)sizeForCell;
@end

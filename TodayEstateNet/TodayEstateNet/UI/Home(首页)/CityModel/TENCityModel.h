//
//  TENCityModel.h
//  TodayEstateNet
//
//  Created by Admin on 2018/9/19.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TENCityModel : NSObject
/*
*  城市ID
*/
@property (nonatomic, strong) NSString *cityID;

/*
 *  城市名称
 */
@property (nonatomic, strong) NSString *cityName;

/*
 *  短名称
 */
@property (nonatomic, strong) NSString *shortName;

/*
 *  城市名称-拼音
 */
@property (nonatomic, strong) NSString *pinyin;

/*
 *  城市名称-拼音首字母
 */
@property (nonatomic, strong) NSString *initials;

@end

#pragma mark - TENCityGroup
@interface TENCityGroup : NSObject

/*
 *  分组标题
 */
@property (nonatomic, strong) NSString *groupName;

/*
 *  城市数组
 */
@property (nonatomic, strong) NSMutableArray *arrayCitys;
@end

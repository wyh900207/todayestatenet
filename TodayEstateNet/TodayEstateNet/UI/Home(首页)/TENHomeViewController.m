//
//  TENHomeViewController.m
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/9/11.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENHomeViewController.h"
#import "TENMapSearchViewController.h"
//百度定位
#import "TENLocationManager.h"
#import "TENNetwork.h"
#import "TENTableView.h"
#import "TENHomeSearchView.h"
#import "TENHomeBannerCell.h"
#import "TENHomeIconsCell.h"
// 今日头条 cell
#import "TENHomeTodayHeadlineCell.h"
#import "TENHeaderLinesViewController.h"
// 地图附近
#import "TENHomeMapNearCell.h"
// Normal salling cell
#import "TENHomeNormalSallingCell.h"
// 热门活动 cell
#import "TENHomepageActivityCell.h"
#import "TENActivityViewController.h"
// Ad cell
#import "TENHomeFirstStyleAdCell.h"
#import "TENHomeSecondStyleAdCell.h"
#import "TENHomeThirdStyleAdCell.h"
#import "TENHomeFourthStyleAdCell.h"
#import "TENHomeFifthStyleAdCell.h"
#import "TENHomeSixthStyleAdCell.h"
#import "TENHomeSeventhStyleAdCell.h"
#import "TENHomeThreeAdsCell.h"
// Section Header
#import "TENHomeSectionHeaderView.h"
// 搜索
#import "TENSearchViewController.h"
//选择城市
#import "TENSearchCityViewController.h"
#import "TENCityModel.h"
// 楼盘活动详情
#import "TENOfferHouseViewController.h"
// 楼盘详情
#import "TENHouseDetailViewController.h"
//海报
//#import "TENPostView.h"
#import "TENHomePageAdsView.h"
//条件找房
#import "TENSearchDepartmentViewController.h"

// 导航栏按
#import "UIButton+JKImagePosition.h"
#import "SearchNavigationView.h"

@interface TENHomeViewController () <UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate,TENChooseCityDelegate,NearBuildingBtnClickDelegate,YMECTableViewRefreshProtocol,TENAdserScrollviewDelegate,ActivityItemClickDelegate>

@property (nonatomic, strong) TENTableView *tableView;
@property (nonatomic, strong) NSArray *sectionHeaderTitles;
@property (nonatomic, strong) UIButton *leftButton;
// banner 数据
@property (nonatomic, strong) NSArray *topAdsObjects;
// 推荐楼盘 Section == 5
@property (nonatomic, strong) NSMutableArray *commendList;
// icons
@property (nonatomic, strong) NSMutableArray *iconList;
// 今日头条 Section == 1 && row = 0
@property (nonatomic, strong) NSMutableArray *headlineList;
// 今日头条下边的广告的样式 1 - 7
@property (nonatomic, strong) NSNumber *middleAdType;
// 今日头条下边的广告 Section == 1 && row = 1
@property (nonatomic, strong) NSMutableArray *middleAdList;
// 热门活动 Section == 2
@property (nonatomic, strong) NSMutableArray *actiityList;
// 今日优推 Section == 4
@property (nonatomic, strong) NSMutableArray *todayCommendList;
//附近地图
@property (nonatomic, strong) NSMutableDictionary *nearBuildingDic;
// 海报数据
@property (nonatomic, strong) NSMutableDictionary *posterDic;
//定位
@property (nonatomic, strong) NSString *addrString;
// 海报
@property (nonatomic, strong) TENHomePageAdsView *adsView;
// 导航栏
@property (nonatomic, strong) SearchNavigationView *customNavigationView;

@end

@implementation TENHomeViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:NO];
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:NO];
    self.navigationController.navigationBarHidden = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //[self customNaivgationBar];
    
    [self.view addSubview:self.customNavigationView];
    
    [self.customNavigationView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.view);
        make.height.equalTo(@(TENNavigationBarHeight));
    }];
    
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        //make.edges.equalTo(self.view);
        make.top.equalTo(self.customNavigationView.mas_bottom);
        make.left.bottom.right.equalTo(self.view);
    }];
    
    [self loadRequestWith:nil type:YES];
    //    [self loadRoomTypeData];
    
    // 添加海报
    [self presentAds];
}

- (void)customNaivgationBar {
    
    [self.navigationController.navigationBar cc_setGradientBGcolor:[UIColor colorWithHexString:TENThemeBlueColor]];
    
//    self.leftButton = [[ImageLeftButton alloc] initWithLeft];
    self.leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.leftButton.frame = CGRectMake(0, 0, 65, 44);
    [self.leftButton setImage:TENImage(@"arrow_down") forState:UIControlStateNormal];
    [self.leftButton setTitle:@"上海市" forState:0];
    self.leftButton.titleLabel.font = [UIFont systemFontOfSize:15.0];
    [self.leftButton setTitleColor:[UIColor colorWithHexString:TENWhiteColor] forState:0];
    [self.leftButton jk_setImagePosition:LXMImagePositionRight spacing:5.f];
    [self.leftButton setTitle:[TENLocationManager share].displayCityName forState:0];
    [self.leftButton addTarget:self action:@selector(reSelectCity) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.leftButton];
    
    ImageLeftButton *rightButton = [[ImageLeftButton alloc] initWithLeft];
    rightButton.frame = CGRectMake(0, 0, 65, 44);
    rightButton.titleLabel.font = TENFont15;
    [rightButton setImage:TENImage(@"home_map") forState:UIControlStateNormal];
    [rightButton setTitle:@"地图" forState:0];
    [rightButton setTitleColor:[UIColor colorWithHexString:TENWhiteColor] forState:0];
    [rightButton addTarget:self action:@selector(searchMap) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    
    TENHomeSearchView *searchView = [TENHomeSearchView new];
    @weakify(self);
    searchView.searchHandle = ^{
        NSLog(@"push to search controller");
        @strongtify(self);
        TENSearchViewController *searchVC = [[TENSearchViewController alloc]init];
        [self.navigationController pushViewController:searchVC animated:YES];
    };
    //searchView.frame = CGRectMake(0, 0, 300,26);
    searchView.backgroundColor = [UIColor colorWithHexString:TENWhiteColor];
    searchView.layer.cornerRadius = 13;
    searchView.layer.masksToBounds = YES;
    self.navigationItem.titleView = searchView;
}

#pragma mark---地图找房

- (void)searchMap {
//    TENMapSearchViewController *mapVC = [[TENMapSearchViewController alloc] init];
//    mapVC.locationCityID = [TENLocationManager share].cityCode;
//    [self.navigationController pushViewController:mapVC animated:YES];
    TENBaseWebViewController *mapSearch = [TENBaseWebViewController new];
    mapSearch.uri = [[TENUrlManager share] url:@"search" detail:@"map-search"];
    [mapSearch load];
    [self.navigationController pushViewController:mapSearch animated:YES];
}

- (void)reSelectCity {
    NSLog(@"Reselect city");
    
    TENSearchCityViewController *cityVC = [[TENSearchCityViewController alloc]init];
    [cityVC setDelegate:self];
    [self.navigationController pushViewController:cityVC animated:YES];
}

#pragma mark 获取首页房型查询条件接口

- (void)loadRoomTypeData {
    //    @weakify(self);
    [TENHomepageRequest requestHomeCotrollerRoomTypeData:nil response:^(NSDictionary *responseObject) {
        //        @strongtify(self);
        TEN_LOG(@"response json : %@", responseObject);
        NSDictionary *data = responseObject[@"data"];
        
        TEN_LOG(@"-------%@", data);
    } failure:^(NSError *error) {
        TEN_LOG(@"-------%@", error);
    }];
}

#pragma mark - Request

// 首页数据
- (void)loadRequestWith:(NSDictionary *)params type:(BOOL)isHeaderRefresh {
    @weakify(self);
    // 默认从 page count == 1 开始
    NSString *cityName = [TENLocationManager share].displayCityName;
    //    NSString *cityId = [TENLocationManager share].cityCode;
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValue:cityName forKey:@"cityName"];
    [parameters setValue:@(1) forKey:@"pageNo"];
    [parameters setValue:@(10) forKey:@"pageSize"];
    [parameters setValue:[TENLocationManager share].latitude forKey:@"lat"];
    [parameters setValue:[TENLocationManager share].longitude forKey:@"lng"];
    //    NSString *tempId = DL_IS_NULL(cityId);
    //    if (tempId.length > 0) {
    //        [parameters setValue:cityId forKey:@"cityId"];
    //    }
    [TENHomepageRequest fetchHomePageDataWithParameter:parameters response:^(NSDictionary *responseObject) {
        @strongtify(self);
        // 数据
        NSDictionary *content = responseObject[@"content"];
        // 推荐楼盘
        self.commendList = content[@"commendList"];
        // Banner
        self.topAdsObjects = content[@"topAds"];
        // icons
        self.iconList = content[@"iconLink"];
        // 今日头条
        self.headlineList = content[@"news"];
        // 中间广告样式
        self.middleAdType = content[@"middleAdsType"];
        // 中间广告
        self.middleAdList = content[@"middleAds"];
        // 今日优推
        self.todayCommendList = content[@"todayCommendList"];
        // 热门活动
        self.actiityList = content[@"activityList"];
        //附近地图
        self.nearBuildingDic = content[@"nearBuilding"];
        // 海报
        self.posterDic = content[@"posterAds"];
        // CityID 使用`城市名`获取数据的时候, 会返回
        NSNumber *cityID = content[@"cityId"];
        if (cityID) {
            TEN_UD_SET_VALUE(cityID, kCityCode);
            TEN_UD_SYNC
            TEN_LOG(@"%@", TEN_UD_VALUE(kCityCode));
        }
        
        if (isHeaderRefresh) {
            [self.tableView endHeaderRefreshWith:self.commendList];
        } else {
            [self.tableView endFooterRefreshWith:self.commendList];
        }
        
        [self.tableView reloadData];
    } failure:^(NSError *error) {
        TEN_LOG(@"-------%@", error);
    }];
}

#pragma mark - GYZCityPickerDelegate

- (void)cityPickerController:(TENSearchCityViewController *)chooseCityController didSelectCity:(TENCityModel *)city
{
    //发出海报
//    [self showPostViewWithCity:city.cityID];
    TEN_UD_SET_VALUE(city.cityName, kDisplayCityName);
    [self.leftButton setTitle:city.cityName forState:0];
    [self.customNavigationView.cityBtn setTitle:TEN_UD_VALUE(kDisplayCityName) forState:0];
    //    [chooseCityController popoverPresentationController];
    //    [chooseCityController dismissViewControllerAnimated:YES completion:^{
    //
    //    }];
}

- (void)cityPickerControllerDidCancel:(TENSearchCityViewController *)chooseCityController {
    //    [chooseCityController popoverPresentationController];
    //    [chooseCityController dismissViewControllerAnimated:YES completion:^{
    //
    //    }];
}

#pragma mark - UITableViewDelegate, UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 6;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger rows = 0;
    switch (section) {
        case 0:
            rows = 2;
            break;
        case 1:
            rows = self.middleAdList.count > 1 ? 2 : 1;
            break;
        case 4:
            rows = self.todayCommendList.count;
            break;
        case 5:
            rows = self.tableView.dataSourceArray.count;
            break;
        default:
            rows = 1;
            break;
    }
    return rows;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    // 泛型接收者
    UITableViewCell *cell = nil;
    // 各种样式cell
    TENHomeBannerCell *bannerCell = [tableView dequeueReusableCellWithIdentifier:@"banner" forIndexPath:indexPath];
    TENHomeIconsCell *iconsCell = [tableView dequeueReusableCellWithIdentifier:@"icons" forIndexPath:indexPath];
    TENHomeNormalSallingCell *sallingCell = [tableView dequeueReusableCellWithIdentifier:@"sale" forIndexPath:indexPath];
    TENHomeTodayHeadlineCell *headlineCell = [tableView dequeueReusableCellWithIdentifier:@"headline" forIndexPath:indexPath];
    //附近地图
    TENHomeMapNearCell *nearCell = [tableView dequeueReusableCellWithIdentifier:@"near" forIndexPath:indexPath];
    TENHomeSecondStyleAdCell *secondStyleAdCell = [tableView dequeueReusableCellWithIdentifier:@"second" forIndexPath:indexPath];
    TENHomeThirdStyleAdCell *thirdStyleAdCell = [tableView dequeueReusableCellWithIdentifier:@"third" forIndexPath:indexPath];
    TENHomeSeventhStyleAdCell *seventhStyleAdCell = [tableView dequeueReusableCellWithIdentifier:@"seventh" forIndexPath:indexPath];
    TENHomepageActivityCell *activityCell = [tableView dequeueReusableCellWithIdentifier:@"activity" forIndexPath:indexPath];
    TENHomeThreeAdsCell *threeAdsCell = [tableView dequeueReusableCellWithIdentifier:@"threeads" forIndexPath:indexPath];
    
    switch (indexPath.section) {
        case 0: // banner 和 icons
            if (indexPath.row == 0) {
                bannerCell.adsObjects = self.topAdsObjects;
                cell = bannerCell;
            } else {
                iconsCell.iconObjectes = self.iconList;
                @weakify(self);
                iconsCell.clickItemHandler = ^(NSIndexPath *indexPath, NSDictionary *object) {
                    @strongtify(self);
                    NSString *iconName = object[@"iconName"];
                    if ([iconName isEqualToString:@"地图找房"]){
                        TENMapSearchViewController *msVC = [TENMapSearchViewController new];
                        msVC.locationCityID = [TENLocationManager share].cityCode;
                        [self.navigationController pushViewController:msVC animated:YES];
                    } else {
                        TENBaseWebViewController *webController = [TENBaseWebViewController new];
                        NSString *uri = object[@"linkUrl"];
                        if ([uri isActive]) {
                            webController.uri = uri;
                            [self.navigationController pushViewController:webController animated:YES];
                            [webController load];
                        }
                    }
                };
                cell = iconsCell;
            }
            break;
        case 1:
            if (indexPath.row == 0) {
                headlineCell.news = self.headlineList;
                headlineCell.adsersScrollview.delegate = self;
                cell = headlineCell;
            } else {
                //                NSNumber *parternType = self.middleAdType;
                switch (self.middleAdList.count) {
                    case 2: {
                        secondStyleAdCell.advertisementImageObjects = self.middleAdList;
                        cell = secondStyleAdCell;
                    }
                        break;
                    case 3: {
                        thirdStyleAdCell.advertisementImageObjects = self.middleAdList;
                        cell = thirdStyleAdCell;
                    }
                        break;
                    case 4: {
                        seventhStyleAdCell.advertisementImageObjects = self.middleAdList;
                        cell = seventhStyleAdCell;
                    }
                        break;
                    default: {
                        //2.3.4个广告外不显示广告cell
                        seventhStyleAdCell.advertisementImageObjects = self.middleAdList;
                        cell = secondStyleAdCell;
                        
                    }
                        break;
                }
            }
            break;
        case 2: // 轮播广告(热门活动)
            activityCell.activityObjects = self.actiityList;
            activityCell.delegate = self;
            cell = activityCell;
            break;
        case 3: // 附近地图
            nearCell.nearBuildingDic = self.nearBuildingDic;
            nearCell.delegate = self;
            cell = nearCell;
            break;
        case 4: {// 今日优推
            sallingCell.dataObject = self.todayCommendList[indexPath.row];
            cell = sallingCell;
//            cell = threeAdsCell;
        }
            break;
            
        default: // 推荐楼盘
            sallingCell.dataObject = self.tableView.dataSourceArray[indexPath.row];
            cell = sallingCell;
            break;
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    TENHouseDetailViewController *detailController = [TENHouseDetailViewController new];
    if (indexPath.section == 3) {//地图附近
        detailController.houseId = self.nearBuildingDic[@"id"];
        [self.navigationController pushViewController:detailController animated:YES];
    }else if (indexPath.section == 4) {//推荐楼盘
        @weakify(self);
        [[DLUserManager shareManager] actionAfterLogin:^{
            @strongtify(self);
            NSDictionary *cellObject = self.todayCommendList[indexPath.row];
            detailController.houseId = cellObject[@"id"];
            detailController.lat = cellObject[@"lat"];
            detailController.lng = cellObject[@"lng"];
            [self.navigationController pushViewController:detailController animated:YES];
        } currentController:self];
    }else if (indexPath.section == 5 ) {
        @weakify(self);
        [[DLUserManager shareManager] actionAfterLogin:^{
            @strongtify(self);
            // 楼盘详情
            NSDictionary *cellObject = self.tableView.dataSourceArray[indexPath.row];
            detailController.houseId = cellObject[@"id"];
            detailController.lat = cellObject[@"lat"];
            detailController.lng = cellObject[@"lng"];
            //        detailController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:detailController animated:YES];
        } currentController:self];
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section != 0) {
        TENHomeSectionHeaderView *sectionHeaderView = [TENHomeSectionHeaderView new];
        sectionHeaderView.frame = CGRectMake(0, 0, TENScreenWidth, 60);
        sectionHeaderView.titlLabel.text = self.sectionHeaderTitles[section];
        if (section == 1) {
            [sectionHeaderView.moreButton addTarget:self action:@selector(pushToHeaderLineVC) forControlEvents:UIControlEventTouchUpInside];
        }else if (section == 2){
            [sectionHeaderView.moreButton addTarget:self action:@selector(pushToActivityVC) forControlEvents:UIControlEventTouchUpInside];
        }else if (section == 3) {
            [sectionHeaderView.moreButton setImage:[UIImage imageNamed:@"refresh"] forState:normal];
            [sectionHeaderView.moreButton addTarget:self action:@selector(locationNow) forControlEvents:UIControlEventTouchUpInside];
            [sectionHeaderView.moreButton setTitle:@"" forState:normal];
            sectionHeaderView.rightLabel.text = self.addrString;
        }else if (section == 4) {
            [sectionHeaderView.moreButton setTitle:@"" forState:normal];
        }
        return sectionHeaderView;
    } else {
        UIView *sectionHeaderView = [UIView new];
        sectionHeaderView.frame = CGRectMake(0, 0, TENScreenWidth, CGFLOAT_MIN);
        return sectionHeaderView;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *sectionFooterView = [UIView new];
    sectionFooterView.frame = CGRectMake(0, 0, TENScreenWidth, CGFLOAT_MIN);
    return sectionFooterView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    CGFloat sectionHeaderHeight = section == 0 ? 0.01 : 60;
    return sectionHeaderHeight;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}
- (void)nearBuildingSearchBtnClicked{
    TENMapSearchViewController *mapVC = [TENMapSearchViewController new];
    mapVC.locationCityID = [TENLocationManager share].cityCode;
    [self.navigationController pushViewController:mapVC animated:YES];
}
#pragma mark - Getter

- (SearchNavigationView *)customNavigationView {
    if (!_customNavigationView) {
        _customNavigationView = [SearchNavigationView new];
        @weakify(self);
        _customNavigationView.cityButtonHandler = ^(SearchNavigationView *view) {
            @strongtify(self);
            [self reSelectCity];
        };
        _customNavigationView.mapButtonHandler = ^(SearchNavigationView *view) {
            @strongtify(self);
            [self searchMap];
        };
        _customNavigationView.clickedSearch = ^(SearchNavigationView *view) {
            @strongtify(self);
            TENSearchViewController *searchVC = [[TENSearchViewController alloc]init];
            [self.navigationController pushViewController:searchVC animated:YES];
        };
    }
    return _customNavigationView;
}

- (TENTableView *)tableView {
    if (!_tableView) {
        _tableView = [[TENTableView alloc] initWithFrame:CGRectZero style:UITableViewStyleGrouped];
        _tableView.refreshType = TENTableViewRefreshTypeBoth;
        _tableView.refreshDelegate = self;
        _tableView.backgroundColor = TENHexColor(TENWhiteColor);
        _tableView.separatorColor = TENHexClearColor;
        _tableView.estimatedRowHeight = 44.f;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerNib:[UINib nibWithNibName:@"TENHomeBannerCell" bundle:nil] forCellReuseIdentifier:@"banner"];
        [_tableView registerNib:[UINib nibWithNibName:@"TENHomeIconsCell" bundle:nil] forCellReuseIdentifier:@"icons"];
        [_tableView registerNib:[UINib nibWithNibName:@"TENHomeNormalSallingCell" bundle:nil] forCellReuseIdentifier:@"sale"];
        [_tableView registerNib:[UINib nibWithNibName:@"TENHomeTodayHeadlineCell" bundle:nil] forCellReuseIdentifier:@"headline"];
        [_tableView registerNib:[UINib nibWithNibName:@"TENHomeMapNearCell" bundle:nil] forCellReuseIdentifier:@"near"];
        [_tableView registerNib:[UINib nibWithNibName:@"TENHomeFirstStyleAdCell" bundle:nil] forCellReuseIdentifier:@"first"];
        [_tableView registerNib:[UINib nibWithNibName:@"TENHomeSecondStyleAdCell" bundle:nil] forCellReuseIdentifier:@"second"];
        [_tableView registerNib:[UINib nibWithNibName:@"TENHomeThirdStyleAdCell" bundle:nil] forCellReuseIdentifier:@"third"];
        [_tableView registerNib:[UINib nibWithNibName:@"TENHomeFourthStyleAdCell" bundle:nil] forCellReuseIdentifier:@"fourth"];
        [_tableView registerNib:[UINib nibWithNibName:@"TENHomeFifthStyleAdCell" bundle:nil] forCellReuseIdentifier:@"fifth"];
        [_tableView registerNib:[UINib nibWithNibName:@"TENHomeSixthStyleAdCell" bundle:nil] forCellReuseIdentifier:@"sixth"];
        [_tableView registerNib:[UINib nibWithNibName:@"TENHomeSeventhStyleAdCell" bundle:nil] forCellReuseIdentifier:@"seventh"];
        [_tableView registerNib:[UINib nibWithNibName:@"TENHomepageActivityCell" bundle:nil] forCellReuseIdentifier:@"activity"];
        [_tableView registerClass:[TENHomeThreeAdsCell class] forCellReuseIdentifier:@"threeads"];
        [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"notMatch"];
    }
    return _tableView;
}

- (NSArray *)sectionHeaderTitles {
    if (!_sectionHeaderTitles) {
        _sectionHeaderTitles = @[@"", @"今日头条", @"热门活动", @"附近地图", @"今日优推", @"推荐楼盘"];
    }
    return _sectionHeaderTitles;
}

- (TENHomePageAdsView *)adsView {
    if (!_adsView) {
        _adsView = [[TENHomePageAdsView alloc] initWithFrame:self.view.bounds];
//        _adsView.adsImageURL = @"http://h5ip.cn/nIYx";
        @weakify(self);
        _adsView.openHandler = ^{
            TEN_LOG(@"navigate to ads");
            @strongtify(self);
            [self closeAds];
            TENBaseWebViewController *controller = [TENBaseWebViewController new];
            NSString *uri = self.posterDic[@"linkUrl"];
            if (uri && [uri isActive]) {
                controller.uri = uri;
                [self.navigationController pushViewController:controller animated:YES];
            }
        };
        _adsView.closeHandler = ^{
            TEN_LOG(@"close ads");
            @strongtify(self);
            [self.adsView removeFromSuperview];
        };
    }
    return _adsView;
}

- (void)pushToHeaderLineVC{
    TENHeaderLinesViewController *hlVC = [TENHeaderLinesViewController new];
    [self.navigationController pushViewController:hlVC animated:YES];
}

- (void)pushToActivityVC{
    TENActivityViewController *aVC = [TENActivityViewController new];
    [self.navigationController pushViewController:aVC animated:YES];
}

#pragma mark - Setter

- (void)setPosterDic:(NSMutableDictionary *)posterDic {
    _posterDic = posterDic;
    if (self.posterDic[@"picPath"]) {
        self.adsView.adsImageURL = self.posterDic[@"picPath"];
    }
}

#pragma mark - YMECTableViewRefreshProtocol

- (void)headerWillRefresh {
    [self loadRequestWith:nil type:YES];
}

- (void)footerWillRefresh {
    [self loadRequestWith:nil type:NO];
}

#pragma mark-------------TENAdserScrollviewDelegate

- (void)advertScrollView:(TENAdserScrollview *)advertScrollView didSelectedItemAtIndex:(NSInteger)index{
    NSDictionary *new = self.headlineList[index];
    
    TENBaseWebViewController *webviewController = [TENBaseWebViewController new];
    NSString *contentURL = [[TENUrlManager share] url:@"recommend" detail:@"headlines"];
    webviewController.uri = [contentURL stringByAppendingPathComponent:[new[@"id"] stringValue]];
    [webviewController load];
    
    [self.navigationController pushViewController:webviewController animated:YES];
}

#pragma mark-------------ActivityItemClickDelegate

- (void)activityItemClicked:(NSInteger)index{
    NSString *activityId = [NSString stringWithFormat:@"%@",self.actiityList[index][@"id"]];
    NSString *contentURL = [[TENUrlManager share] url:@"recommend" detail:@"activity-building-detail"];
    TENBaseWebViewController *web = [TENBaseWebViewController new];
    web.uri = [contentURL stringByAppendingPathComponent:activityId];
    web.navigationItem.title = @"优惠活动详情";
    [web load];
    [self.navigationController pushViewController:web animated:YES];
}

#pragma mark - 定位

- (void)locationNow {
    
}

#pragma mark - Private

- (void)presentAds {
    [self.view addSubview:self.adsView];
}

- (void)closeAds {
    [self.adsView removeFromSuperview];
}

@end

//
//  TENMapSearchViewController.h
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/9/11.
//  Copyright © 2018年 TEN. All rights reserved.
//
//地图缩放级别
#define Section_11        11
#define Section_13        13
#define Section_14        14
#define Section_15        15
#define Section_16        16
#import "TENMapSearchViewController.h"
//#import "TENUtilsHeader.h"
#import "TENHomeNormalSallingCell.h"
#import "TENHomeSearchView.h"
#import "TENSearchViewController.h"
#import "TENCustomSelectionMenu.h"
#import "TENCustomSelectRoomView.h"
#import "TENdistrictView.h"
#import "TENSearchPageRequest.h"
#import "SearchData.h"
#import <MJExtension.h>

#import "TENpriceView.h"
#import "TENmodelTypeView.h"
#import "TENMoreView.h"
#import "TENorderByView.h"
#import "SearchDetail.h"
#import "TENTableView.h"

#import "MapSearchModel.h"
#import "DistrictMapModel.h"
#import "TENHouseDetailModel.h"
#import "TENHouseDetailViewController.h"

#import "TENAnnotationView1.h"
#import "TENAnnoTationView2.h"
#import "TENMapBottomView.h"
#import "TENHouseDetailViewController.h"
@interface TENMapSearchViewController () <UITableViewDelegate, UITableViewDataSource,YMECTableViewRefreshProtocol,BMKMapViewDelegate,TENBottomViewSelectedDelegate>
{
    NSInteger _neededZoomLevel;//聚合层级条件
}
@property (nonatomic, strong) TENTableView *tableview;
@property (nonatomic, strong) ImageLeftButton *leftButton;
@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic, strong) TENCustomSelectionMenu *menuView;
@property (nonatomic, strong) NSMutableArray <SearchDetail *>*detaiDataArr;
//默认参数
@property (nonatomic, strong) NSDictionary *condition;
//mapView
@property (nonatomic, strong) BMKMapView *mapView;
/**传城市id返回的数组*/
@property (nonatomic, strong) NSMutableArray * cityIdArray;
/**传区域id返回的数组*/
@property (nonatomic, strong) NSMutableArray * districtIdArray;
/**mapDataArray*/
@property (nonatomic, strong) NSMutableArray * mapDataArray;
/**底部弹出房源view*/
@property (nonatomic, strong) TENMapBottomView * mapBottomView;

@end

@implementation TENMapSearchViewController

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    [self setupSubviews];
    [self requestMapDataWithParameter:@{@"cityId":[TENLocationManager share].cityCode}];
    [self requestDataWithCityId:[TENLocationManager share].cityCode];
    
    
    
    
//    [self requestDataWxithCityId:self.locationCityID];
//    [self requestMapDataWithParameter:@{@"cityId":[TENLocationManager share].cityCode}];
    
//    [self requestMapDataWithParameter:@{@"districtId":@"310117"}];
}
- (BMKMapView *)mapView{
    if (!_mapView) {
        _mapView = [[BMKMapView alloc] initWithFrame:CGRectMake(0, 44, TENScreenWidth, TENScreenHeight-44)];
        _mapView.minZoomLevel = 8.f;
        _mapView.maxZoomLevel = 18.f;
    }
    return _mapView;
}
#pragma mark - UI

- (void)initUI{
    self.title = @"地图找房";
    [self.view addSubview:self.mapView];
    //设置mapView代理
    _mapView.delegate = self;
    _mapView.zoomLevel = Section_11;
    _neededZoomLevel = Section_11;
    //设置中心点
    double latitude = [[TENLocationManager share].latitude doubleValue];
    double longitude = [[TENLocationManager share].longitude doubleValue];
    [_mapView setCenterCoordinate:CLLocationCoordinate2DMake(latitude, longitude) animated:YES];
    
    
}
//初始化标注
- (void)createAnnotationWithData:(NSArray *)dataArray{
    
    [self.mapView removeAnnotations:self.mapDataArray];
    if (self.mapDataArray.count > 0) {
        [self.mapDataArray removeAllObjects];
    }

    if ([dataArray[0] isKindOfClass:[MapSearchModel class]]) {//设置城市级标注
        
        for (int i = 0; i < dataArray.count; i++) {
            //初始化标注类BMKPointAnnotation的实例
            MapSearchModel *annotation = [[MapSearchModel alloc] init];
            //设置标注的经纬度坐标
            MapSearchModel *model = dataArray[i];
            float lat = [model.latitude floatValue];
            float lgt = [model.longitude floatValue];
            CLLocationCoordinate2D coor = {lat,lgt};
            annotation.coordinate = coor;
            annotation.title = [NSString stringWithFormat:@"%@\n%@个", model.districtName,model.countNum];
            annotation.districtId = model.districtId;
            [self.mapDataArray addObject:annotation];
        }
        [self.mapView addAnnotations:self.mapDataArray];
    }else{//设置区域级标注

        for (int i = 0; i < dataArray.count; i++) {
            //初始化标注类BMKPointAnnotation的实例
            DistrictMapModel *annotation = [[DistrictMapModel alloc] init];
            //设置标注的经纬度坐标
            DistrictMapModel *model = dataArray[i];
            float lat = [model.lat floatValue];
            float lgt = [model.lng floatValue];
            CLLocationCoordinate2D coor = {lat,lgt};
            annotation.coordinate = coor;
            annotation.title = [NSString stringWithFormat:@"%@\n%.1f万元/㎡", model.name,[model.price floatValue]/10000];
            annotation.saleType = model.saleType;
            annotation.logo = model.logo;
            annotation.name = model.name;
            annotation.roomArea = model.roomArea;
            annotation.roomType = model.roomType;
            annotation.provinceName = model.provinceName;
            annotation.cityName = model.cityName;
            annotation.distance = model.distance;
            annotation.price = model.price;
            annotation.totalPrice = model.totalPrice;
            annotation.tags = model.tags;
            annotation.ID = model.ID;
            
            [self.mapDataArray addObject:annotation];
        }
        [self.mapView addAnnotations:self.mapDataArray];
    }
    
}
- (void)getAreaMainDataWithZoomLevel:(NSInteger)zoomLevel isDidSelect:(BOOL)isDidSelect{
    if (isDidSelect) {
        [self.mapView setZoomLevel:zoomLevel];
    }
    if (zoomLevel == Section_11) {//获取城市级标注
        [self requestMapDataWithParameter:@{@"cityId":[TENLocationManager share].cityCode}];
    }else if (zoomLevel == Section_14) {//获取区域级标注
        double lat = self.mapView.centerCoordinate.latitude;
        double lng = self.mapView.centerCoordinate.longitude;
        
        [self requestMapDataWithParameter:@{
                                            @"lat":[NSString stringWithFormat:@"%f",lat],
                                            @"lng":[NSString stringWithFormat:@"%f",lng],
                                            @"distance":@"100000"
                                            }];
    }
}
- (void)setupSubviews {
    NSMutableArray *items = [NSMutableArray array];
    
    //区域
    TENMenuItem *areaItem = [[TENMenuItem alloc] init];
    areaItem.title = @"区域";
    TENdistrictView *areaView = [[TENdistrictView alloc] init];
    __weak typeof(self) weakSelf = self;
    areaView.dismissView = ^(BOOL sure, NSDictionary *condition) {
        [weakSelf.menuView dismissAll:self.menuView.backgroundView];
        if (sure) {
            //请求筛选
            [self findWithCondition:condition];
        }
    };
    
    [areaView updateUIWith:self.dataArr[0]];
    areaItem.contentView = areaView;
    [items addObject:areaItem];
    //价格
    TENMenuItem *priceItem = [[TENMenuItem alloc] init];
    priceItem.title = @"价格";
    TENpriceView *priceView = [[TENpriceView alloc] init];
    priceView.dismissView = ^(BOOL sure,NSDictionary *condition) {
        [weakSelf.menuView dismissAll:self.menuView.backgroundView];
        if (sure) {
            [self findWithCondition:condition];
        }
    };
    [priceView updateUIWith:_dataArr[1]];
    priceItem.contentView = priceView;
    [items addObject:priceItem];
    //户型
    TENMenuItem *typeItem = [[TENMenuItem alloc] init];
    typeItem.title = @"户型";
    TENmodelTypeView *typeView = [[TENmodelTypeView alloc] init];
    typeView.dismissView = ^(BOOL sure,NSDictionary *conditon) {
        [weakSelf.menuView dismissAll:self.menuView.backgroundView];
        if (sure) {
            [self findWithCondition:conditon];
        }
    };
    [typeView updateUIWith:_dataArr[2]];
    typeItem.contentView = typeView;
    [items addObject:typeItem];
    //更多
    TENMenuItem *moreItem = [[TENMenuItem alloc] init];
    moreItem.title = @"更多";
    TENMoreView *moreView = [[TENMoreView alloc] init];
    moreView.dismissView = ^(BOOL sure,NSDictionary *condition) {
        [weakSelf.menuView dismissAll:self.menuView.backgroundView ];
        if (sure) {
            [self findWithCondition:condition];
        }
    };
    [moreView updateUIWith:_dataArr[3]];
    moreItem.contentView = moreView;
    [items addObject:moreItem];

    self.menuView = [[TENCustomSelectionMenu alloc] initWithItems:items];
    
    [self.view addSubview:self.menuView];
    
    [self.menuView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(self.mas_topLayoutGuide);
        make.height.equalTo(@(44.f));
    }];
    
}
#pragma mark ----mapView delegate
- (BMKAnnotationView *)mapView:(BMKMapView *)mapView viewForAnnotation:(id<BMKAnnotation>)annotation{
    
    if ([annotation isKindOfClass:[MapSearchModel class]] ) {
        if (_neededZoomLevel == Section_11) {//显示圆形标注点
            static NSString *customReuseIndetifier = @"TENAnnotationView1";
            TENAnnotationView1 *annotationView = (TENAnnotationView1 *)[mapView dequeueReusableAnnotationViewWithIdentifier:customReuseIndetifier];
            MapSearchModel *model = (MapSearchModel *)annotation;
            annotationView.annotation = model;
            
            if (annotationView == nil){
                annotationView = [[TENAnnotationView1 alloc] initWithAnnotation:annotation reuseIdentifier:customReuseIndetifier];
                annotationView.canShowCallout = NO;
                annotationView.draggable = YES;
                annotationView.name = annotation.title;
            }else{
                annotationView.name = annotation.title;
            }
            return annotationView;
        }
    }else{
            static NSString *customRectangleView = @"TENAnnotationView2";
            TENAnnotationView2 *rectangleView = (TENAnnotationView2 *)[mapView dequeueReusableAnnotationViewWithIdentifier:customRectangleView];
        DistrictMapModel *model = (DistrictMapModel *)annotation;
        rectangleView.annotation = model;
            if (rectangleView == nil){
                rectangleView = [[TENAnnotationView2 alloc] initWithAnnotation:annotation reuseIdentifier:customRectangleView];
                rectangleView.canShowCallout = NO;
                rectangleView.draggable = YES;
                
                rectangleView.name = annotation.title;
            }else{
                rectangleView.name = annotation.title;
            }
    
            rectangleView.portrait = [UIImage imageNamed:@"loc_off"];
            
            
            return rectangleView;
        }
    return nil;
}


- (void)mapView:(BMKMapView *)mapView didSelectAnnotationView:(BMKAnnotationView *)view{

    if ([view isKindOfClass:[TENAnnotationView1 class]]) {
        TENAnnotationView1 *cusView = (TENAnnotationView1 *)view;
        CLLocationCoordinate2D coorinate = [cusView.annotation coordinate];

        [self.mapView setCenterCoordinate:coorinate animated:YES];//设置中心点位置
        if (mapView.zoomLevel < Section_13) {
            _neededZoomLevel = Section_14;
            [self getAreaMainDataWithZoomLevel:_neededZoomLevel isDidSelect:YES];
        }
    }else{
        TENAnnotationView2 *rectangleView =(TENAnnotationView2 *)view;
        CLLocationCoordinate2D coorinate = [rectangleView.annotation coordinate];
        self.mapView.frame = CGRectMake(0, 44, TENScreenWidth, TENScreenHeight-60-TENNavigationBarHeight-44);
        [self.mapView setCenterCoordinate:coorinate animated:YES];//设置中心点位置
        DistrictMapModel *disModel = (DistrictMapModel *)rectangleView.annotation;
        [self.mapBottomView reloadDataWithData:[disModel mj_keyValues]];
        [self.view addSubview:self.mapBottomView];
        [self.mapBottomView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.equalTo(self.view);
            make.height.mas_equalTo(200);
        }];
        
    }
}

- (void)mapView:(BMKMapView *)mapView regionDidChangeAnimated:(BOOL)animated{

    NSLog(@"标注点个数 ====%@",mapView.annotations);
        switch (_neededZoomLevel) {
            case Section_11://城市级
                {
                    if (mapView.zoomLevel<Section_13) {//当前设置的缩放级别为11时，如果真实缩放级别小于13，不处理
                    }else{
                        if (mapView.zoomLevel>=Section_13) {
                            //当前设置的缩放级别为11时，如果真实缩放级别满足(13,15]，则设置
                            _neededZoomLevel = Section_14;
                        }
                        [self getAreaMainDataWithZoomLevel:_neededZoomLevel isDidSelect:NO];
                }
                break;
                case Section_14://区域级
            {
                //当前设置的缩放级别为14时，如果真实缩放级别(13,15]，不处理
                if (mapView.zoomLevel > Section_13 && mapView.zoomLevel <= 15) {
//
                }else{
                    if (mapView.zoomLevel <= Section_13){
                        _neededZoomLevel = Section_11;
                        
                    }else{
                        _neededZoomLevel = Section_14;
                    }
                    [self getAreaMainDataWithZoomLevel:_neededZoomLevel isDidSelect:NO];
                }
            }
                break;
            default:
                break;
        }
    }
}

- (void)mapView:(BMKMapView *)mapView onClickedMapBlank:(CLLocationCoordinate2D)coordinate{
    [self.mapBottomView removeFromSuperview];
    self.mapView.frame = CGRectMake(0, 44, TENScreenWidth, TENScreenHeight-TENNavigationBarHeight);
    [mapView setCenterCoordinate:coordinate animated:YES];
    self.mapBottomView = nil;
}
#pragma mark - Request
//请求地图数据
- (void)requestMapDataWithParameter:(NSDictionary *)params{
    [TENSearchPageRequest requestFindDataWithURL:[NSString stringWithFormat:@"%@",kSearchMapListURL] Condition:params success:^(NSDictionary * _Nonnull responseObject) {
        NSLog(@"请求地图数据返回值-=-=-%@",responseObject);
        if ([responseObject[@"code"] integerValue] == 1) {
            
            if ([params.allKeys[0] isEqualToString:@"cityId"]) {//请求城市级数据
                [self.cityIdArray removeAllObjects];
                for (NSDictionary *dic in responseObject[@"content"]) {
                    MapSearchModel *mapSearchModel = [MapSearchModel mj_objectWithKeyValues:dic];
                    [self.cityIdArray addObject:mapSearchModel];
                }
                [self createAnnotationWithData:self.cityIdArray];
            }else if ([params.allKeys[0] isEqualToString:@"districtId"]){//请求区域级数据
                [self.districtIdArray removeAllObjects];
                for (NSDictionary *dic in responseObject[@"content"]) {
                    DistrictMapModel *sameModel = [DistrictMapModel mj_objectWithKeyValues:dic];
                    [self.districtIdArray addObject:sameModel];
                }
                [self createAnnotationWithData:self.districtIdArray];
            }else{//传中心点和距离获取数据
                [self.districtIdArray removeAllObjects];
                for (NSDictionary *dic in responseObject[@"content"]) {
                    DistrictMapModel *districtMapModel = [DistrictMapModel mj_objectWithKeyValues:dic];
                    [self.districtIdArray addObject:districtMapModel];
                }
                [self createAnnotationWithData:self.districtIdArray];
            }
        }
        
        
    } failure:^(NSError * _Nonnull error) {
        NSLog(@"%@",error);
    }];
}

//请求排序数据
- (void)requestDataWithCityId:(NSString *)cityId{
    //请求筛选条件
    [TENSearchPageRequest requestSearchPageDataWithParameters:@{@"cityIds":cityId,@"lat":[TENLocationManager share].latitude,@"lng":[TENLocationManager share].longitude} success:^(NSDictionary * _Nonnull responseObject) {
        NSString *code = responseObject[@"code"];
        if ([code integerValue] == 1) {
            self.dataArr = [NSMutableArray array];
            for (NSDictionary *dict in responseObject[@"content"]) {
                SearchData *tem = [SearchData mj_objectWithKeyValues:dict];
                [self.dataArr addObject:tem];
            }
            [self setupSubviews];
            //默认数据
//            [self findWithCondition:self.condition];
        }
    } failure:^(NSError * _Nonnull error) {
        
    }];
}
//根据条件筛选
- (void)findWithCondition:(NSDictionary *)condition{
//    self.tableview
    self.condition = condition;
    [TENSearchPageRequest requestFindDataWithCondition:condition success:^(NSDictionary * _Nonnull responseObject) {
        NSLog(@"筛选结果：%@",responseObject);
        NSString *code = responseObject[@"code"];
        if ([code integerValue] == 1) {
            self.detaiDataArr = [NSMutableArray array];
            for (NSDictionary *dic in responseObject
                 [@"content"]) {
                SearchDetail *tem = [SearchDetail mj_objectWithKeyValues:dic];
                [self.detaiDataArr addObject:tem];
            }
//            [self.tableview endHeaderRefresh];
        }
//        [self.tableview reloadData];
        self->_neededZoomLevel = Section_14;
        [self getAreaMainDataWithZoomLevel:self->_neededZoomLevel isDidSelect:YES];
    } failure:^(NSError * _Nonnull error) {
        [self.tableview endHeaderRefresh];
    }];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //当mapView即将被显示的时候调用，恢复之前存储的mapView状态
    [_mapView viewWillAppear];
}
- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    //当mapView即将被隐藏的时候调用，存储当前mapView的状态
    [_mapView viewWillDisappear];
}

#pragma mark - Private action

- (void)reSelectCity:(UIButton *)button {
    
}
#pragma mark - 下拉刷新

- (void)headerWillRefresh{
//    [self findWithCondition:self.condition];
}

#pragma mark - UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.detaiDataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TENHomeNormalSallingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"search" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.data = self.detaiDataArr[indexPath.row];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    TENHouseDetailViewController *vc = [TENHouseDetailViewController new];
    vc.houseId = self.detaiDataArr[indexPath.row].detailId;
    [self.navigationController pushViewController:vc animated:YES];
}
#pragma mark-------------TENBottomViewSelectedDelegate
- (void)switchToBuildingDetailVCWithId:(NSString *)houseId{
    TENHouseDetailViewController *detailVC = [[TENHouseDetailViewController alloc] init];
    detailVC.houseId = houseId;
    [self.navigationController pushViewController:detailVC animated:YES];
}
#pragma mark - Getter

- (UITableView *)tableview {
    if (!_tableview) {
        _tableview = [[TENTableView alloc]initWithFrame:CGRectMake(0, 46, TENScreenWidth, TENScreenHeight - 46) style:UITableViewStylePlain];
        _tableview.refreshType = TENTableViewRefreshTypeHeader;
        _tableview.refreshDelegate = self;
        _tableview.scrollEnabled = YES;
        _tableview.estimatedRowHeight = 80.f;
        _tableview.rowHeight = UITableViewAutomaticDimension;
        _tableview.backgroundColor = TENHexColor(TENWhiteColor);
        _tableview.separatorColor = TENHexClearColor;
        _tableview.delegate = self;
        _tableview.dataSource = self;
        [_tableview registerNib:[UINib nibWithNibName:@"TENHomeNormalSallingCell" bundle:nil] forCellReuseIdentifier:@"search"];
    }
    return _tableview;
}

-(NSMutableArray *)detaiDataArr{
    if (!_detaiDataArr) {
        _detaiDataArr = [NSMutableArray array];
    }
    return _detaiDataArr;
}

- (NSDictionary *)condition{
    if (!_condition) {
        TENLocationManager *locatoin = [TENLocationManager share];
        _condition = @{
                       @"cityIds" : locatoin.cityCode,
                       @"lat" : locatoin.latitude.stringValue,
                       @"lng" : locatoin.longitude.stringValue,
                       @"pageNo" : @1,
                       @"pageSize" : @20
                       };
    }
    return _condition;
}
- (NSMutableArray *)cityIdArray{
    if (!_cityIdArray) {
        _cityIdArray = [NSMutableArray array];
    }
    return _cityIdArray;
}

- (NSMutableArray *)districtIdArray{
    if (!_districtIdArray) {
        _districtIdArray = [NSMutableArray array];
    }
    return _districtIdArray;
}
- (NSMutableArray *)mapDataArray{
    if (!_mapDataArray) {
        _mapDataArray = [NSMutableArray array];
    }
    return _mapDataArray;
}
- (TENMapBottomView *)mapBottomView{
    if (!_mapBottomView) {
        _mapBottomView = [TENMapBottomView TENMapBottomView];
        _mapBottomView.delegate = self;
    }
    return _mapBottomView;
}
@end

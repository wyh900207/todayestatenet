//
//  TENSearchCityViewController.m
//  TodayEstateNet
//
//  Created by Admin on 2018/9/19.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENSearchCityViewController.h"
//#import "TENUtilsHeader.h"
#import "TENLocationManager.h"
#import <CoreLocation/CoreLocation.h>
//city  model
#import "TENCityModel.h"

//City  item
#import "TENCityGroupCell.h"
//City  头部视图
#import "TENCityHeaderView.h"
#import "TENSearchNavigationBar.h"

// 网络
#import "TENSearchPageRequest.h"

@interface TENSearchCityViewController ()<UISearchBarDelegate,UITableViewDelegate,UITableViewDataSource,TENCityGroupCellDelegate,CLLocationManagerDelegate>

@property (nonatomic,strong)UISearchBar *searchBar;
@property (nonatomic,strong)UITableView *tableView;
/**
 *  记录所有城市信息，用于搜索
 */
@property (nonatomic, strong) NSMutableArray *recordCityData;
/**
 *  定位城市
 */
@property (nonatomic, strong) NSMutableArray *localCityData;
/**
 *  热门城市
 */
@property (nonatomic, strong) NSMutableArray *hotCityData;
/**
 *  最近访问城市
 */
@property (nonatomic, strong) NSMutableArray *commonCityData;
@property (nonatomic, strong) NSMutableArray *arraySection;
/**
 *  是否是search状态
 */
@property(nonatomic, assign) BOOL isSearch;

/** 分区中心动画label */
@property (strong, nonatomic) UILabel *sectionTitle;
/** 是否开始拖拽 */
@property (assign, nonatomic, getter=isBegainDrag) BOOL begainDrag;

/**
 *  搜索城市列表
 */
@property (nonatomic, strong) NSMutableArray *searchCities;
@property (nonatomic, strong) NSString    *locationCity;
@property (nonatomic, retain) CLLocationManager *locationManager;
@property (nonatomic, strong) TENSearchNavigationBar *customNavigationView;

@end

NSString *const cityHeaderView = @"CityHeaderView";
NSString *const cityGroupCell = @"CityGroupCell";
NSString *const cityCell = @"CityCell";

@implementation TENSearchCityViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:NO];
    self.navigationController.navigationBarHidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:NO];
    self.navigationController.navigationBarHidden = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    //[self customNav];
    [self setupSubviews];
    [self locationNow];
    [self sectionAnimationView];
}

#pragma mark - UI

- (void)setupSubviews {
    [self.view addSubview:self.customNavigationView];
    [self.view addSubview:self.tableView];
    [self.customNavigationView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.top.right.offset(0);
        make.height.equalTo(@(TENNavigationBarHeight));
    }];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.offset(0);
        make.top.equalTo(self.customNavigationView.mas_bottom);
    }];
}

#pragma mark - 定位

- (void)locationNow {
    @weakify(self)
    [[TENLocationManager share] locationJustOneceTimeWith:^(CLLocation *location, BMKLocationReGeocode *rgcData) {
        NSLog(@"location:%@",location);
        NSLog(@"rgcData:%@",rgcData);
        @strongtify(self)
        self.locationCity = rgcData.city;
        [self.tableView reloadData];
    } faild:^(NSError *error) {
        @strongtify(self)
        self.locationCity = @"定位失败";
        [self.tableView reloadData];
    }];
}

#pragma mark -- 分区中心动画视图添加

- (void)sectionAnimationView {
    [self.tableView.superview addSubview:self.sectionTitle];
    [self.sectionTitle mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.centerY.equalTo(self.tableView.superview);
        make.width.height.mas_equalTo(50);
    }];
}

#pragma mark -- UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    
    UITableView *tableView = (UITableView *)scrollView;
    NSArray *array = [tableView indexPathsForRowsInRect:CGRectMake(0, tableView.contentOffset.y, [UIScreen mainScreen].bounds.size.width, 30)];
    NSIndexPath *indexPath = [NSIndexPath new];
    indexPath = array.count? array[0]: [NSIndexPath indexPathForRow:0 inSection:0];
    if (indexPath.section >= 3) {
        self.sectionTitle.text = self.arraySection[indexPath.section-3];
        self.sectionTitle.hidden = NO;
    }
    else
    {
        self.sectionTitle.hidden = YES;
    }
    // 是否开始拖拽
    if (self.isBegainDrag) {
        [UIView animateWithDuration:1 animations:^{
            self.sectionTitle.alpha = 1.0;
        }];
    }
    
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    
    self.begainDrag = YES;
    
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    
    [UIView animateWithDuration:1 animations:^{
        self.sectionTitle.alpha = 0.;
    }];
    
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    
    self.begainDrag = NO;
    if (!velocity.y) {
        [UIView animateWithDuration:1 animations:^{
            self.sectionTitle.alpha = 0.;
        }];
    }
}

// 分区动画标题
- (UILabel *)sectionTitle {
    if (!_sectionTitle) {
        _sectionTitle = [UILabel new];
        _sectionTitle.backgroundColor = [UIColor colorWithHexString:TENThemeBlueColor];
        _sectionTitle.textColor = [UIColor whiteColor];
        _sectionTitle.layer.cornerRadius = 50 / 2.0;
        _sectionTitle.layer.masksToBounds = YES;
        _sectionTitle.alpha = 0;
        _sectionTitle.textAlignment = NSTextAlignmentCenter;
    }
    return _sectionTitle;
}
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.backgroundColor =  TENHexColor(TENWhiteColor);
        _tableView.separatorColor = TENHexClearColor;
        _tableView.estimatedRowHeight = 44.f;
        _tableView.rowHeight =  UITableViewAutomaticDimension;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.sectionIndexColor = [UIColor colorWithHexString:TENThemeBlueColor];
        [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:cityCell];
        [_tableView registerClass:[TENCityGroupCell class] forCellReuseIdentifier:cityGroupCell];
        [_tableView registerClass:[TENCityHeaderView class] forHeaderFooterViewReuseIdentifier:cityHeaderView];
    }
    return _tableView;
}
#pragma mark 懒加载

- (UISearchBar *)searchBar{
    if (!_searchBar) {
        _searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0,TENScreenWidth -100, 30)];
        _searchBar.backgroundColor = [UIColor whiteColor];
        _searchBar.delegate = self;
        _searchBar.layer.cornerRadius = 15;
        _searchBar.placeholder = @"输入城市名称进行搜索";
        UITextField *searchField = [self.searchBar valueForKey:@"searchField"];
        searchField.font = [UIFont systemFontOfSize:13];
        if (searchField) {
            searchField.layer.cornerRadius = 15.0f;
            searchField.layer.masksToBounds = YES;
            // 设置放大镜
            UIImage *image = [UIImage imageNamed: @"tabbar_search"];
            UIImageView *iView = [[UIImageView alloc] initWithImage:image];
            iView.frame = CGRectMake(0, 0, image.size.width , image.size.height);
            searchField.leftView = iView;
        }
    }
    return _searchBar;
}

- (TENSearchNavigationBar *)customNavigationView {
    if (!_customNavigationView) {
        _customNavigationView = [TENSearchNavigationBar new];
        _customNavigationView.placeHolder = @"输入城市名称进行搜索";
        @weakify(self);
        _customNavigationView.backButtonHandler = ^{
            @strongtify(self);
            [self.navigationController popViewControllerAnimated:YES];
        };
        _customNavigationView.searchButtonHandler = ^{
            @strongtify(self);
            NSString *text = self.customNavigationView.searchText;
            [self searchCityWith:text];
        };
    }
    return _customNavigationView;
}

#pragma mark  自定义导航视图

-(void)customNav
{
    self.navigationController.navigationBar.backgroundColor = TENHexColor(TENThemeBlueColor);
    //    [self setStatusBarBackgroundColor:TENHexColor(TENThemeBlueColor)];
    //[self.navigationController.navigationBar DJSetBackgroundColor:TENHexColor(TENThemeBlueColor)];
    //返回
    //    ImageLeftButton *leftButton = [[ImageLeftButton alloc] initWithRight];
    //    leftButton.frame = CGRectMake(0, 0, 30, 30);
    //    [leftButton setImage:[UIImage imageNamed:@"ten-nav-back"] forState:UIControlStateNormal];
    //    [leftButton addTarget:self action:@selector(dismissBack:) forControlEvents:UIControlEventTouchUpInside];
    //    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:leftButton];
    //搜索
    UIBarButtonItem *buttonItem=[[UIBarButtonItem alloc]initWithTitle:@"搜索" style:UIBarButtonItemStylePlain target:self action:@selector(searchCity)];
    buttonItem.tintColor = [UIColor whiteColor];
    self.navigationItem.rightBarButtonItem=buttonItem;
    
    UIView  *searchView = [[UIView alloc]initWithFrame:CGRectMake(0, 0,TENScreenWidth - 100,30)];
    searchView.layer.cornerRadius = 15;
    searchView.clipsToBounds = YES;
    [searchView addSubview:self.searchBar];
    self.navigationItem.titleView = searchView;
}

- (NSMutableArray *)cityDatas{
    if (_cityDatas == nil) {
        NSArray *array = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"CityData" ofType:@"plist"]];
        _cityDatas = [[NSMutableArray alloc] init];
        for (NSDictionary *groupDic in array) {
            TENCityGroup *group = [[TENCityGroup alloc] init];
            group.groupName = [groupDic objectForKey:@"initial"];
            for (NSDictionary *dic in [groupDic objectForKey:@"citys"]) {
                TENCityModel *city = [[TENCityModel alloc] init];
                city.cityID = [dic objectForKey:@"city_key"];
                city.cityName = [dic objectForKey:@"city_name"];
                city.shortName = [dic objectForKey:@"short_name"];
                city.pinyin = [dic objectForKey:@"pinyin"];
                city.initials = [dic objectForKey:@"initials"];
                [group.arrayCitys addObject:city];
                [self.recordCityData addObject:city];
            }
            [self.arraySection addObject:group.groupName];
            [_cityDatas addObject:group];
        }
    }
    return _cityDatas;
}
- (NSMutableArray *)recordCityData
{
    if (_recordCityData == nil) {
        _recordCityData = [[NSMutableArray alloc] init];
    }
    return _recordCityData;
}
- (NSMutableArray *)localCityData
{
    if (_localCityData == nil) {
        _localCityData = [[NSMutableArray alloc] init];
        if (self.locationCityID != nil) {
            TENCityModel *city = nil;
            for (TENCityModel *item in self.recordCityData) {
                if ([item.cityID isEqualToString:self.locationCityID]) {
                    city = item;
                    break;
                }
            }
            if (city == nil) {
                NSLog(@"Not Found City: %@", self.locationCityID);
            }
            else {
                [_localCityData addObject:city];
            }
        }
    }
    return _localCityData;
}

- (NSArray *)hotCitys {
    if (_hotCitys == nil) {
        _hotCitys = [NSArray arrayWithObjects:@"北京市",@"上海",@"广州",@"深圳",@"杭州",@"重庆",@"南京" , nil];
    }
    return _hotCitys;
}

- (NSMutableArray *)hotCityData {
    if (_hotCityData == nil) {
        _hotCityData = [[NSMutableArray alloc] init];
        for (NSString *str in self.hotCitys) {
            TENCityModel *city = nil;
            for (TENCityModel *item in self.recordCityData) {
                if ([item.cityName isEqualToString:str]) {
                    city = item;
                    break;
                }
            }
            if (city == nil) {
                NSLog(@"Not Found City: %@", str);
            }
            else {
                [_hotCityData addObject:city];
            }
        }
        //        _hotCityData = [[NSMutableArray alloc] initWithObjects:@"北京",@"上海",@"广州",@"深圳",@"杭州",@"重庆",@"南京" ,nil];
    }
    return _hotCityData;
}
- (NSMutableArray *) commonCityData
{
    if (_commonCityData == nil) {
        _commonCityData = [[NSMutableArray alloc] init];
        for (NSString *str in self.commonCitys) {
            TENCityModel *city = nil;
            for (TENCityModel *item in self.recordCityData) {
                if ([item.cityName isEqualToString:str]) {
                    city = item;
                    break;
                }
            }
            if (city == nil) {
                NSLog(@"Not Found City: %@", str);
            }
            else {
                [_commonCityData addObject:city];
            }
        }
    }
    return _commonCityData;
}
- (NSMutableArray *) arraySection
{
    if (_arraySection == nil) {
        //        _arraySection = [[NSMutableArray alloc] initWithObjects:UITableViewIndexSearch, @"定位", @"最近", @"最热", nil];
        _arraySection = [[NSMutableArray alloc]init];
    }
    return _arraySection;
}
- (NSMutableArray *) commonCitys
{
    if (_commonCitys == nil) {
        NSArray *array = [[NSUserDefaults standardUserDefaults] objectForKey:COMMON_CITY_DATA_KEY];
        _commonCitys = (array == nil ? [[NSMutableArray alloc] init] : [[NSMutableArray alloc] initWithArray:array copyItems:YES]);
    }
    return _commonCitys;
}
#pragma mark - Getter
- (NSMutableArray *) searchCities
{
    if (_searchCities == nil) {
        _searchCities = [[NSMutableArray alloc] init];
    }
    return _searchCities;
}
#pragma mark UITableViewDataSource
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    //搜索出来只显示一块
    if (self.isSearch) {
        return 1;
    }
    return self.cityDatas.count + 3;
    //    return self.cityDatas.count ;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (self.isSearch) {
        return self.searchCities.count;
    }
    if (section < 3) {
        return 1;
    }
    TENCityGroup *group = [self.cityDatas objectAtIndex:section - 3];
    //    GYZCityGroup *group = [self.cityDatas objectAtIndex:section ];
    return group.arrayCitys.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.isSearch) {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cityCell];
        TENCityModel *city =  [self.searchCities objectAtIndex:indexPath.row];
        [cell.textLabel setText:city.cityName];
        return cell;
    }
    
    if (indexPath.section < 3) {
        TENCityGroupCell *cell = [tableView dequeueReusableCellWithIdentifier:cityGroupCell];
        if (indexPath.section == 0) {
            cell.titleLabel.text = @"当前定位城市";
            cell.noDataLabel.text = self.locationCity;
            [cell setCityArray:self.localCityData];
        }
        else if (indexPath.section == 1) {
            cell.titleLabel.text = @"历史记录";
            [cell setCityArray:self.commonCityData];
        }
        else {
            cell.titleLabel.text = @"热门城市";
            [cell setCityArray:self.hotCityData];
        }
        [cell setDelegate:self];
        return cell;
    }
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cityCell];
    TENCityGroup *group = [self.cityDatas objectAtIndex:indexPath.section - 3];
    TENCityModel *city =  [group.arrayCitys objectAtIndex:indexPath.row];
    [cell.textLabel setText:city.cityName];
    return cell;
}
#pragma mark UITableViewDelegate
- (UIView *) tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section < 3 || self.isSearch) {
        return nil;
    }
    TENCityHeaderView *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:cityHeaderView];
    //    NSString *title = [_arraySection objectAtIndex:section + 1];
    NSString *title = [_arraySection objectAtIndex:section - 3];
    headerView.titleLabel.text = title;
    return headerView;
}
- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.isSearch) {
        return 44.0f;
    }
    if (indexPath.section == 0) {
        return [TENCityGroupCell getCellHeightOfCityArray:self.localCityData];
    }
    else if (indexPath.section == 1) {
        return [TENCityGroupCell getCellHeightOfCityArray:self.commonCityData];
    }
    else if (indexPath.section == 2){
        return [TENCityGroupCell getCellHeightOfCityArray:self.hotCityData];
    }
    return 44.0f;
}
- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section < 3 || self.isSearch) {
        return 0.0f;
    }
    return 23.5f;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    TENCityModel *city = nil;
    if (self.isSearch) {
        city =  [self.searchCities objectAtIndex:indexPath.row];
    }else{
        if (indexPath.section < 3) {
            if (indexPath.section == 0 && self.localCityData.count <= 0) {
                //                [self locationStart];
            }
            return;
        }
        TENCityGroup *group = [self.cityDatas objectAtIndex:indexPath.section - 3];
        city =  [group.arrayCitys objectAtIndex:indexPath.row];
    }
    [self didSelctedCity:city];
}
- (NSArray *) sectionIndexTitlesForTableView:(UITableView *)tableView
{
    if (self.isSearch) {
        return nil;
    }
    return self.arraySection;
}
- (NSInteger) tableView:(UITableView *)tableView sectionForSectionIndexTitle:(NSString *)title atIndex:(NSInteger)index
{
    return index + 3;
}
#pragma mark searchBarDelegete

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    [searchBar setShowsCancelButton:YES animated:YES];
    UIButton *btn=[searchBar valueForKey:@"_cancelButton"];
    [btn setTitle:@"取消" forState:UIControlStateNormal];
    [btn.titleLabel setFont:[UIFont systemFontOfSize:14.0f]];
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    [self.searchCities removeAllObjects];
    
    if (searchText.length == 0) {
        self.isSearch = NO;
    }else{
        self.isSearch = YES;
        for (TENCityModel *city in self.recordCityData){
            NSRange chinese = [city.cityName rangeOfString:searchText options:NSCaseInsensitiveSearch];
            NSRange  letters = [city.pinyin rangeOfString:searchText options:NSCaseInsensitiveSearch];
            NSRange  initials = [city.initials rangeOfString:searchText options:NSCaseInsensitiveSearch];
            
            if (chinese.location != NSNotFound || letters.location != NSNotFound || initials.location != NSNotFound) {
                [self.searchCities addObject:city];
            }
        }
    }
    [self.tableView reloadData];
}
//添加搜索事件：
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    [searchBar resignFirstResponder];
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    [searchBar setShowsCancelButton:NO animated:YES];
    searchBar.text=@"";
    [searchBar resignFirstResponder];
    self.isSearch = NO;
    [self.tableView reloadData];
}
#pragma mark GYZCityGroupCellDelegate
- (void) cityGroupCellDidSelectCity:(TENCityModel *)city
{
    [self didSelctedCity:city];
}

#pragma mark - Event Response
- (void) cancelButtonDown:(UIBarButtonItem *)sender
{
    if (_delegate && [_delegate respondsToSelector:@selector(cityPickerControllerDidCancel:)]) {
        [_delegate cityPickerControllerDidCancel:self];
    }
}
#pragma mark - Private Methods
- (void) didSelctedCity:(TENCityModel *)city
{
    if (_delegate && [_delegate respondsToSelector:@selector(cityPickerController:didSelectCity:)]) {
        
        [_delegate cityPickerController:self didSelectCity:city];
    }
    
    if (self.commonCitys.count >= MAX_COMMON_CITY_NUMBER) {
        [self.commonCitys removeLastObject];
    }
    for (NSString *str in self.commonCitys) {
        if ([city.cityName isEqualToString:str]) {
            [self.commonCitys removeObject:str];
            break;
        }
    }
    [self.commonCitys insertObject:city.cityName atIndex:0];
    [[NSUserDefaults standardUserDefaults] setValue:self.commonCitys forKey:COMMON_CITY_DATA_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    TEN_UD_SET_VALUE(city.cityName, kDisplayCityName);
    
    [self.navigationController popViewControllerAnimated:YES];
}

//开始定位
-(void)locationStart{
    //判断定位操作是否被允许
    
    if([CLLocationManager locationServicesEnabled]) {
        self.locationManager = [[CLLocationManager alloc] init] ;
        self.locationManager.delegate = self;
        //设置定位精度
        self.locationManager.desiredAccuracy=kCLLocationAccuracyBest;
        self.locationManager.distanceFilter = kCLLocationAccuracyHundredMeters;//每隔多少米定位一次（这里的设置为每隔百米)
        if (IOS8) {
            //使用应用程序期间允许访问位置数据
            [self.locationManager requestWhenInUseAuthorization];
        }
        // 开始定位
        [self.locationManager startUpdatingLocation];
    }else {
        //提示用户无法进行定位操作
        NSLog(@"%@",@"定位服务当前可能尚未打开，请设置打开！");
        
    }
}
#pragma mark - CoreLocation Delegate

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations

{
    //系统会一直更新数据，直到选择停止更新，因为我们只需要获得一次经纬度即可，所以获取之后就停止更新
    [self.locationManager stopUpdatingLocation];
    //此处locations存储了持续更新的位置坐标值，取最后一个值为最新位置，如果不想让其持续更新位置，则在此方法中获取到一个值之后让locationManager stopUpdatingLocation
    CLLocation *currentLocation = [locations lastObject];
    
    //获取当前所在的城市名
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    //根据经纬度反向地理编译出地址信息
    [geocoder reverseGeocodeLocation:currentLocation completionHandler:^(NSArray *array, NSError *error)
     {
         if (array.count >0)
         {
             CLPlacemark *placemark = [array objectAtIndex:0];
             //获取城市
             NSString *currCity = placemark.locality;
             if (!currCity) {
                 //四大直辖市的城市信息无法通过locality获得，只能通过获取省份的方法来获得（如果city为空，则可知为直辖市）
                 currCity = placemark.administrativeArea;
             }
             if (self.localCityData.count <= 0) {
                 TENCityModel *city = [[TENCityModel alloc] init];
                 city.cityName = currCity;
                 city.shortName = currCity;
                 [self.localCityData addObject:city];
                 
                 [self.tableView reloadData];
             }
             
         } else if (error ==nil && [array count] == 0)
         {
             NSLog(@"No results were returned.");
         }else if (error !=nil)
         {
             NSLog(@"An error occurred = %@", error);
         }
         
     }];
    
}
- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error {
    if (error.code ==kCLErrorDenied) {
        // 提示用户出错原因，可按住Option键点击 KCLErrorDenied的查看更多出错信息，可打印error.code值查找原因所在
    }
}

#pragma mark 搜索城市

- (void)searchCityWith:(NSString *)text {
    [self.view endEditing:YES];
    NSDictionary *params = @{@"key": @"99bf91bec40f5b30645d2612edced65aab2c27b7",
                             @"name": text
                             };
    [TENSearchPageRequest requestCityDataWithParameters:params success:^(NSDictionary *responseObject) {
        TEN_LOG(@"%@", responseObject);
        NSNumber *code = responseObject[@"code"];
        if (code.integerValue != 0) {
            [NSObject showHudTipStr:responseObject[@"msg"]];
        }
    } failure:^(NSError *error) {
        
    }];
}

#pragma mark 返回

-(void)dismissBack:(UIButton *)button
{
    //    [self dismissViewControllerAnimated:YES completion:nil];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end

//
//  TENChooseCityDelegate.h
//  TodayEstateNet
//
//  Created by Admin on 2018/9/19.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import <Foundation/Foundation.h>
@class TENCityModel, TENSearchCityViewController;

@protocol TENChooseCityDelegate <NSObject>

- (void)cityPickerController:(TENSearchCityViewController *)chooseCityController didSelectCity:(TENCityModel *)city;
- (void)cityPickerControllerDidCancel:(TENSearchCityViewController *)chooseCityController;

@end

@protocol TENCityGroupCellDelegate <NSObject>

- (void) cityGroupCellDidSelectCity:(TENCityModel *)city;

@end

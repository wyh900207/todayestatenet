//
//  TENHouseDetailViewController.m
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/11/4.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENHouseDetailViewController.h"
//#import "TENUtilsHeader.h"
#import "TENHouseDetailBannerCell.h"
#import "TENHouseDetailBottomView.h"
#import "TENHouseDetailTitleCell.h"
#import "TENHouseDetailAveragePriceCell.h"
#import "TENHouseDetailRequest.h"
#import "TENHouseDetailModel.h"
#import <MJExtension/MJExtension.h>
#import "TENHouseDetailDescriptionCell.h"
#import "TENActivityCell.h"
#import "TENSectionView.h"
#import "TENLookHouseServiceCell.h"
#import "TENBuildingDynamicCell.h"
#import "TENRoomTypeCell.h"
#import "TENBuildingInfoCell.h"
#import "TENAroundInfoCell.h"
#import "TENCommentCell.h"
//#import "TENHouseDetailFooterView.h"
#import "TENPriceOrDistrictCell.h"
#import "TENMapPoiViewController.h"
#import "TENBaseWebViewController.h"
#import "TENLoginViewController.h"
#import "TENNoticeView.h"
//#import "TENMineRequest.h"
#import "SSChatController.h"
#import "TENLoginViewController.h"
@interface TENHouseDetailViewController () <UITableViewDelegate, UITableViewDataSource,BMKMapViewDelegate,BottomBtnClickedDelegate>

@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) TENHouseDetailBottomView *bottomView;
@property (nonatomic, strong) TENHouseDetailModel *dataSourceModel;

@property (nonatomic, strong) NSMutableArray *bannerImgArr;
@property (nonatomic, assign) BOOL isFavorite;
@property (nonatomic, assign) BOOL isShop;
//test
@property (nonatomic, strong) BMKMapView *mapView;
@property (nonatomic, strong) UIImage *areaInfoImage;
@property (nonatomic, strong) TENNoticeView *noticeView;
@property (nonatomic, strong) NSDictionary *bookInfoDic;
@property (nonatomic, strong) NSDictionary *lookServiceDic;
@end

@implementation TENHouseDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadDetailInfo];
    [self setupSubviews];
    
//    [self setupMapView];
    
}
//制作地图周边截图
//- (void)setupMapView{
//    self.mapView = [[BMKMapView alloc] initWithFrame:CGRectMake(0, -300, TENScreenWidth, 90)];
//    self.mapView.centerCoordinate = CLLocationCoordinate2DMake([self.lat doubleValue], [self.lng doubleValue]);
//    self.mapView.zoomLevel = 15;
//    self.mapView.showsUserLocation = YES;
//    self.mapView.delegate = self;
//    [self.view addSubview:self.mapView];
//    BMKPointAnnotation *annotation = [[BMKPointAnnotation alloc] init];
//    annotation.coordinate = self.mapView.centerCoordinate;
//    [_mapView addAnnotation:annotation];
//}

//- (void)mapViewDidFinishRendering:(BMKMapView *)mapView{
//    self.areaInfoImage = [self.mapView takeSnapshot];
//    [self.mapView removeFromSuperview];
//    [self.tableView reloadData];
//}
//- (BMKAnnotationView *)mapView:(BMKMapView *)mapView viewForAnnotation:(id<BMKAnnotation>)annotation{
//    if ([annotation isKindOfClass:[BMKPointAnnotation class]]) {
//        BMKPinAnnotationView *newAnnotationView = [[BMKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"myAnnotation"];
//        //        newAnnotationView.animatesDrop = YES;
//        newAnnotationView.annotation=annotation;
//        //大头针图片
//        newAnnotationView.image = [UIImage imageNamed:@"paopao.png"];
//        //        newAnnotationView.paopaoView = [[BMKActionPaopaoView alloc]initWithCustomView:paopaoView];
//        [newAnnotationView setSelected:YES animated:YES];
//        return newAnnotationView;
//    }
//    return nil;
//
//}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    //    self.tabBarController.tabBar.hidden = YES;
}
-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    //    self.tabBarController.tabBar.hidden = NO;
}
#pragma mark - Network

- (void)loadDetailInfo {
    NSMutableDictionary *parameters = [NSMutableDictionary dictionary];
    [parameters setValuesForKeysWithDictionary:@{@"id":self.houseId,
                                                 @"lat":[TENLocationManager share].latitude.stringValue,
                                                 @"lng":[TENLocationManager share].longitude.stringValue,
                                                 @"accountId":kAccountId
                                                 }];
    [TENHouseDetailRequest fetchHouseDetailWithParameter:parameters success:^(NSDictionary *responseObject) {
        TEN_LOG(@"楼盘详情-=--=%@", responseObject);
        
        self.dataSourceModel = [TENHouseDetailModel mj_objectWithKeyValues:responseObject[@"content"]];
        self.isFavorite = [responseObject[@"content"][@"isFavorite"] boolValue];
//        self.isShop = [responseObject[@"content"][@"isShop"] boolValue];
        self.bookInfoDic = responseObject[@"content"][@"booking"];
        [[NSNotificationCenter defaultCenter] postNotificationName:TENChangeBtnName object:nil userInfo:@{@"isFavorite":@(self.isFavorite),@"isSubscribe":@([self.bookInfoDic[@"news"] boolValue])}];
        
        
        [self.tableView reloadData];
    } failure:^(NSError *error) {
        TEN_LOG(@"%@", error.description);
    }];
}

#pragma mark - UI

- (void)setupSubviews {
    // Custom navigation title
    self.navigationItem.title = @"楼盘详情";
    // Add all subview to controller
    [self.contentView addSubview:self.tableView];
    [self.contentView addSubview:self.bottomView];
    //    self.tableView.tableFooterView = self.detailFooterView;
    
    //    self.detailFooterView.priceEqlBtn.selected = YES;
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self.contentView);
    }];
    [self.bottomView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.tableView.mas_bottom);
        make.left.bottom.right.equalTo(self.contentView);
        make.height.equalTo(@50);
    }];
}

#pragma mark - UITableViewDelegate, UITableViewDataSource
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *view;
    TENSectionView *secView = [[NSBundle mainBundle] loadNibNamed:@"TENSectionView" owner:nil options:nil].firstObject;
    switch (section) {
        case 3:{
            
            secView.sectionNameLabel.text = @"优惠活动";
            [secView.detailBtn setTitle:@"详情" forState:normal];
            [secView.detailBtn addTarget:self action:@selector(presentActivityH5) forControlEvents:UIControlEventTouchUpInside];
            view = secView;
        }
            break;
        case 4:{
            secView.sectionNameLabel.text = @"看房服务";
            [secView.detailBtn setTitle:@"" forState:normal];
            secView.rightImgView.hidden = YES;
            view = secView;
        }
            break;
        case 5:{
            secView.sectionNameLabel.text = @"楼盘动态";
            secView.noteNumLabel.text = self.dataSourceModel.newsNum.stringValue;
            secView.noteNumLabel.hidden = NO;
            [secView.detailBtn setTitle:@"更多" forState:normal];
            view = secView;
        }
            break;
        case 6:{
            secView.sectionNameLabel.text = @"全部户型";
            [secView.detailBtn setTitle:@"更多" forState:normal];
            view = secView;
        }
            break;
        case 7:{
            secView.sectionNameLabel.text = @"楼栋信息";
            secView.rightImgView.hidden = YES;
            [secView.detailBtn setTitle:@"" forState:normal];
            view = secView;
        }
            break;
        case 8:{
            secView.sectionNameLabel.text = @"周边信息";
            //            secView.rightImgView.hidden = YES;
            [secView.detailBtn setTitle:@"更多" forState:normal];
            [secView.detailBtn addTarget:self action:@selector(pushToMapPoiViewController) forControlEvents:UIControlEventTouchUpInside];
            view = secView;
        }
            break;
        case 9:{
            secView.sectionNameLabel.text = @"点评";
            secView.rightImgView.hidden = YES;
            [secView.detailBtn setTitle:@"" forState:normal];
            view = secView;
        }
            break;
        default:
            break;
    }
    return view;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    CGFloat height;
    switch (section) {
        case 0:
            height = 0;
            break;
        case 1:
            height = 0;
            break;
        case 2:
            height = 0;
            break;
        case 10:
            height = 0;
            break;
        default:
            height = 44;
            break;
    }
    return height;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 11;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger number = 0;
    switch (section) {
        case 0:
            number = 2;
            break;
        case 1:
            number = self.dataSourceModel.buildTypeList.count;
            break;
        case 4:{
            NSArray *array = [self.dataSourceModel.buildingInfo.houseServiceIds componentsSeparatedByString:@","];
            number = array.count;
        }
            break;
        case 6:{
            number = self.dataSourceModel.buildTypeList.count;
        }
            break;
        default:
            number = 1;
            break;
    }
    return number;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    TENHouseDetailBannerCell *bannerCell = [tableView dequeueReusableCellWithIdentifier:@"banner" forIndexPath:indexPath];
    TENHouseDetailTitleCell *titleCell = [tableView dequeueReusableCellWithIdentifier:@"title" forIndexPath:indexPath];
    TENHouseDetailAveragePriceCell *averageCell = [tableView dequeueReusableCellWithIdentifier:@"buildTypeList" forIndexPath:indexPath];
    TENHouseDetailDescriptionCell *descriptionCell = [tableView dequeueReusableCellWithIdentifier:@"TENHouseDetailDescriptionCell" forIndexPath:indexPath];
    TENActivityCell *activityCell = [tableView dequeueReusableCellWithIdentifier:@"TENActivityCell" forIndexPath:indexPath];
    TENLookHouseServiceCell *serviceCell = [tableView dequeueReusableCellWithIdentifier:@"TENLookHouseServiceCell" forIndexPath:indexPath];
    TENCommentCell *commentCell = [tableView dequeueReusableCellWithIdentifier:@"TENCommentCell" forIndexPath:indexPath];
    TENAroundInfoCell *aroundCell = [tableView dequeueReusableCellWithIdentifier:@"TENAroundInfoCell" forIndexPath:indexPath];
    TENBuildingInfoCell *infoCell = [tableView dequeueReusableCellWithIdentifier:@"TENBuildingInfoCell" forIndexPath:indexPath];
    TENRoomTypeCell *roomTypeCell = [tableView dequeueReusableCellWithIdentifier:@"TENRoomTypeCell" forIndexPath:indexPath];
    TENBuildingDynamicCell *dynamicCell = [tableView dequeueReusableCellWithIdentifier:@"TENBuildingDynamicCell" forIndexPath:indexPath];
    TENPriceOrDistrictCell *priceOrDistrictCell = [tableView dequeueReusableCellWithIdentifier:@"TENPriceOrDistrictCell" forIndexPath:indexPath];
    
    // 泛型返回值
    UITableViewCell *returnCell;
    
    switch (indexPath.section) {
        case 0: {
            if (indexPath.row == 0) {
                for (TENAttachmentModel *model in self.dataSourceModel.buildingInfo.attachment) {
                    [self.bannerImgArr addObject:model.picPath];
                }
                bannerCell.bannerView.imageURLStringsGroup = self.bannerImgArr;
                returnCell = bannerCell;
            } else {
                NSArray *array = [self.dataSourceModel.buildingInfo.tags componentsSeparatedByString:@"-"];
                //FIXME:tagview显示不全bug
                titleCell.tagsView.tags = array;
                titleCell.titleLabel.text = self.dataSourceModel.buildingInfo.name;
                returnCell = titleCell;
            }
        }
            break;
        case 1: {
            averageCell.averageModel = self.dataSourceModel.buildTypeList[indexPath.row];
            averageCell.row = indexPath.row;
            returnCell = averageCell;
        }
            break;
        case 2:{
            descriptionCell.buildingInfoModel = self.dataSourceModel.buildingInfo;
            [descriptionCell.moreInfoButton addTarget:self action:@selector(presentH5DetailWebController) forControlEvents:UIControlEventTouchUpInside];
            [descriptionCell.discountBtn addTarget:self action:@selector(showNoticeView) forControlEvents:UIControlEventTouchUpInside];
            [descriptionCell.openBtn addTarget:self action:@selector(showNoticeView) forControlEvents:UIControlEventTouchUpInside];
            returnCell = descriptionCell;
        }
            break;
        case 3:{
            activityCell.activityModel = self.dataSourceModel.activity;
            returnCell = activityCell;
        }
            break;
        case 4:{//看房服务
            NSArray *array = [self.dataSourceModel.buildingInfo.houseServiceIds componentsSeparatedByString:@","];
            NSString *tempName = array[indexPath.row];
            [serviceCell.serviceBtn setImage:[UIImage imageNamed:self.lookServiceDic[tempName]] forState:UIControlStateNormal];
            returnCell = serviceCell;
        }
            break;
        case 5:{//楼盘动态
            dynamicCell.buildingNewsModel = self.dataSourceModel.buildingNews;
            [dynamicCell.subscribeBtn addTarget:self action:@selector(showNoticeView) forControlEvents:UIControlEventTouchUpInside];
            returnCell = dynamicCell;
        }
            break;
        case 6:{//户型
            roomTypeCell.buildTypeModel = self.dataSourceModel.buildTypeList[indexPath.row];
            returnCell = roomTypeCell;
        }
            break;
        case 7:{//楼栋信息
            [infoCell.infoImgView sd_setImageWithURL:[NSURL  URLWithString:self.dataSourceModel.buildingInfo.panorama]];
            returnCell = infoCell;
        }
            break;
        case 8:{//周边信息
            aroundCell.buildingInfoModel = self.dataSourceModel.buildingInfo;
            [aroundCell.areaInfoImgView sd_setImageWithURL:[NSURL URLWithString:self.dataSourceModel.buildingInfo.peripheryPic]];
            returnCell = aroundCell;
        }
            break;
        case 9:{//点评
            commentCell.buildScoreModel = self.dataSourceModel.buildScore;
            returnCell = commentCell;
        }
            break;
        case 10:{//同价格or同区域
            priceOrDistrictCell.sameDistictDataArray = self.dataSourceModel.nearBuildingList;
            priceOrDistrictCell.samePriceDataArray = self.dataSourceModel.samePriceList;
            returnCell = priceOrDistrictCell;
        }
            break;
        default:
            break;
    }
    
    returnCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return returnCell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch (indexPath.section) {
        case 7:{
            NSString *contentURL = [[TENUrlManager share] url:@"house" detail:@"house-detail-buildingInfo"];
            [TENBaseWebViewController pushWith:self.navigationController uri:[contentURL stringByAppendingPathComponent:self.houseId.stringValue] hiddenBottom:YES];
        }
            break;
        case 8:{
            [self pushToMapPoiViewController];
        }
            break;
            
        default:
            break;
    }
}
//跳转周边地图
- (void)pushToMapPoiViewController{
    TENMapPoiViewController *poiVC = [[TENMapPoiViewController alloc] init];
    poiVC.lat = self.lat;
    poiVC.lng = self.lng;
    [self.navigationController pushViewController:poiVC animated:YES];
}
//跳转h5详情
- (void)presentH5DetailWebController{
    NSString *contentURL = [[TENUrlManager share] url:@"house" detail:@"house-detail"];
    [TENBaseWebViewController pushWith:self.navigationController uri:[contentURL stringByAppendingPathComponent:self.houseId.stringValue] hiddenBottom:YES];
}
//跳转活动详情
- (void)presentActivityH5{
    NSString *contentURL = [[TENUrlManager share] url:@"recommend" detail:@"activity-building-detail"];
    [TENBaseWebViewController pushWith:self.navigationController uri:[contentURL stringByAppendingPathComponent:self.dataSourceModel.activity.ID.stringValue] hiddenBottom:YES];
}
#pragma mark-------------bottomBtnClickedDelegate
- (void)bottomBtnClickedWith:(UIButton *)btn{
    if (kLoginState == NO) {
        
        TENLoginViewController *loginVC = [TENLoginViewController new];
        TENNavigationViewController *loginNav = [[TENNavigationViewController alloc] initWithRootViewController:loginVC];
        [self.navigationController presentViewController:loginNav animated:YES completion:nil];
    }else{
        switch (btn.tag) {
            case 1://微信分享
            {
                
            }
                break;
            case 2://订阅
            {
                [self showNoticeView];
            }
                break;
            case 3://收藏
            {
                NSDictionary *parameters = @{@"accountId":kAccountId,
                                             @"targetType":@(1),
                                             @"targetId":@([self.houseId integerValue])
                                             };
                [TENHouseDetailRequest favoriteInfoWithParamters:parameters success:^(NSDictionary *responseObject) {
                    TEN_LOG(@"收藏结果---%@",responseObject);
                    if ([responseObject[@"code"] integerValue] == 1) {
                        [NSObject showHudTipStr:responseObject[@"msg"]];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [self.bottomView.favoriteBtn setTitle:[responseObject[@"content"] integerValue] == 1 ? @"已收藏" : @"收藏" forState:UIControlStateNormal];
                            [self.bottomView.favoriteBtn setImage:[UIImage imageNamed:[responseObject[@"content"] integerValue] == 1 ? @"houseDetail_favorite_selected" : @"houseDetail_favorite"] forState:UIControlStateNormal];
                        });
                    }
                } failure:^(NSError *error) {
                    TEN_LOG(@"%@",error.description);
                }];
            }
                break;
            case 4://在线咨询
            {
                SSChatController *chatVC = [SSChatController new];
                chatVC.chatType = SSChatConversationTypeChat;
                chatVC.titleString = @"客服";
                chatVC.houseId = self.houseId.stringValue;
                [self.navigationController pushViewController:chatVC animated:YES];
            }
                break;
            case 5://我要看房  vip：报备客户
            {
//                NSString *contentURL = [[TENUrlManager share] url:@"house" detail:@"house-detail-report"];
//                NSString *url = [contentURL stringByAppendingPathComponent:[NSString stringWithFormat:@"%@?buildingGroupName=%@",self.houseId.stringValue,self.dataSourceModel.buildingInfo.name]];
//                [TENBaseWebViewController pushWith:self.navigationController uri:[url stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding] hiddenBottom:YES];
            }
                break;
            default:
                break;
    }
  }
}

- (void)showNoticeView{
    [[UIApplication sharedApplication].keyWindow addSubview:self.noticeView];
    [self.noticeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.navigationController.navigationBar.mas_bottom);
        make.left.right.bottom.equalTo(self.view);
    }];
}
#pragma mark - Getter

- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[UITableView alloc] initWithFrame:CGRectZero style:UITableViewStylePlain];
        _tableView.backgroundColor = TENHexColor(TENWhiteColor);
        _tableView.separatorColor = TENHexClearColor;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        _tableView.estimatedRowHeight = 100;
        _tableView.delegate = self;
        _tableView.dataSource = self;
        // TENHotSearchCell
        [_tableView registerNib:[UINib nibWithNibName:@"TENSearchCell" bundle:nil] forCellReuseIdentifier:@"cell"];
        // Banner
        [_tableView registerNib:[UINib nibWithNibName:@"TENHouseDetailBannerCell" bundle:nil] forCellReuseIdentifier:@"banner"];
        // Title
        [_tableView registerNib:[UINib nibWithNibName:@"TENHouseDetailTitleCell" bundle:nil] forCellReuseIdentifier:@"title"];
        // BuildTypeList
        [_tableView registerNib:[UINib nibWithNibName:@"TENHouseDetailAveragePriceCell" bundle:nil] forCellReuseIdentifier:@"buildTypeList"];
        // TENHouseDetailDescriptionCell
        [_tableView registerNib:[UINib nibWithNibName:@"TENHouseDetailDescriptionCell" bundle:nil] forCellReuseIdentifier:@"TENHouseDetailDescriptionCell"];
        //TENActivityCell
        [_tableView registerNib:[UINib nibWithNibName:@"TENActivityCell" bundle:nil] forCellReuseIdentifier:@"TENActivityCell"];
        //TENLookHouseServiceCell
        [_tableView registerNib:[UINib nibWithNibName:@"TENLookHouseServiceCell" bundle:nil] forCellReuseIdentifier:@"TENLookHouseServiceCell"];
        //TENBuildingDynamicCell
        [_tableView registerNib:[UINib nibWithNibName:@"TENBuildingDynamicCell" bundle:nil] forCellReuseIdentifier:@"TENBuildingDynamicCell"];
        //TENRoomTypeCell
        [_tableView registerNib:[UINib nibWithNibName:@"TENRoomTypeCell" bundle:nil] forCellReuseIdentifier:@"TENRoomTypeCell"];
        //TENBuildingInfoCell
        [_tableView registerNib:[UINib nibWithNibName:@"TENBuildingInfoCell" bundle:nil] forCellReuseIdentifier:@"TENBuildingInfoCell"];
        //TENAroundInfoCell
        [_tableView registerNib:[UINib nibWithNibName:@"TENAroundInfoCell" bundle:nil] forCellReuseIdentifier:@"TENAroundInfoCell"];
        //TENCommentCell
        [_tableView registerNib:[UINib nibWithNibName:@"TENCommentCell" bundle:nil] forCellReuseIdentifier:@"TENCommentCell"];
        //TENPriceOrDistrictCell
        
        [_tableView registerNib:[UINib nibWithNibName:@"TENPriceOrDistrictCell" bundle:nil] forCellReuseIdentifier:@"TENPriceOrDistrictCell"];
    }
    return _tableView;
}

- (TENHouseDetailBottomView *)bottomView {
    if (!_bottomView) {
        _bottomView = [[NSBundle mainBundle] loadNibNamed:@"TENHouseDetailBottomView" owner:nil options:nil].firstObject;
        _bottomView.delegate = self;
        if (kLoginState == YES && [kAccountType integerValue] == 3) {
            [_bottomView.reportBtn setTitle:@"我要推客" forState:UIControlStateNormal];
        }
    }
    return _bottomView;
}

- (NSMutableArray *)bannerImgArr{
    if (!_bannerImgArr) {
        _bannerImgArr = [NSMutableArray array];
    }
    return _bannerImgArr;
}
- (TENNoticeView *)noticeView{
    if (!_noticeView) {
        _noticeView = [[NSBundle mainBundle] loadNibNamed:@"TENNoticeView" owner:nil options:nil].firstObject;
        _noticeView.dataDic = self.bookInfoDic;
        _noticeView.houseId = self.houseId.stringValue;
    }
    return _noticeView;
}
- (NSDictionary *)lookServiceDic{
    if (!_lookServiceDic) {
        _lookServiceDic = @{@"113":@"自驾补贴",@"114":@"免费班车",@"115":@"免费专车"};
    }
    return _lookServiceDic;
}
@end

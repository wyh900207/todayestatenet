//
//  DLNull.m
//  DateLove
//
//  Created by HomerLynn on 2018/10/12.
//  Copyright © 2018 Zilu. All rights reserved.
//

#import "DLNull.h"

@implementation DLNull

+ (BOOL)isEmptyObject:(id)obj {
    return obj == nil || [obj isEqual:[NSNull null]] || ([obj respondsToSelector:@selector(length)] && [(NSData *) obj length] == 0) ||
    ([obj respondsToSelector:@selector(count)] && [(NSArray *) obj count] == 0);
}

+ (BOOL)isEmptyString:(NSString *)string {
    NSString *str = [NSString stringWithFormat:@"%@", string];
    return [str isEqual:[NSNull null]] || str == nil || str.length == 0 || [str isEqualToString:@"<null>"] ||
    [str isEqualToString:@"(null)"];
}

@end

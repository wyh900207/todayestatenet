//
//  DLNull.h
//  DateLove
//
//  Created by HomerLynn on 2018/10/12.
//  Copyright © 2018 Zilu. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DLNull : NSObject

+ (BOOL)isEmptyObject:(id)obj;
+ (BOOL)isEmptyString:(NSString *)string;

@end

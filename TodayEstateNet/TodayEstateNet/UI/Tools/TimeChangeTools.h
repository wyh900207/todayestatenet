//
//  TimeChangeTools.h
//  SP2P_9
//
//  Created by md005 on 16/4/12.
//  Copyright © 2016年 EIMS. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TimeChangeTools : NSObject

+ (NSString *)timeFormatterWithTime:(NSString *)str;

+ (NSString *)regularTimeFormatterWithTime:(NSString *)str;

+ (NSString *)shortTimeFormatterWithTime:(NSString *)str;

+ (NSString *)longTimeFormatterWithTime:(NSString *)str;

+ (NSString *)longSpecalTimeFormatterWithTime:(NSString *)str;

+ (NSString *)customTimeFormatterWithTime:(NSString *)str andFormat:(NSString *)format;

+ (NSString *)agoTimeForematterWithTime:(NSString *)str;

+ (NSTimeInterval)getIntervalTimeWithValue:(long)timeValue;

@end

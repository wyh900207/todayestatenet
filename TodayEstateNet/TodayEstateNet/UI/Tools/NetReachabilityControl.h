//
//  NetReachabilityControl.h
//  CPS
//
//  Created by dj-xxzx-10065 on 2017/4/19.
//  Copyright © 2017年 dj-xxzx-10065. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^netStateBlock)(NSInteger netState);

@interface NetReachabilityControl : NSObject

//单例
+ (id)reachabilityInstance;
//网络状态码回调
-(void)netWorkState:(netStateBlock)block;

@end

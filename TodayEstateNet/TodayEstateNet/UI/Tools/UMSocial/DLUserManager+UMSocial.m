//
//  DLUserManager+UMSocial.m
//  DateLove
//
//  Created by  HomerLynn on 2018/10/13.
//  Copyright © 2018 Zilu. All rights reserved.
//

#import "DLUserManager+UMSocial.h"

@implementation DLUserManager (UMSocial)

- (void)loginWithWechat:(WechatLoginHandle)wechatLoginHandle {
    [self getUserInfoForPlatform:UMSocialPlatformType_WechatSession response:wechatLoginHandle];
}

- (void)getUserInfoForPlatform:(UMSocialPlatformType)platformType response:(WechatLoginHandle)wechatLoginHandle {
    if (platformType == UMSocialPlatformType_WechatSession) {
        [self getWechatUserInfoWithResponse:wechatLoginHandle];
    }
}

/// 微信登录
- (void)getWechatUserInfoWithResponse:(WechatLoginHandle)wechatLoginHandle {
    [[UMSocialManager defaultManager] getUserInfoWithPlatform:UMSocialPlatformType_WechatSession currentViewController:nil completion:^(id result, NSError *error) {
        TEN_LOG(@"----- wechat ---%@ %@", result, error);
        if (error) {
            [NSObject showHudTipStr:@"微信登录失败"];
        } else {
            UMSocialUserInfoResponse *resp = result;
            
            // 授权信息
            TEN_LOG(@"Wechat uid: %@", resp.uid);
            TEN_LOG(@"Wechat openid: %@", resp.openid);
            TEN_LOG(@"Wechat unionid: %@", resp.unionId);
            TEN_LOG(@"Wechat accessToken: %@", resp.accessToken);
            TEN_LOG(@"Wechat refreshToken: %@", resp.refreshToken);
            TEN_LOG(@"Wechat expiration: %@", resp.expiration);
            
            // 用户信息
            TEN_LOG(@"Wechat name: %@", resp.name);
            TEN_LOG(@"Wechat iconurl: %@", resp.iconurl);
            TEN_LOG(@"Wechat gender: %@", resp.unionGender);
            
            // 第三方平台SDK源数据
            TEN_LOG(@"Wechat originalResponse: %@", resp.originalResponse);
            dispatch_async(dispatch_get_main_queue(), ^{
                wechatLoginHandle(resp);
            });
        }
    }];
}
@end

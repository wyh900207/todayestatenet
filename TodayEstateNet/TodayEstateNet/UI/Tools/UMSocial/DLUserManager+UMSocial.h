//
//  DLUserManager+UMSocial.h
//  DateLove
//
//  Created by  HomerLynn on 2018/10/13.
//  Copyright © 2018 Zilu. All rights reserved.
//

#import "DLUserManager.h"
#import <UMSocialCore/UMSocialCore.h>

@interface DLUserManager (UMSocial)

- (void)loginWithWechat:(void (^)(UMSocialUserInfoResponse *))wechatResponse;

@end

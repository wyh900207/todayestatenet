//
//  DLUserManager.h
//  DateLove
//
//  Created by HomerLynn on 2018/10/11.
//  Copyright © 2018 Zilu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UMSocialCore/UMSocialCore.h>

#define ShareUserManager [DLUserManager shareManager]

typedef void(^WechatLoginHandle)(UMSocialUserInfoResponse *wechatUserInfo);

@interface DLUserManager : NSObject

// MARK: -------------------------- Object --------------------------
// 单例对象
+ (instancetype)shareManager;

// MARK: ---------------------- Status & Proprety --------------------------

- (BOOL)loginState;
- (NSString *)phone;
- (NSString *)userID;
- (NSString *)loginToken;

// MARK: -------------------------- Handler --------------------------

// 登录成功`Handler`
- (void)loginSuccessWith:(NSDictionary *)userInfo;
// 登录状态下的操作
- (void)actionAfterLogin:(void (^)(void))handler currentController:(UIViewController *)controller;

// MARK: -------------------------- Action --------------------------

// 退出
- (void)logout;

@end

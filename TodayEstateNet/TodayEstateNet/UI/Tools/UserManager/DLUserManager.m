//
//  DLUserManager.m
//  DateLove
//
//  Created by HomerLynn on 2018/10/11.
//  Copyright © 2018 Zilu. All rights reserved.
//

#import "DLUserManager.h"
#import "TENLoginViewController.h"

@implementation DLUserManager

// Singleton object

+ (instancetype)shareManager {
    static DLUserManager *manager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [DLUserManager new];
    });
    
    return manager;
}

#pragma mark - Status & Proprety

- (BOOL)loginState {
    NSString *token = (NSString *)[TEN_UD objectForKey:@"loginToken"];
    BOOL result = [DLNull isEmptyString:token];
    return !result;
}

- (NSString *)phone {
    if ([[DLUserManager shareManager] loginState]) {
        NSString * phone = [TEN_UD valueForKey:@"yyAccount.phone"];
        
        return phone;
    }
    return @"";
}

- (NSString *)userID {
    if ([[DLUserManager shareManager] loginState]) {
        NSString * userID = [TEN_UD valueForKey:@"yyAccount.id"];
        
        return userID;
    }
    return @"";
}

- (NSString *)loginToken {
    if ([[DLUserManager shareManager] loginState]) {
        NSString * loginToken = [TEN_UD valueForKey:@"loginToken"];
        
        return loginToken;
    }
    return @"";
}

#pragma mark - Action

- (void)logout {
    if (!self.loginState) {
        [NSObject showHudTipStr:@"您当前还未登录"];
        return;
    }
    [TEN_UD removeObjectForKey:kUserAccountId];
    [TEN_UD removeObjectForKey:kUserLoginToken];
    [TEN_UD removeObjectForKey:kUserPhone];
    [TEN_UD removeObjectForKey:kUserName];
    [TEN_UD removeObjectForKey:kUserId];
    [TEN_UD removeObjectForKey:kUserAvatar];
    TEN_UD_SYNC
    NSNotification * logoutSuccess = [[NSNotification alloc] initWithName:YMEC_LOGOUT_SUCCESS_NOTIFICATION object:nil userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotification:logoutSuccess];
}

#pragma mark - Handler

- (void)loginSuccessWith:(NSDictionary *)userInfo {
    NSDictionary * content = userInfo[@"content"];
    NSDictionary * yyAccount = content[@"yyAccount"];
    
    NSString * myAccountId = DL_IS_NULL(yyAccount[@"id"]);
    NSString * loginToken = DL_IS_NULL(content[@"loginToken"]);
    NSString * userName = DL_IS_NULL(yyAccount[@"yyUser"][@"userName"]);
    NSString * phone = DL_IS_NULL(yyAccount[@"phone"]);
    NSString * yyUserId = DL_IS_NULL(yyAccount[@"yyUser"][@"id"]);
    NSString * avatar = DL_IS_NULL(yyAccount[@"yyUser"][@"avatar"]);
    NSString *type = DL_IS_NULL(yyAccount[@"type"]);//1游客2会员3VIP
    
    [TEN_UD setValue:myAccountId forKey:@"yyAccount.id"];
    [TEN_UD setValue:loginToken forKey:@"loginToken"];
    [TEN_UD setValue:phone forKey:@"yyAccount.phone"];
    [TEN_UD setValue:userName forKey:@"userName"];
    [TEN_UD setValue:yyUserId forKey:@"yyUserId"];
    [TEN_UD setValue:avatar forKey:@"yyUser.avatar"];
    [TEN_UD setValue:type forKey:@"yyAccount.type"];
    
    [TEN_UD synchronize];
    NSNotification * loginSuccess = [[NSNotification alloc] initWithName:YMEC_LOGIN_SUCCESS_NOTIFICATION object:nil userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotification:loginSuccess];
    // 设置极光推送别名
    //[self updateJPushAlisa];
    // 友盟统计登录账号
    //NSString *account = [NSString stringWithFormat:@"%@", [YMECUserManager share].username];
    //[MobClick profileSignInWithPUID:account];
    //[UserManager updateUserLever];
}

- (void)actionAfterLogin:(void (^)(void))handler currentController:(UIViewController *)controller {
    if (handler) {
        if ([[DLUserManager shareManager] loginState]) {
            handler();
        }
        else {
            UINavigationController *loginNav = [[UINavigationController alloc] initWithRootViewController:[TENLoginViewController new]];
            [controller presentViewController:loginNav animated:YES completion:nil];
        }
    }
}

@end

//
//  UIView+CornerRaidus.m
//  YMEconomicCircle
//
//  Created by HomerLynn on 2018/6/1.
//  Copyright © 2018年 YME. All rights reserved.
//

#import "UIView+CornerRaidus.h"

@implementation UIView (CornerRaidus)

- (void)maskWith:(CGFloat)cornerRaidus {
    self.layer.cornerRadius = cornerRaidus;
    self.layer.masksToBounds = YES;
}

@end

//
//  NSString+Change.m
//  HomeGardenTreasure
//
//  Created by paat on 2018/3/19.
//  Copyright © 2018年 Paat. All rights reserved.
//

#import "NSString+Change.h"

@implementation NSString (Change)
-(CGSize)sizeWithFont:(UIFont *)font constrainedToSize:(CGSize)size mode:(NSLineBreakMode)lineBreakMode
{
    CGSize result;
    if (!font) font = TENFont12;
    NSMutableDictionary *dic = [NSMutableDictionary new];
    dic[NSFontAttributeName] = font;
    if (lineBreakMode !=NSLineBreakByWordWrapping) {
        NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
        paragraphStyle.lineBreakMode = lineBreakMode;
        dic[NSParagraphStyleAttributeName] =paragraphStyle;
    }
    CGRect rect  = [self boundingRectWithSize:size options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:dic context:nil];
    result = rect.size;
    return result;
}
- (CGFloat)widthWithFont:(UIFont *)font width:(CGFloat)width
{
    CGSize size = [self sizeWithFont:font constrainedToSize:CGSizeMake(width, HUGE) mode:NSLineBreakByWordWrapping];
    return size.width;
}

- (CGFloat)heightWithFont:(UIFont *)font width:(CGFloat)width
{
    CGSize size = [self sizeWithFont:font constrainedToSize:CGSizeMake(width, HUGE) mode:NSLineBreakByWordWrapping];
    return size.height;
}

- (CGFloat)widthWithFont:(UIFont *)font
{
    CGSize size = [self sizeWithFont:font constrainedToSize:CGSizeMake(HUGE, HUGE) mode:NSLineBreakByWordWrapping];
    return size.width;
}

- (CGFloat)heightWithFont:(UIFont *)font
{
    CGSize size = [self sizeWithFont:font constrainedToSize:CGSizeMake(HUGE, HUGE) mode:NSLineBreakByWordWrapping];
    return size.height;
}

-(CGSize)sizeWithString:(NSString *)str font:(UIFont *)font maxSize:(CGSize)maxSize
{
    NSDictionary *dict = @{NSFontAttributeName :font};
    CGSize size = [str boundingRectWithSize:maxSize options: NSStringDrawingTruncatesLastVisibleLine |
                   NSStringDrawingUsesLineFragmentOrigin |
                   NSStringDrawingUsesFontLeading attributes:dict context:nil].size;
    return size;
}

- (NSMutableAttributedString *)changeTextWithString:(NSString *)str withPoint:(int)point WithPreFont:(UIFont *)preFont withPreColor:(UIColor *)preColor withBehindFont:(UIFont *)behindFont withBehindColor:(UIColor *)behindColor{
    NSMutableAttributedString* attributedStr = [[NSMutableAttributedString alloc] initWithString:str];
    NSRange range1 = NSMakeRange(0, point);
    NSRange range2 = NSMakeRange(point, attributedStr.length-point);
    [attributedStr addAttribute:NSForegroundColorAttributeName value:preColor range:range1];//设置前面字符的颜色
    [attributedStr addAttribute:NSFontAttributeName value:preFont range:range1];//设置前面字符的字号大小
    [attributedStr addAttribute:NSFontAttributeName value:behindFont range:range2];//设置后面字符的字号大小
    [attributedStr addAttribute:NSForegroundColorAttributeName value:behindColor range:range2];//设置后面字符的颜色
    return attributedStr;
}

- (NSMutableAttributedString *)changeTextForSlashWithString:(NSString *)str withPoint:(int)point WithPreFont:(UIFont *)preFont withPreColor:(UIColor *)preColor withBehindFont:(UIFont *)behindFont withBehindColor:(UIColor *)behindColor{
    NSMutableAttributedString* attributedStr = [[NSMutableAttributedString alloc] initWithString:str];
    NSRange range1 = NSMakeRange(0, point);
    NSRange range2 = NSMakeRange(point, attributedStr.length-point);
    [attributedStr addAttribute:NSForegroundColorAttributeName value:preColor range:range1];//设置前面字符的颜色
    [attributedStr addAttribute:NSFontAttributeName value:preFont range:range1];//设置前面字符的字号大小
    [attributedStr addAttribute:NSFontAttributeName value:behindFont range:range2];//设置后面字符的字号大小
    [attributedStr addAttribute:NSForegroundColorAttributeName value:behindColor range:range2];//设置后面字符的颜色
    [attributedStr addAttribute:NSStrikethroughStyleAttributeName value:@(2) range:range2];
    [attributedStr addAttribute:NSBaselineOffsetAttributeName value:@(NSUnderlineStyleSingle) range:range2];
    return attributedStr;
}

+(NSString *)stringWithlong:(int)t string:(NSString *)str {
    if (str.length<=t) return str;
    
    NSString *string =  [str substringToIndex:t];
    return string;
}

@end

//
//  NSString+Change.h
//  HomeGardenTreasure
//
//  Created by paat on 2018/3/19.
//  Copyright © 2018年 Paat. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Change)
/**
 *  根据字体计算size
 *
 *  @param font 字体
 *  @param size Rect大小
 *  @param lineBreakMode 分行模式
 */
- (CGSize)sizeWithFont:(UIFont *)font constrainedToSize:(CGSize)size mode:(NSLineBreakMode)lineBreakMode;

/**
 *  根据字体计算宽度
 *
 *  @param font 字体
 */
- (CGFloat)widthWithFont:(UIFont *)font;

- (CGFloat)widthWithFont:(UIFont *)font width:(CGFloat)width;

- (CGFloat)heightWithFont:(UIFont *)font;

- (CGFloat)heightWithFont:(UIFont *)font width:(CGFloat)width;

-(CGSize)sizeWithString:(NSString *)str font:(UIFont *)font maxSize:(CGSize)maxSize;

- (NSMutableAttributedString *)changeTextWithString:(NSString *)str withPoint:(int)point WithPreFont:(UIFont *)preFont withPreColor:(UIColor *)preColor withBehindFont:(UIFont *)behindFont withBehindColor:(UIColor *)behindColor;

- (NSMutableAttributedString *)changeTextForSlashWithString:(NSString *)str withPoint:(int)point WithPreFont:(UIFont *)preFont withPreColor:(UIColor *)preColor withBehindFont:(UIFont *)behindFont withBehindColor:(UIColor *)behindColor;

/*
 
 截取字符串
 */
+(NSString *)stringWithlong:(int)t string:(NSString *)str;
@end

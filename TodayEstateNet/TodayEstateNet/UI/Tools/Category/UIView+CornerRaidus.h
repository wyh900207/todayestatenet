//
//  UIView+CornerRaidus.h
//  YMEconomicCircle
//
//  Created by HomerLynn on 2018/6/1.
//  Copyright © 2018年 YME. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (CornerRaidus)

/// 切圆角
- (void)maskWith:(CGFloat)cornerRaidus;

@end

//
//  NSString+Time.m
//  YMEconomicCircle
//
//  Created by HomerLynn on 2018/5/20.
//  Copyright © 2018年 YME. All rights reserved.
//

#import "NSString+Time.h"

@implementation NSString (Time)

- (NSString *)convertToTime {
    NSDateFormatter *stampFormatter = [[NSDateFormatter alloc] init];
    [stampFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    //以 1970/01/01 GMT为基准，然后过了secs秒的时间
    NSDate *stampDate = [NSDate dateWithTimeIntervalSince1970:self.integerValue * 0.001];
    
    return [stampFormatter stringFromDate:stampDate];
}

- (NSString *)dateTimeConvertToTime {
    NSDateFormatter *stampFormatter = [[NSDateFormatter alloc] init];
    [stampFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    //以 1970/01/01 GMT为基准，然后过了secs秒的时间
    NSDate *stampDate;
    if (self.length < 11) {
        stampDate = [NSDate dateWithTimeIntervalSince1970:self.integerValue];
    } else {
        stampDate = [NSDate dateWithTimeIntervalSince1970:self.integerValue * 0.001];
    }
    
    return [stampFormatter stringFromDate:stampDate];
}

- (NSString *)convertToData {
    NSDateFormatter *stampFormatter = [[NSDateFormatter alloc] init];
    [stampFormatter setDateFormat:@"YYYY-MM-dd"];
    //以 1970/01/01 GMT为基准，然后过了secs秒的时间
    NSDate *stampDate;
    if (self.length < 11) {
        stampDate = [NSDate dateWithTimeIntervalSince1970:self.integerValue];
    } else {
        stampDate = [NSDate dateWithTimeIntervalSince1970:self.integerValue * 0.001];
    }
    
    return [stampFormatter stringFromDate:stampDate];
}

- (NSString *)convertToTimeStamp {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    //指定时间显示样式: HH表示24小时制 hh表示12小时制
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    NSDate * date = [formatter dateFromString:self];
    long timeStamp = [date timeIntervalSince1970] * 1000;
    
    return [NSString stringWithFormat:@"%ld", timeStamp];
}

- (NSString *)timeStamp {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd"];
    NSDate * date = [formatter dateFromString:self];
    long timeStamp = [date timeIntervalSince1970] * 1000;
    
    return [NSString stringWithFormat:@"%ld", timeStamp];
}

+ (NSString *)currentTimeStamp {
    NSDate* date = [NSDate dateWithTimeIntervalSinceNow:0];//获取当前时间0秒后的时间
    NSTimeInterval time=[date timeIntervalSince1970];// *1000 是精确到毫秒，不乘就是精确到秒
    NSString *timeString = [NSString stringWithFormat:@"%.0f", time];
    return timeString;
}

+ (NSString *)getLaterFrom:(NSString *)timeStamp {
    // 获取当前时时间戳 1466386762.345715 十位整数 6位小数
    NSTimeInterval currentTime = [[NSDate date] timeIntervalSince1970];
    // 创建歌曲时间戳(后台返回的时间 一般是13位数字)
    NSTimeInterval createTime = timeStamp.integerValue/1000;
    // 时间差
    NSTimeInterval time = currentTime - createTime;
    
    // 秒转小时
    NSInteger hours = time/3600;
    if (hours<24) {
        return [NSString stringWithFormat:@"%ld小时前",hours];
    }
    //秒转天数
    NSInteger days = time/3600/24;
    if (days < 30) {
        return [NSString stringWithFormat:@"%ld天前",days];
    }
    //秒转月
    NSInteger months = time/3600/24/30;
    if (months < 12) {
        return [NSString stringWithFormat:@"%ld月前",months];
    }
    //秒转年
    NSInteger years = time/3600/24/30/12;
    return [NSString stringWithFormat:@"%ld年前",years];
}

@end

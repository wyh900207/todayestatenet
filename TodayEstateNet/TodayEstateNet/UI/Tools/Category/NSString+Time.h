//
//  NSString+Time.h
//  YMEconomicCircle
//
//  Created by HomerLynn on 2018/5/20.
//  Copyright © 2018年 YME. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Time)

// 时间戳转时间  lg: YYYY-MM-dd HH:mm
- (NSString *)convertToTime;
// 约会专用
- (NSString *)dateTimeConvertToTime;
/// 时间戳转时间 lg: YYYY-MM-dd
- (NSString *)convertToData;
// 时间转时间戳  lg: YYYY-MM-dd HH:mm:ss
- (NSString *)convertToTimeStamp;
// 获取timeStamp到现在的时间
+ (NSString *)getLaterFrom:(NSString *)timeStamp;
- (NSString *)timeStamp;
+ (NSString *)currentTimeStamp;

@end

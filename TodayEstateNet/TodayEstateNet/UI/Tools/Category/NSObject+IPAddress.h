//
//  NSObject+IPAddress.h
//  YMEconomicCircle
//
//  Created by HomerLynn on 2018/6/4.
//  Copyright © 2018年 YME. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (IPAddress)

+ (NSString *)getIPAddress;

@end

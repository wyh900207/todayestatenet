//
//  YMECPayTools.h
//  YMEconomicCircle
//
//  Created by HomerLynn on 2018/6/2.
//  Copyright © 2018年 YME. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AlipaySDK/AlipaySDK.h>
#import "WXApi.h"
#import "WXApiObject.h"
#import "WechatAuthSDK.h"

typedef NS_ENUM(NSUInteger, YMECPayType) {
    YMECPayTypeUnity = 1,               // 社群
    YMECPayTypeDoc = 2,                 // 文库
    YMECPayTypeClass = 3,               // 课程
    YMECPayTypeProject = 4,             // 项目
    YMECPayTypeDateDepost = 5,          // 约会保证金
    YMECPayTypeAttendDeposit = 6,       // 报名保证金
    YMECPayTypeMall = 7,                // 商城
    YMECPayTypeRecharge = 8,            // 充值
    YMECPayTypeMember = 9               // 会员
};

@interface YMECPayTools : NSObject <WXApiDelegate>

// 1: 社群 /2: 文库 /3: 课程 /4: 商城 /5: 发起约会 /6: 报名约会
+ (instancetype)share;

// 发起支付(购买课程/项目等)
- (void)payWithPrice:(NSNumber *)price goodId:(NSString *)goodsId goodsType:(YMECPayType)goodsType completion:(void(^)(void))completionHandle;
// 获取支付宝订单状态(该接口仅限于AppDelegate中调用)
- (void)getAliPayResult;
/// 充值
- (void)rechargeWith:(NSString *)goodsId completion:(void(^)(void))completionHandle;
// 发起约会
- (void)publishDateWith:(NSDictionary *)dateInfo depositCash:(NSNumber *)deposit completion:(void(^)(void))completionHandle;
// 报名约会
- (void)dateSignUpWith:(NSDictionary *)info depositCash:(NSNumber *)deposit completion:(void(^)(void))completionHandle;
// 删除约会/报名信息
- (void)deleteInfo;

@end

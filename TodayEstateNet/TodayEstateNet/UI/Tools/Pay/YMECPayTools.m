//
//  YMECPayTools.m
//  YMEconomicCircle
//
//  Created by HomerLynn on 2018/6/2.
//  Copyright © 2018年 YME. All rights reserved.
//

#import "YMECPayTools.h"
//#import "KeyboardManager.h"
#import "YMECOtherPayViewController.h"
#import "YNPayPasswordView.h"
#import "YMECResetPayPasswordViewController.h"
//#import <IQUIWindow+Hierarchy.h>
#import "TENPaymentRequest.h"
@interface YMECPayTools () <YNPayPasswordViewDelegate, YNPayPasswordViewDelegate>

@property (copy  , nonatomic) NSString * currentWechatTradeNo;
@property (strong, nonatomic) YNPayPasswordView * payPswView;
@property (strong, nonatomic) YNPayPasswordView * publishDatePswView;
@property (strong, nonatomic) YNPayPasswordView * signUpDatePswView;

@property (strong, nonatomic) NSNumber * price;
@property (copy  , nonatomic) NSString * goodsId;
@property (copy  , nonatomic) NSString * goodsType;
@property (strong, nonatomic) NSMutableDictionary * dateInfo;   // 约会参数
@property (strong, nonatomic) NSNumber * depositCash;           // 约会保证金
@property (strong, nonatomic) NSMutableDictionary * dateSignUpInfo;    // 报名约会参数

@property (assign, nonatomic) NSNumber * type;                  // 支付类型
@property (copy  , nonatomic) void (^paySuccessHandle)(void);       // 支付成功回调;

@end

@implementation YMECPayTools

+ (instancetype)share {
    static id instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [self new];
    });
    return instance;
}

- (YNPayPasswordView *)payPswView {
    if (!_payPswView) {
        _payPswView = [[YNPayPasswordView alloc] initWithFrame:CGRectMake(0, 0, TENScreenWidth, TENScreenHeight) WithTitle:@"输入支付密码" WithSubTitle:@""];
        _payPswView.payPasswordDelegate = self;
        __weak typeof(self) wSelf = self;
        _payPswView.otherPayMethodAction = ^{
            __strong typeof(wSelf) sSelf = wSelf;
            [sSelf.payPswView hiddenAllPoint];
            [sSelf.payPswView removeFromView];
            [sSelf presentThirdPayController];
        };
        _payPswView.forgetPayPassword = ^{
            __strong typeof(wSelf) sSelf = wSelf;
            [sSelf.payPswView hiddenAllPoint];
            [sSelf.payPswView removeFromView];
            YMECResetPayPasswordViewController * controller = [[YMECResetPayPasswordViewController alloc] initWithNibName:@"YMECResetPayPasswordViewController" bundle:nil];

            TENNavigationViewController * nav = [[TENNavigationViewController alloc] initWithRootViewController:controller];
            controller.phone = TEN_USER_PHONE;
            UIViewController * currentController = [sSelf getCurrentViewController];
            [currentController presentViewController:nav animated:YES completion:nil];
        };
    }
    return _payPswView;
}

- (YNPayPasswordView *)publishDatePswView {
    if (!_publishDatePswView) {
        _publishDatePswView = [[YNPayPasswordView alloc] initWithFrame:CGRectMake(0, 0, TENScreenWidth, TENScreenHeight) WithTitle:@"输入支付密码" WithSubTitle:@""];
        _publishDatePswView.payPasswordDelegate = self;
        __weak typeof(self) wSelf = self;
        _publishDatePswView.otherPayMethodAction = ^{
            __strong typeof(wSelf) sSelf = wSelf;
            [sSelf.publishDatePswView hiddenAllPoint];
            [sSelf.publishDatePswView removeFromView];
            [sSelf saveDateInfoWith:@"2"];
        };
        _publishDatePswView.forgetPayPassword = ^{
            __strong typeof(wSelf) sSelf = wSelf;
            [sSelf.payPswView hiddenAllPoint];
            [sSelf.payPswView removeFromView];
            YMECResetPayPasswordViewController * controller = [[YMECResetPayPasswordViewController alloc] initWithNibName:@"YMECResetPayPasswordViewController" bundle:nil];

            TENNavigationViewController * nav = [[TENNavigationViewController alloc] initWithRootViewController:controller];
            controller.phone = TEN_USER_PHONE;
            UIViewController * currentController = [sSelf getCurrentViewController];
            [currentController presentViewController:nav animated:YES completion:nil];
        };
    }
    return _publishDatePswView;
}

- (YNPayPasswordView *)signUpDatePswView {
    if (_signUpDatePswView) {
        _signUpDatePswView = [[YNPayPasswordView alloc] initWithFrame:CGRectMake(0, 0, TENScreenWidth, TENScreenHeight) WithTitle:@"输入支付密码" WithSubTitle:@""];
        _signUpDatePswView.payPasswordDelegate = self;
        __weak typeof(self) wSelf = self;
        _signUpDatePswView.otherPayMethodAction = ^{
            __strong typeof(wSelf) sSelf = wSelf;
            [sSelf.signUpDatePswView hiddenAllPoint];
            [sSelf.signUpDatePswView removeFromView];
            [sSelf signUpDateWith:@"2"];
        };
        _signUpDatePswView.forgetPayPassword = ^{
            __strong typeof(wSelf) sSelf = wSelf;
            [sSelf.payPswView hiddenAllPoint];
            [sSelf.payPswView removeFromView];
            YMECResetPayPasswordViewController * controller = [[YMECResetPayPasswordViewController alloc] initWithNibName:@"YMECResetPayPasswordViewController" bundle:nil];

            TENNavigationViewController * nav = [[TENNavigationViewController alloc] initWithRootViewController:controller];
            controller.phone = TEN_USER_PHONE;
            UIViewController * currentController = [sSelf getCurrentViewController];
            [currentController presentViewController:nav animated:YES completion:nil];
        };
    }
    return _signUpDatePswView;
}

#pragma mark -

- (void)aliPayWith:(NSDictionary *)productInfo {
    @weakify(self);
    [TENPaymentRequest zfbUnifiedOrderWithParameters:productInfo response:^(NSDictionary * _Nonnull responseObject) {
        @strongtify(self);
        TEN_LOG(@"%@", responseObject);
        NSDictionary * content = responseObject[@"content"];
        NSString * prepay_id = [content objectForKey:@"zfbResultStr"];
        self.currentWechatTradeNo = [content objectForKey:@"orderNo"];
        [[AlipaySDK defaultService] payOrder:prepay_id fromScheme:@"jrfwfjjalipayurlschemes" callback:^(NSDictionary *resultDic) {
            TEN_LOG(@"%@", resultDic);
            [self getAliPayResult];
        }];
    } failure:^(NSError * _Nonnull error) {
        TEN_LOG(@"%@", error);
    }];
//    [self postWithURL:YMEC_PAY_ALI_URL params:productInfo success:^(NSDictionary *dict) {
//        TEN_LOG(@"%@", dict);
//        NSDictionary * content = dict[@"content"];
//        NSString * prepay_id = [content objectForKey:@"zfbResultStr"];
//        self.currentWechatTradeNo = [content objectForKey:@"orderNo"];
//        [[AlipaySDK defaultService] payOrder:prepay_id fromScheme:@"yimicaiquanalipaydistribute" callback:^(NSDictionary *resultDic) {
//            TEN_LOG(@"%@", resultDic);
//            [self getAliPayResult];
//        }];
//    } failure:^(NSError *error) {
//        TEN_LOG(@"%@", error);
//    } hud:NO];
}

- (void)wechatPayWith:(NSDictionary *)productInfo {
    [TENPaymentRequest wxUnifiedOrderWithParameters:productInfo response:^(NSDictionary * _Nonnull responseObject) {
        TEN_LOG(@"%@", responseObject);
        NSDictionary * content = responseObject[@"content"];
        NSString * timestamp = [content objectForKey:@"timestamp"];
        NSString * package = [content objectForKey:@"package_"];
        NSString * sign = [content objectForKey:@"sign"];
        NSString * prepay_id = [content objectForKey:@"prepay_id"];
        NSString * nonce_str = [content objectForKey:@"nonce_str"];
        NSString * partnerid = [content objectForKey:@"mch_id"];
        self.currentWechatTradeNo = content[@"orderNo"];
        PayReq * req            = [[PayReq alloc] init];
        req.partnerId           = partnerid;
        req.prepayId            = prepay_id;
        req.nonceStr            = nonce_str;
        req.timeStamp           = timestamp.intValue;
        req.package             = package;
        req.sign                = sign;
        [WXApi sendReq:req];
    } failure:^(NSError * _Nonnull error) {
        TEN_LOG(@"%@", error);
    }];
//    [self postWithURL:YMEC_PAY_WECHAT_URL params:productInfo success:^(NSDictionary *dict) {
//        TEN_LOG(@"%@", dict);
//        NSDictionary * content = dict[@"content"];
//        NSString * timestamp = [content objectForKey:@"timestamp"];
//        NSString * package = [content objectForKey:@"package_"];
//        NSString * sign = [content objectForKey:@"sign"];
//        NSString * prepay_id = [content objectForKey:@"prepay_id"];
//        NSString * nonce_str = [content objectForKey:@"nonce_str"];
//        NSString * partnerid = [content objectForKey:@"mch_id"];
//        self.currentWechatTradeNo = content[@"orderNo"];
//        PayReq * req            = [[PayReq alloc] init];
//        req.partnerId           = partnerid;
//        req.prepayId            = prepay_id;
//        req.nonceStr            = nonce_str;
//        req.timeStamp           = timestamp.intValue;
//        req.package             = package;
//        req.sign                = sign;
//        [WXApi sendReq:req];
//    } failure:^(NSError *error) {
//        TEN_LOG(@"%@", error);
//    } hud:NO];
}

#pragma mark -

/// 充值
- (void)rechargeWith:(NSString *)goodsId completion:(void (^)(void))completionHandle {
    self.goodsId = goodsId;
    self.goodsType = @"1";//商品类型1充值
    self.paySuccessHandle = completionHandle;
    [self presentThirdPayController];
}

#pragma mark - 购买

/// 1 STEP: 获取余额
- (void)payWithPrice:(NSNumber *)price goodId:(NSString *)goodsId goodsType:(YMECPayType)goodsType completion:(void (^)(void))completionHandle {
    self.type = @(0);
    self.price = price;
    self.goodsId = goodsId;
    self.goodsType = [NSString stringWithFormat:@"%lu", goodsType];
    self.paySuccessHandle = completionHandle;
    
    UIViewController * controller = [self getCurrentViewController];
//    if (![[YMECUserManager share] loginState]) {
//        [[YMECUserManager share] loginWith:controller];
//        return;
//    }
    NSDictionary * params = @{@"accountId": kAccountId,
                              @"loginToken": kLoginToken
                              };
    __weak typeof(self) wSelf = self;
//    [self postWithURL:YMEC_LOGIN_GET_BALANCE_URL params:params success:^(NSDictionary *dict) {
//        __strong typeof(wSelf) sSelf = wSelf;
//        TEN_LOG(@"%@", dict);
//        [sSelf selectedPayTypeWith:dict];
//    } failure:^(NSError *error) {
//        TEN_LOG(@"%@", error);
//    } hud:NO];
}

// 2 STEP: 根据是否设置密码/余额是否充足, 选择支付方式
- (void)selectedPayTypeWith:(NSDictionary *)dict {
    NSNumber * balance = dict[@"content"][@"balance"];
    NSNumber * pwSet = dict[@"content"][@"pwSet"];
    // 2.1 判断是否设置支付密码
    if (pwSet.integerValue == 1) {
        // 2.1.1 判断余额是否充足
        if (self.price.floatValue > balance.floatValue) {
            // 余额不足
            [self balanceNotEnough];
        } else {
            // 余额充足
            UIViewController * controller = [self getCurrentViewController];
            if (![self.payPswView.TF becomeFirstResponder]){
                [self.payPswView.TF becomeFirstResponder];
            }
            [controller.view.window addSubview:self.payPswView];
        }
    } else {
        // 采用三方支付
        [self presentThirdPayController];
    }
}

// 3. STEP: 钱包余额不足, 询问是否继续支付
- (void)balanceNotEnough {
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"温馨提示" message:@"当前余额不足, 是否继续支付" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * canler = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    __weak typeof(self) weakSelf = self;
    // 3.1 继续支付, 选择三方支付
    UIAlertAction * sure = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        __strong typeof(weakSelf) strongSelf = weakSelf;
        [strongSelf presentThirdPayController];
    }];
    [alert addAction:canler];
    [alert addAction:sure];
    
    UIWindow * keyWindow = [[[UIApplication sharedApplication] delegate] window];
    UIViewController * controller = keyWindow.rootViewController;
    [controller presentViewController:alert animated:YES completion:nil];
}

// 4 STEP: 弹出第三方支付选择页面
- (void)presentThirdPayController {
    UIViewController * currentController = [self getCurrentViewController];
    // 第三方支付
    YMECOtherPayViewController * controller = [[YMECOtherPayViewController alloc] initWithNibName:@"YMECOtherPayViewController" bundle:nil];
    // self.definesPresentationContext = YES;
    controller.view.backgroundColor = YMEC_HEX_COLOR_A(0x000000, 0.5);
    controller.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    NSDictionary * params = @{@"accountId": kAccountId,
                              @"userName": TEN_USER_NAME,
                              @"mobileNumber": TEN_USER_PHONE,
                              @"goodsId": self.goodsId,
                              @"goodsType": self.goodsType,
                              @"payVersion": @"1.0.1",
                              @"spbillCreateIp": [NSObject getIPAddress]
                              };
    
    controller.wechaMethod = ^{
        TEN_LOG(@"微信支付");
        [[YMECPayTools share] wechatPayWith:params];
    };
    controller.aliMethod = ^{
        TEN_LOG(@"支付宝支付");
        [[YMECPayTools share] aliPayWith:params];
    };
    
    [currentController presentViewController:controller animated:YES completion:nil];
}

- (void)getWechatPayResult {
    NSDictionary * params = @{@"orderNo": self.currentWechatTradeNo};
    [TENPaymentRequest checkWxResultWithParameters:params response:^(NSDictionary * _Nonnull responseObject) {
        TEN_LOG(@"%@", responseObject);
        if ([responseObject[@"code"] integerValue] == 1) {
            [[NSNotificationCenter defaultCenter] postNotificationName:YMEC_PAY_SUCCESS_NOTIFICATION object:nil];
            if (self.paySuccessHandle) {
                self.paySuccessHandle();
            }
        }
    } failure:^(NSError * _Nonnull error) {
        TEN_LOG(@"%@", error);
    }];
//    [self postWithURL:YMEC_PAY_GET_WECHAT_RESULT_URL params:params success:^(NSDictionary *dict) {
//        TEN_LOG(@"%@", dict);
//        // [NSObject showHudTipStr:@"支付成功"];
//        [[NSNotificationCenter defaultCenter] postNotificationName:YMEC_PAY_SUCCESS_NOTIFICATION object:nil];
//        /// FIXME: 这里没有判断status
//        if (self.paySuccessHandle) {
//            self.paySuccessHandle();
//        }
//    } failure:^(NSError *error) {
//        TEN_LOG(@"%@", error);
//    } hud:NO];
}

- (void)getAliPayResult {
    NSDictionary * params = @{@"orderNo": self.currentWechatTradeNo};
    [TENPaymentRequest checkZfbOrderWithParameters:params response:^(NSDictionary * _Nonnull responseObject) {
        TEN_LOG(@"%@", responseObject);
        if ([responseObject[@"code"] integerValue] == 1) {
            [[NSNotificationCenter defaultCenter] postNotificationName:YMEC_PAY_SUCCESS_NOTIFICATION object:nil];
            if (self.paySuccessHandle) {
                self.paySuccessHandle();
            }
        }
        
        
    } failure:^(NSError * _Nonnull error) {
        TEN_LOG(@"%@", error.description);
    }];
//    [self postWithURL:YMEC_PAY_GET_ALI_RESULT_URL params:params success:^(NSDictionary *dict) {
//        TEN_LOG(@"%@", dict);
//        // [NSObject showHudTipStr:@"支付成功"];
//        [[NSNotificationCenter defaultCenter] postNotificationName:YMEC_PAY_SUCCESS_NOTIFICATION object:nil];
//        /// FIXME: 这里没有判断status
//        if (self.paySuccessHandle) {
//            self.paySuccessHandle();
//        }
//    } failure:^(NSError *error) {
//        TEN_LOG(@"%@", error);
//    } hud:NO];
}

- (UIViewController *)getCurrentViewController {
    UIWindow * keyWindow = [[[UIApplication sharedApplication] delegate] window];
    UIViewController * controller = keyWindow.rootViewController;
    
    return controller;
}

#pragma mark - WXApiDelegate
// 微信支付回调
- (void)onResp:(BaseResp *)resp {
    if([resp isKindOfClass:[PayResp class]]) {
        
        // 支付返回结果，实际支付结果需要去微信服务器端查询
        NSString *strMsg = [NSString stringWithFormat:@"支付结果"];
        
        switch (resp.errCode) {
            case WXSuccess:
//                 strMsg = @"支付结果：成功！";
//                 TEN_LOG(@"支付成功－PaySuccess，retcode = %d", resp.errCode);
                /** 发送支付成功的通知 */
//                 [[NSNotificationCenter defaultCenter] postNotificationName:HQPaySuccessNotification object:nil userInfo:nil];
//                 [NSObject showHudTipStr:@"支付成功"];
                [self getWechatPayResult];
                break;
            case WXErrCodeUserCancel:
                [self deleteInfo];
                
            default:
                strMsg = [NSString stringWithFormat:@"支付失败"];
                TEN_LOG(@"错误，retcode = %d, retstr = %@", resp.errCode,resp.errStr);
                /** 发送支付失败的通知 */
                // [[NSNotificationCenter defaultCenter] postNotificationName:HQCancelPayNotification object:nil userInfo:nil];
                break;
        }
    }
}

#pragma mark - YNPayPasswordViewDelegate

- (void)YNPayPasswordView:(YNPayPasswordView *)view WithPasswordString:(NSString *)Password {
    NSLog(@"密码 = %@",Password);
    view.TF.text = @"";
    if (view == self.payPswView) {
        [self verifyPayPasswordWith:view pw:Password type:1];
    } else if (view == self.publishDatePswView) {
        [self verifyPayPasswordWith:view pw:Password type:2];
    } else {
        [self verifyPayPasswordWith:view pw:Password type:3];
    }
}

/// 验证支付密码是否正确 type: 1: 普通/ 2: 发布约会/ 3: 报名约会
- (void)verifyPayPasswordWith:(YNPayPasswordView *)view pw:(NSString *)password type:(NSInteger)type {
    if (type == 1) {
        [self balancePayVerifySuccessWith:password];
        [view hiddenAllPoint];
        [view removeFromView];
    } else {
        NSDictionary * params = @{@"accountId": kAccountId,
                                  @"loginToken": kLoginToken,
//                                  @"password": [password md5String],
                                  @"vcode": @""
                                  };
//        [self postWithURL:YMEC_PAY_IS_PAYPSW_TURE_URL params:params success:^(NSDictionary *dict) {
//            TEN_LOG(@"%@", dict);
//            // 发布约会 和 报名 直接保存, 购买 调用余额支付
//            if (type == 2) {
//                [self saveDateInfoWith:@"1"];
//            } else if (type == 3) {
//                [self signUpDateWith:@"1"];
//            }
//            [view hiddenAllPoint];
//            [view removeFromView];
//        } failure:^(NSError *error) {
//            TEN_LOG(@"%@", error);
//        }];
    }
}

/// 余额下单
- (void)balancePayVerifySuccessWith:(NSString *)password {
    NSDictionary * params = @{@"accountId": kAccountId,
                              @"goodsId": self.goodsId,
                              @"goodsType": self.goodsType,
                              @"loginToken": kLoginToken,
//                              @"password": [password md5String]
                              };
    
//    [self postWithURL:YMEC_PAY_BALANCE_URL params:params success:^(NSDictionary *dict) {
//        TEN_LOG(@"%@", dict);
//        [NSObject showHudTipStr:@"支付成功"];
//    } failure:^(NSError *error) {
//        TEN_LOG(@"%@", error);
//    } hud:NO];
}

#pragma mark ------------------------
#pragma mark - 发布约会

/// 1 STEP 获取余额
- (void)publishDateWith:(NSDictionary *)dateInfo depositCash:(NSNumber *)deposit completion:(void(^)(void))completionHandle {
    self.type = @(5);
    self.dateInfo = [dateInfo mutableCopy];
    self.depositCash = deposit;
    self.paySuccessHandle = completionHandle;
    UIViewController * controller = [self getCurrentViewController];
//    if (![[YMECUserManager share] loginState]) {
//        [[YMECUserManager share] loginWith:controller];
//        return;
//    }
    NSDictionary * params = @{@"accountId": kAccountId,
                              @"loginToken": kLoginToken
                              };
    __weak typeof(self) wSelf = self;
//    [self postWithURL:YMEC_LOGIN_GET_BALANCE_URL params:params success:^(NSDictionary *dict) {
//        __strong typeof(wSelf) sSelf = wSelf;
//        TEN_LOG(@"%@", dict);
//        [sSelf dateSelectedPayTypeWith:dict];
//    } failure:^(NSError *error) {
//        TEN_LOG(@"%@", error);
//    } hud:NO];
}

// 2 STEP: 根据是否设置密码/余额是否充足, 选择支付方式
- (void)dateSelectedPayTypeWith:(NSDictionary *)dict {
    NSNumber * balance = dict[@"content"][@"balance"];
    NSNumber * pwSet = dict[@"content"][@"pwSet"];
    // 2.1 判断是否设置支付密码
    if (pwSet.integerValue == 1) {
        // 2.1.1 判断余额是否充足
        if (self.depositCash.floatValue > balance.floatValue) {
            // 余额不足, 调用saveData接口
            [self dateBalanceNotEnough];
        } else {
            // 余额充足, 选择钱包支付
            UIViewController * controller = [self getCurrentViewController];
            if (![self.payPswView.TF becomeFirstResponder]){
                [self.payPswView.TF becomeFirstResponder];
            }
            [controller.view.window addSubview:self.publishDatePswView];
        }
    } else {
        // 先保存, 再采用三方支付
        [self saveDateInfoWith:@"2"];
    }
}

// 3. STEP: 钱包余额不足, 询问是否继续支付
- (void)dateBalanceNotEnough {
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"温馨提示" message:@"当前余额不足, 是否继续支付" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * canler = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    __weak typeof(self) weakSelf = self;
    // 3.1 继续支付, 保存, 再支付
    UIAlertAction * sure = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        __strong typeof(weakSelf) strongSelf = weakSelf;
        // [strongSelf publishPresentThirdPayController:NO];
        [strongSelf saveDateInfoWith:@"2"];
    }];
    [alert addAction:canler];
    [alert addAction:sure];
    
    UIWindow * keyWindow = [[[UIApplication sharedApplication] delegate] window];
    UIViewController * controller = keyWindow.rootViewController;
    [controller presentViewController:alert animated:YES completion:nil];
}

/// 4. STEP 保存约会数据
- (void)saveDateInfoWith:(NSString *)payType {
    [self.dateInfo setValue:payType forKey:@"payType"];
//    [self postWithURL:YMEC_NEW_DATE_URL params:self.dateInfo success:^(NSDictionary *dict) {
//        TEN_LOG(@"%@", dict);
//        NSString * dateId = dict[@"content"][@"id"];
//        self.goodsId = dateId;
//        NSNumber * needPay = dict[@"content"][@"needPay"];
//        if (needPay.integerValue == 1) {
//            [self publishPresentThirdPayController];
//        } else {
//            [NSObject showHudTipStr:@"发布成功"];
//        }
//    } failure:^(NSError *error) {
//        TEN_LOG(@"%@", error);
//    } hud:NO];
}

// 5 STEP: 弹出第三方支付选择页面
- (void)publishPresentThirdPayController {
    UIViewController * currentController = [self getCurrentViewController];
    // 第三方支付
    YMECOtherPayViewController * controller = [[YMECOtherPayViewController alloc] initWithNibName:@"YMECOtherPayViewController" bundle:nil];
    // self.definesPresentationContext = YES;
    controller.view.backgroundColor = YMEC_HEX_COLOR_A(0x000000, 0.5);
    controller.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    
    NSDictionary * params = @{@"accountId": kAccountId,
//                              @"userName": [[YMECUserManager share] username],
                              @"mobileNumber": TEN_USER_PHONE,
                              @"goodsId": self.goodsId,
                              @"goodsType": @"5",
                              @"payVersion": @"1.0.1",
                              @"spbillCreateIp": [NSObject getIPAddress]
                              };
    
    __weak typeof(self) wself = self;
    controller.wechaMethod = ^{
        __strong typeof(wself) sself = wself;
        TEN_LOG(@"微信支付");
        [sself wechatPayWith:params];
    };
    controller.aliMethod = ^{
        __strong typeof(wself) sself = wself;
        TEN_LOG(@"支付宝支付");
        [sself aliPayWith:params];
    };
    controller.cancleMethod = ^{
        // 删除约会
        __strong typeof(wself) sself = wself;
        [sself deleMyDate];
    };
    
    [currentController presentViewController:controller animated:YES completion:nil];
}

// 6. STEP 删除约会

- (void)deleMyDate {
    NSDictionary * params = @{@"id": self.goodsId,
                              @"accountId": kAccountId
                              };
//    [self postWithURL:YMEC_DATE_DELETE_URL params:params success:^(NSDictionary *dict) {
//        TEN_LOG(@"%@", dict);
//    } failure:^(NSError *error) {
//        TEN_LOG(@"%@", error);
//    } hud:NO];
}

#pragma mark ------------------------
#pragma mark - 报名约会

- (void)dateSignUpWith:(NSDictionary *)info depositCash:(NSNumber *)deposit completion:(void (^)(void))completionHandle {
    self.type = @(6);
    self.dateSignUpInfo = [info mutableCopy];
    self.depositCash = deposit;
    self.paySuccessHandle = completionHandle;
    UIViewController * controller = [self getCurrentViewController];
//    if (![[YMECUserManager share] loginState]) {
//        [[YMECUserManager share] loginWith:controller];
//        return;
//    }
    NSDictionary * params = @{@"accountId": kAccountId,
                              @"loginToken": kLoginToken
                              };
    __weak typeof(self) wSelf = self;
//    [self postWithURL:YMEC_LOGIN_GET_BALANCE_URL params:params success:^(NSDictionary *dict) {
//        __strong typeof(wSelf) sSelf = wSelf;
//        TEN_LOG(@"%@", dict);
//        [sSelf signUpSelectedPayTypeWith:dict];
//    } failure:^(NSError *error) {
//        TEN_LOG(@"%@", error);
//    } hud:NO];
}

// 2 STEP: 根据是否设置密码/余额是否充足, 选择支付方式
- (void)signUpSelectedPayTypeWith:(NSDictionary *)dict {
    NSNumber * balance = dict[@"content"][@"balance"];
    NSNumber * pwSet = dict[@"content"][@"pwSet"];
    // 2.1 判断是否设置支付密码
    if (pwSet.integerValue == 1) {
        // 2.1.1 判断余额是否充足
        if (self.depositCash.floatValue > balance.floatValue) {
            // 余额不足, 调用saveData接口
            [self signUpBalanceNotEnough];
        } else {
            // 余额充足, 选择钱包支付
            UIViewController * controller = [self getCurrentViewController];
            if (![self.payPswView.TF becomeFirstResponder]){
                [self.payPswView.TF becomeFirstResponder];
            }
            [controller.view.window addSubview:self.signUpDatePswView];
        }
    } else {
        // 先保存, 再采用三方支付
        [self signUpDateWith:@"2"];
    }
}

// 3. STEP: 钱包余额不足, 询问是否继续支付
- (void)signUpBalanceNotEnough {
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"温馨提示" message:@"当前余额不足, 是否继续支付" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * canler = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    __weak typeof(self) weakSelf = self;
    // 3.1 继续支付, 保存, 再支付
    UIAlertAction * sure = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        __strong typeof(weakSelf) strongSelf = weakSelf;
        // [strongSelf publishPresentThirdPayController:NO];
        [strongSelf signUpDateWith:@"2"];
    }];
    [alert addAction:canler];
    [alert addAction:sure];
    
    UIWindow * keyWindow = [[[UIApplication sharedApplication] delegate] window];
    UIViewController * controller = keyWindow.rootViewController;
    [controller presentViewController:alert animated:YES completion:nil];
}

/// 4. STEP: signUp date
- (void)signUpDateWith:(NSString *)payType {
    [self.dateSignUpInfo setValue:payType forKey:@"payType"];
//    [self postWithURL:YMEC_JOIN_DATE_URL params:self.dateSignUpInfo success:^(NSDictionary *dict) {
//        TEN_LOG(@"%@", dict);
//        NSString * dateId = dict[@"content"][@"id"];
//        self.goodsId = dateId;
//        NSNumber * needPay = dict[@"content"][@"needPay"];
//        if (needPay.integerValue == 1) {
//            [self signUpPresentThirdPayController];
//        } else {
//            [NSObject showHudTipStr:@"加入成功"];
//        }
//    } failure:^(NSError *error) {
//        TEN_LOG(@"%@", error);
//    } hud:NO];
}

// 5 STEP: 弹出第三方支付选择页面
- (void)signUpPresentThirdPayController {
    UIViewController * currentController = [self getCurrentViewController];
    // 第三方支付
    YMECOtherPayViewController * controller = [[YMECOtherPayViewController alloc] initWithNibName:@"YMECOtherPayViewController" bundle:nil];
    // self.definesPresentationContext = YES;
    controller.view.backgroundColor = YMEC_HEX_COLOR_A(0x000000, 0.5);
    controller.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    
    NSDictionary * params = @{@"accountId": kAccountId,
//                              @"userName": [[YMECUserManager share] username],
                              @"mobileNumber": TEN_USER_PHONE,
                              @"goodsId": self.goodsId,
                              @"goodsType": @"6",
                              @"payVersion": @"1.0.1",
                              @"spbillCreateIp": [NSObject getIPAddress]
                              };
    
    __weak typeof(self) wself = self;
    controller.wechaMethod = ^{
        __strong typeof(wself) sself = wself;
        TEN_LOG(@"微信支付");
        [sself wechatPayWith:params];
    };
    controller.aliMethod = ^{
        __strong typeof(wself) sself = wself;
        TEN_LOG(@"支付宝支付");
        [sself aliPayWith:params];
    };
    controller.cancleMethod = ^{
        // 删除约会
        __strong typeof(wself) sself = wself;
        [sself deleSinUpDate];
    };
    
    [currentController presentViewController:controller animated:YES completion:nil];
}

// 6. STEP 删除报名约会

- (void)deleSinUpDate {
    NSDictionary * params = @{@"id": self.goodsId,
                              @"accountId": kAccountId
                              };
//    [self postWithURL:YMEC_DATE_DELETE_MY_SIGNUP_URL params:params success:^(NSDictionary *dict) {
//        TEN_LOG(@"%@", dict);
//    } failure:^(NSError *error) {
//        TEN_LOG(@"%@", error);
//    } hud:NO];
}

#pragma mark -

- (void)deleteInfo {
    if (self.type.integerValue == 5) {
        [self deleMyDate];
    } else if (self.type.integerValue == 6) {
        [self deleSinUpDate];
    }
}

#pragma mark -

/// 社圈首次分享免费

- (void)circleFirstTimeFree {
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"温馨提示" message:@"你有一次分享APP, 免费加入社群的机会, 确定使用该次机会, 加入该社群吗?" preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction * canler = [UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }];
    __weak typeof(self) weakSelf = self;
    UIAlertAction * sure = [UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        __strong typeof(weakSelf) strongSelf = weakSelf;
        // TODO: 分享APP
    }];
    [alert addAction:canler];
    [alert addAction:sure];
    
    UIWindow * keyWindow = [[[UIApplication sharedApplication] delegate] window];
    UIViewController * controller = keyWindow.rootViewController;
    [controller presentViewController:alert animated:YES completion:nil];
}

@end

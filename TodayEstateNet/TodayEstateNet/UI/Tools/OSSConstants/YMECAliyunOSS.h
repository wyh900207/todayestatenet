//
//  YMECAliyunOSS.h
//  YMEconomicCircle
//
//  Created by 杨攀 on 2018/5/29.
//  Copyright © 2018年 YME. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface YMECAliyunOSS : NSObject

+ (instancetype)sharedInstance;

- (void)setupEnviroment;

- (void)uploadObjectAsyncWithData:(NSData *)uploadData
                    withObjectKey:(NSString *)objectKey
                          success:(void(^)(void))successBlock;

- (void)uploadVideoWithFilePath:(NSString *)filePath
                     objectName:(NSString *)objectName
                       progress:(void(^)(NSString *progress))progressBlock
                       successu:(void(^)(void))successBlock;

@end

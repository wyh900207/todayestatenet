//
//  OSSConstants.h
//  YMEconomicCircle
//
//  Created by 杨攀 on 2018/5/29.
//  Copyright © 2018年 YME. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString *const BUCKET_NAME;
extern NSString *const UPLOAD_OBJECT_KEY;
extern NSString *const SECRET_KEY;
extern NSString *const imageEndPoint;
extern NSString *const endPoint;
extern NSString *const videoEndPoint;
extern NSString *const VIDEO_BUCKET_NAME;
extern NSString *const audioEndPoint;
extern NSString *const AUDIO_BUCKET_NAME;

//
//  YMECAliyunOSS.m
//  YMEconomicCircle
//
//  Created by 杨攀 on 2018/5/29.
//  Copyright © 2018年 YME. All rights reserved.
//

#import "YMECAliyunOSS.h"
#import "OSSConstants.h"
#import <VODUpload/VODUploadClient.h>

@interface YMECAliyunOSS() {
    OSSClient *client;
    NSString *_endPoint;
    NSString *_bucket_name;
    NSString *_filePath;
}

@end

@implementation YMECAliyunOSS

+ (instancetype)sharedInstance {
    static YMECAliyunOSS *instance;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [YMECAliyunOSS new];
    });
    return instance;
}
- (void)setupEnviroment{
    [OSSLog enableLog];
    _endPoint = imageEndPoint;
    _bucket_name = BUCKET_NAME;
    [self initOSSClient];
}

- (void)initOSSClient {
    id<OSSCredentialProvider> credential = [[OSSPlainTextAKSKPairCredentialProvider alloc] initWithPlainTextAccessKey:UPLOAD_OBJECT_KEY secretKey:SECRET_KEY];
    client = [[OSSClient alloc] initWithEndpoint:_endPoint credentialProvider:credential];
}
- (void)uploadObjectAsyncWithData:(NSData *)uploadData withObjectKey:(NSString *)objectKey success:(void(^)(void))successBlock {
    OSSPutObjectRequest *request = [OSSPutObjectRequest new];
    request.bucketName = _bucket_name;
    request.objectKey = objectKey;
    request.uploadingData = uploadData;
    
    OSSTask *putTask = [client putObject:request];
    
    [putTask continueWithBlock:^id _Nullable(OSSTask * _Nonnull task) {
        OSSPutObjectResult *result = task.result;
        if (!task.error) {
            successBlock();
            NSLog(@"上传成功 %@", result.serverReturnJsonString);
        } else {
            NSLog(@"upload object failed, error: %@", task.error);
        }
        return nil;
    }];
    
    request.uploadProgress =^(int64_t bytesSent, int64_t totalBytesSent, int64_t totalBytesExpectedToSend) {
        NSLog(@"%lld, %lld, %lld", bytesSent, totalBytesSent, totalBytesExpectedToSend);
    };
}
- (void)uploadVideoWithFilePath:(NSString *)filePath objectName:(NSString *)objectName progress:(void (^)(NSString *))progressBlock successu:(void (^)(void))successBlock  {
    
    OnUploadStartedListener testUploadStartedCallbackFunc = ^(UploadFileInfo* fileInfo) {
        NSLog(@"upload started .");
    };
    OnUploadTokenExpiredListener testTokenExpiredCallbackFunc = ^{
        NSLog(@"*token expired.");
        // get token and call resmeUploadWithAuth.
    };
    OnUploadRertyListener testUploadRertyListener = ^{
        NSLog(@"retry begin.");
    };
    OnUploadRertyResumeListener testUploadRertyResumeListener = ^{
        NSLog(@"retry resume.");
    };
    VODUploadListener *listener;
    listener = [[VODUploadListener alloc] init];
    listener.started = testUploadStartedCallbackFunc;
    [listener setSuccess:^(UploadFileInfo *fileInfo) {
        NSLog(@"上传成功");
        successBlock();
    }];
    [listener setFailure:^(UploadFileInfo *fileInfo, NSString *code, NSString *message) {
        
    }];
    [listener setProgress:^(UploadFileInfo *fileInfo, long uploadedSize, long totalSize) {
        progressBlock([NSString stringWithFormat:@"%ld", uploadedSize*100/totalSize]);
    }];
    listener.expire = testTokenExpiredCallbackFunc;
    listener.retry = testUploadRertyListener;
    listener.retryResume = testUploadRertyResumeListener;
    
    VODUploadClient *uploader = [[VODUploadClient alloc] init];
    [uploader init:UPLOAD_OBJECT_KEY accessKeySecret:SECRET_KEY listener:listener];
    [uploader addFile:filePath endpoint:endPoint bucket:VIDEO_BUCKET_NAME object:objectName];
    [uploader start];
}
@end

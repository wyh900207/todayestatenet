//
//  TENUrlManager.h
//  TodayEstateNet
//
//  Created by  HomerLynn on 2019/1/3.
//  Copyright © 2019 TEN. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TENUrlManager : NSObject

+ (instancetype)share;

#pragma mark - public func

// JSON 文件中的 所有URL
- (nullable NSDictionary *)all;
// 根据模块 和 明细, 获取`URL`
- (nullable NSString *)url:(NSString *)module detail:(NSString *)detail;

@end

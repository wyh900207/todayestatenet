//
//  TENUrlManager.m
//  TodayEstateNet
//
//  Created by  HomerLynn on 2019/1/3.
//  Copyright © 2019 TEN. All rights reserved.
//

#import "TENUrlManager.h"

@implementation TENUrlManager

+ (instancetype)share {
    static TENUrlManager *instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [TENUrlManager new];
    });
    
    return instance;
}

#pragma mark - public func

// JSON 文件中的 所有URL
- (nullable NSDictionary *)all {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"TENWebUrl" ofType:@"json"];
    NSData *JSON = [[NSData alloc] initWithContentsOfFile:path];
    
    NSError *error;
    
    NSDictionary *uris = [NSJSONSerialization JSONObjectWithData:JSON options:NSJSONReadingMutableContainers error:&error];
    
    if (error) {
        TEN_LOG(@"本地URL解析失败");
        return nil;
    }
    
    return uris;
}

- (nullable NSString *)url:(NSString *)module detail:(NSString *)detail {
    NSDictionary *all = [self all];
    NSDictionary *mod = all[module];
    if (!mod) {
        return nil;
    }
    NSString *uri = mod[detail];
    if (!uri) {
        return nil;
    }
    else {
        return uri;
    }
}

@end

//
//  DJCountdownButton.m
//  OwnerOfCargo
//
//  Created by DJAPPLE3-ysy on 2018/4/8.
//  Copyright © 2018年 DJAPpple_4. All rights reserved.
//

#import "DJCountdownButton.h"

@implementation DJCountdownButton

- (void)onclickCountDown {
    self.enabled=NO;
    self.secondsCountDown=60;
    [self.timer invalidate];
    [self setTitleColor:TENHexColor(TENTextBlackColor3) forState:UIControlStateNormal];
    [self setTitle:[NSString stringWithFormat:@"%.0f 秒", self.secondsCountDown] forState:UIControlStateNormal];
    [self setBackgroundColor:[UIColor clearColor]];
//    [self addTarget:self action:@selector(clickedButton:) forControlEvents:UIControlEventTouchUpInside];
    self.timer=[NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(countDown:) userInfo:nil repeats:YES];
    
    [self.timer fire];
}

#pragma mark - Private methods

//- (void)clickedButton:(UIButton *)button {
//    if (self.clickedHandler) self.clickedHandler();
//}

- (void)stopTimer {
    self.secondsCountDown=1;
//    [self.timer invalidate];
//    self.timer = nil;
}

- (void)countDown:(NSTimer *)timer {
    _secondsCountDown--;
    if (_secondsCountDown > 0) {
        self.titleLabel.text = [NSString stringWithFormat:@"%.0f 秒后可重新获取验证码", _secondsCountDown];//防止 button_title 闪烁
        [self setTitle:[NSString stringWithFormat:@"%.0f 秒后可重新获取验证码",_secondsCountDown] forState:UIControlStateNormal];
        [self setTitleColor:TENHexColor(TENTextGrayColor) forState:UIControlStateNormal];
        
    } else {
        self.enabled=YES;
        [self.timer invalidate];
        self.timer=nil;
        self.titleLabel.text = [NSString stringWithFormat:@"获取验证码"];//防止 button_title 闪烁
        [self setTitle:@"获取验证码" forState:UIControlStateNormal];
        [self setBackgroundColor:[UIColor clearColor]];
        [self setTitleColor:TENHexColor(TENTextBlackColor3) forState:UIControlStateNormal];
        [self.timer invalidate];
        self.timer = nil;
    }
    
//    if (_secondsCountDown<=0)
//    {
//        self.enabled=YES;
//        [self.timer invalidate];
//        self.timer=nil;
//        [self setTitle:@"获取验证码" forState:UIControlStateNormal];
//        [self setBackgroundColor:[UIColor whiteColor]];
//        [self setTitleColor:RGB16(0x0098FF) forState:UIControlStateNormal];
//
//    }
}

@end

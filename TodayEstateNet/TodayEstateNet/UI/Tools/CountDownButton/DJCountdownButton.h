//
//  DJCountdownButton.h
//  OwnerOfCargo
//
//  Created by DJAPPLE3-ysy on 2018/4/8.
//  Copyright © 2018年 DJAPpple_4. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DJCountdownButton : UIButton

//存放60秒倒计时时间
@property (nonatomic, assign) BOOL isCountDown;
@property (nonatomic, assign) NSTimeInterval secondsCountDown;
@property (nonatomic, copy  ) void (^clickedHandler)(void);

@property(nonatomic,strong)NSTimer *timer;

-(void)stopTimer;
-(void)onclickCountDown;

@end

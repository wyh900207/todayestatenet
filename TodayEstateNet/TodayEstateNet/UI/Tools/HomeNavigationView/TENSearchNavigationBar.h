//
//  TENSearchNavigationBar.h
//  TodayEstateNet
//
//  Created by HomerLynn on 2019/2/25.
//  Copyright © 2019 TEN. All rights reserved.
//

#import "BaseView.h"

@interface TENSearchNavigationBar : BaseView

@property (nonatomic, strong) UIButton *backButton; //返回按钮
@property (nonatomic, strong) UIButton *searchButton;  //搜索按钮

@property (nonatomic, copy) NSString *placeHolder;
// 搜索文本
@property (nonatomic, copy) NSString *searchText;

@property (nonatomic, copy) void(^backButtonHandler)(void);
@property (nonatomic, copy) void(^searchButtonHandler)(void);

@end

//
//  CustomSearchView.h
//  Refining
//
//  Created by paat on 2018/7/10.
//  Copyright © 2018年 Paat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseView.h"
@interface CustomSearchView : BaseView
@property (nonatomic, copy) void(^clickedSearchView)(CustomSearchView *view);

@end

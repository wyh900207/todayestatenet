//
//  CustomSearchView.m
//  Refining
//
//  Created by paat on 2018/7/10.
//  Copyright © 2018年 Paat. All rights reserved.
//

#import "CustomSearchView.h"

@interface CustomSearchView()

@property (nonatomic,strong)UIButton *searchBackgroundBtn;
@property (nonatomic,strong)UIImageView *searchIcon;
@property (nonatomic,strong)UILabel *tipLabel;

@end
@implementation CustomSearchView

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
         if ( !self ) return nil;
        [self customSearchSetupView];
    }
    return self;
}

-(void)customSearchSetupView
{
    [self.containerView addSubview:self.searchBackgroundBtn];
    [_searchBackgroundBtn addSubview:self.searchIcon];
    [_searchBackgroundBtn addSubview:self.tipLabel];

    [_searchBackgroundBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.right.left.equalTo(self). offset(0.0f);
    }];
    [_searchIcon mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self->_searchBackgroundBtn).offset(8);
        make.centerY.offset(0);
    }];

    [_tipLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.equalTo(self->_searchIcon.mas_trailing).offset(12.0f);
        make.centerY.equalTo(self->_searchIcon);
    }];
    
}
-(void)clickBtn:(UIButton *)sender
{
    if (_clickedSearchView) _clickedSearchView(self);
}

-(UIButton *)searchBackgroundBtn
{
    if (_searchBackgroundBtn)return _searchBackgroundBtn;
    _searchBackgroundBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    [_searchBackgroundBtn setBackgroundColor:TENHexColor(TENWhiteColor)];
    [_searchBackgroundBtn addTarget:self action:@selector(clickBtn:) forControlEvents:UIControlEventTouchUpInside];
    return _searchBackgroundBtn;
}

-(UIImageView *)searchIcon
{
    if (_searchIcon) return _searchIcon;
    _searchIcon = [UIImageView new];
    _searchIcon.image = [UIImage imageNamed:@"tabbar_search_hl"];
    return _searchIcon;
}

-(UILabel *)tipLabel{
    
    if (_tipLabel)return _tipLabel;
    _tipLabel = [UILabel new];
    _tipLabel.textAlignment= NSTextAlignmentLeft;
    _tipLabel.font = TENFont14;
    _tipLabel.textColor = TENHexColor(TENTextGrayColor);

     _tipLabel.text = @"搜索";
    return _tipLabel;
}

@end

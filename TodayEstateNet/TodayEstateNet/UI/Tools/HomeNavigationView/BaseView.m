//
//  BaseView.m
//  Refining
//
//  Created by paat on 2018/7/10.
//  Copyright © 2018年 Paat. All rights reserved.
//

#import "BaseView.h"

@implementation BaseView


- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        
    }
    return self;
}

- (UIView *)containerView {
    if (!_containerView) {
        _containerView = [UIView new];
        _containerView.backgroundColor = [UIColor clearColor];
        [self addSubview:_containerView];
        [_containerView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.edges.offset(0);
        }];
    }
    return _containerView;
}

@end

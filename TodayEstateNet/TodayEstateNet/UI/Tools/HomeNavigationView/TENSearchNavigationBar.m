//
//  TENSearchNavigationBar.m
//  TodayEstateNet
//
//  Created by HomerLynn on 2019/2/25.
//  Copyright © 2019 TEN. All rights reserved.
//

#import "TENSearchNavigationBar.h"
#import "TENTextField.h"

@interface TENSearchNavigationBar ()

@property (nonatomic, strong) UIView *topContainerView;
@property (nonatomic, strong) UIView *bottomContainerView;
@property (nonatomic, strong) UIView *searchContainerView;
@property (nonatomic, strong) UIImageView *searchIconImageView;
@property (nonatomic, strong) TENTextField *searchTextField;

@end

@implementation TENSearchNavigationBar

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        if ( !self ) return nil;
        [self setupSubviews];
    }
    return self;
}

- (void)setupSubviews {
    self.backgroundColor = TENHexColor(TENThemeBlueColor);
    
    [self addSubview:self.bottomContainerView];
    [self addSubview:self.topContainerView];
    [self.bottomContainerView addSubview:self.backButton];
    [self.bottomContainerView addSubview:self.searchButton];
    [self.bottomContainerView addSubview:self.searchContainerView];
    [self.searchContainerView addSubview:self.searchIconImageView];
    [self.searchContainerView addSubview:self.searchTextField];
    
    [self.bottomContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self);
        make.height.equalTo(@44);
    }];
    [self.topContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self);
        make.bottom.equalTo(self.bottomContainerView.mas_top);
    }];
    [self.backButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(10);
        make.centerY.offset(0);
        make.width.height.equalTo(@25);
    }];
    [self.searchButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.offset(-10);
        make.top.bottom.offset(0);
        make.width.equalTo(@50);
    }];
    [self.searchContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerY.offset(0);
        make.left.equalTo(self.backButton.mas_right).offset(10);
        make.right.equalTo(self.searchButton.mas_left).offset(-10);
        make.height.equalTo(@30);
    }];
    [self.searchIconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(8);
        make.centerY.offset(0);
        make.width.height.equalTo(@20);
    }];
    [self.searchTextField mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.searchIconImageView.mas_right).offset(8);
        make.top.bottom.offset(0);
        make.right.offset(-8);
    }];
    TENCornerRadius(self.searchContainerView, 15);
}

#pragma mark - Private

- (void)citylick:(UIButton *)sender {
    if (_backButtonHandler) _backButtonHandler();
}

- (void)clickMap:(UIButton *)button {
    if (_searchButtonHandler) _searchButtonHandler();
}

#pragma mark - Getter

- (UIView *)topContainerView {
    if (!_topContainerView) {
        _topContainerView = [UIView new];
        _topContainerView.backgroundColor = [UIColor clearColor];
    }
    return _topContainerView;
}

- (UIView *)bottomContainerView {
    if (!_bottomContainerView) {
        _bottomContainerView = [UIView new];
        _bottomContainerView.backgroundColor = [UIColor clearColor];
    }
    return _bottomContainerView;
}

- (UIView *)searchContainerView {
    if (!_searchContainerView) {
        _searchContainerView = [UIView new];
        _searchContainerView.backgroundColor = TENHexColor(TENWhiteColor);
    }
    return _searchContainerView;
}

- (UIImageView *)searchIconImageView {
    if (!_searchIconImageView) {
        _searchIconImageView = [UIImageView new];
        _searchIconImageView.image = TENImage(@"tabbar_search_hl");
    }
    return _searchIconImageView;
}

- (TENTextField *)searchTextField {
    if (!_searchTextField) {
        _searchTextField = [TENTextField new];
        @weakify(self);
        _searchTextField.textChangedHandler = ^(UITextField *textField, NSString *text) {
            @strongtify(self);
            self.searchText = text;
        };
    }
    return _searchTextField;
}

- (UIButton *)backButton {
    if (!_backButton) {
        _backButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_backButton setTitleColor:TENHexColor(TENWhiteColor) forState:0];
        _backButton.titleLabel.font = TENFont14;
        [_backButton setImage:TENImage(@"ten-nav-back") forState:UIControlStateNormal];
        [_backButton addTarget:self action:@selector(citylick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _backButton;
}

- (UIButton *)searchButton {
    if (!_searchButton) {
        _searchButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_searchButton setTitleColor:TENHexColor(TENWhiteColor) forState:UIControlStateNormal];
        _searchButton.titleLabel.font = TENFont14;
        [_searchButton setTitle:@"搜索" forState:UIControlStateNormal];
        [_searchButton addTarget:self action:@selector(clickMap:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _searchButton;
}

#pragma mark - Setter

- (void)setPlaceHolder:(NSString *)placeHolder {
    _placeHolder = placeHolder;
    self.searchTextField.placeholder = placeHolder;
}

@end

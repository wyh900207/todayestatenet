//
//  SearchNavigationView.h
//  Refining
//
//  Created by paat on 2018/7/10.
//  Copyright © 2018年 Paat. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseView.h"
@class CustomSearchView;

@interface SearchNavigationView : BaseView

@property (nonatomic, strong) UIButton *cityBtn; //城市按钮
@property (nonatomic, strong) UIButton *mapBtn;  //题图按钮

@property (nonatomic, strong) CustomSearchView *customSearchView;

@property (nonatomic, copy) void(^clickedSearch)(SearchNavigationView *view);
@property (nonatomic, copy) void(^cityButtonHandler)(SearchNavigationView *view);
@property (nonatomic, copy) void(^mapButtonHandler)(SearchNavigationView *view);

@end

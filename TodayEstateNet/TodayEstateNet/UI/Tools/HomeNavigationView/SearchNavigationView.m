//
//  SearchNavigationView.m
//  Refining
//
//  Created by paat on 2018/7/10.
//  Copyright © 2018年 Paat. All rights reserved.
//

#import "SearchNavigationView.h"
#import "CustomSearchView.h"
#import "UIButton+JKImagePosition.h"

@interface SearchNavigationView ()

@property (nonatomic, strong) UIView *topContainerView;
@property (nonatomic, strong) UIView *bottomContainerView;

@end

@implementation SearchNavigationView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        if ( !self ) return nil;
        [self searchNavView];
    }
    return self;
}

- (void)searchNavView {
//    UIColor *bgColor = TENHexColor(TENThemeBlueColor);
//    CAGradientLayer *gradientLayer = [CAGradientLayer layer];
//    gradientLayer.colors = [NSArray arrayWithObjects:(id)bgColor.CGColor,(id)[UIColor colorWithHexString:@"4C5DDC"].CGColor, (id)[UIColor colorWithHexString:@"5E6DE4"].CGColor, nil];
//    gradientLayer.locations = @[@0.3, @0.5, @1.0];
//    gradientLayer.startPoint = CGPointMake(0, 0);
//    gradientLayer.endPoint = CGPointMake(1, 0);
//    gradientLayer.frame = self.frame;
//    [self.layer addSublayer:gradientLayer];
    
    self.backgroundColor = TENHexColor(TENThemeBlueColor);
    
    [self addSubview:self.bottomContainerView];
    [self addSubview:self.topContainerView];
    [self.bottomContainerView addSubview:self.cityBtn];
    [self.bottomContainerView addSubview:self.mapBtn];
    [self.bottomContainerView addSubview:self.customSearchView];
    
    [self.bottomContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self);
        make.height.equalTo(@44);
    }];
    [self.topContainerView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self);
        make.bottom.equalTo(self.bottomContainerView.mas_top);
    }];
    [self.cityBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(12);
        make.top.bottom.offset(0);
        make.width.equalTo(@65);
    }];
    [self.mapBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.offset(-12);
        make.top.bottom.offset(0);
        make.width.equalTo(@65);
    }];
    [self.customSearchView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.cityBtn.mas_right).offset(12.0f);
        make.right.equalTo(self.mapBtn.mas_left).offset(-12);
        make.centerY.offset(0);
        make.height.offset(30);
    }];
    TENCornerRadius(self.customSearchView, 15.0f);
}

#pragma mark - Private

- (void)citylick:(UIButton *)sender {
    if (_cityButtonHandler) _cityButtonHandler(self);
}

- (void)clickMap:(UIButton *)button {
    if (_mapButtonHandler) _mapButtonHandler(self);
}

#pragma mark - Getter

- (CustomSearchView *)customSearchView {
    if (!_customSearchView) {
        _customSearchView = [[CustomSearchView alloc] init];
        @weakify(self);
        _customSearchView.clickedSearchView = ^(CustomSearchView *view) {
            @strongtify(self);
            if (self.clickedSearch) self.clickedSearch(self);
        };
    }
    return _customSearchView;
}

- (UIView *)topContainerView {
    if (!_topContainerView) {
        _topContainerView = [UIView new];
        _topContainerView.backgroundColor = [UIColor clearColor];
    }
    return _topContainerView;
}

- (UIView *)bottomContainerView {
    if (!_bottomContainerView) {
        _bottomContainerView = [UIView new];
        _bottomContainerView.backgroundColor = [UIColor clearColor];
    }
    return _bottomContainerView;
}

- (UIButton *)cityBtn {
    if (!_cityBtn) {
        _cityBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_cityBtn setTitleColor:TENHexColor(TENWhiteColor) forState:UIControlStateNormal];
        _cityBtn.titleLabel.font = TENFont14;
        [_cityBtn setTitle:@"上海市" forState:UIControlStateNormal];
        [_cityBtn setImage:TENImage(@"arrow_down") forState:UIControlStateNormal];
        [_cityBtn jk_setImagePosition:LXMImagePositionRight spacing:10.0f];
        [_cityBtn addTarget:self action:@selector(citylick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _cityBtn;
}

- (UIButton *)mapBtn {
    if (!_mapBtn) {
        _mapBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_mapBtn setTitleColor:TENHexColor(TENWhiteColor) forState:UIControlStateNormal];
        _mapBtn.titleLabel.font = TENFont14;
        [_mapBtn setTitle:@"地图" forState:UIControlStateNormal];
        [_mapBtn setImage:TENImage(@"home_map") forState:UIControlStateNormal];
        [_mapBtn jk_setImagePosition:LXMImagePositionLeft spacing:10.0f];
        [_mapBtn addTarget:self action:@selector(clickMap:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _mapBtn;
}

@end

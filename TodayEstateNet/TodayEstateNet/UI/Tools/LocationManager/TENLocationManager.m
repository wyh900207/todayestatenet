//
//  TENLocationManager.m
//  TodayEstateNet
//
//  Created by Admin on 2018/9/22.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENLocationManager.h"

@interface TENLocationManager ()

@property (strong, nonatomic) BMKLocationManager * locationManager;

@end

@implementation TENLocationManager

+ (instancetype)share {
    static id instance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [self new];
    });
    return instance;
}

- (void)locationJustOneceTimeWith:(LoactionSuccessHandle)success faild:(LoactionFaildHandle)faild {
    [self.locationManager requestLocationWithReGeocode:YES withNetworkState:YES completionBlock:^(BMKLocation * _Nullable location, BMKLocationNetworkState state, NSError * _Nullable error) {
        if (error) {
            if (faild) faild(error);
        }
        if (location) {
            if (success) success(location.location, location.rgcData);
        }
        // NSLog(@"netstate = %d", state);
    }];
}

#pragma mark - Location

- (BMKLocationManager *)locationManager {
    if (!_locationManager) {
        _locationManager = [[BMKLocationManager alloc] init];
        _locationManager.coordinateType = BMKLocationCoordinateTypeBMK09LL;
        _locationManager.distanceFilter = kCLDistanceFilterNone;
        _locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        _locationManager.activityType = CLActivityTypeAutomotiveNavigation;
        _locationManager.pausesLocationUpdatesAutomatically = NO;
        _locationManager.allowsBackgroundLocationUpdates = YES;
        _locationManager.locationTimeout = 10;
        _locationManager.reGeocodeTimeout = 10;
    }
    return _locationManager;
}

- (void)locationWith:(void (^)(void))successHandler faild:(void (^)(void))faildHandler {
    if ([CLLocationManager locationServicesEnabled] && ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined || [CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorized)) {
        //定位功能可用
        if (successHandler) {
            successHandler();
        }
    } else if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusDenied) {
        //定位不能用
        if (faildHandler) {
            faildHandler();
        }
    }
}

#pragma mark - Getter (默认值)

- (NSString *)displayCityName {
    NSString *name = TEN_UD_VALUE(kDisplayCityName);
    if (!name || [name isEqualToString:@""]) {
        name = @"上海市";
    }
    return name;
}

- (NSString *)displaySectionName {
    NSString *section = TEN_UD_VALUE(kSectionCode);
    if (!section || [section isEqualToString:@""]) {
        section = @"定位失败";
    }
    return section;
}

- (NSString *)cityName {
    NSString *name = TEN_UD_VALUE(kCityName);
    NSString *display = TEN_UD_VALUE(kDisplayCityName);
    if (!name || [name isEqualToString:@""]) {
        return display;
    }
    return name;
}

- (NSString *)cityCode {
    NSString *code = [NSString stringWithFormat:@"%@", TEN_UD_VALUE(kCityCode)];
    if (!code || [code isEqualToString:@""]) {
        code = @"3010100";
    }
    return code;
}

- (NSNumber *)longitude {
    NSNumber *lon = TEN_UD_VALUE(kCityLongitude);
    if (!lon || lon.longValue == 0.0) {
        lon = @(121.473);
    }
    return lon;
}

- (NSNumber *)latitude {
    NSNumber *lat = TEN_UD_VALUE(kCityLatitude);
    if (!lat || lat.longValue == 0.0) {
        lat = @(31.2317);
    }
    return lat;
}

@end

//
//  TENLocationManager.h
//  TodayEstateNet
//
//  Created by Admin on 2018/9/22.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <BMKLocationKit/BMKLocationManager.h>

typedef void(^LoactionSuccessHandle)(CLLocation * location, BMKLocationReGeocode * rgcData);
typedef void(^LoactionFaildHandle)(NSError *error);

@interface TENLocationManager : NSObject

// 默认城市
@property (nonatomic, copy) NSString *displayCityName;
// 默认街道
@property (nonatomic, copy) NSString *displaySectionName;
// 定位城市
@property (nonatomic, copy) NSString *cityName;
// 城市代码
@property (nonatomic, copy) NSString *cityCode;
// Longitude (默认上海)
@property (nonatomic, strong) NSNumber *longitude;
// latitude (默认上海)
@property (nonatomic, strong) NSNumber *latitude;

+ (instancetype)share;
// 是否开启定位权限
- (void)locationWith:(void (^)(void))successHandler faild:(void (^)(void))faildHandler;
// 单次定位
- (void)locationJustOneceTimeWith:(LoactionSuccessHandle)success faild:(LoactionFaildHandle)faild;

@end

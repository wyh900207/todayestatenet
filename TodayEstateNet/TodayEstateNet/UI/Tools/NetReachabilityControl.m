//
//  NetReachabilityControl.m
//  CPS
//
//  Created by dj-xxzx-10065 on 2017/4/19.
//  Copyright © 2017年 dj-xxzx-10065. All rights reserved.
//

#import "NetReachabilityControl.h"
#import "AFNetworkReachabilityManager.h"
@implementation NetReachabilityControl

+ (id)reachabilityInstance {
    static dispatch_once_t once;
    static id sharedInstance = nil;
    dispatch_once(&once, ^{
        sharedInstance = [[self alloc] init];
    });
    return sharedInstance;
}

-(void)netWorkState:(netStateBlock)block {
    AFNetworkReachabilityManager *manager = [AFNetworkReachabilityManager sharedManager];
    // 提示：要监控网络连接状态，必须要先调用单例的startMonitoring方法
    [manager startMonitoring];
    //检测的结果
    [manager setReachabilityStatusChangeBlock:^(AFNetworkReachabilityStatus status) {
            //将netState值传入block中
            block(status);
    }];
}
@end

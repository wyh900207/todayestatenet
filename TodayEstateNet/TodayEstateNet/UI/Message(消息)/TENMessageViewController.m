//
//  TENMessageViewController.m
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/9/11.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENMessageViewController.h"
#import "TENUtilsHeader.h"
#import "TENChatInputView.h"
#import "TENTableView.h"
#import "TENProfileCell.h"
#import "TENRssViewController.h"
#import "TENMessageListRequest.h"
#import "TENUnreadMessageModel.h"
#import "TENChatListViewController.h"

@interface TENMessageViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) TENTableView *tableView;
@property (nonatomic, strong) NSArray *cellTitlesArray;
@property (nonatomic, strong) NSArray *cellIconsArray;
@property (nonatomic, strong) NSArray *cellDescriptionArray;
@property (nonatomic, strong) TENUnreadMessageModel *unreadCountModel;

@end

@implementation TENMessageViewController

#pragma mark - Life cycle

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self getUnreadMessageCount];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupSubviews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - UI

- (void)setupSubviews {
    self.navigationItem.title = @"消息";
    [self.contentView addSubview:self.tableView];
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    }];
}

#pragma mark - Network

- (void)getUnreadMessageCount {
    [TENMessageListRequest fetchUnreadCountWithSuccess:^(NSDictionary *responseObject) {
        TEN_LOG(@"%@", responseObject);
        [TENUnreadMessageModel mj_objectWithKeyValues:responseObject[@"content"]];
        [self.tableView reloadData];
    } failure:^(NSError *error) {
        TEN_LOG(@"%@", error.description);
    }];
}

#pragma mark - UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.cellTitlesArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TENProfileCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TENProfileCell" forIndexPath:indexPath];
    cell.imageView.image = [UIImage imageNamed:self.cellIconsArray[indexPath.row]];
    cell.titleLabel.text = self.cellTitlesArray[indexPath.row];
    cell.descriptionLabel.text = self.cellDescriptionArray[indexPath.row];
    cell.spViewPaddingLeft = @8;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // 媒体类型 1报备提醒 2 群聊提醒 3 楼盘动态 4 楼盘推荐 5 订阅消息 6 客户动态 7系统通知
    @weakify(self);
    [[DLUserManager shareManager] actionAfterLogin:^{
        @strongtify(self);
        if (indexPath.row == 1) {
            TENChatListViewController *listController = [TENChatListViewController new];
            [self.navigationController pushViewController:listController animated:YES];
        } else {
            TENRssViewController *rssViewController = [TENRssViewController new];
            rssViewController.mediaType = @(indexPath.row + 1);
            rssViewController.navigationItem.title = self.cellTitlesArray[indexPath.row];
            [self.navigationController pushViewController:rssViewController animated:YES];
        }
    } currentController:self];
    

}

#pragma mark - Getter

- (TENTableView *)tableView {
    if (!_tableView) {
        _tableView = [TENTableView new];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.separatorColor = [UIColor clearColor];
        _tableView.rowHeight = 44;
        [_tableView registerNib:[UINib nibWithNibName:@"TENProfileCell" bundle:nil] forCellReuseIdentifier:@"TENProfileCell"];
    }
    return _tableView;
}

- (NSArray *)cellTitlesArray {
    //获取信息列表 媒体类型 1报备提醒 2 群聊提醒 3 楼盘动态 4 楼盘推荐 5 订阅消息 6 客户动态 7系统通知
    return @[@"报备提醒",
             @"群聊消息",
             @"楼盘推荐",
             @"楼盘动态",
             @"订阅消息",
             @"客户动态",
             @"系统消息"];
}

- (NSArray *)cellIconsArray {
    return @[@"ten-message-baobei",
             @"ten-message-group",
             @"ten-message-notebook",
             @"ten-message-building",
             @"ten-message-rss",
             @"ten-message-customer",
             @"ten-message-system"];
}

- (NSArray *)cellDescriptionArray {
    return @[@"XXX报备了XXX楼盘,请确认是否带看",
             @"XXX群聊有10条新消息",
             @"您有5条新的消息",
             @"系统向您推荐了一个楼盘",
             @"XXX楼盘发布了新动态",
             @"XXX客户更新了跟进记录",
             @"您有5条新的系统消息"];
}

@end

//
//  TENRssViewController.m
//  TodayEstateNet
//
//  Created by  HomerLynn on 2018/11/16.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENRssViewController.h"
#import "TENTableView.h"
#import "TENUtilsHeader.h"
#import "TENRssCell.h"
#import "TENMessageListRequest.h"
#import "TENMessageListModel.h"

@interface TENRssViewController () <UITableViewDelegate, UITableViewDataSource, YMECTableViewRefreshProtocol>

@property (nonatomic, strong) TENTableView *tableView;

@end

@implementation TENRssViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupSubviews];
}

- (void)setupSubviews {
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.view);
    }];
    
    // 请求网络
    [self fetchMessageListWith:YES];
}

- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    self.tabBarController.tabBar.hidden = NO;
}

#pragma mark - Network

- (void)fetchMessageListWith:(BOOL)isHeaderRefresh {
    // 媒体类型 1报备提醒 2 群聊提醒 3 楼盘动态 4 楼盘推荐 5 订阅消息 6 客户动态 7系统通知
    NSMutableDictionary *params = [@{@"mediaType": self.mediaType.stringValue,
                                     @"pageNo": [NSString stringWithFormat:@"%ld", self.tableView.pageCount]
                                     } mutableCopy];
    [TENMessageListRequest fetchMessageListWithParams:params success:^(NSDictionary *responseObject) {
        TEN_LOG(@"%@", responseObject);
        TENMessageListModel *model = [TENMessageListModel mj_objectWithKeyValues:responseObject];
        if (isHeaderRefresh) {
            [self.tableView endHeaderRefreshWith:model.content];
        } else {
            [self.tableView endFooterRefreshWith:model.content];
        }
        [self.tableView reloadData];
    } failure:^(NSError *error) {
        TEN_LOG(@"%@", error.description);
    }];
}

#pragma mark - UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.tableView.dataSourceArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TENRssCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    TENMessageDetailModel *model = self.tableView.dataSourceArray[indexPath.row];
    NSString *contentText = [NSString stringWithFormat:@"%@\n%@", model.title, model.content];
    cell.contentLabel.text = contentText;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

#pragma mark - YMECTableViewRefreshProtocol

- (void)headerWillRefresh {
    [self fetchMessageListWith:YES];
}

- (void)footerWillRefresh {
    [self fetchMessageListWith:NO];
}

#pragma mark - Getter

- (TENTableView *)tableView {
    if (!_tableView) {
        _tableView = [TENTableView new];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.refreshDelegate = self;
        _tableView.estimatedRowHeight = 40;
        _tableView.separatorColor = TENHexClearColor;
        _tableView.refreshType = TENTableViewRefreshTypeBoth;
        _tableView.rowHeight = UITableViewAutomaticDimension;
        [_tableView registerClass:[TENRssCell class] forCellReuseIdentifier:@"cell"];
    }
    return _tableView;
}

@end

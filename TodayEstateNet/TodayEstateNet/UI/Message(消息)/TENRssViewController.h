//
//  TENRssViewController.h
//  TodayEstateNet
//
//  Created by  HomerLynn on 2018/11/16.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENBaseViewController.h"

@interface TENRssViewController : TENBaseViewController

@property (nonatomic, strong) NSNumber *mediaType;

@end

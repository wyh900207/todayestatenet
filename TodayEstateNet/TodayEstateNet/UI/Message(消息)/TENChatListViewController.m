//
//  TENChatListViewController.m
//  TodayEstateNet
//
//  Created by HomerLynn on 2019/2/28.
//  Copyright © 2019 TEN. All rights reserved.
//

#import "TENChatListViewController.h"
#import "TENChatListCell.h"
#import "TENMessageListRequest.h"

@interface TENChatListViewController () <UITableViewDelegate, UITableViewDataSource, YMECTableViewRefreshProtocol>

@property (nonatomic, strong) TENTableView *tableview;

@end

@implementation TENChatListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupSubviews];
    [self requestListDataWith:YES];
}

#pragma mark - UI

- (void)setupSubviews {
    [self.view addSubview:self.tableview];
    [self.tableview mas_makeConstraints:^(MASConstraintMaker *make) {
        if (TEN_iOS_10_OR_LATER) {
            make.edges.offset(0);
        } else {
            make.top.offset(TENNavigationBarHeight);
            make.left.right.bottom.offset(0);
        }
    }];
}

#pragma mark - Network

- (void)requestListDataWith:(BOOL)isHeaderRefresh {
    NSMutableDictionary *params = [@{@"accountId": TEN_UD_VALUE(kUserAccountId),
                                     @"pageNo": @(self.tableview.pageCount),
                                     @"pageSize": @(self.tableview.pageSize)
                                     } mutableCopy];
    [TENMessageListRequest fetchChatListWithParams:params success:^(NSDictionary *responseObject) {
        TEN_LOG(@"%@", responseObject);
        NSArray *list = responseObject[@"content"];
        if (isHeaderRefresh) {
            [self.tableview endHeaderRefreshWith:list];
        } else {
            [self.tableview endFooterRefreshWith:list];
        }
        [self.tableview reloadData];
    } failure:^(NSError *error) {
        TEN_LOG(@"%@", error.description);
    }];
}

#pragma mark - UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.tableview.dataSourceArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TENChatListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    cell.model = self.tableview.dataSourceArray[indexPath.row];
    
    return cell;
}

#pragma mark - YMECTableViewRefreshProtocol

- (void)headerWillRefresh {
    [self requestListDataWith:YES];
}

- (void)footerWillRefresh {
    [self requestListDataWith:NO];
}

#pragma mark - Getter

- (TENTableView *)tableview {
    if (!_tableview) {
        _tableview = [TENTableView new];
        _tableview.delegate = self;
        _tableview.dataSource = self;
        _tableview.refreshDelegate = self;
        _tableview.refreshType = TENTableViewRefreshTypeBoth;
        _tableview.rowHeight = UITableViewAutomaticDimension;
        _tableview.separatorColor = [UIColor clearColor];
        _tableview.estimatedRowHeight = 56;
        [_tableview registerClass:[TENChatListCell class] forCellReuseIdentifier:@"cell"];
    }
    return _tableview;
}

@end

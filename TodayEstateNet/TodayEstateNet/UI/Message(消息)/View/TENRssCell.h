//
//  TENRssCell.h
//  TodayEstateNet
//
//  Created by  HomerLynn on 2018/11/16.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENTableViewCell.h"

@interface TENRssCell : TENTableViewCell

@property (strong, nonatomic) UILabel *contentLabel;

@end

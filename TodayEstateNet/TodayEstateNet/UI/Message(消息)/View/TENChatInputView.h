//
//  TENChatInputView.h
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/11/8.
//  Copyright © 2018 TEN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TENChatInputView : UIView

@property (weak, nonatomic) IBOutlet UIButton *voiceButton;
@property (weak, nonatomic) IBOutlet UITextView *textInputView;
@property (weak, nonatomic) IBOutlet UIButton *emojiButton;
@property (weak, nonatomic) IBOutlet UIButton *moreButton;

@end

//
//  TENChatListCell.m
//  TodayEstateNet
//
//  Created by HomerLynn on 2019/2/28.
//  Copyright © 2019 TEN. All rights reserved.
//

#import "TENChatListCell.h"

@interface TENChatListCell ()

@property (nonatomic, strong) UIButton *iconButton;
@property (nonatomic, strong) UILabel *usernameLabel;
@property (nonatomic, strong) UILabel *messageLabel;
@property (nonatomic, strong) UILabel *dateLabel;
@property (nonatomic, strong) UIView  *spaceView;

@end

@implementation TENChatListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setupSubviews];
    }
    return self;
}

#pragma mark - UI

- (void)setupSubviews {
    [self.contentView addSubview:self.iconButton];
    [self.contentView addSubview:self.usernameLabel];
    [self.contentView addSubview:self.messageLabel];
    [self.contentView addSubview:self.dateLabel];
    [self.contentView addSubview:self.spaceView];
    
    [self.iconButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.offset(8);
        make.bottom.offset(-8);
        make.width.height.equalTo(@50);
    }];
    [self.usernameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.iconButton);
        make.left.equalTo(self.iconButton.mas_right).offset(10);
        make.right.offset(-10);
    }];
    [self.messageLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.height.equalTo(self.usernameLabel);
        make.top.equalTo(self.usernameLabel.mas_bottom);
    }];
    [self.dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.height.equalTo(self.messageLabel);
        make.top.equalTo(self.messageLabel.mas_bottom);
        make.bottom.equalTo(self.iconButton);
    }];
    [self.spaceView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.offset(0);
        make.height.equalTo(@1);
    }];
    
    TENCornerRadius(self.iconButton, 3);
}

#pragma mark - Getter

- (UIButton *)iconButton {
    if (!_iconButton) {
        _iconButton = [UIButton new];
    }
    return _iconButton;
}

- (UILabel *)usernameLabel {
    if (!_usernameLabel) {
        _usernameLabel = [UILabel new];
        _usernameLabel.font = TENFont14;
        _usernameLabel.textColor = TENHexColor(TENTextGrayColor);
    }
    return _usernameLabel;
}

- (UILabel *)messageLabel {
    if (!_messageLabel) {
        _messageLabel = [UILabel new];
        _messageLabel.font = TENFont14;
        _messageLabel.textColor = TENHexColor(TENTextGrayColor);
    }
    return _messageLabel;
}

- (UILabel *)dateLabel {
    if (!_dateLabel) {
        _dateLabel = [UILabel new];
        _dateLabel.font = TENFont14;
        _dateLabel.textColor = TENHexColor(TENTextGrayColor);
        _dateLabel.textAlignment = NSTextAlignmentRight;
    }
    return _dateLabel;
}

- (UIView *)spaceView {
    if (!_spaceView) {
        _spaceView = [UIView new];
        _spaceView.backgroundColor = TENHexColor(TENSpaceViewColor);
    }
    return _spaceView;
}

#pragma mark - Setter

- (void)setModel:(NSDictionary *)model {
    _model = model;
    [self.iconButton sd_setImageWithURL:[NSURL URLWithString:model[@"picPath"]] forState:0];
    self.usernameLabel.text = model[@"groupName"];
    self.messageLabel.text = model[@"lastMsn"];
    NSNumber *time = model[@"lastMsnDate"];
    self.dateLabel.text = [time.stringValue convertToTime];
}

@end

//
//  TENChatInputView.m
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/11/8.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENChatInputView.h"
#import "TENUtilsHeader.h"

@implementation TENChatInputView

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupSubviews];
}

#pragma mark - UI

- (void)setupSubviews {
    self.textInputView.layer.cornerRadius = 11;
    self.textInputView.layer.masksToBounds = YES;
    self.textInputView.layer.borderWidth = 1;
    self.textInputView.layer.borderColor = TENHexColor(TENTextGrayColor).CGColor;
}

@end

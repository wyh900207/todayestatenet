//
//  TENChatListCell.h
//  TodayEstateNet
//
//  Created by HomerLynn on 2019/2/28.
//  Copyright © 2019 TEN. All rights reserved.
//

#import "TENTableViewCell.h"

@interface TENChatListCell : TENTableViewCell

@property (nonatomic, strong) NSDictionary *model;

@end

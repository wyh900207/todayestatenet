//
//  TENRssCell.m
//  TodayEstateNet
//
//  Created by  HomerLynn on 2018/11/16.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENRssCell.h"
#import "TENUtilsHeader.h"

@interface TENRssCell ()

@property (nonatomic, strong) UIView *seperateView;

@end

@implementation TENRssCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(nullable NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setupSubviews];
    }
    return self;
}

#pragma mark - UI

- (void)setupSubviews {
    [self.contentView addSubview:self.contentLabel];
    [self.contentView addSubview:self.seperateView];

    [self.contentLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.contentView).offset(10);
        make.left.equalTo(self.contentView).offset(8);
        make.bottom.equalTo(self.contentView).offset(-8);
        make.right.equalTo(self.contentView).offset(-10);
    }];
    [self.seperateView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.contentView).offset(8);
        make.bottom.right.equalTo(self.contentView);
        make.height.equalTo(@1);
    }];
}

#pragma mark - Getter

- (UILabel *)contentLabel {
    if (!_contentLabel) {
        _contentLabel = [UILabel new];
        _contentLabel.font = TENFont14;
        _contentLabel.numberOfLines = 0;
        _contentLabel.lineBreakMode = NSLineBreakByCharWrapping;
        _contentLabel.textColor = TENHexColor(TENTextGrayColor);
    }
    return _contentLabel;
}

- (UIView *)seperateView {
    if (!_seperateView) {
        _seperateView = [UIView new];
        _seperateView.backgroundColor = TENHexColor(TENTextGrayColor);
        _seperateView.alpha = 0.2;
    }
    return _seperateView;
}

@end

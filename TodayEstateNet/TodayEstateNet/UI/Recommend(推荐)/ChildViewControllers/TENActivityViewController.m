//
//  TENActivityViewController.m
//  TodayEstateNet
//
//  Created by Admin on 2018/9/26.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENActivityViewController.h"
#import "TENUtilsHeader.h"
#import "TENRecommendCell.h"
#import "TENRecommendActivityCell.h"
#import "TENRecommendRequest.h"

#import "RecommendModel.h"
#import "TENTableView.h"
#import "TENHouseDetailViewController.h"
//#import "TENTa"
//#import "tennnfe"
@interface TENActivityViewController ()<UITableViewDelegate, UITableViewDataSource,YMECTableViewRefreshProtocol>
    
@property (nonatomic, strong) TENTableView *tableview;
@property (nonatomic, strong) NSMutableArray <RecommendActivity *>*acticityList;
    
@end

@implementation TENActivityViewController

#pragma mark - Life cycle
    
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupSubviews];
    [self fetchActivityList];
}
    
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
    
#pragma mark - Request
    
- (void)fetchActivityList {
    TENRecommendRequest *request = [TENRecommendRequest new];
    [request fetchActivityDataWithCityId:@"310100" parameter:nil response:^(NSDictionary *responseObject) {
        TEN_LOG(@"%@", responseObject);
        NSString *code = responseObject[@"code"];
        if ([code integerValue] == 1) {
            self.acticityList = [NSMutableArray array];
            for (NSDictionary *dict in responseObject[@"content"]) {
                RecommendActivity *model = [RecommendActivity mj_objectWithKeyValues:dict];
                [self.acticityList addObject:model];
            }
            [self.tableview endHeaderRefresh];
            [self.tableview reloadData];
        }
    } failure:^(NSError *error) {
        TEN_LOG(@"%@", error.description);
        [self.tableview endHeaderRefreshWith:@[]];
    }];
    
}
    
#pragma mark - UI
    
- (void)setupSubviews {
    [self.contentView addSubview:self.tableview];
    [self.tableview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    }];
}
#pragma mark - 下拉刷新

- (void)headerWillRefresh{
    [self fetchActivityList];
}

#pragma mark - UITableViewDelegate, UITableViewDataSource
    
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.acticityList.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TENRecommendActivityCell *activityCell = [tableView dequeueReusableCellWithIdentifier:@"TENRecommendActivityCell" forIndexPath:indexPath];
    activityCell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    activityCell.data = self.acticityList[indexPath.row];
    
    return activityCell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
//    TENHouseDetailViewController *vc = [TENHouseDetailViewController new];
//    vc.houseId = [NSString stringWithFormat:@"%@",self.acticityList[indexPath.row].activityId];
//    [self.navigationController pushViewController:vc animated:YES];
//    activity-building-detail
    
//    TENBaseWebViewController *webviewController = [TENBaseWebViewController new];
    NSString *contentURL = [[TENUrlManager share] url:@"recommend" detail:@"activity-building-detail"];
//    webviewController.uri = [contentURL stringByAppendingPathComponent:@"3"];
//    [webviewController load];
//    webviewController.hidesBottomBarWhenPushed = YES;
//    [self.navigationController pushViewController:webviewController animated:YES];
    [TENBaseWebViewController pushWith:self.navigationController uri:[contentURL stringByAppendingPathComponent:@"3"] hiddenBottom:YES];
}

#pragma mark - Getter
    
- (UITableView *)tableview {
    if (!_tableview) {
        _tableview = [TENTableView new];
        _tableview.estimatedRowHeight = 232;
        _tableview.rowHeight = UITableViewAutomaticDimension;
        _tableview.backgroundColor = TENHexColor(TENWhiteColor);
        _tableview.separatorColor = TENHexClearColor;
        _tableview.delegate = self;
        _tableview.dataSource = self;
        [_tableview registerNib:[UINib nibWithNibName:@"TENRecommendActivityCell" bundle:nil] forCellReuseIdentifier:@"TENRecommendActivityCell"];
        
        _tableview.refreshDelegate = self;
        _tableview.refreshType = TENTableViewRefreshTypeHeader;
    }
    return _tableview;
}

@end

//
//  TENBuildingViewController.m
//  TodayEstateNet
//
//  Created by Admin on 2018/9/26.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENBuildingViewController.h"
#import "TENUtilsHeader.h"
#import "TENRecommendCell.h"
#import "TENRecommendRequest.h"
#import "RecommendModel.h"
#import "TENHomeNormalSallingCell.h"
#import "TENTableView.h"
#import "TENHouseDetailViewController.h"

@interface TENBuildingViewController ()<UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) TENTableView *tableview;
@property (nonatomic,strong) NSMutableArray <RecommendBuilding *>*dataArr;

@end

@implementation TENBuildingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupSubviews];
    [self requestData];
}

- (void)setupSubviews {
    [self.contentView addSubview:self.tableview];
    [self.tableview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    }];
}

#pragma mark - 网络请求

- (void)requestData {
    [TENRecommendRequest fetchBuildingDataWithCityId:@"310100" parameter:nil response:^(NSDictionary *responseObject) {
        TEN_LOG(@"%@",responseObject);
        NSNumber *code = responseObject[@"code"];
        if ([code integerValue] == 1) {
            self.dataArr = [NSMutableArray array];
            for (NSDictionary *dict in responseObject[@"content"]) {
                RecommendBuilding *model = [RecommendBuilding mj_objectWithKeyValues:dict];
                [self.dataArr addObject:model];
            }
            [self.tableview reloadData];
            [self.tableview endHeaderRefresh];
        }
    } failure:^(NSError *error) {
        TEN_LOG(@"%@",error);
        [self.tableview endHeaderRefreshWith:@[]];
    }];
}

#pragma mark - 下拉刷新

-(void)headerWillRefresh {
    [self requestData];
}

#pragma mark - UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TENHomeNormalSallingCell *recommendCell = [tableView dequeueReusableCellWithIdentifier:@"TENHomeNormalSallingCell" forIndexPath:indexPath];
    recommendCell.selectionStyle = UITableViewCellSelectionStyleNone;
    recommendCell.buildingData = self.dataArr[indexPath.row];
    return recommendCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    TENHouseDetailViewController *vc = [TENHouseDetailViewController new];
    vc.houseId = [NSString stringWithFormat:@"%@",self.dataArr[indexPath.row].buildingId];
    [self.navigationController pushViewController:vc animated:YES];
    
//    RecommendBuilding *building = self.dataArr[indexPath.row];
//
//    TENBaseWebViewController *webviewController = [TENBaseWebViewController new];
//    NSString *contentURL = [[TENUrlManager share] url:@"recommend" detail:@"headlines"];
//    webviewController.uri = [contentURL stringByAppendingPathComponent:building.newsId];
//    [webviewController load];
//    webviewController.hidesBottomBarWhenPushed = YES;
//
//    [self.navigationController pushViewController:webviewController animated:YES];
}

#pragma mark - Setter && Getter

- (UITableView *)tableview {
    if (!_tableview) {
        _tableview = [TENTableView new];
        _tableview.estimatedRowHeight = 80.f;
        _tableview.rowHeight = UITableViewAutomaticDimension;
        _tableview.backgroundColor = TENHexColor(TENWhiteColor);
        _tableview.separatorColor = TENHexClearColor;
        _tableview.delegate = self;
        _tableview.dataSource = self;
        [_tableview registerNib:[UINib nibWithNibName:@"TENHomeNormalSallingCell" bundle:nil] forCellReuseIdentifier:@"TENHomeNormalSallingCell"];
        _tableview.refreshDelegate = self;
        _tableview.refreshType = TENTableViewRefreshTypeHeader;
    }
    return _tableview;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

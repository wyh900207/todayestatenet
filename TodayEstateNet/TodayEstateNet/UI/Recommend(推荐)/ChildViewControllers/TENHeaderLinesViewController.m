//
//  TENHeaderLinesViewController.m
//  TodayEstateNet
//
//  Created by Admin on 2018/9/26.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENHeaderLinesViewController.h"
#import "TENUtilsHeader.h"
#import "TENRecommendCell.h"
#import "SPPageMenu.h"
#import "TENRecommendRequest.h"
#import "RecommendModel.h"
#import <MJExtension/MJExtension.h>
#import "TENTableView.h"
#import "TENHouseDetailViewController.h"

@interface TENHeaderLinesViewController ()<UITableViewDelegate, UITableViewDataSource,SPPageMenuDelegate>

@property (nonatomic, strong) TENTableView *tableview;
@property (nonatomic, weak  ) TENTableView *pageMenu;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) NSArray      *dataArr;
@property (nonatomic, copy  ) NSString     *groupId;
@property (nonatomic, strong) NSMutableArray<RecommendNews *> *newsListArr;

@end

@implementation TENHeaderLinesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupSubviews];
    [self setItem];
    [self requestNewsList:@"0"];
}

- (void)setupSubviews {
    [self.contentView addSubview:self.tableview];
    [self.tableview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(35);
        make.left.offset(0);
        make.right.offset(0);
        make.bottom.offset(0);
    }];
}

- (void)setItem{
    self.dataArr = @[@"全部",@"精选",@"本地",@"百科",@"爆料"];
    // trackerStyle:跟踪器的样式
    SPPageMenu *pageMenu = [SPPageMenu pageMenuWithFrame:CGRectMake(0, 0, TENScreenWidth, 0) trackerStyle:SPPageMenuTrackerStyleNothing];
    pageMenu.permutationWay = SPPageMenuPermutationWayNotScrollEqualWidths;
    pageMenu.backgroundColor = [UIColor whiteColor];
    // 传递数组，默认选中第0个
    [pageMenu setItems:self.dataArr selectedItemIndex:0];
    // 设置代理
    pageMenu.delegate = self;
    pageMenu.tracker.backgroundColor = [UIColor whiteColor];
    // 给pageMenu传递外界的大scrollView，内部监听self.scrollView的滚动，从而实现让跟踪器跟随self.scrollView移动的效果
//    pageMenu.bridgeScrollView = self.scrollView;
    pageMenu.selectedItemTitleColor = [UIColor colorWithHexString:TENThemeBlueColor];
    pageMenu.unSelectedItemTitleColor = [UIColor blackColor];
    [self.contentView addSubview:pageMenu];
    [pageMenu mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.offset(0);
        make.left.offset(0);
        make.right.offset(0);
        make.height.offset(35);
    }];
    self.groupId = @"0";
}

//网络请求
- (void)requestNewsList:(NSString *)groupId{
    self.groupId = groupId;
    
    // 1. 本地新闻传cityId,
    // 2. 精选 百科 爆料 传typeId 分别为109 110 111
    // 3. 不传cityId 和 typeId 为全部
    // 自定义: 本地 typeId = 112, 全部 = 113

    NSNumber *typeId = @113;
    switch (self.groupId.intValue) {
        case 0:
            typeId = @113;
            break;
        case 1:
            typeId = @109;
            break;
        case 2:
            typeId = @112;
            break;
        case 3:
            typeId = @110;
            break;
        case 4:
            typeId = @111;
            break;
            
        default:
            break;
    }
    [TENRecommendRequest fetchNewsWithTypeId:typeId response:^(NSDictionary *respose) {
        TEN_LOG(@"%@",respose);
        NSNumber *code = respose[@"code"];
        if ([code integerValue] == 1) {
            self.newsListArr = [NSMutableArray array];
            for (NSDictionary *dict in respose[@"content"]) {
                RecommendNews *model = [RecommendNews mj_objectWithKeyValues:dict];
                [self.newsListArr addObject:model];
            }
            [self.tableview reloadData];
            [self.tableview endHeaderRefresh];
        }
    } failure:^(NSError *error) {
        [self.tableview endHeaderRefreshWith:@[]];
    }];
}

#pragma mark - 下拉刷新

- (void)headerWillRefresh{
    [self requestNewsList:self.groupId];
}

#pragma mark - UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.newsListArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TENRecommendCell *recommendCell = [tableView dequeueReusableCellWithIdentifier:@"recommend" forIndexPath:indexPath];
    recommendCell.selectionStyle = UITableViewCellSelectionStyleNone;
    recommendCell.cellData = self.newsListArr[indexPath.row];
    return recommendCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    RecommendNews *new = self.newsListArr[indexPath.row];
    
    TENBaseWebViewController *webviewController = [TENBaseWebViewController new];
    NSString *contentURL = [[TENUrlManager share] url:@"recommend" detail:@"headlines"];
    webviewController.uri = [contentURL stringByAppendingPathComponent:new.newsId];
    [webviewController load];
    webviewController.hidesBottomBarWhenPushed = YES;
    
    [self.navigationController pushViewController:webviewController animated:YES];
}

#pragma mark - SPPageMenu的代理方法

- (void)pageMenu:(SPPageMenu *)pageMenu itemSelectedAtIndex:(NSInteger)index {
    NSLog(@"%zd",index);
    NSString *groupId = [NSString stringWithFormat:@"%ld",(long)index];
    [self requestNewsList:groupId];
}

#pragma - mark Gettet&&Setter

- (UITableView *)tableview {
    if (!_tableview) {
        _tableview = [TENTableView new];
        _tableview.estimatedRowHeight = 80.f;
        _tableview.rowHeight = UITableViewAutomaticDimension;
        _tableview.backgroundColor = TENHexColor(TENWhiteColor);
        _tableview.separatorColor = TENHexClearColor;
        _tableview.delegate = self;
        _tableview.dataSource = self;
        [_tableview registerNib:[UINib nibWithNibName:@"TENRecommendCell" bundle:nil] forCellReuseIdentifier:@"recommend"];
        _tableview.refreshDelegate = self;
        _tableview.refreshType = TENTableViewRefreshTypeHeader;
    }
    return _tableview;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

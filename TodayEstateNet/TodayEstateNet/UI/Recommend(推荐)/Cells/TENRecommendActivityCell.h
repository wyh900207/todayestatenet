//
//  TENRecommendActivityCell.h
//  TodayEstateNet
//
//  Created by  HomerLynn on 2018/10/27.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENTableViewCell.h"
#import "HLTagsView.h"
#import "RecommendModel.h"
NS_ASSUME_NONNULL_BEGIN

// 推荐 -> 活动 cell
@interface TENRecommendActivityCell : TENTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet HLTagsView *tagsView;
@property (weak, nonatomic) IBOutlet UIImageView *contentImageView;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *othersLabel;
// 阅读量和时间等
@property (weak, nonatomic) IBOutlet UIButton *orderButton;
    
// 数据源
@property (nonatomic, strong) NSDictionary *cellData;
@property (nonatomic, strong) RecommendActivity *data;
// 立即报名按钮回调
@property (nonatomic, copy) void (^orderNowHandler)(void);
    
@end

NS_ASSUME_NONNULL_END

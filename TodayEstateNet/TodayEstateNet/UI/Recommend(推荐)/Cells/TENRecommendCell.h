//
//  TENRecommendCell.h
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/9/19.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RecommendModel.h"

@interface TENRecommendCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *newsTitle;

@property (weak, nonatomic) IBOutlet UILabel *otherInfo;
@property (weak, nonatomic) IBOutlet UILabel *content;
@property (weak, nonatomic) IBOutlet UILabel *sticky;

@property (nonatomic, strong) RecommendNews *cellData;

@end

//
//  TENRecommendCell.m
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/9/19.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENRecommendCell.h"
#import "TENUtilsHeader.h"

@interface TENRecommendCell ()



@end

@implementation TENRecommendCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.iconImageView sd_setImageWithURL:[NSURL URLWithString:@"https://ss0.baidu.com/7Po3dSag_xI4khGko9WTAnF6hhy/image/h%3D300/sign=0f602b24b7a1cd111ab674208913c8b0/b219ebc4b74543a9759a35d813178a82b9011425.jpg"]];
    self.sticky.layer.cornerRadius = 2;
    self.sticky.layer.borderColor = TENHexColor(TENTextRedColor).CGColor;
    self.sticky.layer.borderWidth = 1;
    self.sticky.layer.masksToBounds = YES;
}
-(void)setCellData:(RecommendNews *)cellData{
    _cellData = cellData;
    NSString *content = _cellData.zhaiyao;
    NSString *newsTitle = _cellData.headline;
    NSString *otherInfo = [NSString stringWithFormat:@"作者：%@  %@  %@阅读",_cellData.resource, [NSObject getTimeStrWithTimestamp:_cellData.createTime format:@"YYYY-MM-dd"], _cellData.readNum];
    NSURL *imgUrl = [NSURL URLWithString:_cellData.picPath];
    
    self.content.text = content;
    self.newsTitle.text = newsTitle;
    self.otherInfo.text = otherInfo;
    [self.iconImageView sd_setImageWithURL:imgUrl placeholderImage:nil];
}
@end

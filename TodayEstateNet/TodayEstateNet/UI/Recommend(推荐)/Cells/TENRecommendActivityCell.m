//
//  TENRecommendActivityCell.m
//  TodayEstateNet
//
//  Created by  HomerLynn on 2018/10/27.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENRecommendActivityCell.h"
#import "TENUtilsHeader.h"

@implementation TENRecommendActivityCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupSubviews];
}
    
#pragma mark - UI
    
- (void)setupSubviews {
    self.orderButton.layer.cornerRadius = 4;
    self.orderButton.layer.masksToBounds = YES;
}
    
#pragma mark - Setter
    
- (void)setCellData:(NSDictionary *)cellData {
    _cellData = cellData;
    self.titleLabel.text = cellData[@"buildingGroupName"];
    [self.contentImageView sd_setImageWithURL:cellData[@"picPath"]];
}
-(void)setData:(RecommendActivity *)data{
    _data = data;
    self.titleLabel.text = data.title;
    [self.contentImageView sd_setImageWithURL:[NSURL URLWithString:data.picPath] placeholderImage:nil];
}
#pragma mark - Action
    
- (IBAction)orderNow:(id)sender {
    
}

@end

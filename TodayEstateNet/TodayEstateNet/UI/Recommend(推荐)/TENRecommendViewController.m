//
//  TENRecommendViewController.m
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/9/11.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENRecommendViewController.h"
#import "TENUtilsHeader.h"
#import "TENRecommendCell.h"
#import "SPPageMenu.h"

@interface TENRecommendViewController ()<UITableViewDelegate, UITableViewDataSource,SPPageMenuDelegate>

@property (nonatomic, strong) UITableView *tableview;
@property (nonatomic, strong) NSArray *dataArr;
@property (nonatomic, weak  ) SPPageMenu *pageMenu;
@property (nonatomic, strong) UIScrollView *scrollView;
@property (nonatomic, strong) NSMutableArray *myChildViewControllers;

@end

@implementation TENRecommendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
//    [self setupSubviews];
    [self customNavBar];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = NO;
}

#pragma mark 自定义导航

- (void)customNavBar {
    [self.navigationController.navigationBar DJSetBackgroundColor:[UIColor colorWithHexString:TENThemeBlueColor]];
    self.dataArr = @[@"活动",@"楼盘",@"头条"];
    // trackerStyle:跟踪器的样式
    SPPageMenu *pageMenu = [SPPageMenu pageMenuWithFrame:CGRectMake(0, 0, TENScreenWidth, 35) trackerStyle:SPPageMenuTrackerStyleLine];
    pageMenu.permutationWay = SPPageMenuPermutationWayNotScrollEqualWidths;
    pageMenu.backgroundColor = [UIColor clearColor];
    pageMenu.dividingLine.hidden = YES;
    // 传递数组，默认选中第2个
    [pageMenu setItems:self.dataArr selectedItemIndex:0];
    // 设置代理
    pageMenu.delegate = self;
    pageMenu.trackerWidth = 20;
    pageMenu.tracker.backgroundColor = [UIColor whiteColor];
    // 给pageMenu传递外界的大scrollView，内部监听self.scrollView的滚动，从而实现让跟踪器跟随self.scrollView移动的效果
    pageMenu.bridgeScrollView = self.scrollView;
    pageMenu.selectedItemTitleColor = [UIColor whiteColor];
    pageMenu.unSelectedItemTitleColor = [UIColor whiteColor];
    self.navigationItem.titleView = pageMenu;
    _pageMenu = pageMenu;
    [self.contentView addSubview:self.scrollView];
    [self.scrollView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    }];
    [self addChildVC];
    
//    self.scrollView.backgroundColor = [UIColor redColor];
}

- (NSMutableArray *)myChildViewControllers {
    
    if (!_myChildViewControllers) {
        _myChildViewControllers = [NSMutableArray array];
    }
    return _myChildViewControllers;
}

#pragma mark  子控制器

- (void)addChildVC {
    NSArray   *controllerClassNames = [NSArray arrayWithObjects:@"TENActivityViewController",@"TENBuildingViewController",@"TENHeaderLinesViewController", nil];
    for (int i = 0; i < self.dataArr.count; i++) {
        if (controllerClassNames.count > i) {
            TENBaseViewController *baseVc = [[NSClassFromString(controllerClassNames[i]) alloc] init];
            
            [self addChildViewController:baseVc];
            [self.myChildViewControllers addObject:baseVc];
        }
    }
    if (self.pageMenu.selectedItemIndex < self.myChildViewControllers.count)
    {
            TENBaseViewController *baseVc = self.myChildViewControllers[self.pageMenu.selectedItemIndex];
            [self.scrollView addSubview:baseVc.view];
            baseVc.view.frame = CGRectMake(TENScreenWidth*self.pageMenu.selectedItemIndex, 0, TENScreenWidth, TENScreenHeight);
            self.scrollView .contentOffset = CGPointMake(TENScreenWidth*self.pageMenu.selectedItemIndex, 0);
            self.scrollView.contentSize = CGSizeMake(self.dataArr.count*TENScreenWidth, 0);
    }
}

#pragma mark - SPPageMenu的代理方法

- (void)pageMenu:(SPPageMenu *)pageMenu itemSelectedAtIndex:(NSInteger)index {
    NSLog(@"%zd",index);
}

- (void)pageMenu:(SPPageMenu *)pageMenu itemSelectedFromIndex:(NSInteger)fromIndex toIndex:(NSInteger)toIndex {
    NSLog(@"%zd------->%zd",fromIndex,toIndex);
    
    // 如果该代理方法是由拖拽self.scrollView而触发，说明self.scrollView已经在用户手指的拖拽下而发生偏移，此时不需要再用代码去设置偏移量，否则在跟踪模式为SPPageMenuTrackerFollowingModeHalf的情况下，滑到屏幕一半时会有闪跳现象。闪跳是因为外界设置的scrollView偏移和用户拖拽产生冲突
    if (!self.scrollView.isDragging) { // 判断用户是否在拖拽scrollView
        // 如果fromIndex与toIndex之差大于等于2,说明跨界面移动了,此时不动画.
        if (labs(toIndex - fromIndex) >= 2) {
            [self.scrollView setContentOffset:CGPointMake(TENScreenWidth * toIndex, 0) animated:NO];
        } else {
            [self.scrollView setContentOffset:CGPointMake(TENScreenWidth * toIndex, 0) animated:YES];
        }
    }
    
    if (self.myChildViewControllers.count <= toIndex) {return;}
    
    UIViewController *targetViewController = self.myChildViewControllers[toIndex];
    // 如果已经加载过，就不再加载
    if ([targetViewController isViewLoaded]) return;
    [self.scrollView addSubview:targetViewController.view];
    targetViewController.view.frame = CGRectMake(TENScreenWidth * toIndex, 0, TENScreenWidth, TENScreenHeight);
    
}

- (UIScrollView *)scrollView {
    if (!_scrollView) {
        _scrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, TENScreenWidth, TENScreenHeight)];
        _scrollView.delegate = self;
        _scrollView.pagingEnabled = YES;
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.showsVerticalScrollIndicator = NO;
    }
    return  _scrollView;
}

#pragma mark - UI

- (void)setupSubviews {
    [self.contentView addSubview:self.tableview];
    [self.tableview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.contentView);
    }];
}

#pragma mark - UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TENRecommendCell *recommendCell = [tableView dequeueReusableCellWithIdentifier:@"recommend" forIndexPath:indexPath];
    recommendCell.selectionStyle = UITableViewCellSelectionStyleNone;
    return recommendCell;
}

#pragma mark - Getter

- (UITableView *)tableview {
    if (!_tableview) {
        _tableview = [UITableView new];
        _tableview.estimatedRowHeight = 80.f;
        _tableview.rowHeight = UITableViewAutomaticDimension;
        _tableview.backgroundColor = TENHexColor(TENWhiteColor);
        _tableview.separatorColor = TENHexClearColor;
        _tableview.delegate = self;
        _tableview.dataSource = self;
        [_tableview registerNib:[UINib nibWithNibName:@"TENRecommendCell" bundle:nil] forCellReuseIdentifier:@"recommend"];
    }
    return _tableview;
}

@end

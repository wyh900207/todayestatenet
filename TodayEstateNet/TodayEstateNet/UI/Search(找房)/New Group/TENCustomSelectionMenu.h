//
//  TENCustomSelectionMenu.h
//  TodayEstateNet
//
//  Created by Admin on 2018/9/25.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol TENMenuItemContentView <NSObject>
@optional
/**
 * 需要定义自身高度 (默认 200.f)
 */
- (CGFloat)contentViewHeight:(CGRect)contentBounds;
@end



@interface TENMenuItem : NSObject

@property (strong ,nonatomic) NSString *title; // 标签名称 (默认状态)
@property (strong ,nonatomic) UIView <TENMenuItemContentView>*contentView; // 标签对应的子菜单
@end


@interface TENCustomSelectionMenu : UIView

/**
 * 唯一初始化
 */
- (id)initWithItems:(NSArray <TENMenuItem *>*)items;
- (void)dismissAll:(UIView *)sender;
@property (strong ,nonatomic ,readonly) UIView *menuView; // 标签view
@property (strong ,nonatomic ,readonly) UIControl *backgroundView; // 背景view
@property (strong ,nonatomic ,readonly) NSArray <TENMenuItem *>*items; // 标签数据
@end

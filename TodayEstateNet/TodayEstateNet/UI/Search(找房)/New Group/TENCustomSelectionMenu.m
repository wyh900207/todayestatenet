//
//  TENCustomSelectionMenu.m
//  TodayEstateNet
//
//  Created by Admin on 2018/9/25.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENCustomSelectionMenu.h"
#import "TENUtilsHeader.h"
#import "UIView+LJExtension.h"
#import "TENSearchButton.h"
#import "TENdistrictView.h"
#import "TENCustomSelectRoomView.h"

@implementation TENMenuItem
@end


@interface TENCustomSelectionMenu ()<TENCustomAreaViewDelegate>

@property (nonatomic, weak) UIView *controlView;
/** 数据源 */
@property (nonatomic, strong) NSArray *dataArray;
@property (nonatomic, strong) NSMutableArray <TENSearchButton *>*buttonArray; // 所有标签

@end
@implementation TENCustomSelectionMenu
- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
//        [self setupSubviews];
    }
    return self;
}



#pragma mark 按钮点击处理
-(void)btnClick:(UIButton *)sender
{
    //在这里来5个自定义view
    sender.selected = !sender.selected;
    
  
}




#pragma mark - - - - - - - - - - - - - - - - - - - -

/**
 * 唯一初始化
 */
- (id)initWithItems:(NSArray <TENMenuItem *>*)items; {
    self = [super init];
    if (self) {
        _items = items;
        [self setup];
    } return self;
}

- (void)setup {
    
    _backgroundView = [[UIControl alloc] init];
    [_backgroundView addTarget:self action:@selector(dismissAll:) forControlEvents:UIControlEventTouchUpInside];
    _backgroundView.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.3];
    _backgroundView.alpha = 0.f;
    [self addSubview:_backgroundView];
    
    _menuView = [[UIView alloc] init];
    _menuView.backgroundColor = [UIColor whiteColor];
    [self addSubview:_menuView];
    
    
    [_backgroundView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.insets(UIEdgeInsetsMake(0, 0, 0, 0));
    }];
    
    [_menuView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.right.equalTo(self);
        make.height.equalTo(@(44.f));
    }];
    
    _buttonArray = [NSMutableArray arrayWithCapacity:_items.count];
    UIView *layoutInstance = nil ,*lastButton = nil; // 约束参照view
    for (int i = 0; i < _items.count ; i ++) {
        TENMenuItem *item = _items[i];
        TENSearchButton *button = [TENSearchButton buttonWithType:UIButtonTypeCustom];
        [button setTitle:item.title forState:UIControlStateNormal];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:@"filter-dropdown-arrow"] forState:UIControlStateNormal];
        [button setImage:[UIImage imageNamed:@"filter-dropdown-arrow-active"] forState:UIControlStateSelected];
        [button addTarget:self action:@selector(clickActions:) forControlEvents:UIControlEventTouchUpInside];
        button.titleLabel.font = [UIFont systemFontOfSize:15.f];
        [_menuView addSubview:button];
        [_buttonArray addObject:button];
        
        if (!layoutInstance) {
            [button mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.top.bottom.equalTo(self.menuView);
            }];
        } else {
            [button mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(layoutInstance.mas_right);
                make.top.bottom.equalTo(self.menuView);
                make.width.equalTo(lastButton);
            }];
        }
        
        if (i < _items.count - 1) {
            // 补充一条分隔线
            UIView *line = [[UIView alloc] init];
            line.backgroundColor = LJColor(0.88, 0.88, 0.88, 1);
            [_menuView addSubview:line];
            [line mas_makeConstraints:^(MASConstraintMaker *make) {
                make.left.equalTo(button.mas_right);
                make.centerY.equalTo(button);
                make.size.sizeOffset(CGSizeMake(.5, 22.f));
            }];
            layoutInstance = line;
        } else {
            [button mas_updateConstraints:^(MASConstraintMaker *make) {
                make.right.equalTo(self.menuView);
            }];
        }
        
        lastButton = button;
    }
}

#pragma mark - actions
- (void)dismissAll:(UIView *)sender {
    
    [self mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(44.f));
    }];
    
    [self.superview setNeedsLayout];
    [self.superview layoutIfNeeded];
    
    self.backgroundView.alpha = 0.f;
    
    TENSearchButton *selectedButton = [self utils_selectedItemButton];
    if (selectedButton) {
        selectedButton.selected = NO;
        [[_items[[_buttonArray indexOfObject:selectedButton]] contentView] removeFromSuperview];
    }
}

- (void)clickActions:(TENSearchButton *)sender {
    
    // 移除当前选中的
    TENSearchButton *selectedButton = [self utils_selectedItemButton];
    [self dismissAll:_backgroundView];
    if (selectedButton == sender) {
        return; // 不需要做切换
    }
    
    // 切换 content view
    sender.selected = YES;
    
    [self mas_updateConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@(TENScreenHeight));
    }];
    
    [self.superview setNeedsLayout];
    [self.superview layoutIfNeeded];
    
    self.backgroundView.alpha = 1.f;
    
    TENMenuItem *item = _items[[_buttonArray indexOfObject:sender]];
    CGFloat height = 200.f;
    if ([item.contentView respondsToSelector:@selector(contentViewHeight:)]) {
        height = [item.contentView contentViewHeight:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
    }
    item.contentView.frame = CGRectMake(0, 44.f, self.frame.size.width, height);
    [self addSubview:item.contentView];
}

#pragma mark - utils
- (TENSearchButton *)utils_selectedItemButton {
    for (TENSearchButton *object in _buttonArray) {
        if (object.selected) {
            return object;
        }
    }
    return nil;
}

@end

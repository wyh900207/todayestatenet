//
//  TENSearchButton.m
//  TodayEstateNet
//
//  Created by Admin on 2018/9/25.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENSearchButton.h"
//#import "UIView+Frame.h"
#import "UIView+LJExtension.h"
@implementation TENSearchButton

-(void)layoutSubviews
{
    [super layoutSubviews];
    //上左下右
    [self setTitleEdgeInsets:UIEdgeInsetsMake(0, -self.imageView.lj_width, 0, self.imageView.lj_width + 5)];
    [self setImageEdgeInsets:UIEdgeInsetsMake(0, self.titleLabel.bounds.size.width + 5, 0, -self.titleLabel.bounds.size.width)];
    
    self.titleLabel.textColor = [UIColor blackColor];
    self.titleLabel.font = [UIFont systemFontOfSize:13];
}

@end

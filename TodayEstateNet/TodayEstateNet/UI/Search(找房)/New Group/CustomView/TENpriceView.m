//
//  TENpriceView.m
//  TodayEstateNet
//
//  Created by 李冲 on 2018/11/5.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENpriceView.h"
#import "TENTableView.h"
#import "TENUtilsHeader.h"
#import <Masonry.h>
#import "SearchData.h"
#import "AreaTableViewCell.h"
@interface TENpriceView ()<UITableViewDelegate,UITableViewDataSource>
//tabbleView
@property (nonatomic, strong) TENTableView *leftTableView;
@property (nonatomic, strong) TENTableView *rightTableView;
//清空取消
@property (nonatomic, strong) UIButton *sureBtn;
@property (nonatomic,strong) UITextField *minPrice;
@property (nonatomic,strong) UITextField *maxPrice;

@property (nonatomic, strong) NSArray <SearchData *>*rightDataArr;

@property (nonatomic, strong) SearchData *priceData;
@property (nonatomic, strong) SearchData *leftSelectData;
@property (nonatomic, strong) SearchData *rightSelectData;


@end

static const CGFloat tableViewHeight = 308;
@implementation TENpriceView
- (instancetype)init {
    self = [super init];
    if (self) {
        UIView *btnBgView = [[UIView alloc] init];
        btnBgView.backgroundColor = TENHexColor(TENWhiteColor) ;
        [self addSubview:btnBgView];
        [btnBgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.offset(0);
            make.left.offset(0);
            make.right.offset(0);
            make.height.offset(63);
        }];
        [self addSubview:self.leftTableView];
        [self.leftTableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0);
            make.left.offset(0);
            make.bottom.mas_equalTo(btnBgView.mas_top).with.offset(0);
            make.width.mas_equalTo(self).multipliedBy(1/4.0);
        }];
    
        
        [self addSubview:self.rightTableView];
        [self.rightTableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0);
            make.left.mas_equalTo(self.leftTableView.mas_right).with.offset(0);
            make.bottom.mas_equalTo(btnBgView.mas_top).with.offset(0);
            make.right.offset(0);
        }];
        
        [btnBgView addSubview:self.minPrice];
         [self.minPrice mas_makeConstraints:^(MASConstraintMaker *make) {
             make.left.offset(20);
             make.centerY.mas_equalTo(btnBgView.mas_centerY);
             make.width.offset(89);
             make.height.offset(30);
         }];
        [btnBgView addSubview:self.maxPrice];
        [self.maxPrice mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(btnBgView.mas_centerY);
            make.width.offset(89);
            make.height.offset(30);
            make.left.mas_equalTo(self.minPrice.mas_right).offset(20);
        }];
        
       

        [btnBgView addSubview:self.sureBtn];
        [self.sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(btnBgView.mas_centerY);
            make.right.offset(-20);
            make.width.offset(120);
            make.height.offset(30);
        }];
        
        UIView *midView = [[UIView alloc] init];
        [btnBgView addSubview:midView];
        midView.backgroundColor = TENHexColor(TENSpaceViewColor);
        [midView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.mas_equalTo(self.minPrice.mas_right).offset(6);
            make.centerY.mas_equalTo(btnBgView.mas_centerY);
            make.height.offset(1);
            make.right.mas_equalTo(self.maxPrice.mas_left).offset(-6);
        }];
        
        UIView *lineView = [[UIView alloc] init];
        lineView.backgroundColor = TENHexColor(TENSpaceViewColor);
        [btnBgView addSubview:lineView];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0);
            make.right.offset(0);
            make.top.offset(0);
            make.height.offset(1);
        }];
    }
    return self;
}
- (void)updateUIWith:(SearchData *)data{
    self.priceData = data;
    self.priceData.list.firstObject.isSelected = YES;
    self.leftSelectData = self.priceData.list.firstObject;
    
    self.rightDataArr = data.list.firstObject.list;
    self.rightDataArr.firstObject.isSelected = YES;
    self.rightSelectData = self.rightDataArr.firstObject;
    
    [self.leftTableView reloadData];
    [self.rightTableView reloadData];
    
}
#pragma mark  - private
- (void)sure{
    NSDictionary *parameter = @{
                                @"cityIds" : @"310100",
                                @"lat" : @"31.2317",
                                @"lng" : @"121.473",
                                @"pageNo" : @1,
                                @"pageSize" : @10,
                                self.leftSelectData.type:self.rightSelectData.name
                                };
    //    @"totalPrice":self.rigntSlectData.searchId
    self.dismissView(YES, parameter);

}
- (void)cancle{
    //清空筛选条件
//    self.leftSelectData.
    self.dismissView(NO,nil);
}
#pragma mark - UITableViewDataSource
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (tableView.tag) {
        case 101:
            return self.priceData.list.count;
            break;
        case 102:
            return self.rightDataArr.count;
            break;
        default:
            return 0;
            break;
    }
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AreaTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    switch (tableView.tag) {
        case 101:
            cell.cellTitle.text = self.priceData.list[indexPath.row].name;
            if (self.priceData.list[indexPath.row].isSelected) {
                cell.cellTitle.textColor = TENHexColor(TENRedColor);
            }else{
                cell.cellTitle.textColor = TENHexColor(TENBlackColor);
            }
            break;
        case 102:
            cell.cellTitle.text = self.rightDataArr[indexPath.row].name;
            cell.backgroundColor = TENHexColor(@"F9F9F9");
            if (self.rightDataArr[indexPath.row].isSelected) {
                cell.cellTitle.textColor = TENHexColor(TENRedColor);
            }else{
                cell.cellTitle.textColor = TENHexColor(TENBlackColor);
            }
            break;

        default:
            break;
    }
    return cell;
}
//选中tableview的cell
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    AreaTableViewCell *selectCell = [tableView cellForRowAtIndexPath:indexPath];;
    AreaTableViewCell *deselectCell = nil;

    switch (tableView.tag) {
        case 101:
            //点击处理
            if (self.leftSelectData.selectIndex != indexPath.row) {
                deselectCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:self.leftSelectData.selectIndex inSection:0]];
                selectCell.cellTitle.textColor = TENHexColor(TENRedColor);
                deselectCell.cellTitle.textColor = TENHexColor(TENTextBlackColor3);
                self.priceData.list[indexPath.row].isSelected = YES;
                self.priceData.list[self.leftSelectData.selectIndex].isSelected = NO;
                self.leftSelectData = self.priceData.list[indexPath.row];
                self.leftSelectData.selectIndex = indexPath.row;
                //数据源
                self.rightDataArr = [NSArray arrayWithArray:self.priceData.list[indexPath.row].list];
                self.rightSelectData = self.rightDataArr.firstObject;
                self.rightSelectData.selectIndex = 0;
                self.rightSelectData.isSelected = YES;
                [self.rightTableView reloadData];
            }
            
            break;
        default:
            //点击处理
            if (self.rightSelectData.selectIndex != indexPath.row) {
                deselectCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:self.rightSelectData.selectIndex inSection:0]];
                selectCell.cellTitle.textColor = TENHexColor(TENRedColor);
                deselectCell.cellTitle.textColor = TENHexColor(TENTextBlackColor3);
                self.rightDataArr[indexPath.row].isSelected = YES;
                self.rightDataArr[self.rightSelectData.selectIndex].isSelected = NO;
                self.rightSelectData = self.rightDataArr[indexPath.row];
                self.rightSelectData.selectIndex = indexPath.row;
                self.rightDataArr[indexPath.row].isSelected = YES;
                self.rightDataArr[self.rightSelectData.selectIndex].isSelected = NO;
            }
            
            break;
    }
}
-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
//    AreaTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//    cell.cellTitle.textColor = TENHexColor(TENTextBlackColor3);
//    switch (tableView.tag) {
//        case 101:
//            self.priceData.list[indexPath.row].isSelected = NO;
//            break;
//        default:
//            self.rightDataArr[indexPath.row].isSelected = NO;
//            break;
//    }
}

#pragma mark - Getter&&Setter
-(UITextField *)minPrice{
    if (!_minPrice) {
        _minPrice = [UITextField new];
        _minPrice.font = TENFont12;
        _minPrice.textColor = TENHexColor(TENTextGrayColor);
        _minPrice.backgroundColor = TENHexColor(@"F8F8F8");
        NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
        style.alignment = NSTextAlignmentCenter;
        NSAttributedString *attri = [[NSAttributedString alloc] initWithString:@"最低总价" attributes:@{NSParagraphStyleAttributeName:style}];
        _minPrice.attributedPlaceholder = attri;
        _minPrice.keyboardType = UIKeyboardTypeNumberPad;
        _minPrice.textAlignment = NSTextAlignmentCenter;
    }
    return _minPrice;
}
-(UITextField *)maxPrice{
    if (!_maxPrice) {
        _maxPrice = [UITextField new];
        _maxPrice.font = TENFont12;
        _maxPrice.textColor = TENHexColor(TENTextGrayColor);
        _maxPrice.backgroundColor = TENHexColor(@"F8F8F8");
        NSMutableParagraphStyle *style = [[NSMutableParagraphStyle alloc] init];
        style.alignment = NSTextAlignmentCenter;
        NSAttributedString *attri = [[NSAttributedString alloc] initWithString:@"最高总价" attributes:@{NSParagraphStyleAttributeName:style}];
        _maxPrice.attributedPlaceholder = attri;
        _maxPrice.keyboardType = UIKeyboardTypeNumberPad;
        _maxPrice.textAlignment = NSTextAlignmentCenter;
    }
    return _maxPrice;
}

-(UIButton *)sureBtn{
    if (!_sureBtn) {
        _sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_sureBtn setTitleColor:TENHexColor(TENWhiteColor) forState:UIControlStateNormal];
        _sureBtn.layer.borderWidth = 1;
        _sureBtn.layer.borderColor = TENHexColor(TENTextGrayColor6).CGColor;
        _sureBtn.layer.cornerRadius = 4;
        _sureBtn.clipsToBounds = YES;
        _sureBtn.titleLabel.font = TENFont15;
        _sureBtn.backgroundColor = TENHexColor(TENThemeBlueColor);
        [_sureBtn addTarget:self action:@selector(sure) forControlEvents:UIControlEventTouchUpInside];
        [_sureBtn setTitle:@"确定" forState:UIControlStateNormal];
    }
    return _sureBtn;
}
-(UITableView *)leftTableView{
    if (!_leftTableView) {
        _leftTableView = [[TENTableView alloc] init];
        _leftTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        _leftTableView.delegate = self;
        _leftTableView.dataSource = self;
        [_leftTableView registerClass:[AreaTableViewCell class] forCellReuseIdentifier:@"cell"];
        _leftTableView.tag = 101;
    }
    return _leftTableView;
}

-(UITableView *)rightTableView{
    if (!_rightTableView) {
        _rightTableView = [[TENTableView alloc] init];
        _rightTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        _rightTableView.delegate = self;
        _rightTableView.dataSource = self;
        _rightTableView.tag = 102;
        [_rightTableView registerClass:[AreaTableViewCell class] forCellReuseIdentifier:@"cell"];
    }
    return _rightTableView;
}

-(NSArray *)rightDataArr{
    if (!_leftTableView) {
        _rightDataArr = [NSArray array];
    }
    return _rightDataArr;
}
-(SearchData *)leftSelectData{
    if (!_leftSelectData) {
        _leftSelectData = [[SearchData alloc] init];
        _leftSelectData.selectIndex = 0;
    }
    return _leftSelectData;
}
-(SearchData *)rightSelectData{
    if (!_rightSelectData) {
        _rightSelectData = [[SearchData alloc] init];
        _rightSelectData.selectIndex = 0;
    }
    return _rightSelectData;
}


/**
 * 需要定义自身高度 (默认 200.f)
 */
- (CGFloat)contentViewHeight:(CGRect)contentBounds; {
    return tableViewHeight;
}
@end

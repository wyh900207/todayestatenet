//
//  MoreCollectionViewCell.m
//  TodayEstateNet
//
//  Created by 李冲 on 2018/11/5.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "MoreCollectionViewCell.h"
#import "TENUtilsHeader.h"
@implementation MoreCollectionViewCell
-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self.contentView addSubview:self.cellTitle];
        [self.cellTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0);
            make.right.offset(0);
            make.left.offset(0);
            make.bottom.offset(0);
        }];
    }
    return self;
}
-(UILabel *)cellTitle{
    if (!_cellTitle) {
        _cellTitle = [[UILabel alloc]init];
        _cellTitle.font = TENFont12;
        _cellTitle.textColor = TENHexColor(@"999999");
        _cellTitle.textAlignment = NSTextAlignmentCenter;
        _cellTitle.layer.cornerRadius = 4;
        _cellTitle.clipsToBounds = YES;
        _cellTitle.backgroundColor = TENHexColor(@"F0F0F0");
    }
    return _cellTitle;
}
@end

//
//  TENdistrictView.m
//  TodayEstateNet
//
//  Created by Admin on 2018/9/25.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENdistrictView.h"
//#import "TENUtilsHeader.h"
#import "UIView+LJExtension.h"
#import "TENTableView.h"
#import "AreaTableViewCell.h"
#import "SearchData.h"

/** tableView高度 */
static const CGFloat tableViewHeight = 308;
@interface TENdistrictView()<UITableViewDelegate,UITableViewDataSource>


//tabbleView
@property (nonatomic, strong) TENTableView *leftTableView;
@property (nonatomic, strong) TENTableView *midTableView;
@property (nonatomic, strong) TENTableView *rightTableView;
//tableView的数据源
@property (nonatomic, strong) NSArray <SearchData *>*rightDataArr;
@property (nonatomic, strong) NSArray <SearchData *>*midDataArr;


@property (nonatomic, strong) SearchData *areaData;

@property (nonatomic, strong) UIButton *cancleBtn;
@property (nonatomic, strong) UIButton *sureBtn;

//bottomView
@property (nonatomic, strong) UIView *areaBottomView;
@property (nonatomic, strong) UIView *priceBottomView;
@property (nonatomic, strong) UIView *roomBottomView;
//@property (nonatomic, copy) nss
@property (nonatomic) SearchData *rigntSlectData;
@property (nonatomic) SearchData *midSlectData;
@property (nonatomic) SearchData *leftSlectData;

//@property (nonatomic ,strong) NSMutableDictionary *condition;
@end
@implementation TENdistrictView

- (instancetype)init {
    self = [super init];
    if (self) {
        UIView *btnBgView = [[UIView alloc] init];
        btnBgView.backgroundColor = TENHexColor(TENWhiteColor) ;
        [self addSubview:btnBgView];
        [btnBgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.offset(0);
            make.left.offset(0);
            make.right.offset(0);
            make.height.offset(63);
        }];
        [self addSubview:self.leftTableView];
        [self.leftTableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0);
            make.left.offset(0);
            make.bottom.mas_equalTo(btnBgView.mas_top).with.offset(0);
            make.width.mas_equalTo(self).multipliedBy(1/4.0);
        }];
        
        [self addSubview:self.midTableView];
        [self.midTableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0);
            make.left.mas_equalTo(self.leftTableView.mas_right).with.offset(0);
            make.bottom.mas_equalTo(btnBgView.mas_top).with.offset(0);
            make.width.mas_equalTo(self).multipliedBy(1/4.0);
        }];
        
        [self addSubview:self.rightTableView];
        [self.rightTableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0);
            make.left.mas_equalTo(self.midTableView.mas_right).with.offset(0);
            make.bottom.mas_equalTo(btnBgView.mas_top).with.offset(0);
            make.width.mas_equalTo(self).multipliedBy(1/2.0);
        }];
        
        [btnBgView addSubview:self.cancleBtn];
        [self.cancleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(btnBgView.mas_centerY);
            make.centerX.mas_equalTo(btnBgView.mas_centerX).multipliedBy(0.5);
            make.left.offset(20);
            make.width.offset((TENScreenWidth-50)/2);
            make.height.offset(30);
        }];
        
        [btnBgView addSubview:self.sureBtn];
        [self.sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(btnBgView.mas_centerY);
            make.centerX.mas_equalTo(btnBgView.mas_centerX).multipliedBy(1.5);
            make.right.offset(-20);
            make.width.offset((TENScreenWidth-50)/2);
            make.height.offset(30);
        }];
        UIView *lineView = [[UIView alloc] init];
        lineView.backgroundColor = TENHexColor(TENSpaceViewColor);
        [btnBgView addSubview:lineView];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0);
            make.right.offset(0);
            make.top.offset(0);
            make.height.offset(1);
        }];
        
    }
    return self;
}
- (void)updateUIWith:(SearchData *)data{
    self.areaData = data;
    self.areaData.list.firstObject.isSelected = YES;
    
    self.midDataArr = data.list.firstObject.list;
    self.midDataArr.firstObject.isSelected = YES;
    
    self.rightDataArr = self.midDataArr.firstObject.child;
    self.rightDataArr.firstObject.isSelected = YES;
    
    [self.leftTableView reloadData];
    [self.midTableView reloadData];
    [self.rightTableView reloadData];
    
}
#pragma mark  - private
- (void)sure{
    NSDictionary *parameter = @{
                                @"cityIds" : @"310100",
                                @"lat" : @"31.2317",
                                @"lng" : @"121.473",
                                @"pageNo" : @1,
                                @"pageSize" : @10,
                                
                                };
    //    @"districtId":self.rigntSlectData.searchId
    self.dismissView(YES, parameter);
}
- (void)cancle{
    self.dismissView(NO,nil);
}
#pragma mark - UITableViewDataSource
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (tableView.tag) {
        case 101:
            return self.areaData.list.count;
            break;
        case 102:
            return self.midDataArr.count;
            break;
        case 103:
            return self.rightDataArr.count;
            break;
        default:
            return 0;
            break;
    }
}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    AreaTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    switch (tableView.tag) {
        case 101:
            
            cell.cellTitle.text = self.areaData.list[indexPath.row].name;
            if (self.areaData.list[indexPath.row].isSelected) {
                cell.cellTitle.textColor = TENHexColor(TENRedColor);
            }else{
                cell.cellTitle.textColor = TENHexColor(TENBlackColor);
            }
            
            break;
        case 102:
            cell.cellTitle.text = self.midDataArr[indexPath.row].name;
            cell.backgroundColor = TENHexColor(@"F9F9F9");
            if (self.midDataArr[indexPath.row].isSelected) {
                cell.cellTitle.textColor = TENHexColor(TENRedColor);
            }else{
                cell.cellTitle.textColor = TENHexColor(TENBlackColor);
            }
            break;
        case 103:
            cell.cellTitle.text = self.rightDataArr[indexPath.row].name;
            cell.backgroundColor = TENHexColor(@"F3F3F3");
            if (self.rightDataArr[indexPath.row].isSelected) {
                cell.cellTitle.textColor = TENHexColor(TENRedColor);
            }else{
                cell.cellTitle.textColor = TENHexColor(TENBlackColor);
            }
            break;
        default:
            break;
    }
    return cell;
}
//选中tableview的cell
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    AreaTableViewCell *selectedCell = nil;
    AreaTableViewCell *deselectedCell = nil;
    //字体变色
    
    switch (tableView.tag) {
        case 101:{
            //点击处理
            if (self.leftSlectData.selectIndex != indexPath.row) {
                selectedCell = [tableView cellForRowAtIndexPath:indexPath];
                deselectedCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:self.leftSlectData.selectIndex inSection:0]];
                selectedCell.cellTitle.textColor = TENHexColor(TENRedColor);
                deselectedCell.cellTitle.textColor = TENHexColor(TENTextBlackColor3);
                self.areaData.list[indexPath.row].isSelected = YES;
                self.areaData.list[self.leftSlectData.selectIndex].isSelected = NO;
                self.leftSlectData = self.areaData.list[indexPath.row];
                self.leftSlectData.selectIndex = indexPath.row;
                
                //第二个tableview的数据源
                self.midDataArr = [NSArray arrayWithArray:self.areaData.list[indexPath.row].list];
                [self.midTableView reloadData];
            }
            
            break;
        }
        case 102:
            if (self.midSlectData.selectIndex != indexPath.row) {
                selectedCell = [tableView cellForRowAtIndexPath:indexPath];
                deselectedCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:self.midSlectData.selectIndex inSection:0]];
                selectedCell.cellTitle.textColor = TENHexColor(TENRedColor);
                deselectedCell.cellTitle.textColor = TENHexColor(TENTextBlackColor3);
                self.midDataArr[indexPath.row].isSelected = YES;
                self.midDataArr[self.midSlectData.selectIndex].isSelected = NO;
                self.midSlectData = self.midDataArr[indexPath.row];
                self.midSlectData.selectIndex = indexPath.row;
                //第三个tableview的数据源
                self.rightDataArr = [NSArray arrayWithArray:self.midDataArr[indexPath.row].child];
                [self.rightTableView reloadData];
            }
            
            break;
        case 103:
            if (self.rigntSlectData.selectIndex != indexPath.row){
                selectedCell = [tableView cellForRowAtIndexPath:indexPath];
                deselectedCell = [tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:self.rigntSlectData.selectIndex inSection:0]];
                selectedCell.cellTitle.textColor = TENHexColor(TENRedColor);
                deselectedCell.cellTitle.textColor = TENHexColor(TENTextBlackColor3);
                self.rightDataArr[indexPath.row].isSelected = YES;
                self.rightDataArr[self.rigntSlectData.selectIndex].isSelected = NO;
                self.rigntSlectData = self.rightDataArr[indexPath.row];
                self.rigntSlectData.selectIndex = indexPath.row;
            }
            
            break;
        default:
            break;
    }
}
-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
    AreaTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.cellTitle.textColor = TENHexColor(TENTextBlackColor3);
    
    switch (tableView.tag) {
        case 101:
            self.areaData.list[indexPath.row].isSelected = NO;
            
            break;
        case 102:
            self.midDataArr[indexPath.row].isSelected = NO;
            
            break;
        case 103:
            self.rightDataArr[indexPath.row].isSelected = NO;
            break;
        default:
            break;
    }
}

#pragma mark - Getter&&Setter
-(UIButton *)cancleBtn{
    if (!_cancleBtn) {
        _cancleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_cancleBtn setTitleColor:TENHexColor(TENTextGrayColor6) forState:UIControlStateNormal];
        _cancleBtn.layer.borderWidth = 1;
        _cancleBtn.layer.borderColor = TENHexColor(TENTextGrayColor6).CGColor;
        _cancleBtn.layer.cornerRadius = 4;
        _cancleBtn.clipsToBounds = YES;
        _cancleBtn.titleLabel.font = TENFont15;
        [_cancleBtn setTitle:@"清空" forState:UIControlStateNormal];
        [_cancleBtn addTarget:self action:@selector(cancle) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _cancleBtn;
}
-(UIButton *)sureBtn{
    if (!_sureBtn) {
        _sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_sureBtn setTitleColor:TENHexColor(TENWhiteColor) forState:UIControlStateNormal];
        _sureBtn.layer.borderWidth = 1;
        _sureBtn.layer.borderColor = TENHexColor(TENTextGrayColor6).CGColor;
        _sureBtn.layer.cornerRadius = 4;
        _sureBtn.clipsToBounds = YES;
        _sureBtn.titleLabel.font = TENFont15;
        _sureBtn.backgroundColor = TENHexColor(TENThemeBlueColor);
        [_sureBtn addTarget:self action:@selector(sure) forControlEvents:UIControlEventTouchUpInside];
        [_sureBtn setTitle:@"确定" forState:UIControlStateNormal];
    }
    return _sureBtn;
}
-(UITableView *)leftTableView{
    if (!_leftTableView) {
        _leftTableView = [[TENTableView alloc] init];
        _leftTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        _leftTableView.delegate = self;
        _leftTableView.dataSource = self;
        [_leftTableView registerClass:[AreaTableViewCell class] forCellReuseIdentifier:@"cell"];
        _leftTableView.tag = 101;
    }
    return _leftTableView;
}
-(UITableView *)midTableView{
    if (!_midTableView) {
        _midTableView = [[TENTableView alloc] init];
        _midTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        _midTableView.delegate = self;
        _midTableView.dataSource = self;
        [_midTableView registerClass:[AreaTableViewCell class] forCellReuseIdentifier:@"cell"];
        _midTableView.tag = 102;
    }
    return _midTableView;
}
-(UITableView *)rightTableView{
    if (!_rightTableView) {
        _rightTableView = [[TENTableView alloc] init];
        _rightTableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        _rightTableView.delegate = self;
        _rightTableView.dataSource = self;
        _rightTableView.tag = 103;
        [_rightTableView registerClass:[AreaTableViewCell class] forCellReuseIdentifier:@"cell"];
    }
    return _rightTableView;
}

-(NSArray *)rightDataArr{
    if (!_leftTableView) {
        _rightDataArr = [NSArray array];
    }
    return _rightDataArr;
}
-(NSArray *)midDataArr{
    if (!_midDataArr) {
        _midDataArr = [NSArray array];
    }
    return _midDataArr;
}
-(SearchData *)midSlectData{
    if (!_midSlectData) {
        _midSlectData = [[SearchData alloc] init];
        _midSlectData.selectIndex = 0;
    }
    return _midSlectData;
}
-(SearchData *)leftSlectData{
    if (!_leftSlectData) {
        _leftSlectData = [[SearchData alloc] init];
        _leftSlectData.selectIndex = 0;
    }
    return _leftSlectData;
}
-(SearchData *)rigntSlectData{
    if (!_rigntSlectData) {
        _rigntSlectData = [[SearchData alloc] init];
        _rigntSlectData.selectIndex = 0;
    }
    return _rigntSlectData;
}
/**
 * 需要定义自身高度 (默认 200.f)
 */
- (CGFloat)contentViewHeight:(CGRect)contentBounds; {
    return tableViewHeight;
}

@end

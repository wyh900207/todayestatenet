//
//  MoreHeaderCollectionReusableView.h
//  TodayEstateNet
//
//  Created by 李冲 on 2018/11/5.
//  Copyright © 2018 TEN. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MoreHeaderCollectionReusableView : UICollectionReusableView
@property (nonatomic, strong) UILabel *headerTitle;
@property (nonatomic, strong) UIButton *clearBtn;
@property (nonatomic, copy) void (^clearCondition)(void);
@end

NS_ASSUME_NONNULL_END

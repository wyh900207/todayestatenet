//
//  MoreHeaderCollectionReusableView.m
//  TodayEstateNet
//
//  Created by 李冲 on 2018/11/5.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "MoreHeaderCollectionReusableView.h"
#import <Masonry.h>
#import "TENUtilsHeader.h"
@implementation MoreHeaderCollectionReusableView
-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        [self addSubview:self.headerTitle];
        [self.headerTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(8);
            make.width.offset(70);
            make.height.offset(21);
            make.centerY.mas_equalTo(self.mas_centerY);
        }];
        
        [self addSubview:self.clearBtn];
        [self.clearBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-8);
            make.width.offset(30);
            make.height.offset(17);
            make.centerY.mas_equalTo(self.mas_centerY);
        }];
    
    }
    return self;
}
- (void)clear{
    self.clearCondition();
}
-(UILabel *)headerTitle{
    if (!_headerTitle) {
        _headerTitle = [[UILabel alloc] init];
        _headerTitle.font = TENFont15;
        _headerTitle.textColor = TENHexColor(TENTextBlackColor3);
        _headerTitle.textAlignment = NSTextAlignmentCenter;
                
    }
    return _headerTitle;
}
-(UIButton *)clearBtn{
    if (!_clearBtn) {
        _clearBtn = [[UIButton alloc] init];
        [_clearBtn setTitleColor:TENHexColor(@"999999") forState:UIControlStateNormal];
        _clearBtn.titleLabel.font = TENFont12;
        [_clearBtn setTitle:@"清除" forState:UIControlStateNormal];
        [_clearBtn addTarget:self action:@selector(clear) forControlEvents:UIControlEventTouchUpInside];
    }
    return _clearBtn;
}
@end

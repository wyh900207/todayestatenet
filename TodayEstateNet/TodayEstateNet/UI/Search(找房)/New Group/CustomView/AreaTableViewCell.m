//
//  AreaTableViewCell.m
//  TodayEstateNet
//
//  Created by 李冲 on 2018/10/31.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "AreaTableViewCell.h"
#import "TENUtilsHeader.h"
@interface AreaTableViewCell()

@end
@implementation AreaTableViewCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.cellTitle];
        [self.cellTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0);
            make.left.offset(0);
            make.right.offset(0);
            make.bottom.offset(0);
        }];
    }
    return self;
}

- (void)updateCell{
    
}
-(UILabel *)cellTitle{
    if (!_cellTitle) {
        _cellTitle = [[UILabel alloc]init];
        _cellTitle.font = TENFont14;
        _cellTitle.textColor = TENHexColor(TENTextBlackColor3);
        _cellTitle.textAlignment = NSTextAlignmentCenter;
    }
    return _cellTitle;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

//
//  AreaTableViewCell.h
//  TodayEstateNet
//
//  Created by 李冲 on 2018/10/31.
//  Copyright © 2018 TEN. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AreaTableViewCell : UITableViewCell
@property (nonatomic, strong) UILabel *cellTitle;
@end

NS_ASSUME_NONNULL_END

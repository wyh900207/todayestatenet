//
//  ModelTypeTableViewCell.m
//  TodayEstateNet
//
//  Created by 李冲 on 2018/11/5.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "ModelTypeTableViewCell.h"
#import <Masonry.h>
#import "TENUtilsHeader.h"
@implementation ModelTypeTableViewCell
-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self.contentView addSubview:self.selectImage];
        [self.selectImage mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(self.mas_centerY);
            make.width.offset(15);
            make.height.offset(15);
            make.right.offset(-8);
        }];
        [self.contentView addSubview:self.cellTitle];
        [self.cellTitle mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0);
            make.left.offset(8);
            make.right.mas_equalTo(self.selectImage.mas_left).offset(-8);
            make.bottom.offset(0);
        }];
        
    }
    return self;
}
-(UILabel *)cellTitle{
    if (!_cellTitle) {
        _cellTitle = [[UILabel alloc]init];
        _cellTitle.font = TENFont14;
        _cellTitle.textColor = TENHexColor(TENTextBlackColor3);
        _cellTitle.textAlignment = NSTextAlignmentLeft;
    }
    return _cellTitle;
}
-(UIImageView *)selectImage{
    if (!_selectImage) {
        _selectImage = [[UIImageView alloc] init];
        _selectImage.image = [UIImage imageNamed:@"search_deselected"];
    }
    return _selectImage;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

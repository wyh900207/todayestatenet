//
//  ModelTypeTableViewCell.h
//  TodayEstateNet
//
//  Created by 李冲 on 2018/11/5.
//  Copyright © 2018 TEN. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ModelTypeTableViewCell : UITableViewCell
@property (nonatomic, strong) UIImageView *selectImage;
@property (nonatomic, strong) UILabel *cellTitle;
@property (nonatomic) BOOL hadSelect;
@end

NS_ASSUME_NONNULL_END

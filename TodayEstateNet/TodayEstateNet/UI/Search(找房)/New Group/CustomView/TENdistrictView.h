//
//  TENdistrictView.h
//  TodayEstateNet
//
//  Created by Admin on 2018/9/25.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TENCustomSelectionMenu.h"
#import "SearchData.h"
@class TENdistrictView;

@protocol TENCustomAreaViewDelegate <NSObject>
- (void)districtAndSubwayViewClick:(TENdistrictView *)districtAndSubwayView title:(NSString *)titile;
@end


@interface TENdistrictView : UIView <TENMenuItemContentView>
@property (nonatomic, weak)  id<TENCustomAreaViewDelegate> delegate;
@property (nonatomic, copy) void(^dismissView)(BOOL sure,NSDictionary *condition);

- (void)updateUIWith:(SearchData *)data;
@end

//
//  TENorderByView.m
//  TodayEstateNet
//
//  Created by 李冲 on 2018/11/5.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENorderByView.h"
#import "TENTableView.h"
#import "TENUtilsHeader.h"
#import <Masonry.h>
#import "SearchData.h"
#import "ModelTypeTableViewCell.h"

@interface TENorderByView ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic, strong) TENTableView *tableView;
@property (nonatomic, strong) NSArray <SearchData *>*rightDataArr;

@property (nonatomic, strong) SearchData *searchData;
@end
static const CGFloat tableViewHeight = 500;
@implementation TENorderByView

- (instancetype)init {
    self = [super init];
    if (self) {
        [self addSubview:self.tableView];
        [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0);
            make.left.offset(0);
            make.right.offset(0);
            make.bottom.offset(0);
        }];
        
        
        
    }
    return self;
}
- (void)updateUIWith:(SearchData *)data{
    self.searchData = data;
    [self.tableView reloadData];
    
    
    
}
#pragma mark  - private


#pragma mark - UITableViewDataSource
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.searchData.list.count;

}
-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ModelTypeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    switch (tableView.tag) {
        case 101:
            cell.cellTitle.text = self.searchData.list[indexPath.row].name;
            
            
            break;
        case 102:
            cell.cellTitle.text = self.rightDataArr[indexPath.row].name;
            cell.backgroundColor = TENHexColor(@"F9F9F9");
            cell.cellTitle.textColor = TENHexColor(TENBlackColor);
            break;
            
        default:
            break;
    }
    return cell;
}
//选中tableview的cell
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ModelTypeTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.cellTitle.textColor = TENHexColor(TENRedColor);
    cell.selectImage.image = [UIImage imageNamed:@"search_selected"];
    cell.cellTitle.textColor = TENHexColor(TENRedColor);
    NSDictionary *parameter = @{@"cityIds" : @"310100",
                                @"lat" : @"31.2317",
                                @"lng" : @"121.473",
                                @"pageNo" : @1,
                                @"pageSize" : @10,
                                @"orderBy" :self.searchData.list[indexPath.row].searchId
                                };
    self.dismissView(YES,parameter);
    cell.hadSelect = !cell.hadSelect;
}
-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
        ModelTypeTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.cellTitle.textColor = TENHexColor(TENTextBlackColor3);
        cell.selectImage.image = [UIImage imageNamed:@"search_deselected"];
    
}

#pragma mark - Getter&&Setter

-(TENTableView *)tableView{
    if (!_tableView) {
        _tableView = [[TENTableView alloc] init];
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerClass:[ModelTypeTableViewCell class] forCellReuseIdentifier:@"cell"];
        _tableView.tag = 101;
    }
    return _tableView;
    
}


/**
 * 需要定义自身高度 (默认 200.f)
 */
- (CGFloat)contentViewHeight:(CGRect)contentBounds; {
    return tableViewHeight;
}


@end

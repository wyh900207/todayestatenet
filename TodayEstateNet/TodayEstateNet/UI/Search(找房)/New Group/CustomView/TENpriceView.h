//
//  TENpriceView.h
//  TodayEstateNet
//
//  Created by 李冲 on 2018/11/5.
//  Copyright © 2018 TEN. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SearchData.h"
#import "TENCustomSelectionMenu.h"
NS_ASSUME_NONNULL_BEGIN

@interface TENpriceView : UIView<TENMenuItemContentView>
@property (nonatomic, copy) void(^dismissView)(BOOL sure,NSDictionary *condition);

- (void)updateUIWith:(SearchData *)data;
@end

NS_ASSUME_NONNULL_END

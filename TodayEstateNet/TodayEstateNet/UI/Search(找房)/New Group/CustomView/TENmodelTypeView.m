//
//  TENmodelTypeView.m
//  TodayEstateNet
//
//  Created by 李冲 on 2018/11/5.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENmodelTypeView.h"
#import "TENTableView.h"
#import "TENUtilsHeader.h"
#import <Masonry.h>
#import "SearchData.h"
#import "ModelTypeTableViewCell.h"
@interface TENmodelTypeView ()<UITableViewDelegate,UITableViewDataSource>
//清空取消
@property (nonatomic, strong) UIButton *cancleBtn;
@property (nonatomic, strong) UIButton *sureBtn;
@property (nonatomic, strong) TENTableView *tableView;

@property (nonatomic, strong) SearchData *typeData;
@property (nonatomic, strong) NSMutableArray<SearchData *> *selectDataArr;
@end

static const CGFloat tableViewHeight = 308;
@implementation TENmodelTypeView
- (instancetype)init {
    self = [super init];
    if (self) {
        UIView *btnBgView = [[UIView alloc] init];
        btnBgView.backgroundColor = TENHexColor(TENWhiteColor) ;
        [self addSubview:btnBgView];
        [btnBgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.offset(0);
            make.left.offset(0);
            make.right.offset(0);
            make.height.offset(63);
        }];
        [self addSubview:self.tableView];
        [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0);
            make.left.offset(0);
            make.right.offset(0);
            make.bottom.mas_equalTo(btnBgView.mas_top).with.offset(0);
        }];
        
        [btnBgView addSubview:self.sureBtn];
        [self.sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(btnBgView.mas_centerY);
            make.centerX.mas_equalTo(btnBgView.mas_centerX).multipliedBy(1.5);
            make.right.offset(-20);
            make.left.offset(20);
            make.height.offset(30);
        }];
        
        UIView *lineView = [[UIView alloc] init];
        lineView.backgroundColor = TENHexColor(TENSpaceViewColor);
        [btnBgView addSubview:lineView];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0);
            make.right.offset(0);
            make.top.offset(0);
            make.height.offset(1);
        }];
    }
    return self;
}
- (void)updateUIWith:(SearchData *)data{
    self.typeData = data;
    self.typeData.list.firstObject.isSelected = YES;
    [self.tableView reloadData];
}
#pragma mark  - private
- (void)sure{
    NSMutableArray *temArr = [NSMutableArray array] ;
    for (SearchData *tem in self.selectDataArr) {
       [temArr addObject:tem.name];
    }
    NSString *str = [temArr componentsJoinedByString:@","];
    NSDictionary *parameter = @{
                                @"cityIds" : @"310100",
                                @"lat" : @"31.2317",
                                @"lng" : @"121.473",
                                @"pageNo" : @1,
                                @"pageSize" : @10,
                                @"moduleType":str
                                };
    self.dismissView(YES,parameter);
}

#pragma mark - UITableViewDataSource
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 50;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.typeData.list.count;
}

-(UITableViewCell*)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ModelTypeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.cellTitle.text = self.typeData.list[indexPath.row].name;
    cell.cellTitle.textColor = TENHexColor(TENBlackColor);
    if (self.typeData.list[indexPath.row].isSelected) {
        cell.cellTitle.textColor = TENHexColor(TENRedColor);
        cell.selectImage.image = [UIImage imageNamed:@"search_selected"];
    }else{
        cell.cellTitle.textColor = TENHexColor(TENBlackColor);
        cell.selectImage.image = [UIImage imageNamed:@"search_deselected"];
    }
    return cell;
}
//选中tableview的cell
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    ModelTypeTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.cellTitle.textColor = TENHexColor(TENRedColor);

    if (self.typeData.list[indexPath.row].isSelected) {
        cell.selectImage.image = [UIImage imageNamed:@"search_deselected"];
        cell.cellTitle.textColor = TENHexColor(TENTextBlackColor3);
        [self.selectDataArr removeObject:self.typeData.list[indexPath.row]];
    }else{
        cell.selectImage.image = [UIImage imageNamed:@"search_selected"];
        cell.cellTitle.textColor = TENHexColor(TENRedColor);
        [self.selectDataArr addObject:self.typeData.list[indexPath.row]];
    }
    BOOL tem = self.typeData.list[indexPath.row].isSelected;
    self.typeData.list[indexPath.row].isSelected = !tem;
}
-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath{
//    ModelTypeTableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
//    cell.cellTitle.textColor = TENHexColor(TENTextBlackColor3);
    
}

#pragma mark - Getter&&Setter

-(UIButton *)sureBtn{
    if (!_sureBtn) {
        _sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_sureBtn setTitleColor:TENHexColor(TENWhiteColor) forState:UIControlStateNormal];
        _sureBtn.layer.borderWidth = 1;
        _sureBtn.layer.borderColor = TENHexColor(TENTextGrayColor6).CGColor;
        _sureBtn.layer.cornerRadius = 4;
        _sureBtn.clipsToBounds = YES;
        _sureBtn.titleLabel.font = TENFont15;
        _sureBtn.backgroundColor = TENHexColor(TENThemeBlueColor);
        [_sureBtn addTarget:self action:@selector(sure) forControlEvents:UIControlEventTouchUpInside];
        [_sureBtn setTitle:@"确定" forState:UIControlStateNormal];
    }
    return _sureBtn;
}
-(TENTableView *)tableView{
    if (!_tableView) {
        _tableView = [[TENTableView alloc] init];
        _tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        [_tableView registerClass:[ModelTypeTableViewCell class] forCellReuseIdentifier:@"cell"];
        _tableView.tag = 101;
    }
    return _tableView;

}
-(NSMutableArray<SearchData *> *)selectDataArr{
    if (!_selectDataArr) {
        _selectDataArr = [NSMutableArray array];
        [_selectDataArr addObject:self.typeData.list.firstObject];
    }
    return _selectDataArr;
}

/**
 * 需要定义自身高度 (默认 200.f)
 */
- (CGFloat)contentViewHeight:(CGRect)contentBounds; {
    return tableViewHeight;
}

@end

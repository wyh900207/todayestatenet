//
//  TENMoreView.m
//  TodayEstateNet
//
//  Created by 李冲 on 2018/11/5.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENMoreView.h"
#import "TENCollectionVIew.h"
#import "TENUtilsHeader.h"
#import <Masonry.h>
#import "SearchData.h"
#import "ModelTypeTableViewCell.h"
#import "MoreCollectionViewCell.h"
#import "MoreHeaderCollectionReusableView.h"
@interface TENMoreView ()<UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
@property (nonatomic, strong) UIButton *cancleBtn;
@property (nonatomic, strong) UIButton *sureBtn;

@property (nonatomic, strong) TENCollectionVIew *collectionView;
@property (nonatomic, strong) NSArray <SearchData *>*rightDataArr;

@property (nonatomic, strong) SearchData *moreData;
@property (nonatomic, strong) NSMutableArray <SearchData *>*selectArrData;
@property (nonatomic, strong) NSIndexPath *selectIndexPath;
@end
static const CGFloat tableViewHeight = 508;

@implementation TENMoreView

- (instancetype)init {
    self = [super init];
    if (self) {
        
        UIView *btnBgView = [[UIView alloc] init];
        btnBgView.backgroundColor = TENHexColor(TENWhiteColor) ;
        [self addSubview:btnBgView];
        [btnBgView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.offset(0);
            make.left.offset(0);
            make.right.offset(0);
            make.height.offset(63);
        }];
        [self addSubview:self.collectionView];
        [self.collectionView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.offset(0);
            make.left.offset(0);
            make.right.offset(0);
            make.bottom.mas_equalTo(btnBgView.mas_top).with.offset(0);
        }];
        
        [btnBgView addSubview:self.cancleBtn];
        [self.cancleBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(btnBgView.mas_centerY);
            make.centerX.mas_equalTo(btnBgView.mas_centerX).multipliedBy(0.5);
            make.left.offset(20);
            make.width.offset((TENScreenWidth-50)/2);
            make.height.offset(30);
        }];
        
        [btnBgView addSubview:self.sureBtn];
        [self.sureBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerY.mas_equalTo(btnBgView.mas_centerY);
            make.centerX.mas_equalTo(btnBgView.mas_centerX).multipliedBy(1.5);
            make.right.offset(-20);
            make.width.offset((TENScreenWidth-50)/2);
            make.height.offset(30);
        }];
        
        UIView *lineView = [[UIView alloc] init];
        lineView.backgroundColor = TENHexColor(TENSpaceViewColor);
        [btnBgView addSubview:lineView];
        [lineView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.offset(0);
            make.right.offset(0);
            make.top.offset(0);
            make.height.offset(1);
        }];
    }
    return self;
}
- (void)updateUIWith:(SearchData *)data{
    self.moreData = data;
    [self.collectionView reloadData];
    
    
    
}
#pragma mark  - private
- (void)sure{
    if (self.selectArrData.count > 0) {
        NSMutableArray *temArr = [NSMutableArray array];
        for (SearchData *data in self.selectArrData) {
            [temArr addObject:data.searchId];
        }
        NSString *temStr = [temArr componentsJoinedByString:@","];
        NSDictionary *parameter = @{@"cityIds" : @"310100",
                                    @"lat" : @"31.2317",
                                    @"lng" : @"121.473",
                                    @"pageNo" : @1,
                                    @"pageSize" : @10,
                                    @"moreIds" :temStr
                                    };
        self.dismissView(YES,parameter);
    }else{
        [NSObject showHudTipStr:@"请选择筛选条件"];
    }
    
}
- (void)cancle{
    self.dismissView(NO,nil);
}
#pragma mark - UICollectionViewDelegate  &&  UICollectionViewDataSource
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView{
    return self.moreData.list.count;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.moreData.list[section].list.count;
}

- (__kindof UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    MoreCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    cell.cellTitle.text = self.moreData.list[indexPath.section].list[indexPath.row].name;
    if (self.moreData.list[indexPath.section].list[indexPath.row].isSelected) {
        cell.cellTitle.backgroundColor = TENHexColor(TENThemeBlueColor);
        cell.cellTitle.textColor = TENHexColor(TENWhiteColor);
    }else{
        cell.cellTitle.backgroundColor = TENHexColor(@"F0F0F0");
        cell.cellTitle.textColor = TENHexColor(@"999999");
    }
    return cell;
}
- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    MoreHeaderCollectionReusableView *header = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"header" forIndexPath:indexPath];
    header.headerTitle.text = self.moreData.list[indexPath.section].name;
    //清除筛选条件
    __weak typeof(self) weakSelf = self;
    header.clearCondition = ^{
        for (SearchData *data in self.moreData.list[indexPath.section].list) {
            if (data.isSelected) {
                [weakSelf.selectArrData removeObject:data];
            }
            data.isSelected = NO;
            
        }
        [weakSelf.collectionView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section]];
    };
    return header;
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
   
    MoreCollectionViewCell *selectedCell = (MoreCollectionViewCell *) [collectionView cellForItemAtIndexPath:indexPath];
    
    if (self.moreData.list[indexPath.section].list[indexPath.row].isSelected ) {
        selectedCell.cellTitle.backgroundColor = TENHexColor(@"F0F0F0");
        selectedCell.cellTitle.textColor = TENHexColor(@"999999");
        [self.selectArrData removeObject:self.moreData.list[indexPath.section].list[indexPath.row]];
        self.moreData.list[indexPath.section].list[indexPath.row].isSelected = NO;
    }else{
        selectedCell.cellTitle.backgroundColor = TENHexColor(TENThemeBlueColor);
        selectedCell.cellTitle.textColor = TENHexColor(TENWhiteColor);
        self.moreData.list[indexPath.section].list[indexPath.row].isSelected = YES;
    }
    
    
    [self.selectArrData addObject:self.moreData.list[indexPath.section].list[indexPath.row]];
    

}


#pragma mark - Getter&&Setter

-(UIButton *)sureBtn{
    if (!_sureBtn) {
        _sureBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_sureBtn setTitleColor:TENHexColor(TENWhiteColor) forState:UIControlStateNormal];
        _sureBtn.layer.borderWidth = 1;
        _sureBtn.layer.borderColor = TENHexColor(TENTextGrayColor6).CGColor;
        _sureBtn.layer.cornerRadius = 4;
        _sureBtn.clipsToBounds = YES;
        _sureBtn.titleLabel.font = TENFont15;
        _sureBtn.backgroundColor = TENHexColor(TENThemeBlueColor);
        [_sureBtn addTarget:self action:@selector(sure) forControlEvents:UIControlEventTouchUpInside];
        [_sureBtn setTitle:@"确定" forState:UIControlStateNormal];
    }
    return _sureBtn;
}
-(UIButton *)cancleBtn{
    if (!_cancleBtn) {
        _cancleBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_cancleBtn setTitleColor:TENHexColor(TENTextGrayColor6) forState:UIControlStateNormal];
        _cancleBtn.layer.borderWidth = 1;
        _cancleBtn.layer.borderColor = TENHexColor(TENTextGrayColor6).CGColor;
        _cancleBtn.layer.cornerRadius = 4;
        _cancleBtn.clipsToBounds = YES;
        _cancleBtn.titleLabel.font = TENFont15;
        [_cancleBtn setTitle:@"清空" forState:UIControlStateNormal];
        [_cancleBtn addTarget:self action:@selector(cancle) forControlEvents:UIControlEventTouchUpInside];
        
    }
    return _cancleBtn;
}
-(TENCollectionVIew *)collectionView{
    if (!_collectionView) {
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        layout.itemSize = CGSizeMake(80, 30);
        layout.minimumLineSpacing = (TENScreenWidth-(80*4))/5;
        layout.minimumInteritemSpacing = 8;
        layout.sectionInset = UIEdgeInsetsMake(8, (TENScreenWidth-(80*4))/5, 8, (TENScreenWidth-(80*4))/5);
        layout.headerReferenceSize = CGSizeMake(TENScreenWidth, 35);
        _collectionView = [[TENCollectionVIew alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
        _collectionView.backgroundColor = TENHexColor(TENWhiteColor);
        _collectionView.delegate = self;
        _collectionView.dataSource = self;
        [_collectionView registerClass:[MoreCollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
        [_collectionView registerClass:[MoreHeaderCollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"header"];
    }
    return _collectionView;
}
-(NSMutableArray<SearchData *> *)selectArrData{
    if (!_selectArrData) {
        _selectArrData = [NSMutableArray array];
    }
    return _selectArrData;
}

/**
 * 需要定义自身高度 (默认 200.f)
 */
- (CGFloat)contentViewHeight:(CGRect)contentBounds; {
    return tableViewHeight;
}


@end

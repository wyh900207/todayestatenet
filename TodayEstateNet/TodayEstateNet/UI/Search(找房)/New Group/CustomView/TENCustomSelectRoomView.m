//
//  TENCustomSelectRoomView.m
//  TodayEstateNet
//
//  Created by Admin on 2018/9/25.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENCustomSelectRoomView.h"
#import "TENUtilsHeader.h"
@interface TENCustomSelectRoomView()<UITableViewDelegate,UITableViewDataSource>
/** 数据 */
@property (nonatomic, strong) NSArray *dataArray;
/** 选中数组 */
@property (nonatomic, strong) NSMutableArray *selectArray;
@property (weak, nonatomic) IBOutlet UIButton *submitBtn;

@end


@implementation TENCustomSelectRoomView

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupSubviews];
}
-(void)setupSubviews
{
    self.submitBtn.backgroundColor = [UIColor colorWithHexString:TENThemeBlueColor];
    [self.submitBtn setTitleColor:[UIColor whiteColor] forState:0];
}
#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *identifier = @"roomCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    if (!cell) {
        cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifier];
    }
    cell.textLabel.text = self.dataArray[indexPath.row];
    cell.textLabel.font = [UIFont systemFontOfSize:16];
    
    NSNumber *num = self.selectArray[indexPath.row];
    NSInteger integer = [num integerValue];
    
    if (integer == 0) {
        cell.textLabel.textColor = LJColor(0, 0, 0, 1);
        if (indexPath.row != 0) {
            cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkbox"]];
        }
    }
    else {
        cell.textLabel.textColor = LJColor(0.22, 0.67, 0.42, 1);
        if (indexPath.row != 0) {
            cell.accessoryView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"checkbox-active"]];
        }
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}
#pragma mark - UITableViewDelegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (indexPath.row == 0) {
        self.selectArray[0] = @(1);
        for (int i = 0; i < self.selectArray.count; ++i) {
            if (i != 0) {
                self.selectArray[i] = @(0);
            }
        }
    }
    else {
        self.selectArray[0] = @(0);
        
        NSNumber *num = self.selectArray[indexPath.row];
        NSInteger integer = [num integerValue];
        
        if (integer == 0) {
            integer = 1;
        }
        else {
            integer = 0;
        }
        
        self.selectArray[indexPath.row] = [NSNumber numberWithInteger:integer];
    }
    
    [tableView reloadData];
}

#pragma mark - Lazy
- (NSArray *)dataArray {
    if (_dataArray == nil) {
        _dataArray = @[@"不限",@"1室",@"2室",@"3室",@"4室",@"5室",@"5室以上"];
    }
    return _dataArray;
}

- (NSMutableArray *)selectArray {
    if (_selectArray == nil) {
        _selectArray = [NSMutableArray array];
        NSArray *array = @[@0,@0,@0,@0,@0,@0,@0];
        _selectArray.array = array;
    }
    return _selectArray;
}


/**
 * 需要定义自身高度 (默认 200.f)
 */
- (CGFloat)contentViewHeight:(CGRect)contentBounds; {
    return 44.f * 8;
}

@end

//
//  TENSearchDepartmentViewController.m
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/9/11.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENSearchDepartmentViewController.h"
#import "TENUtilsHeader.h"
#import "TENHomeNormalSallingCell.h"
#import "TENHomeSearchView.h"
#import "TENSearchViewController.h"
#import "TENCustomSelectionMenu.h"
#import "TENCustomSelectRoomView.h"
#import "TENdistrictView.h"
#import "TENSearchPageRequest.h"
#import "SearchData.h"
#import <MJExtension.h>

#import "TENpriceView.h"
#import "TENmodelTypeView.h"
#import "TENMoreView.h"
#import "TENorderByView.h"
#import "SearchDetail.h"
#import "TENTableView.h"

#import "TENHouseDetailViewController.h"

//导航栏按钮控制器
#import "TENSearchCityViewController.h"
#import "TENCityModel.h"
@interface TENSearchDepartmentViewController () <UITableViewDelegate, UITableViewDataSource,YMECTableViewRefreshProtocol,TENChooseCityDelegate>

@property (nonatomic, strong) TENTableView *tableview;
@property (nonatomic, strong) ImageLeftButton *leftButton;
@property (nonatomic, strong) NSMutableArray *dataArr;
@property (nonatomic, strong) TENCustomSelectionMenu *menuView;
@property (nonatomic, strong) NSMutableArray <SearchDetail *>*detaiDataArr;
//默认参数
@property (nonatomic, strong) NSDictionary *condition;
@end

@implementation TENSearchDepartmentViewController

#pragma mark - Life cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [self initUI];
    [self locationNow];
    [self requestDataWithCityId:@"310100"];
}

- (void)locationNow {
    @weakify(self)
    [[TENLocationManager share] locationJustOneceTimeWith:^(CLLocation *location, BMKLocationReGeocode *rgcData) {
        NSLog(@"location:%@",location);
        NSLog(@"rgcData:%@",rgcData);
        @strongtify(self)
        // FIXME: 没有拿到数据
        [self.leftButton setTitle:rgcData.city forState:0];
        //        [self displayShanghaiCity];
    } faild:^(NSError *error) {
        @strongtify(self)
        [self displayShanghaiCity];
    }];
}

- (void)displayShanghaiCity {
    [self.leftButton setTitle:[TENLocationManager share].cityName forState:0];
}
#pragma mark - UI

- (void)initUI{
    [self customNaivgationBar];
    [self.view addSubview:self.tableview];
}

- (void)setupSubviews {
    NSMutableArray *items = [NSMutableArray array];
    
    //区域
    TENMenuItem *areaItem = [[TENMenuItem alloc] init];
    areaItem.title = @"区域";
    TENdistrictView *areaView = [[TENdistrictView alloc] init];
    __weak typeof(self) weakSelf = self;
    areaView.dismissView = ^(BOOL sure, NSDictionary *condition) {
        [weakSelf.menuView dismissAll:self.menuView.backgroundView];
        if (sure) {
            //请求筛选
            [self findWithCondition:condition];
        }
    };
    
    [areaView updateUIWith:self.dataArr[0]];
    areaItem.contentView = areaView;
    [items addObject:areaItem];
    //价格
    TENMenuItem *priceItem = [[TENMenuItem alloc] init];
    priceItem.title = @"价格";
    TENpriceView *priceView = [[TENpriceView alloc] init];
    priceView.dismissView = ^(BOOL sure,NSDictionary *condition) {
        [weakSelf.menuView dismissAll:self.menuView.backgroundView];
        if (sure) {
            [self findWithCondition:condition];
        }
    };
    [priceView updateUIWith:_dataArr[1]];
    priceItem.contentView = priceView;
    [items addObject:priceItem];
    //户型
    TENMenuItem *typeItem = [[TENMenuItem alloc] init];
    typeItem.title = @"户型";
    TENmodelTypeView *typeView = [[TENmodelTypeView alloc] init];
    typeView.dismissView = ^(BOOL sure,NSDictionary *conditon) {
        [weakSelf.menuView dismissAll:self.menuView.backgroundView];
        if (sure) {
            [self findWithCondition:conditon];
        }
    };
    [typeView updateUIWith:_dataArr[2]];
    typeItem.contentView = typeView;
    [items addObject:typeItem];
    //更多
    TENMenuItem *moreItem = [[TENMenuItem alloc] init];
    moreItem.title = @"更多";
    TENMoreView *moreView = [[TENMoreView alloc] init];
    moreView.dismissView = ^(BOOL sure,NSDictionary *condition) {
        [weakSelf.menuView dismissAll:self.menuView.backgroundView ];
        if (sure) {
            [self findWithCondition:condition];
        }
    };
    [moreView updateUIWith:_dataArr[3]];
    moreItem.contentView = moreView;
    [items addObject:moreItem];
    //排序
    TENMenuItem *sortItem = [[TENMenuItem alloc] init];
    sortItem.title = @"排序";
    TENorderByView *sortView = [[TENorderByView alloc] init];
    sortView.dismissView = ^(BOOL sure, NSDictionary * _Nonnull condition) {
        [weakSelf.menuView dismissAll:self.menuView.backgroundView ];
        if (sure) {
            [self findWithCondition:condition];
        }
    };
    [sortView updateUIWith:_dataArr[4]];
    sortItem.contentView = sortView;
    [items addObject:sortItem];

    self.menuView = [[TENCustomSelectionMenu alloc] initWithItems:items];
    
    [self.view addSubview:self.menuView];
    
    [self.menuView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(self.mas_topLayoutGuide);
        make.height.equalTo(@(44.f));
    }];
    
    [self.tableview mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.bottom.right.equalTo(self.view);
        make.top.equalTo(self.mas_topLayoutGuide).offset(44.f);
    }];
}
#pragma mark - Request
//请求排序数据
- (void)requestDataWithCityId:(NSString *)cityId{

    //请求筛选条件
    [TENSearchPageRequest requestSearchPageDataWithParameters:@{@"cityIds":cityId,@"lat":[TENLocationManager share].latitude,@"lng":[TENLocationManager share].longitude} success:^(NSDictionary * _Nonnull responseObject) {
        NSString *code = responseObject[@"code"];
        if ([code integerValue] == 1) {
            self.dataArr = [NSMutableArray array];
            for (NSDictionary *dict in responseObject[@"content"]) {
                SearchData *tem = [SearchData mj_objectWithKeyValues:dict];
                [self.dataArr addObject:tem];
            }
            [self setupSubviews];
            //默认数据
            [self findWithCondition:self.condition];
        }
    } failure:^(NSError * _Nonnull error) {
        
    }];

   
    
}
//根据条件筛选
- (void)findWithCondition:(NSDictionary *)condition{
//    self.tableview
    self.condition = condition;
    [TENSearchPageRequest requestFindDataWithCondition:condition success:^(NSDictionary * _Nonnull responseObject) {
        NSLog(@"筛选结果：%@",responseObject);
        NSString *code = responseObject[@"code"];
        if ([code integerValue] == 1) {
            self.detaiDataArr = [NSMutableArray array];
            for (NSDictionary *dic in responseObject
                 [@"content"]) {
                SearchDetail *tem = [SearchDetail mj_objectWithKeyValues:dic];
                [self.detaiDataArr addObject:tem];
            }
            [self.tableview endHeaderRefresh];
        }
        [self.tableview reloadData];
    } failure:^(NSError * _Nonnull error) {
        [self.tableview endHeaderRefresh];
    }];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = NO;
}

- (void)customNaivgationBar {
    [self.navigationController.navigationBar DJSetBackgroundColor:[UIColor colorWithHexString:TENThemeBlueColor]];
    
    self.leftButton = [[ImageLeftButton alloc] initWithLeft];
    self.leftButton.frame = CGRectMake(0, 0, 65, 44);
    [self.leftButton setImage:TENImage(@"arrow_down") forState:UIControlStateNormal];
    self.leftButton.titleLabel.font = [UIFont systemFontOfSize:17.0];
    [self.leftButton setTitle:[TENLocationManager share].cityName forState:0];
    [self.leftButton setTitleColor:[UIColor colorWithHexString:TENWhiteColor] forState:0];
    [self.leftButton addTarget:self action:@selector(reSelectCity:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:self.leftButton];
    
    ImageLeftButton *rightButton = [[ImageLeftButton alloc] initWithLeft];
    rightButton.frame = CGRectMake(0, 0, 65, 44);
    [rightButton setImage:TENImage(@"home_map") forState:UIControlStateNormal];
    [rightButton setTitle:@"地图" forState:0];
    [rightButton setTitleColor:[UIColor colorWithHexString:TENWhiteColor] forState:0];
    [rightButton addTarget:self action:@selector(reSelectCity:) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:rightButton];
    
    TENHomeSearchView *searchView = [TENHomeSearchView new];
    //WS(weakSelf)
    @weakify(self);
    searchView.searchHandle = ^{
        @strongtify(self);
        TEN_LOG(@"push to search controller")
        TENSearchViewController *searchViewController = [[TENSearchViewController alloc]init];
        [self.navigationController pushViewController:searchViewController animated:YES];
    };
    //searchView.frame = CGRectMake(0, 0, 300,26);
    searchView.backgroundColor = [UIColor colorWithHexString:TENWhiteColor];
    searchView.layer.cornerRadius = 13;
    searchView.layer.masksToBounds = YES;
    self.navigationItem.titleView = searchView;
}

#pragma mark - Private action

- (void)reSelectCity:(UIButton *)button {
    TENSearchCityViewController *cityVC = [[TENSearchCityViewController alloc]init];
    [cityVC setDelegate:self];
    UINavigationController *nav = [[UINavigationController alloc]initWithRootViewController:cityVC];
    [self presentViewController:nav animated:YES completion:nil];
}
#pragma mark - GYZCityPickerDelegate
- (void)cityPickerController:(TENSearchCityViewController *)chooseCityController didSelectCity:(TENCityModel *)city
{
    [self.leftButton setTitle:city.cityName forState:0];
    [chooseCityController dismissViewControllerAnimated:YES completion:^{
        
    }];
}
#pragma mark - 下拉刷新
- (void)headerWillRefresh{
    [self findWithCondition:self.condition];
}

#pragma mark - UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.detaiDataArr.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TENHomeNormalSallingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"search" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.data = self.detaiDataArr[indexPath.row];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    TENHouseDetailViewController *vc = [TENHouseDetailViewController new];
    vc.houseId = self.detaiDataArr[indexPath.row].detailId;
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - Getter

- (UITableView *)tableview {
    if (!_tableview) {
        _tableview = [[TENTableView alloc]initWithFrame:CGRectMake(0, 46, TENScreenWidth, TENScreenHeight - 46) style:UITableViewStylePlain];
        _tableview.refreshType = TENTableViewRefreshTypeHeader;
        _tableview.refreshDelegate = self;
        _tableview.scrollEnabled = YES;
        _tableview.estimatedRowHeight = 80.f;
        _tableview.rowHeight = UITableViewAutomaticDimension;
        _tableview.backgroundColor = TENHexColor(TENWhiteColor);
        _tableview.separatorColor = TENHexClearColor;
        _tableview.delegate = self;
        _tableview.dataSource = self;
        [_tableview registerNib:[UINib nibWithNibName:@"TENHomeNormalSallingCell" bundle:nil] forCellReuseIdentifier:@"search"];
    }
    return _tableview;
}

-(NSMutableArray *)detaiDataArr{
    if (!_detaiDataArr) {
        _detaiDataArr = [NSMutableArray array];
    }
    return _detaiDataArr;
}

- (NSDictionary *)condition{
    if (!_condition) {
        TENLocationManager *locatoin = [TENLocationManager share];
        _condition = @{
                       @"cityIds" : locatoin.cityCode,
                       @"lat" : locatoin.latitude.stringValue,
                       @"lng" : locatoin.longitude.stringValue,
                       @"pageNo" : @1,
                       @"pageSize" : @20
                       };
    }
    return _condition;
}

@end

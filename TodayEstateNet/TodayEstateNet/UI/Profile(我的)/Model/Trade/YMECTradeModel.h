//
//  YMECTradeModel.h
//  YMEconomicCircle
//
//  Created by HomerLynn on 2018/6/8.
//  Copyright © 2018年 YME. All rights reserved.
//

#import <Foundation/Foundation.h>
@class YMECTradeDetailModel;

@interface YMECTradeModel : NSObject

@property (strong, nonatomic) NSMutableArray<YMECTradeDetailModel *> * payments;

@end


@interface YMECTradeDetailModel : NSObject

//id = 4;
//payMoney = 168;
//payTime = 1527585128000;
//paymentName = "\U5145\U503c";
//type = 1;
@property (copy, nonatomic) NSString * tradeId;
@property (copy, nonatomic) NSString * payTime;
@property (copy, nonatomic) NSString * paymentName;
@property (strong, nonatomic) NSNumber * payMoney;
@property (strong, nonatomic) NSNumber * type;

@end

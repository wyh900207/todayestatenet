//
//  YMECTradeModel.m
//  YMEconomicCircle
//
//  Created by HomerLynn on 2018/6/8.
//  Copyright © 2018年 YME. All rights reserved.
//

#import "YMECTradeModel.h"

@implementation YMECTradeModel

+ (NSDictionary *)mj_objectClassInArray {
    return @{@"payments": @"YMECTradeDetailModel"};
}


@end

@implementation YMECTradeDetailModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return  @{@"tradeId": @"id"};
}

@end

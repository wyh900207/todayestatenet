//
//  YMECOrderModel.m
//  YMEconomicCircle
//
//  Created by HomerLynn on 2018/6/8.
//  Copyright © 2018年 YME. All rights reserved.
//

#import "YMECOrderModel.h"

@implementation YMECOrderModel

+ (NSDictionary *)mj_objectClassInArray {
    return @{@"content": @"YMECOrderDetailModel"};
}

@end

@implementation YMECOrderDetailModel

+ (NSDictionary *)mj_replacedKeyFromPropertyName {
    return @{@"order_id": @"id"};
}

@end

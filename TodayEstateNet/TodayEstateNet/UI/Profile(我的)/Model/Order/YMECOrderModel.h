//
//  YMECOrderModel.h
//  YMEconomicCircle
//
//  Created by HomerLynn on 2018/6/8.
//  Copyright © 2018年 YME. All rights reserved.
//

#import <Foundation/Foundation.h>
@class YMECOrderDetailModel;

@interface YMECOrderModel : NSObject

@property (strong, nonatomic) NSMutableArray<YMECOrderDetailModel *> * content;

@end


@interface YMECOrderDetailModel : NSObject

//createTime = 1528095820000;
//goodsCost = 39;
//goodsCover = "http://img.ymcaijing.com/manage/1527745400210.png";
//goodsId = 37;
//goodsName = "\U4e00\U7c73\U8d22\U5708\U5927\U7fa41";
//goodsType = 1;
//id = 49;
//mobileNumber = 12345678900;
//orderNo = 2220180604150340013017;
//orderStatus = 1;
//payTime = "<null>";
//payType = ZFB;
//userAccountId = 22;
//userName = "3728Homer\U00b7Lynn";
@property (copy, nonatomic) NSString * createTime;
@property (copy, nonatomic) NSString * goodsCover;
@property (copy, nonatomic) NSString * goodsId;
@property (copy, nonatomic) NSString * goodsName;
@property (copy, nonatomic) NSString * order_id;
@property (copy, nonatomic) NSString * mobileNumber;
@property (copy, nonatomic) NSString * orderNo;
@property (copy, nonatomic) NSString * payTime;
@property (copy, nonatomic) NSString * payType;
@property (copy, nonatomic) NSString * userAccountId;
@property (copy, nonatomic) NSString * userName;
@property (copy, nonatomic) NSString * goodsTypeName;
@property (strong, nonatomic) NSNumber * goodsCost;
@property (strong, nonatomic) NSNumber * goodsType;
@property (strong, nonatomic) NSNumber * orderStatus;

@end

//
//  TENProfileSectionOneCell.h
//  TodayEstateNet
//
//  Created by  HomerLynn on 2018/12/2.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENTableViewCell.h"

@protocol FuncBtnClickedDelegate <NSObject>

- (void)signIn;
- (void)inviteFriend;

@end

@interface TENProfileSectionOneCell : TENTableViewCell

@property (weak, nonatomic) IBOutlet UIButton *signInButton;
@property (weak, nonatomic) IBOutlet UIButton *inviteButton;

@property (nonatomic, assign) id<FuncBtnClickedDelegate> delegate;

@end

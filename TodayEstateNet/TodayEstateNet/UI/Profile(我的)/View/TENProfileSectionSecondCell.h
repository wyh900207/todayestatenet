//
//  TENProfileSectionSecondCell.h
//  TodayEstateNet
//
//  Created by  HomerLynn on 2018/12/2.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENTableViewCell.h"

@interface TENProfileSectionSecondCell : TENTableViewCell

@property (nonatomic, copy) void (^buttonHandler)(NSInteger index);

@end

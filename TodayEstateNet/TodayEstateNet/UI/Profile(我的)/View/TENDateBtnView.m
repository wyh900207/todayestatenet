//
//  TENDateBtnView.m
//  HouseEconomics
//
//  Created by apple on 2019/2/21.
//  Copyright © 2019  HomerLynn. All rights reserved.
//

#import "TENDateBtnView.h"

@interface TENDateBtnView ()

@end

@implementation TENDateBtnView

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupSubviews];
    }
    return self;
}

- (void)setupSubviews{
    self.dateLabel = [UILabel new];
    self.dateLabel.font = [UIFont boldSystemFontOfSize:15];
    self.dateLabel.textAlignment = NSTextAlignmentCenter;
    
    [self addSubview:self.dateLabel];
    [self.dateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self);
        make.height.equalTo(self).multipliedBy(0.5);
    }];
    self.cnDateLabel = [UILabel new];
    self.cnDateLabel.textAlignment = NSTextAlignmentCenter;
    self.cnDateLabel.font = TENFont10;
    [self addSubview:self.cnDateLabel];
    [self.cnDateLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.bottom.equalTo(self);
        make.top.equalTo(self.dateLabel.mas_bottom);
    }];
}

@end

//
//  TENProfileToosCell.m
//  TodayEstateNet
//
//  Created by  HomerLynn on 2018/12/2.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENProfileToosCell.h"
#import "TENUtilsHeader.h"
#import "TENHomeIconCollectionViewCell.h"

@interface TENProfileToosCell () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) NSArray *toolsArray;
    @property (weak, nonatomic) IBOutlet UILabel *toolsTitleLabel;
    
@end

@implementation TENProfileToosCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self.collectionView registerNib:[UINib nibWithNibName:@"TENHomeIconCollectionViewCell" bundle:nil] forCellWithReuseIdentifier:@"toolsCell"];
    self.collectionView.delegate = self;
    self.collectionView.dataSource = self;
    // 标题加粗
    self.toolsTitleLabel.font = TENBoldFont15;
}

#pragma mark - UI

- (void)setupSubviews {
    
}

#pragma mark - UICollectionViewDataSource
    
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.toolsArray.count;
}
    
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    TENHomeIconCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"toolsCell" forIndexPath:indexPath];
    
    cell.itemObject = self.toolsArray[indexPath.row];
    
    return cell;
}
    
#pragma mark - UICollectionViewDelegate
    
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.itemClickHandler) self.itemClickHandler(indexPath.row);
}

#pragma mark - UICollectionViewDelegateFlowLayout
    
- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat itemWidth = TENScreenWidth * 0.25;
    CGSize size = CGSizeMake(itemWidth, itemWidth);
    
    return size;
}
    
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}
- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return 0;
}
    
#pragma mark - Getter
    
- (NSArray *)toolsArray {
    if (!_toolsArray) {
        _toolsArray = @[@{@"iconName": @"房贷计算器", @"iconUrl": @"ten-tools-loancal"},
                        @{@"iconName": @"税费计算器", @"iconUrl": @"ten-tools-shuical"},
                        @{@"iconName": @"购房资格测试", @"iconUrl": @"ten-tools-buytest"},
                        @{@"iconName": @"百问百科", @"iconUrl": @"ten-tools-baike"},
                        @{@"iconName": @"意向找房", @"iconUrl": @"ten-tools-yixiang"},
                        ];
    }
    return _toolsArray;
}

@end

//
//  TENProfileSectionSecondCell.m
//  TodayEstateNet
//
//  Created by  HomerLynn on 2018/12/2.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENProfileSectionSecondCell.h"

@interface TENProfileSectionSecondCell ()

@property (weak, nonatomic) IBOutlet UIButton *walletButton;
@property (weak, nonatomic) IBOutlet UIButton *saleButton;
@property (weak, nonatomic) IBOutlet UIButton *qunayiButton;
@property (weak, nonatomic) IBOutlet UIButton *likesButton;
@property (weak, nonatomic) IBOutlet UIButton *orderButton;
@property (weak, nonatomic) IBOutlet UIButton *historyButton;

@end

@implementation TENProfileSectionSecondCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupSubviews];
}

#pragma mark - UI

- (void)setupSubviews {
    [self.walletButton layoutButtonWithEdgeInsetsStyle:TENButtonEdgeInsetsStyleLeft imageTitleSpace:5];
    [self.saleButton layoutButtonWithEdgeInsetsStyle:TENButtonEdgeInsetsStyleLeft imageTitleSpace:5];
    [self.qunayiButton layoutButtonWithEdgeInsetsStyle:TENButtonEdgeInsetsStyleLeft imageTitleSpace:5];
    [self.likesButton layoutButtonWithEdgeInsetsStyle:TENButtonEdgeInsetsStyleLeft imageTitleSpace:5];
    [self.orderButton layoutButtonWithEdgeInsetsStyle:TENButtonEdgeInsetsStyleLeft imageTitleSpace:5];
    [self.historyButton layoutButtonWithEdgeInsetsStyle:TENButtonEdgeInsetsStyleLeft imageTitleSpace:5];
}

- (IBAction)click:(UIButton *)sender {
    NSInteger tag = sender.tag;
    if (self.buttonHandler) {
        self.buttonHandler(tag);
    }
}

@end

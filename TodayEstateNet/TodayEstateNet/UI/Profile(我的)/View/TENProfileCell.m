//
//  TENProfileCell.m
//  TodayEstateNet
//
//  Created by  HomerLynn on 2018/11/15.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENProfileCell.h"

@interface TENProfileCell ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *seperateViewPaddingLeft;

@end

@implementation TENProfileCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSpViewPaddingLeft:(NSNumber *)spViewPaddingLeft {
    _spViewPaddingLeft = spViewPaddingLeft;
    self.seperateViewPaddingLeft.constant = _spViewPaddingLeft.floatValue;
}

@end

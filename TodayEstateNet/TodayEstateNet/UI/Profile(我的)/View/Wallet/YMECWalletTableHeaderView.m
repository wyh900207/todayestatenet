//
//  YMECWalletTableHeaderView.m
//  YMEconomicCircle
//
//  Created by HomerLynn on 2018/5/31.
//  Copyright © 2018年 YME. All rights reserved.
//

#import "YMECWalletTableHeaderView.h"

@implementation YMECWalletTableHeaderView

- (IBAction)gotoBalance:(id)sender {
    if (self.balanceButtonAction) {
        self.balanceButtonAction();
    }
}

@end

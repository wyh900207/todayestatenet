//
//  TENSettingsCell.h
//  TodayEstateNet
//
//  Created by  HomerLynn on 2018/11/15.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENTableViewCell.h"

NS_ASSUME_NONNULL_BEGIN

@interface TENSettingsCell : TENTableViewCell

@end

NS_ASSUME_NONNULL_END

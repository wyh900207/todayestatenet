//
//  YMECBaseTableViewCell.m
//  YMEconomicCircle
//
//  Created by HomerLynn on 2018/5/9.
//  Copyright © 2018年 YME. All rights reserved.
//

#import "YMECBaseTableViewCell.h"

@interface YMECBaseTableViewCell ()

@property (nonatomic, strong) UIView *spaceView;

@end

@implementation YMECBaseTableViewCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setup];
    }
    return self;
}

- (void)setup {
    [self.contentView addSubview:self.spaceView];
    [self.spaceView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.bottom.equalTo(self);
        make.left.equalTo(self).offset(10);
        make.right.equalTo(self).offset(-10);
        make.height.equalTo(@1);
    }];
}

- (UIView *)spaceView {
    if (!_spaceView) {
        _spaceView = [UIView new];
        _spaceView.backgroundColor = [UIColor lightGrayColor];
        _spaceView.alpha = .2f;
    }
    return _spaceView;
}

@end

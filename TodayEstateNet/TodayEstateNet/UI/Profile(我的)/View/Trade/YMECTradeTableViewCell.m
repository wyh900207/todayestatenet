//
//  YMECTradeTableViewCell.m
//  YMEconomicCircle
//
//  Created by HomerLynn on 2018/6/8.
//  Copyright © 2018年 YME. All rights reserved.
//

#import "YMECTradeTableViewCell.h"
#import "YMECTradeModel.h"

@implementation YMECTradeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setModel:(YMECTradeDetailModel *)model {
    _model = model;
    self.nameLabel.text = model.paymentName;
    self.tradeTimeLabel.text = [NSString stringWithFormat:@"交易时间: %@", [TimeChangeTools timeFormatterWithTime:model.payTime]];
    self.moneyLabel.text = [NSString stringWithFormat:@"¥ %.2f", model.payMoney.doubleValue];
}

@end

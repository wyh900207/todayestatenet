//
//  YMECBaseTableViewCell.h
//  YMEconomicCircle
//
//  Created by HomerLynn on 2018/5/9.
//  Copyright © 2018年 YME. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface YMECBaseTableViewCell : UITableViewCell

@end

//
//  YMECTradeTableViewCell.h
//  YMEconomicCircle
//
//  Created by HomerLynn on 2018/6/8.
//  Copyright © 2018年 YME. All rights reserved.
//

#import "YMECBaseTableViewCell.h"
@class YMECTradeDetailModel;

@interface YMECTradeTableViewCell : YMECBaseTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *tradeTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;

@property (strong, nonatomic) YMECTradeDetailModel * model;

@end

//
//  TENCalendarView.m
//  HouseEconomics
//
//  Created by apple on 2019/2/21.
//  Copyright © 2019  HomerLynn. All rights reserved.
//

#import "TENCalendarView.h"
#import "TENDateBtnView.h"

@interface TENCalendarView()

@property (nonatomic, strong) UILabel *dataLabel;
@property (nonatomic, strong) UIButton *backBtn;
@property (nonatomic, strong) UIView *spaceView1;
@property (nonatomic, strong) UIView *spaceView2;
@property (nonatomic, strong) UIView *btnView;
@property (nonatomic, strong) NSArray *weekArray;
@property (nonatomic, strong) NSArray *dateArray;
@property (nonatomic, strong) NSMutableArray *chineseDateArray;

@end

@implementation TENCalendarView

- (instancetype)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]) {
        [self setupSubviews];
    }
    return self;
}

- (void)setupSubviews{
    [self addSubview:self.dataLabel];
    [self addSubview:self.backBtn];
    [self addSubview:self.spaceView1];
    [self addSubview:self.btnView];
    [self addSubview:self.spaceView2];
    
    [self.dataLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self).offset(5);
        make.left.equalTo(self).offset(8);
    }];
    [self.backBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self.dataLabel.mas_right).offset(10);
        make.centerY.equalTo(self.dataLabel.mas_centerY);
        make.width.mas_equalTo(60);
    }];
    [self.spaceView1 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(8);
        make.right.equalTo(self).offset(-8);
        make.top.equalTo(self.backBtn.mas_bottom).offset(5);
        make.height.mas_equalTo(0.5);
    }];
    [self.spaceView2 mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.equalTo(self).offset(8);
        make.right.equalTo(self).offset(-8);
        make.height.mas_equalTo(0.5);
        make.bottom.equalTo(self);
    }];
    [self.btnView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self);
        make.top.equalTo(self.spaceView1.mas_bottom);
        make.bottom.equalTo(self.spaceView2.mas_top);
    }];
}

- (UILabel *)dataLabel{
    if (!_dataLabel) {
        _dataLabel = [UILabel new];
        _dataLabel.textColor = TENHexColor(@"333333");
        _dataLabel.font = TENFont15;
        _dataLabel.text = @"2019年2月";
    }
    return _dataLabel;
}

- (UIButton *)backBtn{
    if (!_backBtn) {
        _backBtn = [UIButton new];
        _backBtn.titleLabel.font = TENFont12;
        [_backBtn setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        _backBtn.layer.cornerRadius = 4;
        _backBtn.layer.masksToBounds = YES;
        [_backBtn setTitle:@"返回今天" forState:UIControlStateNormal];
        _backBtn.hidden = YES;
        _backBtn.backgroundColor = TENHexColor(@"F76C1D");
    }
    return _backBtn;
}

- (UIView *)spaceView1{
    if (!_spaceView1) {
        _spaceView1 = [UIView new];
        _spaceView1.backgroundColor = TENHexColor(TENSpaceViewColor);
    }
    return _spaceView1;
}

- (UIView *)spaceView2{
    if (!_spaceView2) {
        _spaceView2 = [UIView new];
        _spaceView2.backgroundColor = TENHexColor(TENSpaceViewColor);
    }
    return _spaceView2;
}

- (UIView *)btnView{
    self.dateArray  = [self getCurrentWeek];
    if (!_btnView) {
        _btnView = [UIView new];
        for (int i = 0; i < self.weekArray.count; i++) {
            UILabel *label = [UILabel new];
            label.text = self.weekArray[i];
            label.textAlignment = NSTextAlignmentCenter;
            label.font = [UIFont boldSystemFontOfSize:12];
            
            
            float labelWidth = 30.f;
            float space = (TENScreenWidth-16-210)/6;
            label.frame = CGRectMake(8+(space+labelWidth)*i, 0, labelWidth, labelWidth);
            [_btnView addSubview:label];
            float dateHeight = 38.f;
            TENDateBtnView *dateView = [[TENDateBtnView alloc] initWithFrame:CGRectMake(8+(space+labelWidth)*i, labelWidth+2, labelWidth, dateHeight)];
            [_btnView addSubview:dateView];
            
            dateView.dateLabel.text = self.dateArray[i];
            dateView.cnDateLabel.text = self.chineseDateArray[i];
            
            if ([self.weekArray[i] isEqualToString:@"六"] || [self.weekArray[i] isEqualToString:@"日"]) {
                label.textColor = [UIColor redColor];
                dateView.dateLabel.textColor = [UIColor redColor];
                dateView.cnDateLabel.textColor = TENHexColor(@"999999");
            }else{
                label.textColor = TENHexColor(@"333333");
                dateView.dateLabel.textColor = TENHexColor(@"333333");
                dateView.cnDateLabel.textColor = TENHexColor(@"999999");
            }
        }
        
        
    }
    return _btnView;
}

- (NSArray *)weekArray{
    return @[@"日",@"一",@"二",@"三",@"四",@"五",@"六"];
}

- (NSMutableArray *)chineseDateArray{
    if (!_chineseDateArray) {
        _chineseDateArray = [NSMutableArray arrayWithArray:@[@"十三",@"十四",@"十五",@"十六",@"十七",@"十八",@"十九"]];
    }
    return _chineseDateArray;
}

/**
 *  获取当前时间所在一周的第一天和最后一天
 */
- (NSArray *)getCurrentWeek {
    NSDate *now = [NSDate date];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *comp = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitWeekday
                                         fromDate:now];
    // 得到星期几
    // 1(星期天) 2(星期二) 3(星期三) 4(星期四) 5(星期五) 6(星期六) 7(星期天)
    NSInteger weekDay = [comp weekday];
    // 得到几号
    NSInteger day = [comp day];
    
    NSLog(@"weekDay:%ld  day:%ld",weekDay,day);
    
    // 计算当前日期和这周的星期一和星期天差的天数
    long firstDiff,lastDiff;
    if (weekDay == 1) {
        firstDiff = 1;
        lastDiff = 0;
    }else{
        firstDiff = [calendar firstWeekday] - weekDay;
        lastDiff = 7 - weekDay;
    }
    
    NSArray *currentWeeks = [self getCurrentWeeksWithFirstDiff:firstDiff lastDiff:lastDiff];
    
    //    NSLog(@"firstDiff:%ld   lastDiff:%ld",firstDiff,lastDiff);
    
    // 在当前日期(去掉了时分秒)基础上加上差的天数
    //    NSDateComponents *firstDayComp = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:now];
    //    [firstDayComp setDay:day + firstDiff];
    //    NSDate *firstDayOfWeek= [calendar dateFromComponents:firstDayComp];
    //
    //    NSDateComponents *lastDayComp = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay fromDate:now];
    //    [lastDayComp setDay:day + lastDiff];
    //    NSDate *lastDayOfWeek= [calendar dateFromComponents:lastDayComp];
    //
    //    NSDateFormatter *formater = [[NSDateFormatter alloc] init];
    //    [formater setDateFormat:@"yyyy-MM-dd"];
    //    NSLog(@"一周开始 %@",[formater stringFromDate:firstDayOfWeek]);
    //    NSLog(@"当前 %@",[formater stringFromDate:now]);
    //    NSLog(@"一周结束 %@",[formater stringFromDate:lastDayOfWeek]);
    //
    //    NSLog(@"%@",currentWeeks);
    return currentWeeks;
}

//获取一周时间 数组
- (NSMutableArray *)getCurrentWeeksWithFirstDiff:(NSInteger)first lastDiff:(NSInteger)last{
    NSMutableArray *eightArr = [[NSMutableArray alloc] init];
    for (NSInteger i = first; i < last + 1; i ++) {
        //从现在开始的24小时
        NSTimeInterval secondsPerDay = i * 24*60*60;
        NSDate *curDate = [NSDate dateWithTimeIntervalSinceNow:secondsPerDay];                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"d"];
        NSString *dateStr = [dateFormatter stringFromDate:curDate];//几月几号
        //        NSString *dateStr = @"5月31日";
        //        NSDateFormatter *weekFormatter = [[NSDateFormatter alloc] init];
        //        [weekFormatter setDateFormat:@"EEEE"];//星期几 @"HH:mm 'on' EEEE MMMM d"];
        //        NSString *weekStr = [weekFormatter stringFromDate:curDate];
        //组合时间
        NSString *strTime = [NSString stringWithFormat:@"%@",dateStr];
        [eightArr addObject:strTime];
    }
    return eightArr;
}

@end

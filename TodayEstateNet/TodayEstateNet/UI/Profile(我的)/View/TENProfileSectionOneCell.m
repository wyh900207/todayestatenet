//
//  TENProfileSectionOneCell.m
//  TodayEstateNet
//
//  Created by  HomerLynn on 2018/12/2.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENProfileSectionOneCell.h"

@implementation TENProfileSectionOneCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)sigin:(id)sender {
    if (self.delegate) {
        [self.delegate signIn];
    }
}

- (IBAction)invite:(id)sender {
    if (self.delegate) {
        [self.delegate inviteFriend];
    }
}

@end

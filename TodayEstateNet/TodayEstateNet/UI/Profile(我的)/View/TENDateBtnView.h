//
//  TENDateBtnView.h
//  HouseEconomics
//
//  Created by apple on 2019/2/21.
//  Copyright © 2019  HomerLynn. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TENDateBtnView : UIView
@property (nonatomic, strong) UILabel *dateLabel;
@property (nonatomic, strong) UILabel *cnDateLabel;
@end

NS_ASSUME_NONNULL_END

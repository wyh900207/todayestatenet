//
//  TENProfileHeaderView.h
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/11/7.
//  Copyright © 2018 TEN. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TENProfileHeaderViewDelegate<NSObject>
@optional

- (void)userinfo;

@end

@interface TENProfileHeaderView : UIView

// 背景
@property (nonatomic, strong) UIImageView *backgroundImageView;
// 认证
@property (nonatomic, strong) UIView *authenticationView;
@property (nonatomic, strong) UIButton *wechatAuth;
@property (nonatomic, strong) UIButton *realNameAuth;
// User
@property (nonatomic, strong) UIButton *headerIconImageView;
@property (nonatomic, strong) UILabel *usernameLabel;
// 底部视图
@property (nonatomic, strong) UIView *spaceView;
@property (nonatomic, strong) UIView *bottomStackView;
@property (nonatomic, strong) UIButton *myPointsButton;
@property (nonatomic, strong) UIButton *myHouseCoinsButton;
@property (nonatomic, strong) UIButton *myLastDaysButton;

@property (nonatomic, weak  ) id <TENProfileHeaderViewDelegate> delegate;

@end

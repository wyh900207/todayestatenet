//
//  ZQVariableMenuCell.m
//  ZQVariableMenuDemo
//
//  Created by 肖兆强 on 2017/12/1.
//  Copyright © 2017年 ZQDemo. All rights reserved.
//

#import "ZQVariableMenuCell.h"

@interface ZQVariableMenuCell ()
{
    UILabel *_textLabel;
    UIImageView *_iconImage;
    CAShapeLayer *_borderLayer;
}
@end


@implementation ZQVariableMenuCell

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self initUI];
    }
    return self;
}

-(void)initUI
{
    self.userInteractionEnabled = true;
    self.layer.cornerRadius = 5.0f;
    self.backgroundColor = [self backgroundColor];
    
    
    _textLabel = [[UILabel alloc] init];
    [self addSubview:_textLabel];
    _textLabel.font = TENFont14;
    _textLabel.textColor = [self textColor];
    _textLabel.textAlignment = NSTextAlignmentCenter;
    _textLabel.backgroundColor = TENHexColor(@"F5F8FC");
    
    _iconImage = [[UIImageView alloc] init];
    [self addSubview:_iconImage];
    
}


- (void)layoutSubviews
{
    [super layoutSubviews];
    CGFloat iconImageH = self.bounds.size.height;
    CGFloat iconImageW =self.bounds.size.width;
    _iconImage.frame = CGRectMake(iconImageW-7.5, -7.5, 15, 15);
    _textLabel.frame = CGRectMake(0, 0, iconImageW, iconImageH);
    
}


#pragma mark 配置方法

-(UIColor*)backgroundColor{
    return [UIColor clearColor];
}

-(UIColor*)textColor{
    return TENHexColor(@"222222");
}

-(UIColor*)lightTextColor{
    return TENHexColor(@"222222");
}





#pragma mark -
#pragma mark Setter

-(void)setTitle:(NSString *)title
{
    if (!_title) {
        _title = title;
        _textLabel.text = title;
    }
}

- (void)setImageName:(NSString *)imageName
{
    if (!_imageName) {
        _imageName = imageName;
        _iconImage.image = [UIImage imageNamed:@"image_remove"];
    }
}





-(void)setIsMoving:(BOOL)isMoving
{
    _isMoving = isMoving;
    if (_isMoving) {
        self.backgroundColor = [UIColor clearColor];
        _borderLayer.hidden = false;
    }else{
        self.backgroundColor = [self backgroundColor];
        _borderLayer.hidden = true;
    }
}

-(void)setIsFixed:(BOOL)isFixed{
    _isFixed = isFixed;
    if (isFixed) {
        _textLabel.backgroundColor =[UIColor clearColor];
    }else{
        _textLabel.backgroundColor = TENHexColor(@"F5F8FC");
    }
}

-(void)isneedImage:(BOOL)isneedImage {
    if (isneedImage) {
        _iconImage.hidden =NO;
    }else{
        _iconImage.hidden =YES;
    }
}

@end

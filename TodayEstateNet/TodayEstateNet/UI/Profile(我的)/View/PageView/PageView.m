//
//  PageView.m
//  HomeGardenTreasure
//
//  Created by paat on 2018/9/28.
//  Copyright © 2018年 Paat. All rights reserved.
//

#define TitleButtonH kHeight(20)
#define LineViewH kHeight(2)
#define LineH kHeight(2)

#import "PageView.h"

@interface PageView()<UIScrollViewDelegate>

@property (nonatomic, strong) NSArray *titleArray;
@property (nonatomic, strong) UIScrollView *childScrollView;
@property (nonatomic, strong) NSArray *childArray;
@property (nonatomic, strong) UIView *selLineView;
@property (nonatomic, strong) UIView *lineView;

@end

@implementation PageView

-(instancetype)initWithFrame:(CGRect)frame WithTitleArray:(NSArray <NSString *> *)titleArray WithchildControllerArray:(NSArray <UIViewController *>*)childArray
{
    self = [super initWithFrame:frame];
    if (self) {
        self.titleArray = titleArray;
        self.childArray = childArray;
        
        [self creatChildviewScrollView];
    }
    return self;
}
-(void)creatChildviewScrollView
{
    //KNavHeight
    UIScrollView *childScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0,TENNavigationBarHeight, self.bounds.size.width, self.bounds.size.height-TitleButtonH-LineViewH)];
    childScrollView.showsVerticalScrollIndicator = NO;
    childScrollView.showsHorizontalScrollIndicator = NO;
    childScrollView.bounces = NO;
    childScrollView.delegate = self;
    childScrollView.contentSize = CGSizeMake(self.titleArray.count * self.bounds.size.width, 0);
    childScrollView.pagingEnabled = YES;
    [self addSubview:childScrollView];
    self.childScrollView = childScrollView;

    [self addSubview:self.lineView];
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.left.offset(0);
        make.top.offset (TENNavigationBarHeight);
        make.height.offset(0.5);
    }];
    
    for (int i = 0; i<self.childArray.count; i++) {
        UIViewController *childVC = self.childArray[i];
        childVC.view.frame = CGRectMake(i * childScrollView.bounds.size.width, 0, childScrollView.bounds.size.width, childScrollView.bounds.size.height);
        [childScrollView addSubview:childVC.view];
    }
    for (int i = 0; i<self.titleArray.count; i++) {
        CGSize titleSize = [self.titleArray[i] sizeWithString:self.titleArray[i] font:TENFont14 maxSize:CGSizeMake(MAXFLOAT, TitleButtonH)];
        CGFloat TitleButtonW = titleSize.width + 10;
        CGFloat paddin = (self.frame.size.width - TitleButtonW *self.titleArray.count -kWidth(46.0f))/ (self.titleArray.count );
        UIButton *titleButton = [UIButton buttonWithType:UIButtonTypeCustom];
        titleButton.titleLabel.font = TENFont17;
        titleButton.frame = CGRectMake(i*(TitleButtonW+kWidth(46.0f))+paddin, TENNavigationBarHeight-kHeight(32), TitleButtonW, TitleButtonH);
        [titleButton setTitle:self.titleArray[i] forState:UIControlStateNormal];
        [titleButton setTitleColor:TENHexColor(TENTextGrayColor) forState:UIControlStateNormal];
        [titleButton setTitleColor:TENHexColor(TENThemeBlueColor) forState:UIControlStateSelected];
        titleButton.tag = i+1;
        [titleButton addTarget:self action:@selector(titleButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:titleButton];

        
        if (i == 0) {
            UIView *selLineView = [[UIView alloc] init];
            selLineView.bounds = CGRectMake(0, 0, kWidth(30.0f), LineH);
            selLineView.center = CGPointMake(titleButton.center.x, (titleButton.center.y+TitleButtonH/2)+kHeight(11.0f));

            selLineView.backgroundColor = TENHexColor(TENThemeBlueColor);
            [self addSubview:selLineView];
            self.selLineView = selLineView;
            self.selectButton = titleButton;
            titleButton.selected = YES;
        }
    }
    
    [self addSubview:self.searchBtn];
    [_searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.offset(kWidth(12));
        make.top.mas_equalTo(TENNavigationBarHeight-kHeight(38));
        make.height.offset(kHeight(32));
        make.width.offset(kWidth(40));
    }];
//    选择频道btn 下个版本打开
    [self addSubview:self.viewBtn];
    [_viewBtn mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.offset(kWidth(-12));
        make.top.mas_equalTo(TENNavigationBarHeight-kHeight(38));
        make.height.offset(kHeight(32));
        make.width.offset(kWidth(40));
    }];
}

-(void)titleButtonClick:(UIButton *)sender
{
    if (self.selectButton.tag == sender.tag) {
        return;
    }
    [self moveTipview:sender];
    [self moveContentScrollView:sender];
}
- (void)moveTipview:(UIButton *)btn {
    
    [UIView animateWithDuration:0.5f animations:^{
        self.selLineView.center = CGPointMake(btn.center.x, (btn.center.y+TitleButtonH/2)+kHeight(11.0f));
//        CGRect frame = self.lineView.frame;
//        CGRect btnTitle = btn.titleLabel.frame;
//        frame.origin.x = btnTitle.origin.x + btn.frame.origin.x;
//        self.lineView.frame = frame;
    }];
    self.selectButton.selected=NO;
    btn.selected=YES;
    self.selectButton=btn;
}
- (void)moveContentScrollView:(UIButton *)btn {
    
    CGPoint contentOffset = self.childScrollView.contentOffset;
    contentOffset.x = (btn.tag - 1) * self.bounds.size.width;
    [UIView animateWithDuration:0.25f animations:^{
        self.childScrollView.contentOffset = contentOffset;
    }];
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    int index = scrollView.contentOffset.x / self.bounds.size.width + 1;
    for (UIView * view in self.subviews) {
        if (view.tag == index) {
            [self moveTipview:(UIButton *)view];
        }
    }
}
-(UIButton *)searchBtn
{
    if (!_searchBtn) {
        _searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_searchBtn setImage:TENImage(@"search_2") forState:0];
    }
    return _searchBtn;
}
-(UIButton *)viewBtn
{
    if (!_viewBtn) {
        _viewBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        _viewBtn.selected =YES;
        [_viewBtn setImage:TENImage(@"viewBtn_image2") forState:UIControlStateSelected];
        [_viewBtn setImage:TENImage(@"viewBtn_image1") forState:UIControlStateNormal];
    }
    return _viewBtn;
}

-(UIView *)lineView
{
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = TENHexColor(TENSpaceViewColor);
    }
    return _lineView;
}
@end

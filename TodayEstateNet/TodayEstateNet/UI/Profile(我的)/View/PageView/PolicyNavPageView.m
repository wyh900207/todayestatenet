//
//  PolicyNavPageView.m
//  HomeGardenTreasure
//
//  Created by paat on 2018/9/6.
//  Copyright © 2018年 Paat. All rights reserved.
//

#import "PolicyNavPageView.h"

#define TitleButtonH kHeight(20)
#define LineViewH kHeight(2)

#define LineH kHeight(3)

@interface PolicyNavPageView()<UIScrollViewDelegate>

@property (nonatomic , strong) UIView *selLineView;



@property (nonatomic,strong) UIView *lineView;

@end
@implementation PolicyNavPageView
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self creatChildView];
}

- (void)creatChildView
{
 
//KNavHeight
    UIScrollView *childScrollView = [[UIScrollView alloc]initWithFrame:CGRectMake(0,TENNavigationBarHeight, self.view.bounds.size.width, self.view.bounds.size.height-TitleButtonH-LineViewH)];

    childScrollView.showsVerticalScrollIndicator = NO;
    childScrollView.showsHorizontalScrollIndicator = NO;
    childScrollView.bounces = NO;
    childScrollView.delegate = self;
    childScrollView.contentSize = CGSizeMake(self.titleArray.count * self.view.bounds.size.width, 0);
    childScrollView.pagingEnabled = YES;
    [self.view addSubview:childScrollView];
    self.childScrollView = childScrollView;
    
    [self.view addSubview:self.lineView];
    [_lineView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.right.left.offset(0);
        make.top.offset (TENNavigationBarHeight);
        make.height.offset(0.5);
    }];

    for (int i = 0; i<self.titleArray.count; i++) {
        
        CGSize titleSize = [self.titleArray[i] sizeWithString:self.titleArray[i] font:TENFont14 maxSize:CGSizeMake(MAXFLOAT, TitleButtonH)];
        CGFloat TitleButtonW = titleSize.width + 10;
        
        CGFloat paddin = (self.view.frame.size.width - TitleButtonW *self.titleArray.count -kWidth(46.0f))/ (self.titleArray.count );
        
        UIButton *titleButton = [UIButton buttonWithType:UIButtonTypeCustom];
        titleButton.titleLabel.font = TENFont17;
        titleButton.frame = CGRectMake(i*(TitleButtonW+kWidth(46.0f))+paddin, TENNavigationBarHeight-kHeight(32), TitleButtonW, TitleButtonH);
        [titleButton setTitle:self.titleArray[i] forState:UIControlStateNormal];
        [titleButton setTitleColor:TENHexColor(TENTextGrayColor) forState:UIControlStateNormal];
        [titleButton setTitleColor:TENHexColor(TENBlackColor) forState:UIControlStateSelected];
        titleButton.tag = i+1;
        [titleButton addTarget:self action:@selector(titleButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self.view addSubview:titleButton];
        
        [self.view addSubview:self.searchBtn];
        [_searchBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.right.offset(-kWidth(12));
            make.centerY.mas_equalTo(titleButton);
            make.height.offset(kHeight(32));
            make.width.offset(kWidth(40));
        }];
        
        if (i == 0) {
            UIView *selLineView = [[UIView alloc] init];
            selLineView.backgroundColor = TENHexColor(TENThemeBlueColor);
            selLineView.bounds = CGRectMake(0, 0, kWidth(10.0f), LineH);
            selLineView.center = CGPointMake(titleButton.center.x, (titleButton.center.y+TitleButtonH/2)+kHeight(6.5f));
            [self.view addSubview:selLineView];
            self.selLineView = selLineView;
            self.selectButton = titleButton;
            titleButton.selected = YES;
//            [selLineView mas_makeConstraints:^(MASConstraintMaker *make) {
//                make.top.mas_equalTo(titleButton.mas_bottom).offset(kHeight(5.0f));
//                make.centerX.equalTo(titleButton.mas_centerX);
//                make.width.equalTo(@(kWidth(10.0f)));
//                make.height.offset(LineH);
//            }];
        }
    }

}

- (void)setCanSroll:(BOOL)canSroll
{
    _canSroll = canSroll;
    if (canSroll == YES) {
        self.childScrollView.scrollEnabled = YES;
    }else{
        self.childScrollView.scrollEnabled = NO;
    }
}
- (void) titleButtonClick:(UIButton *)titleButton
{
    if (self.selectButton.tag == titleButton.tag) {
        return;
    }
    [self moveTipview:titleButton];
    [self moveContentScrollView:titleButton];
}
- (void)moveTipview:(UIButton *)btn {

    [UIView animateWithDuration:0.5f animations:^{
        self.selLineView.center = CGPointMake(btn.center.x, (btn.center.y+TitleButtonH/2)+kHeight(6.5f));
//        CGRect frame = self.selLineView.frame;
//        CGRect btnTitle = btn.titleLabel.frame;
//        frame.origin.x = btnTitle.origin.x + btn.frame.origin.x;
//        self.selLineView.frame = frame;
    }];
    self.selectButton.selected=NO;
    btn.selected=YES;
    self.selectButton=btn;
    if ([self.delegate respondsToSelector:@selector(slideToViewAtIndex:)]) {
        [self.delegate slideToViewAtIndex:(int)btn.tag-1];
    }
}
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    int index = scrollView.contentOffset.x / self.view.bounds.size.width + 1;
    for (UIView * view in self.view.subviews) {
        if (view.tag == index) {
            [self moveTipview:(UIButton *)view];
        }
    }
}
- (void)moveContentScrollView:(UIButton *)btn {
    
    CGPoint contentOffset = self.childScrollView.contentOffset;
    contentOffset.x = (btn.tag - 1) * self.view.bounds.size.width;
    [UIView animateWithDuration:0.25f animations:^{
        self.childScrollView.contentOffset = contentOffset;
    }];
}
-(UIButton *)searchBtn
{
    if (!_searchBtn) {
        _searchBtn = [UIButton buttonWithType:UIButtonTypeCustom];
        [_searchBtn setImage:TENImage(@"search_2") forState:0];
    }
    return _searchBtn;
}

-(UIView *)lineView
{
    if (!_lineView) {
        _lineView = [UIView new];
        _lineView.backgroundColor = TENHexColor(TENSpaceViewColor);
    }
    return _lineView;
}
@end

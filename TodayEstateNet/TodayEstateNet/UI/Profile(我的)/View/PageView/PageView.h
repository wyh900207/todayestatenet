//
//  PageView.h
//  HomeGardenTreasure
//
//  Created by paat on 2018/9/28.
//  Copyright © 2018年 Paat. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PageView : UIView

@property (nonatomic , strong) UIButton *selectButton;
@property (nonatomic, strong) UIButton  *searchBtn;
@property (nonatomic, strong) UIButton  *viewBtn;//弹出选择框的按钮

-(instancetype)initWithFrame:(CGRect)frame WithTitleArray:(NSArray <NSString *> *)titleArray WithchildControllerArray:(NSArray <UIViewController *>*)childArray;

@end

//
//  PolicyNavPageView.h
//  HomeGardenTreasure
//
//  Created by paat on 2018/9/6.
//  Copyright © 2018年 Paat. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import "BaseView.h"


@protocol PolicyNavPageViewDelegate<NSObject>

- (void)slideToViewAtIndex:(int)index;

@end

@interface PolicyNavPageView : UIViewController

@property (nonatomic, assign) BOOL canSroll;
@property (nonatomic , strong) UIButton *selectButton;

@property (nonatomic, strong) UIButton  *searchBtn;
@property (nonatomic, weak) id<PolicyNavPageViewDelegate> delegate;///<PolicyNavPageView代理

@property (nonatomic,strong)  UIScrollView *childScrollView;
@property (nonatomic, strong) NSArray *titleArray;
@property (nonatomic, strong) NSArray *childArray;

@end

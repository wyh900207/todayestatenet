//
//  TENProfileHeaderView.m
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/11/7.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENProfileHeaderView.h"
#import "TENUtilsHeader.h"

@interface TENProfileHeaderView ()

@end

@implementation TENProfileHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setupSubviews];
    }
    return self;
}
    
#pragma mark - UI

- (void)setupSubviews {
    [self addSubview:self.backgroundImageView];
    
//    [self addSubview:self.bottomStackView];
//    [self addSubview:self.spaceView];
//    [self.bottomStackView addSubview:self.myPointsButton];
//    [self.bottomStackView addSubview:self.myHouseCoinsButton];
//    [self.bottomStackView addSubview:self.myLastDaysButton];
    
    [self addSubview:self.authenticationView];
//    [self.authenticationView addSubview:self.wechatAuth];
    [self.authenticationView addSubview:self.realNameAuth];
    
    [self addSubview:self.usernameLabel];
    [self addSubview:self.headerIconImageView];
    
    [self.backgroundImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self);
    }];
//    [self.bottomStackView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.bottom.right.equalTo(self);
//        make.height.equalTo(@60);
//    }];
//    [self.spaceView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.left.equalTo(self).offset(8);
//        make.right.equalTo(self).offset(-8);
//        make.bottom.equalTo(self.bottomStackView.mas_top);
//        make.height.equalTo(@0.5);
//    }];
//    [self.myPointsButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.left.bottom.equalTo(self.bottomStackView);
//    }];
//    [self.myHouseCoinsButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.bottom.equalTo(self.bottomStackView);
//        make.left.equalTo(self.myPointsButton.mas_right);
//        make.width.equalTo(self.myPointsButton);
//    }];
//    [self.myLastDaysButton mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.bottom.right.equalTo(self.bottomStackView);
//        make.left.equalTo(self.myHouseCoinsButton.mas_right);
//        make.width.equalTo(self.myHouseCoinsButton);
//    }];
    [self.authenticationView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.left.right.equalTo(self);
        make.height.equalTo(@18);
        make.bottom.equalTo(self).offset(-10);
    }];
//    [self.wechatAuth mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.top.bottom.equalTo(self.authenticationView);
//        make.width.equalTo(@100);
//        make.centerX.equalTo(self).offset(-55);
//    }];
    [self.realNameAuth mas_makeConstraints:^(MASConstraintMaker *make) {
        /*
        make.top.bottom.width.equalTo(self.wechatAuth);
        make.left.equalTo(self.wechatAuth.mas_right).offset(20);
        make.centerX.equalTo(self).offset(55);
         */
        make.edges.equalTo(self.authenticationView);
    }];
    [self.usernameLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.equalTo(@20);
        make.left.right.equalTo(self);
        make.bottom.equalTo(self.authenticationView.mas_top).offset(-10);
    }];
    [self.headerIconImageView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.bottom.equalTo(self.usernameLabel.mas_top).offset(-10);
        make.width.height.equalTo(@74);
    }];
    
    [self.headerIconImageView setImage:[UIImage imageNamed:@"ten-profile-userheader"] forState:0];
}
    
#pragma mark - Getter
    
- (UIButton *)headerIconImageView {
    if (!_headerIconImageView) {
        _headerIconImageView = [UIButton new];
        _headerIconImageView.layer.cornerRadius = 37;
        _headerIconImageView.layer.masksToBounds = YES;
        [_headerIconImageView addTarget:self action:@selector(login) forControlEvents:UIControlEventTouchUpInside];
    }
    return _headerIconImageView;
}
    
- (UILabel *)usernameLabel {
    if (!_usernameLabel) {
        _usernameLabel = [UILabel new];
        _usernameLabel.text = @"未登录";
        _usernameLabel.textColor = TENHexColor(TENWhiteColor);
        _usernameLabel.textAlignment = NSTextAlignmentCenter;
        _usernameLabel.font = TENFont15;
    }
    return _usernameLabel;
}
    
- (UIImageView *)backgroundImageView {
    if (!_backgroundImageView) {
        _backgroundImageView = [UIImageView new];
        _backgroundImageView.image = [UIImage imageNamed:@"ten-profile-header"];
    }
    return _backgroundImageView;
}
    
- (UIView *)authenticationView {
    if (!_authenticationView) {
        _authenticationView = [UIView new];
    }
    return _authenticationView;
}
    
- (UIButton *)wechatAuth {
    if (!_wechatAuth) {
        _wechatAuth = [UIButton new];
        _wechatAuth.titleLabel.font = TENFont12;
        [_wechatAuth setImage:[UIImage imageNamed:@"ten-profile-wechat"] forState:0];
        [_wechatAuth setTitleColor:TENHexColor(TENWhiteColor) forState:0];
        [_wechatAuth setTitle:@"未绑定微信" forState:0];
    }
    return _wechatAuth;
}
    
- (UIButton *)realNameAuth {
    if (!_realNameAuth) {
        _realNameAuth = [UIButton new];
        _realNameAuth.titleLabel.font = TENFont12;
        [_realNameAuth setImage:[UIImage imageNamed:@"ten-profile-realname"] forState:0];
        [_realNameAuth setTitleColor:TENHexColor(TENWhiteColor) forState:0];
        [_realNameAuth setTitle:@"已实名认证" forState:0];
    }
    return _realNameAuth;
}
    
- (UIView *)spaceView {
    if (!_spaceView) {
        _spaceView = [UIView new];
        _spaceView.backgroundColor = TENHexColor(TENSpaceViewColor);
    }
    return _spaceView;
}
    
- (UIView *)bottomStackView {
    if (!_bottomStackView) {
        _bottomStackView = [UIView new];
    }
    return _bottomStackView;
}
    
- (UIButton *)myPointsButton {
    if (!_myPointsButton) {
        _myPointsButton = [UIButton new];
        _myPointsButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        _myPointsButton.titleLabel.numberOfLines = 0;
        [_myPointsButton setValue:TENFont12 forKeyPath:@"titleLabel.font"];
        [_myPointsButton setTitle:@"我的积分" forState:0];
        [_myPointsButton setTitleColor:[UIColor whiteColor] forState:0];
    }
    return _myPointsButton;
}
    
- (UIButton *)myHouseCoinsButton {
    if (!_myHouseCoinsButton) {
        _myHouseCoinsButton = [UIButton new];
        _myHouseCoinsButton.titleLabel.numberOfLines = 0;
        _myHouseCoinsButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        _myHouseCoinsButton = [UIButton new];
        [_myHouseCoinsButton setValue:TENFont12 forKeyPath:@"titleLabel.font"];
        [_myHouseCoinsButton setTitle:@"我的房币" forState:0];
        [_myHouseCoinsButton setTitleColor:[UIColor whiteColor] forState:0];
    }
    return _myHouseCoinsButton;
}
    
- (UIButton *)myLastDaysButton {
    if (!_myLastDaysButton) {
        _myLastDaysButton = [UIButton new];
        _myLastDaysButton.titleLabel.textAlignment = NSTextAlignmentCenter;
        _myLastDaysButton.titleLabel.numberOfLines = 0;
        [_myLastDaysButton setValue:TENFont12 forKeyPath:@"titleLabel.font"];
        [_myLastDaysButton setTitle:@"剩余天数" forState:0];
        [_myLastDaysButton setTitleColor:[UIColor whiteColor] forState:0];
    }
    return _myLastDaysButton;
}

#pragma mark - Private Methods

- (void)login {
    if (self.delegate && [self.delegate respondsToSelector:@selector(userInfo)]) {
        [self.delegate userinfo];
    }
}

@end

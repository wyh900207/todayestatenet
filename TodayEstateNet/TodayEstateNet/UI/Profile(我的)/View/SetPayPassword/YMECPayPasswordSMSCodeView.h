//
//  YMECPayPasswordSMSCodeView.h
//  YMEconomicCircle
//
//  Created by HomerLynn on 2018/5/31.
//  Copyright © 2018年 YME. All rights reserved.
//

//#import "YMECBaseView.h"
#import "DJCountdownButton.h"
@interface YMECPayPasswordSMSCodeView : UIView

@property (weak, nonatomic) IBOutlet UILabel *titleLael;
@property (weak, nonatomic) IBOutlet UITextField *smsCodeTextField;
@property (weak, nonatomic) IBOutlet DJCountdownButton *sendSMSButton;

@property (copy, nonatomic) void (^smsTextChangeAction)(NSString * text);
@property (copy, nonatomic) void (^sendButtonAction)(void);

@end

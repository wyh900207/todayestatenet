//
//  YMECPayPasswordSMSCodeView.m
//  YMEconomicCircle
//
//  Created by HomerLynn on 2018/5/31.
//  Copyright © 2018年 YME. All rights reserved.
//

#import "YMECPayPasswordSMSCodeView.h"

@implementation YMECPayPasswordSMSCodeView

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupSubviews];
}

#pragma mark -

- (void)setupSubviews {
    self.layer.cornerRadius = 5;
    self.layer.masksToBounds = YES;
    self.layer.borderWidth = 1;
    self.layer.borderColor = YMEC_COLOR_SEPARATE.CGColor;
    [self.smsCodeTextField addTarget:self action:@selector(smsTextChanged:) forControlEvents:UIControlEventEditingChanged];
}

#pragma mark -

- (IBAction)sendMessage:(id)sender {
    if (self.sendButtonAction) {
        self.sendButtonAction();
    }
}

- (void)smsTextChanged:(UITextField *)textField {
    if (self.smsTextChangeAction) {
        self.smsTextChangeAction(textField.text);
    }
}

@end

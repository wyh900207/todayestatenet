//
//  YMECPayPasswordPhoneView.m
//  YMEconomicCircle
//
//  Created by HomerLynn on 2018/5/31.
//  Copyright © 2018年 YME. All rights reserved.
//

#import "YMECPayPasswordPhoneView.h"

@implementation YMECPayPasswordPhoneView

- (void)awakeFromNib {
    [super awakeFromNib];
    [self setupSubviews];
}

#pragma mark -

- (void)setupSubviews {
    self.layer.cornerRadius = 5;
    self.layer.masksToBounds = YES;
    self.layer.borderWidth = 1;
    self.layer.borderColor = YMEC_COLOR_SEPARATE.CGColor;
    [self.textField addTarget:self action:@selector(textChanged:) forControlEvents:UIControlEventEditingChanged];
}

#pragma mark -

- (void)textChanged:(UITextField *)textField {
    if (self.textChangeAction) {
        self.textChangeAction(textField.text);
    }
}

@end

//
//  YMECPayPasswordPhoneView.h
//  YMEconomicCircle
//
//  Created by HomerLynn on 2018/5/31.
//  Copyright © 2018年 YME. All rights reserved.
//

//#import "YMECBaseView.h"

@interface YMECPayPasswordPhoneView : UIView

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UITextField *textField;

@property (copy, nonatomic) void (^textChangeAction)(NSString *text);

@end

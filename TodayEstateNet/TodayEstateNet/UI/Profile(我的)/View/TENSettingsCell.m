//
//  TENSettingsCell.m
//  TodayEstateNet
//
//  Created by  HomerLynn on 2018/11/15.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENSettingsCell.h"
#import "TENUtilsHeader.h"

@implementation TENSettingsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        [self setupSubviews];
    }
    return self;
}

#pragma mark - UI

- (void)setupSubviews {
    [self setValue:TENFont14 forKeyPath:@"textLabel.font"];
    self.textLabel.textColor = TENHexColor(TENTextBlackColor3);
}

@end

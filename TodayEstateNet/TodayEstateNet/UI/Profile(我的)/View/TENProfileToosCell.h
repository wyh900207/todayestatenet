//
//  TENProfileToosCell.h
//  TodayEstateNet
//
//  Created by  HomerLynn on 2018/12/2.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENTableViewCell.h"

@interface TENProfileToosCell : TENTableViewCell

@property (nonatomic, copy) void (^itemClickHandler)(NSInteger index);

@end

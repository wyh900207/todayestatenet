//
//  TENProfileCell.h
//  TodayEstateNet
//
//  Created by  HomerLynn on 2018/11/15.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENTableViewCell.h"

@interface TENProfileCell : TENTableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *iconImagView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (strong, nonatomic) NSNumber *spViewPaddingLeft;

@end

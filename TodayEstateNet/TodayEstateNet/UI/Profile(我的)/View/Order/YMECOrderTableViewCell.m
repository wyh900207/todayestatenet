//
//  YMECOrderTableViewCell.m
//  YMEconomicCircle
//
//  Created by HomerLynn on 2018/6/7.
//  Copyright © 2018年 YME. All rights reserved.
//

#import "YMECOrderTableViewCell.h"
#import "YMECOrderModel.h"
#import "TENPaymentRequest.h"
@implementation YMECOrderTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark -

- (void)setModel:(YMECOrderDetailModel *)model {
    _model = model;
    self.refreshButton.hidden = NO;
    self.orderTypeLabel.text = @"";
    self.orderNumLabel.text = [@"订单号：" stringByAppendingString:DL_IS_NULL(model.orderNo)];
    self.orderNameLabel.text = model.goodsName;
    self.createTimeLabel.text = [@"创建订单时间: " stringByAppendingString:[TimeChangeTools timeFormatterWithTime:DL_IS_NULL(model.createTime)]];
    self.payTimeLabel.text = [@"支付订单时间: " stringByAppendingString:[TimeChangeTools timeFormatterWithTime:DL_IS_NULL(model.payTime)]];
    if ([model.payType isEqualToString:@"ZFB"]) {
        self.payTypeLabel.text = @"支付方式: 支付宝";
    } else if ([model.payType isEqualToString:@"WX"]) {
        self.payTypeLabel.text = @"支付方式: 微信";
    } else {
        self.payTypeLabel.text = @"支付方式: 余额";
        self.refreshButton.hidden = YES;
        self.orderStatusLabelRight.constant = 15;
    }
    self.orderTypeLabel.text = [NSString stringWithFormat:@"[%@]", model.goodsTypeName];
    self.orderStatusLabel.text = model.orderStatus.integerValue == 2 ? @"已支付" : @"未支付";
    self.orderStatusLabel.textColor = model.orderStatus.integerValue == 2 ? YMEC_HEX_COLOR(0x66C270) : YMEC_COLOR_RED;
    self.moneyLabel.text = [@"¥ " stringByAppendingString:DL_IS_NULL(model.goodsCost.stringValue)];
    self.refreshButton.hidden = (model.orderStatus.integerValue == 2);
    self.orderStatusLabelRight.constant = model.orderStatus.integerValue == 2 ? 15 : 50;
}

#pragma mark -

- (IBAction)refresh:(id)sender {
    // 只能刷新支付宝和微信
    NSDictionary * params = @{@"orderNo": self.model.orderNo};
    if (self.model.orderStatus.integerValue == 2) return;
    if ([self.model.payType isEqualToString:@"YU"]) return;
    if ([self.model.payType isEqualToString:@"ZFB"]) {
        [TENPaymentRequest checkAlipayResultWithParameters:params response:^(NSDictionary * _Nonnull responseObject) {
            TEN_LOG(@"%@", responseObject);
            if ([responseObject[@"content"] integerValue] == 2 && self.updateAliOrderState) {
                self.updateAliOrderState();
                [[NSNotificationCenter defaultCenter] postNotificationName:YMEC_PAY_SUCCESS_NOTIFICATION object:nil];
            }
        } failure:^(NSError * _Nonnull error) {
            TEN_LOG(@"%@", error);
        }];
//        [self postWithURL:YMEC_PAY_GET_ALI_RESULT_URL params:params success:^(NSDictionary *dict) {
//            TEN_LOG(@"%@", dict);
//            NSNumber * content = dict[@"content"];
//            if (content.integerValue == 2 && self.updateWechatOrderState) {
//                self.updateWechatOrderState();
//            }
//        } failure:^(NSError *error) {
//            TEN_LOG(@"%@", error);
//        }];
    }
    if ([self.model.payType isEqualToString:@"WX"]) {
//        [self postWithURL:YMEC_PAY_GET_WECHAT_RESULT_URL params:params success:^(NSDictionary *dict) {
//            TEN_LOG(@"%@", dict);
//            NSNumber * content = dict[@"content"];
//            if (content.integerValue == 2 && self.updateAliOrderState) {
//                self.updateAliOrderState();
//            }
//        } failure:^(NSError *error) {
//            TEN_LOG(@"%@", error);
//        }];
    }
}


@end

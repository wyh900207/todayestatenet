//
//  YMECOrderTableViewCell.h
//  YMEconomicCircle
//
//  Created by HomerLynn on 2018/6/7.
//  Copyright © 2018年 YME. All rights reserved.
//

#import "YMECBaseTableViewCell.h"
@class YMECOrderDetailModel;

@interface YMECOrderTableViewCell : YMECBaseTableViewCell

@property (weak, nonatomic) IBOutlet UILabel *orderNumLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderStatusLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *orderNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *createTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *payTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *payTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *moneyLabel;
@property (weak, nonatomic) IBOutlet UIButton *refreshButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *orderStatusLabelRight;

@property (strong, nonatomic) YMECOrderDetailModel * model;

@property (copy, nonatomic) void (^updateWechatOrderState)(void);
@property (copy, nonatomic) void (^updateAliOrderState)(void);

@end

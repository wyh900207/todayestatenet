//
//  YMECRechargeViewController.m
//  YMEconomicCircle
//
//  Created by HomerLynn on 2018/6/1.
//  Copyright © 2018年 YME. All rights reserved.
//

#import "YMECRechargeViewController.h"
#import "YMECPayPasswordPhoneView.h"
#import "TENPaymentRequest.h"
@interface YMECRechargeViewController ()

@property (weak, nonatomic) IBOutlet UIView *moneyContainerView;
@property (copy, nonatomic) NSString * currentMoney;
@property (weak, nonatomic) IBOutlet UIButton *commitBtn;

@end

@implementation YMECRechargeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupSubviews];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(paySuccess) name:YMEC_PAY_SUCCESS_NOTIFICATION object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:YMEC_PAY_SUCCESS_NOTIFICATION object:nil];
}

#pragma mark -

- (void)setupSubviews {
    self.navigationItem.title = @"充值";
    [self.moneyContainerView addSubview:self.moneyView];
    [self.moneyView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.moneyContainerView);
    }];
//    YMEC_CORNER_RADIUS(self.submitButton, 5);
    TENCornerRadius(self.submitButton, 5);
    
    [self.view addSubview:self.moneyContainerView];
    [self.view addSubview:self.commitBtn];
}

#pragma mark -

- (YMECPayPasswordPhoneView *)moneyView {
    if (!_moneyView) {
        _moneyView = [[NSBundle mainBundle] loadNibNamed:@"YMECPayPasswordPhoneView" owner:nil options:nil].firstObject;
        _moneyView.textField.keyboardType = UIKeyboardTypeNumberPad;
        _moneyView.titleLabel.text = @"金额(元)";
        _moneyView.textField.placeholder = @"请输入金额";
        __weak typeof(self) wSelf = self;
        _moneyView.textChangeAction = ^(NSString *text) {
            __strong typeof(wSelf) sSelf = wSelf;
            sSelf.currentMoney = text;
        };
    }
    return _moneyView;
}

#pragma mark -

- (IBAction)submit:(id)sender {
    if (!self.currentMoney || self.currentMoney.length <= 0 || [self.currentMoney isEqualToString:@"0"]) {
        [NSObject showHudTipStr:@"请输入充值金额"]; return;
    }
    NSDictionary * params = @{@"accountId": kAccountId,
                              @"chargeMoney": self.currentMoney,
                              @"loginToken": kLoginToken
                              };
    @weakify(self);
    [TENPaymentRequest rechargeWithParameters:params response:^(NSDictionary * _Nonnull responseObject) {
        TEN_LOG(@"%@",responseObject);
        @strongtify(self);
        if ([responseObject[@"code"] integerValue] == 1) {
            NSString *goodsId = responseObject[@"content"];
            [self charegeWith:goodsId];
        }
    } failure:^(NSError * _Nonnull error) {
        TEN_LOG(@"%@",error.description);
    }];
//    [self postWithURL:[] params:params success:^(NSDictionary *dict) {
//        TEN_LOG(@"%@", dict);
//        NSString * goodsId = dict[@"content"];
//        __strong typeof(wself) sself = wself;
//        [sself charegeWith:goodsId];
//    } failure:^(NSError *error) {
//        TEN_LOG(@"%@", error);
//    } hud:NO];
}

- (void)charegeWith:(NSString *)goodsId {
    @weakify(self);
    [[YMECPayTools share] rechargeWith:goodsId completion:^{
        @strongtify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

- (void)paySuccess {
    [self.navigationController popViewControllerAnimated:YES];
}

@end

//
//  YMECRechargeViewController.h
//  YMEconomicCircle
//
//  Created by HomerLynn on 2018/6/1.
//  Copyright © 2018年 YME. All rights reserved.
//

//#import "YMECBaseViewController.h"
@class YMECPayPasswordPhoneView;

@interface YMECRechargeViewController : TENBaseViewController

@property (strong, nonatomic) YMECPayPasswordPhoneView * moneyView;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;

@end

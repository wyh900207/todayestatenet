//
//  PolicyViewController.m
//  HomeGardenTreasure
//
//  Created by paat on 2018/8/31.
//  Copyright © 2018年 Paat. All rights reserved.
//

#import "TENFavoriteViewController.h"
#import "PolicyNavPageView.h"
#import "PageView.h"

#import "TENMessageViewController.h"
#import "TENBuildingViewController.h"
#import "TENHeaderLinesViewController.h"

@interface TENFavoriteViewController ()<PolicyNavPageViewDelegate>

@property (nonatomic, strong) TENHeaderLinesViewController *PopularVC;
@property (nonatomic, strong) TENBuildingViewController *featuredVC;
@property (nonatomic, strong) PolicyNavPageView * policyNav;
@property (nonatomic, strong) PageView * page;

@end

@implementation TENFavoriteViewController
- (TENBuildingViewController *)featuredVC
{
    if (!_featuredVC) {
        _featuredVC = [[TENBuildingViewController alloc] init];
    }
    return _featuredVC;
}

- (TENHeaderLinesViewController *)PopularVC
{
    if (!_PopularVC) {
        _PopularVC = [[TENHeaderLinesViewController alloc] init];
    }
    return _PopularVC;
}

//- (void)viewWillAppear:(BOOL)animated {
//    [super viewWillAppear:animated];
//    self.navigationController.navigationBarHidden = YES;
//}
//
//- (void)viewWillDisappear:(BOOL)animated {
//    [super viewWillDisappear:animated];
//    self.navigationController.navigationBarHidden = NO;
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"我的收藏";
    [self setupNavigation];
}

- (void)setupNavigation {
    
//    PolicyNavPageView *policyNav = [[PolicyNavPageView alloc]init];
//    policyNav.titleArray = @[@"精选",@"热门"];
//    policyNav.delegate = self;
//    [policyNav.searchBtn addTarget:self action:@selector(searchBtnClick) forControlEvents:UIControlEventTouchUpInside];
//    [self addChildViewController:policyNav];
//    policyNav.view.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);
//    [self.view addSubview:policyNav.view];
//    self.policyNav = policyNav;
//    [self.policyNav addChildViewController:self.featuredVC];
//    [self.policyNav addChildViewController:self.PopularVC];
//    [self addChildViewController:self.featuredVC WithFrameX:0];
    
    [self addChildViewController:self.featuredVC];
    [self addChildViewController:self.PopularVC];
    self.page = [[PageView alloc]initWithFrame:CGRectMake(0, TENNavigationBarHeight, kScreenWidth, kScreenHeight - TENNavigationBarHeight) WithTitleArray:@[@"楼盘",@"头条"] WithchildControllerArray:@[self.featuredVC,self.PopularVC]];
    [self.view addSubview:self.page];
}

#pragma mark- policyNavPage

- (void)slideToViewAtIndex:(int)index
{
    switch (index) {
        case 1:
            [self addChildViewController:self.PopularVC WithFrameX:1];
            break;
        default:
            break;
    }
}
- (void) addChildViewController:(UIViewController *)childController WithFrameX:(int)X{
    [self.policyNav.childScrollView addSubview:childController.view];
    childController.view.frame = CGRectMake(X * kScreenWidth, 0, kScreenWidth, self.policyNav.childScrollView.frame.size.height);
}

@end

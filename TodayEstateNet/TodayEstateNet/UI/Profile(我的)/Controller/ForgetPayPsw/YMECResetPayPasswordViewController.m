//
//  YMECResetPayPasswordViewController.m
//  YMEconomicCircle
//
//  Created by HomerLynn on 2018/6/13.
//  Copyright © 2018年 YME. All rights reserved.
//

#import "YMECResetPayPasswordViewController.h"
#import "YMECPayPasswordPhoneView.h"
#import "YMECPayPasswordSMSCodeView.h"
#import "YMECPictureCodeViewController.h"

@interface YMECResetPayPasswordViewController ()

@property (strong, nonatomic) YMECPayPasswordPhoneView * phoneView;
@property (strong, nonatomic) YMECPayPasswordSMSCodeView * smsCodeView;
@property (strong, nonatomic) YMECPayPasswordPhoneView * passwordView;
@property (strong, nonatomic) YMECPayPasswordPhoneView * rePasswordView;
@property (strong, nonatomic) UIButton * submitButton;

@property (copy, nonatomic) NSString * currentSmsCode;
@property (copy, nonatomic) NSString * currentPassword;
@property (copy, nonatomic) NSString * currentRePassword;
@property (copy, nonatomic) NSString * currentMessageCodeText;

@end

@implementation YMECResetPayPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupSubviews];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
- (void)setupSubviews {
    self.navigationItem.title = @"重置支付密码";
    
    // Left Navigation Bar
    UIButton * backbutton = [UIButton new];
    [backbutton setTitle:@"返回" forState:0];
    [backbutton setTitleColor:YMEC_COLOR_BLACK forState:0];
    [backbutton addTarget:self action:@selector(dismissController) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem * leftItem = [[UIBarButtonItem alloc] initWithCustomView:backbutton];
    self.navigationItem.leftBarButtonItem = leftItem;
    
    [self.view addSubview:self.phoneView];
    [self.view addSubview:self.smsCodeView];
    [self.view addSubview:self.passwordView];
    [self.view addSubview:self.rePasswordView];
    [self.view addSubview:self.submitButton];
    
    [self.phoneView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.left.equalTo(self.view).offset(10);
        make.right.equalTo(self.view).offset(-10);
        make.height.equalTo(@40);
    }];
    [self.smsCodeView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.phoneView.mas_bottom).offset(10);
        make.left.right.height.equalTo(self.phoneView);
    }];
    [self.passwordView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.smsCodeView.mas_bottom).offset(10);
        make.left.right.height.equalTo(self.smsCodeView);
    }];
    [self.rePasswordView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.passwordView.mas_bottom).offset(10);
        make.left.right.height.equalTo(self.passwordView);
    }];
    [self.submitButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(self.rePasswordView.mas_bottom).offset(10);
        make.left.right.height.equalTo(self.rePasswordView);
    }];
}

#pragma mark -
- (YMECPayPasswordPhoneView *)phoneView {
    if (!_phoneView) {
        _phoneView = [[NSBundle mainBundle] loadNibNamed:@"YMECPayPasswordPhoneView" owner:nil options:nil].firstObject;
        _phoneView.textField.text = self.phone;
        _phoneView.textField.userInteractionEnabled = NO;
    }
    return _phoneView;
}

- (YMECPayPasswordSMSCodeView *)smsCodeView {
    if (!_smsCodeView) {
        _smsCodeView = [[NSBundle mainBundle] loadNibNamed:@"YMECPayPasswordSMSCodeView" owner:nil options:nil].firstObject;
        _smsCodeView.smsCodeTextField.keyboardType = UIKeyboardTypeNumberPad;
        __weak typeof(self) wSelf = self;
        _smsCodeView.sendButtonAction = ^{
            __strong typeof(wSelf) sSelf = wSelf;
            [sSelf showPictureCodeController];
        };
        _smsCodeView.smsTextChangeAction = ^(NSString *text) {
            __strong typeof(wSelf) sSelf = wSelf;
            sSelf.currentSmsCode = text;
        };
    }
    return _smsCodeView;
}

- (YMECPayPasswordPhoneView *)passwordView {
    if (!_passwordView) {
        _passwordView = [[NSBundle mainBundle] loadNibNamed:@"YMECPayPasswordPhoneView" owner:nil options:nil].firstObject;
        _passwordView.textField.placeholder = @"请输入6位支付密码";
        _passwordView.textField.keyboardType = UIKeyboardTypeNumberPad;
        _passwordView.textField.secureTextEntry = YES;
        __weak typeof(self) wSelf = self;
        _passwordView.textChangeAction = ^(NSString *text) {
            __strong typeof(wSelf) sSelf = wSelf;
            sSelf.currentPassword = text;
        };
    }
    return _passwordView;
}

- (YMECPayPasswordPhoneView *)rePasswordView {
    if (!_rePasswordView) {
        _rePasswordView = [[NSBundle mainBundle] loadNibNamed:@"YMECPayPasswordPhoneView" owner:nil options:nil].firstObject;
        _rePasswordView.textField.placeholder = @"请输入6位支付密码";
        _rePasswordView.textField.keyboardType = UIKeyboardTypeNumberPad;
        _rePasswordView.textField.secureTextEntry = YES;
        __weak typeof(self) wSelf = self;
        _rePasswordView.textChangeAction = ^(NSString *text) {
            __strong typeof(wSelf) sSelf = wSelf;
            sSelf.currentRePassword = text;
        };
    }
    return _rePasswordView;
}

- (UIButton *)submitButton {
    if (!_submitButton) {
        _submitButton = [UIButton new];
        _submitButton.layer.cornerRadius = 5;
        _submitButton.layer.masksToBounds = YES;
        _submitButton.backgroundColor = YMEC_COLOR_RED;
        [_submitButton setTitle:@"确认" forState:0];
        [_submitButton setTitleColor:YMEC_COLOR_WHITE forState:0];
        [_submitButton addTarget:self action:@selector(submit) forControlEvents:UIControlEventTouchUpInside];
    }
    return _submitButton;
}

#pragma mark -

- (void)submit {
    if (!self.currentSmsCode || self.currentSmsCode.length <= 0) { [NSObject showHudTipStr:@"请输入验证码"]; return; }
    if (!self.currentPassword || self.currentPassword.length <= 0) { [NSObject showHudTipStr:@"请输入密码"]; return; }
    if (!self.currentRePassword || self.currentRePassword.length <= 0) { [NSObject showHudTipStr:@"请输入确认密码"]; return; }
    if (self.currentPassword.length != 6) { [NSObject showHudTipStr:@"请输入6位密码"]; return; }
    if (self.currentRePassword.length != 6) { [NSObject showHudTipStr:@"请输入6位密码"]; return; }
    if (![self.currentPassword isEqualToString:self.currentRePassword]) { [NSObject showHudTipStr:@"2次密码不一致"]; return; }
    NSDictionary * params = @{@"accountId": kAccountId,
                              @"loginToken": kLoginToken,
//                              @"password": [self.currentPassword md5String],
                              @"vcode": self.currentSmsCode
                              };
    __weak typeof(self) wSelf = self;
//    [self postWithURL:YMEC_LOGIN_SET_PAY_PASSWORD_URL params:params success:^(NSDictionary *dict) {
//        __strong typeof(wSelf) sSelf = wSelf;
//        TEN_LOG(@"%@", dict);
//        [NSObject showHudTipStr:@"重置支付密码成功"];
//        [sSelf dismissViewControllerAnimated:YES completion:nil];
//    } failure:^(NSError *error) {
//        TEN_LOG(@"%@", error);
//    }];
}

- (void)showPictureCodeController {
    YMECPictureCodeViewController *pictureCodeController = [[YMECPictureCodeViewController alloc] initWithNibName:@"YMECPictureCodeViewController" bundle:nil];
    pictureCodeController.phoneText = TEN_USER_PHONE;
    
    __weak typeof(self) weakSelf = self;
    pictureCodeController.commitHandle = ^{
        __strong typeof(weakSelf) self = weakSelf;
        [self getSMSCode];
    };
    pictureCodeController.textDidChangeHandle = ^(NSString *text) {
        __strong typeof(weakSelf) self = weakSelf;
        self.currentMessageCodeText = text;
    };
    
    self.definesPresentationContext = YES;
    pictureCodeController.view.backgroundColor = [UIColor clearColor];
    pictureCodeController.modalPresentationStyle = UIModalPresentationOverCurrentContext;
    
    [self presentViewController:pictureCodeController animated:YES completion:nil];
}

- (void)getSMSCode {
    NSDictionary *params = @{@"country": @"86",
                             @"phone": TEN_USER_PHONE,
                             @"verificationCode": self.currentMessageCodeText};
    __weak typeof(self) weakSelf = self;
    
//    [self postWithURL:YMEC_LOGIN_SMSCODE_URL params:params success:^(NSDictionary *dict) {
//        NSLog(@"%@", dict);
//        NSLog(@"%@", dict[@"msg"]);
//        __strong typeof(weakSelf) self = weakSelf;
//        [self dismissViewControllerAnimated:YES completion:nil];
//    } failure:^(NSError *error) {
//        NSLog(@"%@", error);
//    }];
}

- (void)dismissController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end

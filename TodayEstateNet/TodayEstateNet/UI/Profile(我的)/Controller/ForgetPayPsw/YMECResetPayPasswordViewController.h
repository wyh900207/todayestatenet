//
//  YMECResetPayPasswordViewController.h
//  YMEconomicCircle
//
//  Created by HomerLynn on 2018/6/13.
//  Copyright © 2018年 YME. All rights reserved.
//

//#import "YMECBaseViewController.h"

@interface YMECResetPayPasswordViewController : TENBaseViewController

@property (copy, nonatomic) NSString * phone;

@end

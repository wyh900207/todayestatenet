//
//  TENSettingsViewController.m
//  TodayEstateNet
//
//  Created by  HomerLynn on 2018/11/15.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "TENSettingsViewController.h"
#import "TENTableView.h"
#import <TENUtilsHeader.h>

@interface TENSettingsViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) TENTableView *tableView;
@property (nonatomic, strong) NSArray *cellTitlesArray;
@property (nonatomic, strong) UIButton *logoutButton;

@end

@implementation TENSettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"设置";
    
    [self setupSubviews];
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = YES;
}

#pragma mark - UI
- (void)setupSubviews {
    [self.view addSubview:self.tableView];
    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
        if (TEN_iOS_10_OR_LATER) {
            make.edges.equalTo(self.view);
        }
        else {
            make.left.bottom.right.equalTo(self.view);
            make.top.offset(TENNavigationBarHeight);
        }
    }];
}

#pragma mark - Private methods

- (void)logout {
    [[DLUserManager shareManager] logout];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.cellTitlesArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.textLabel.text = self.cellTitlesArray[indexPath.row];
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0: {
            
        }
            break;
        case 1: {
            
        }
            break;
        case 2: {
            
        }
            break;
        case 3: {
            
        }
            break;
        case 4: {
            
        }
            break;
        case 5: {
            
        }
            break;
        case 6: {
            
        }
            break;
        case 7: {
            NSString *uri = @"http://h5.jrfw360.com/aboutUs";
            TENBaseWebViewController *webController = [TENBaseWebViewController new];
            webController.uri = uri;
            [webController load];
            [self.navigationController pushViewController:webController animated:YES];
        }
            break;
        case 8: {
            
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - Getter

- (TENTableView *)tableView {
    if (!_tableView) {
        _tableView = [TENTableView new];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.rowHeight = 40;
        _tableView.tableFooterView = self.logoutButton;
        [_tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
    }
    return _tableView;
}

- (NSArray *)cellTitlesArray {
    return @[@"推送设置", @"权限控制", @"更换手机号", @"免责说明", @"版本更新", @"应用推荐", @"建议反馈", @"关于我们", @"清除缓存"];
}

- (UIButton *)logoutButton {
    if (!_logoutButton) {
        _logoutButton = [UIButton new];
        _logoutButton.frame = CGRectMake(0, 0, self.tableView.bounds.size.width, 40);
        [_logoutButton setTitle:@"退出登录" forState:UIControlStateNormal];
        [_logoutButton setValue:TENFont14 forKeyPath:@"titleLabel.font"];
        [_logoutButton setTitleColor:TENHexColor(@"D14E33") forState:UIControlStateNormal];
        [_logoutButton addTarget:self action:@selector(logout) forControlEvents:UIControlEventTouchUpInside];
    }
    return _logoutButton;
}

@end

//
//  YMECWalletViewController.m
//  YMEconomicCircle
//
//  Created by HomerLynn on 2018/5/31.
//  Copyright © 2018年 YME. All rights reserved.
//

#import "YMECWalletViewController.h"
#import "YMECWalletTableHeaderView.h"
#import "YMEBalanceViewController.h"
#import "YMECSetPayPasswordViewController.h"
#import "YMECOrdersViewController.h"
#import "YMECTradesViewController.h"
#import "TENPaymentRequest.h"
@interface YMECWalletViewController () <UITableViewDelegate, UITableViewDataSource, YMECTableViewRefreshProtocol>

@property (nonatomic, strong) YMECWalletTableHeaderView * walletTableHeaderView;
@property (nonatomic, strong) NSMutableArray<NSString *> *cellTitleArray;

@end

@implementation YMECWalletViewController
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:YMEC_PAY_SUCCESS_NOTIFICATION object:nil];
}
- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupSubviews];
    [self fetchBalance];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchBalance) name:YMEC_PAY_SUCCESS_NOTIFICATION object:nil];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -

- (void)setupSubviews {
    self.navigationItem.title = @"钱包";
    self.walletTableView.tableHeaderView = self.walletTableHeaderView;
    [self.walletTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"ymecWalletCell"];
    self.walletTableView.refreshType = TENTableViewRefreshTypeHeader;
    self.walletTableView.refreshDelegate = self;
    [self.view addSubview:self.walletTableView];
}

#pragma mark - UITableViewDelegate, UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.cellTitleArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 15;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"ymecWalletCell" forIndexPath:indexPath];
    NSString * cellText = self.cellTitleArray[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.text = cellText;
    cell.textLabel.textColor = YMEC_COLOR_TEXT_GRAY;
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        YMECOrdersViewController * ordersController = [[YMECOrdersViewController alloc] initWithNibName:@"YMECOrdersViewController" bundle:nil];
        [self.navigationController pushViewController:ordersController animated:YES];
    } else if (indexPath.row == 1) {
        YMECTradesViewController * tradesController = [[YMECTradesViewController alloc] initWithNibName:@"YMECTradesViewController" bundle:nil];
        [self.navigationController pushViewController:tradesController animated:YES];
    } else {
        YMECSetPayPasswordViewController * setPayPasswordController = [[YMECSetPayPasswordViewController alloc] initWithNibName:@"YMECSetPayPasswordViewController" bundle:nil];
        setPayPasswordController.phone = TEN_USER_PHONE;
        TENNavigationViewController * navController = [[TENNavigationViewController alloc] initWithRootViewController:setPayPasswordController];
        [self presentViewController:navController animated:YES completion:nil];
    }
}

#pragma mark -

- (void)headerWillRefresh {
    [self fetchBalance];
}

#pragma mark -

/// 获取余额
- (void)fetchBalance {
    NSDictionary * params = @{@"accountId": kAccountId,
                              @"pageNo": @"1",
                              @"pageSize": @"20"
                              };
    @weakify(self)
    [TENPaymentRequest getBalanceAndListParameters:params response:^(NSDictionary * _Nonnull responseObject) {
        @strongtify(self)
        NSDictionary * content = responseObject[@"content"];
        NSNumber * balance = content[@"balance"];
        NSString * balanceString = [NSString stringWithFormat:@"%.2f", balance.doubleValue];
        [self.walletTableHeaderView.balanceLabel setTitle:balanceString forState:0];
        NSNumber * pwdSet = content[@"payPwdSet"];
        if (pwdSet.integerValue == 0) {
            YMECSetPayPasswordViewController * setPayPasswordController = [[YMECSetPayPasswordViewController alloc] initWithNibName:@"YMECSetPayPasswordViewController" bundle:nil];
            setPayPasswordController.phone = TEN_USER_PHONE;
            TENNavigationViewController * navController = [[TENNavigationViewController alloc] initWithRootViewController:setPayPasswordController];
            [self presentViewController:navController animated:YES completion:nil];
        }
        [self.walletTableView endRefreshWithError];
    } failure:^(NSError * _Nonnull error) {
        TEN_LOG(@"%@", error);
        [self.walletTableView endRefreshWithError];
    }];
}

#pragma mark - Getter & Setter

- (YMECWalletTableHeaderView *)walletTableHeaderView {
    if (!_walletTableHeaderView) {
        _walletTableHeaderView = [[NSBundle mainBundle] loadNibNamed:@"YMECWalletTableHeaderView" owner:nil options:nil].firstObject;
        _walletTableHeaderView.frame = CGRectMake(0, 0, TENScreenWidth, 150);
        @weakify(self);
        _walletTableHeaderView.balanceButtonAction = ^{
            @strongtify(self);
            YMEBalanceViewController * balanceController = [[YMEBalanceViewController alloc] initWithNibName:@"YMEBalanceViewController" bundle:nil];
            [self.navigationController pushViewController:balanceController animated:YES];
        };
        
    }
    return _walletTableHeaderView;
}

- (NSMutableArray<NSString *> *)cellTitleArray {
    if (_cellTitleArray == nil) {
        _cellTitleArray = [@[@"订单信息", @"交易明细", @"修改支付密码"] mutableCopy];
    }
    return _cellTitleArray;
}

@end

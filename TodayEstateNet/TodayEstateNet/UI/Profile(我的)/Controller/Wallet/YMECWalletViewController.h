//
//  YMECWalletViewController.h
//  YMEconomicCircle
//
//  Created by HomerLynn on 2018/5/31.
//  Copyright © 2018年 YME. All rights reserved.
//

//#import "YMECBaseViewController.h"

@interface YMECWalletViewController : TENBaseViewController

@property (weak, nonatomic) IBOutlet TENTableView *walletTableView;

@end

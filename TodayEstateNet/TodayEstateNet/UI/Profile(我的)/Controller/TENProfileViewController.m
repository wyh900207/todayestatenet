//
//  TENProfileViewController.m
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/9/11.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENProfileViewController.h"
#import "TENUtilsHeader.h"
#import "TENProfileHeaderView.h"
#import "TENTableView.h"
#import "TENProfileCell.h"
#import "TENSettingsViewController.h"
#import "TENProfileSectionOneCell.h"
#import "TENProfileSectionSecondCell.h"
#import "TENProfileToosCell.h"
#import "TENAttendanceViewController.h"
#import "TENMineRequest.h"
#import "YMECWalletViewController.h"
#import "TENFavoriteViewController.h"

@interface TENProfileViewController () <UITableViewDelegate, UITableViewDataSource, FuncBtnClickedDelegate, TENProfileHeaderViewDelegate>

@property (nonatomic, strong) UIView      *contView;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) TENProfileHeaderView *tableHeaderView;
@property (nonatomic, strong) NSMutableArray *cellTitlesArray;
@property (nonatomic, strong) NSMutableArray *cellIconsArray;

@end

@implementation TENProfileViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = NO;
    [self customNavigationBar];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginSuccessWith:) name:YMEC_LOGIN_SUCCESS_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logoutSuccessWith:) name:YMEC_LOGOUT_SUCCESS_NOTIFICATION object:nil];
    
    if ([[DLUserManager shareManager] loginState]) {
        [self.tableHeaderView.headerIconImageView sd_setImageWithURL:[NSURL URLWithString:TEN_UD_VALUE(kUserAvatar)] forState:0];
        self.tableHeaderView.usernameLabel.text = TEN_UD_VALUE(kUserName);
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];

    self.edgesForExtendedLayout = UIRectEdgeNone;

//
//    [self customTableviewContentSize];
    [self backGroundView];
//    [self.view addSubview:self.tableView];
//    [self.tableView mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.edges.equalTo(self.view);
//    }];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:YMEC_LOGIN_SUCCESS_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:YMEC_LOGOUT_SUCCESS_NOTIFICATION object:nil];
}

#pragma mark -

-(void)backGroundView
{
    CGRect statusRect = [[UIApplication sharedApplication] statusBarFrame];
    CGRect navRect = self.navigationController.navigationBar.frame;
    CGFloat navHeight = navRect.size.height + statusRect.size.height;
    self.contView = [UIView new];
    self.contView.frame =CGRectMake(0, -navHeight, self.view.frame.size.width, self.view.frame.size.height-self.tabBarController.tabBar.frame.size.height);
    self.contView.backgroundColor = [UIColor redColor];
    [self.view addSubview:self.contView];
    [self.contView addSubview:self.tableView];
    if (@available(iOS 11.0, *)) {
        self.tableView.contentInsetAdjustmentBehavior = UIScrollViewContentInsetAdjustmentNever;
    } else {
        self.automaticallyAdjustsScrollViewInsets = NO;
    }
}
- (void)customTableviewContentSize {
//    CGRect statusRect = [[UIApplication sharedApplication] statusBarFrame];
//    CGRect navRect = self.navigationController.navigationBar.frame;
//    CGFloat navHeight = navRect.size.height + statusRect.size.height;
//    self.tableView.contentInset = UIEdgeInsetsMake(-navHeight, 0, 0, 0);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
#pragma mark - UI
- (void)customNavigationBar {
    // Custom tint color   TENHexClearColor
    [self.navigationController.navigationBar DJSetBackgroundColor:TENHexClearColor];
    self.navigationController.navigationBar.translucent = YES;
    [self.navigationController.navigationBar setBackgroundImage:[[UIImage alloc] init] forBarMetrics:UIBarMetricsDefault];
    [self.navigationController.navigationBar setShadowImage:[[UIImage alloc] init]];
    // Custom title text color
    self.navigationController.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: TENHexColor(TENWhiteColor)};
    // Right bar button item
    UIButton *button=[[UIButton alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    [button setImage:TENImage(@"ten-profile-settings.png") forState:UIControlStateNormal];
    [button addTarget:self action:@selector(pushToSettingsViewController) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightitem = [[UIBarButtonItem alloc]initWithCustomView:button];
    self.navigationItem.rightBarButtonItem = rightitem;
    self.navigationItem.title = @"个人中心";
}

#pragma mark - UITableViewDelegate & UITableViewDatasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.cellTitlesArray.count + 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TENProfileCell *profileCell = [tableView dequeueReusableCellWithIdentifier:@"TENProfileCell" forIndexPath:indexPath];
    TENProfileSectionOneCell *sectionOneCell = [tableView dequeueReusableCellWithIdentifier:@"TENProfileSectionOneCell" forIndexPath:indexPath];
    TENProfileSectionSecondCell *sectionSecondCell = [tableView dequeueReusableCellWithIdentifier:@"TENProfileSectionSecondCell" forIndexPath:indexPath];
    TENProfileToosCell *toolsCell = [tableView dequeueReusableCellWithIdentifier:@"TENProfileToosCell" forIndexPath:indexPath];
    
    switch (indexPath.row) {
        case 0: {
            sectionOneCell.selectionStyle = UITableViewCellSelectionStyleNone;
            sectionOneCell.delegate = self;
            return sectionOneCell;
        }
        break;
        case 1: {
            sectionSecondCell.selectionStyle = UITableViewCellSelectionStyleNone;
            @weakify(self);
            sectionSecondCell.buttonHandler = ^(NSInteger index) {
                @strongtify(self);
                [self secondCellHandlerWith:index];
            };
            return sectionSecondCell;
        }
        break;
        case 2: {
            toolsCell.selectionStyle = UITableViewCellSelectionStyleNone;
            @weakify(self);
            toolsCell.itemClickHandler = ^(NSInteger index) {
                @strongtify(self);
                [self clickToolsCellWith:index];
            };
            return toolsCell;
        }
        
        default: {
            profileCell.iconImagView.image = [UIImage imageNamed:self.cellIconsArray[indexPath.row - 3]];
            profileCell.titleLabel.text = self.cellTitlesArray[indexPath.row - 3];
            profileCell.selectionStyle = UITableViewCellSelectionStyleNone;

            return profileCell;
        }
        break;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    TENBaseWebViewController *webviewController = [TENBaseWebViewController new];
//    NSString *contentURL = [[TENUrlManager share] url:@"house" detail:@"house-detail"];
    NSString *contentURL = @"http://h5.jrfw360.com/demo";
    webviewController.uri = contentURL;
//    webviewController.uri = [contentURL stringByAppendingPathComponent:@"20"];
    [webviewController load];
//    [webviewController loadWithPath:[[NSBundle mainBundle] pathForResource:@"uaDemo" ofType:@"html"]];
    
//    [webviewController loadWithPath:[[NSBundle mainBundle] pathForResource:@"index" ofType:@"html"]];
    webviewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:webviewController animated:YES];
}

#pragma mark - FuncBtnClickedDelegate

- (void)signIn {
    TENAttendanceViewController *attendanceVC = [TENAttendanceViewController new];
    [self.navigationController pushViewController:attendanceVC animated:YES];
}

- (void)inviteFriend {
    [UMSocialUIManager setPreDefinePlatforms:@[@(UMSocialPlatformType_WechatSession),@(UMSocialPlatformType_WechatTimeLine)]];
    //显示分享面板
    [UMSocialUIManager showShareMenuViewInWindowWithPlatformSelectionBlock:^(UMSocialPlatformType platformType, NSDictionary *userInfo) {
        // 根据获取的platformType确定所选平台进行下一步操作
        [self shareImageToPlatformType:platformType];
    }];
}

#pragma mark - TENProfileHeaderViewDelegate

- (void)userinfo {
    [[DLUserManager shareManager] actionAfterLogin:^{
        // TODO: - 点击用户头像
    } currentController:self];
}

#pragma mark - Private methods

- (void)secondCellHandlerWith:(NSInteger)index {
    switch (index) {
        case 0: {
            // 钱包
            [[DLUserManager shareManager] actionAfterLogin:^{
                YMECWalletViewController *walletVC = [YMECWalletViewController new];
                [self.navigationController pushViewController:walletVC animated:YES];
            } currentController:self];
        }
            break;
        case 1: {
            // 优惠
            NSString *yixiang = [[TENUrlManager share] url:@"profile" detail:@"youhui"];
            TENBaseWebViewController *webviewController = [TENBaseWebViewController new];
            webviewController.uri = yixiang;
            [webviewController load];
            webviewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:webviewController animated:YES];
        }
            break;
        case 2: {
            // 权益
        }
            break;
        case 3: {
            // 收藏
            NSString *yixiang = [[TENUrlManager share] url:@"profile" detail:@"favorite"];
            TENBaseWebViewController *webviewController = [TENBaseWebViewController new];
            webviewController.uri = yixiang;
            [webviewController load];
            webviewController.hidesBottomBarWhenPushed = YES;
//            TENFavoriteViewController *webviewController = [TENFavoriteViewController new];
            [self.navigationController pushViewController:webviewController animated:YES];
        }
            break;
        case 4: {
            // 订阅
            NSString *yixiang = [[TENUrlManager share] url:@"profile" detail:@"subscribe"];
            TENBaseWebViewController *webviewController = [TENBaseWebViewController new];
            webviewController.uri = yixiang;
            [webviewController load];
            webviewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:webviewController animated:YES];
        }
            break;
        case 5: {
            // 足迹
            NSString *yixiang = [[TENUrlManager share] url:@"profile" detail:@"history"];
            TENBaseWebViewController *webviewController = [TENBaseWebViewController new];
            webviewController.uri = yixiang;
            [webviewController load];
            webviewController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:webviewController animated:YES];
        }
            break;
            
        default:
            break;
    }
}

//分享
- (void)shareImageToPlatformType:(UMSocialPlatformType)platformType
{
    [TENMineRequest fetchMineSharePicWithAccountId:kAccountId Success:^(NSDictionary * _Nonnull response) {
        TEN_LOG(@"%@",response);
        if ([response[@"code"] integerValue] == 1) {
            NSString *imageStr = response[@"content"];
            //创建分享消息对象
            UMSocialMessageObject *messageObject = [UMSocialMessageObject messageObject];
            
            //创建图片内容对象
            UMShareImageObject *shareObject = [[UMShareImageObject alloc] init];
            [shareObject setShareImage:imageStr];
            //分享消息对象设置分享内容对象
            messageObject.shareObject = shareObject;
            //调用分享接口
            [[UMSocialManager defaultManager] shareToPlatform:platformType messageObject:messageObject currentViewController:self completion:^(id data, NSError *error) {
                if (error) {
                    NSLog(@"************Share fail with error %@*********",error);
                }else{
                    NSLog(@"response data is %@",data);
                }
            }];
        }
    } failure:^(NSError * _Nonnull error) {
        TEN_LOG(@"%@",error.description);
    }];
}

- (void)pushToSettingsViewController {
    TENSettingsViewController *settingsViewController = [TENSettingsViewController new];
    [self.navigationController pushViewController:settingsViewController animated:YES];
}

- (void)clickToolsCellWith:(NSInteger)index {
    TENBaseWebViewController *webviewController = [TENBaseWebViewController new];
    
    // 贷款计算器
    NSString *loanCalculatorUri = [[TENUrlManager share] url:@"profile" detail:@"loan-calculator"];
    // 税费计算器
    NSString *taxationCoculatorUri = [[TENUrlManager share] url:@"profile" detail:@"taxation-calculator"];
    // 购房资格测试
    NSString *buyHouseTest = [[TENUrlManager share] url:@"profile" detail:@"buy-house-test"];
    // 百问百科
    NSString *baiwen = [[TENUrlManager share] url:@"profile" detail:@"bai-wen-bai-ke"];
    // 意向找房
    NSString *yixiang = [[TENUrlManager share] url:@"profile" detail:@"yi-xiang-zhao-fang"];
    
    switch (index) {
        case 0:
            webviewController.uri = loanCalculatorUri;
            break;
        case 1:
            webviewController.uri = taxationCoculatorUri;
            break;
        case 2:
            webviewController.uri = buyHouseTest;
            break;
        case 3:
            webviewController.uri = baiwen;
            break;
        case 4:
            webviewController.uri = yixiang;
            break;
            
        default:
            break;
    }
    
    if (webviewController.uri) {
        [webviewController load];
        webviewController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:webviewController animated:YES];
    }
}

- (void)loginSuccessWith:(NSNotification *)notification {
    NSString *iconUri = TEN_UD_VALUE(kUserAvatar);
    NSURL *url = [NSURL URLWithString:iconUri];
    [self.tableHeaderView.headerIconImageView sd_setImageWithURL:url forState:0];
    self.tableHeaderView.usernameLabel.text = TEN_UD_VALUE(kUserName);
}

- (void)logoutSuccessWith:(NSNotification *)notification {
    [self.tableHeaderView.headerIconImageView setImage:TENImage(@"ten-profile-userheader") forState:0];
    self.tableHeaderView.usernameLabel.text = @"未登录";
}

#pragma mark - Getter
- (UITableView *)tableView {
    if (!_tableView) {
        _tableView = [[TENTableView alloc]initWithFrame: CGRectMake(0, 0, self.view.frame.size.width, self.contView.frame.size.height) style:UITableViewStylePlain];
        _tableView.delegate = self;
        _tableView.dataSource = self;
        _tableView.showsVerticalScrollIndicator = NO;
        _tableView.tableHeaderView = self.tableHeaderView;
        _tableView.separatorColor = [UIColor clearColor];
        [_tableView registerClass:[TENProfileCell class] forCellReuseIdentifier:@"TENProfileCell"];
        [_tableView registerNib:[UINib nibWithNibName:@"TENProfileCell" bundle:nil] forCellReuseIdentifier:@"TENProfileCell"];
        [_tableView registerNib:[UINib nibWithNibName:@"TENProfileSectionOneCell" bundle:nil] forCellReuseIdentifier:@"TENProfileSectionOneCell"];
        [_tableView registerNib:[UINib nibWithNibName:@"TENProfileSectionSecondCell" bundle:nil] forCellReuseIdentifier:@"TENProfileSectionSecondCell"];
        [_tableView registerNib:[UINib nibWithNibName:@"TENProfileToosCell" bundle:nil] forCellReuseIdentifier:@"TENProfileToosCell"];
    }
    return _tableView;
}

- (TENProfileHeaderView *)tableHeaderView {
    if (!_tableHeaderView) {
        CGFloat width = self.view.bounds.size.width;
        CGFloat height = width * 0.72 - 60;
        _tableHeaderView = [TENProfileHeaderView new]; //[TENProfileHeaderView dj_viewFromeXib];
        _tableHeaderView.frame = CGRectMake(0, 0, width, height);
    }
    return _tableHeaderView;
}

- (NSMutableArray *)cellTitlesArray {
    if (!_cellTitlesArray) {
        _cellTitlesArray = [@[@"我的奖励（佣金）",
                              @"我的客户",
                              @"我的好友",
                              @"看房记录"] mutableCopy];
    }
    return _cellTitlesArray;
}

- (NSMutableArray *)cellIconsArray {
    if (!_cellIconsArray) {
        _cellIconsArray = [@[@"ten-my-ward.png",
                             @"ten-my-customer.png",
                             @"ten-my-friend.png",
                             @"ten-my-history.png"] mutableCopy];
    }
    return _cellIconsArray;
}

@end

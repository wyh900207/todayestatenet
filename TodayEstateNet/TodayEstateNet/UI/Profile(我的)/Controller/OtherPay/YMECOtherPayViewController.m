//
//  YMECOtherPayViewController.m
//  YMEconomicCircle
//
//  Created by HomerLynn on 2018/6/4.
//  Copyright © 2018年 YME. All rights reserved.
//

#import "YMECOtherPayViewController.h"
#import "UIView+CornerRaidus.h"
@interface YMECOtherPayViewController ()

@end

@implementation YMECOtherPayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    TENCornerRadius(self.wechatPayButton, 5);
    TENCornerRadius(self.aliPayButton, 5);
    [self.view addSubview:self.wechatPayButton];
    [self.view addSubview:self.aliPayButton];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -

- (IBAction)wechatPay:(id)sender {
    if (self.wechaMethod) {
        self.wechaMethod();
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)aliPay:(id)sender {
    if (self.aliMethod) {
        self.aliMethod();
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark -

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if (self.cancleMethod) {
        self.cancleMethod();
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end

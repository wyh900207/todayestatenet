//
//  YMECOtherPayViewController.h
//  YMEconomicCircle
//
//  Created by HomerLynn on 2018/6/4.
//  Copyright © 2018年 YME. All rights reserved.
//

//#import "YMECBaseViewController.h"
#import "TENBaseViewController.h"
@interface YMECOtherPayViewController : TENBaseViewController
@property (weak, nonatomic) IBOutlet UIButton *wechatPayButton;
@property (weak, nonatomic) IBOutlet UIButton *aliPayButton;
@property (copy, nonatomic) void (^wechaMethod)(void);
@property (copy, nonatomic) void (^aliMethod)(void);
@property (copy, nonatomic) void (^cancleMethod)(void);

@end

//
//  YMECTradesViewController.m
//  YMEconomicCircle
//
//  Created by HomerLynn on 2018/6/8.
//  Copyright © 2018年 YME. All rights reserved.
//

#import "YMECTradesViewController.h"
#import "YMECTradeTableViewCell.h"
#import "YMECTradeModel.h"
#import "TENPaymentRequest.h"
@interface YMECTradesViewController () <UITableViewDelegate, UITableViewDataSource,YMECTableViewRefreshProtocol>

@property (weak, nonatomic) IBOutlet TENTableView *tradesTableView;

@end

@implementation YMECTradesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupSubviews];
    [self fetchTradesWith:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -

- (void)setupSubviews {
    self.navigationItem.title = @"交易明细";
    [self.tradesTableView registerNib:[UINib nibWithNibName:@"YMECTradeTableViewCell" bundle:nil] forCellReuseIdentifier:@"tradesTableViewCell"];
    self.tradesTableView.separatorColor = YMEC_COLOR_CLEAR;
    self.tradesTableView.rowHeight = 70;
    self.tradesTableView.estimatedRowHeight = 70;
    self.tradesTableView.refreshType = TENTableViewRefreshTypeBoth;
    self.tradesTableView.refreshDelegate = self;
    [self.contentView addSubview:self.tradesTableView];
}

#pragma mark - UITableViewDelegate, UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.tradesTableView.dataSourceArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    YMECTradeTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"tradesTableViewCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.model = self.tradesTableView.dataSourceArray[indexPath.row];
    
    return cell;
}

#pragma mark - YMECTableViewRefreshProtocol

- (void)headerWillRefresh {
    [self fetchTradesWith:YES];
}

- (void)footerWillRefresh {
    [self fetchTradesWith:NO];
}

#pragma mark -

- (void)fetchTradesWith:(BOOL)isHeaderFresh {

    NSDictionary * params = @{@"accountId": kAccountId,
                              @"loginToken": kLoginToken,
                              @"pageNo": [NSString stringWithFormat:@"%lu", self.tradesTableView.pageCount],
                              @"pageSize": [NSString stringWithFormat:@"%lu", self.tradesTableView.pageSize],
                              @"platform": @""
                              };
    [TENPaymentRequest getBalanceAndListParameters:params response:^(NSDictionary * _Nonnull responseObject) {
        TEN_LOG(@"%@", responseObject);
        YMECTradeModel * ordersModel = [YMECTradeModel mj_objectWithKeyValues:responseObject[@"content"]];
        if (isHeaderFresh) {
                [self.tradesTableView endHeaderRefreshWith:ordersModel.payments];
            } else {
                [self.tradesTableView endFooterRefreshWith:ordersModel.payments];
            }
            [self.tradesTableView reloadData];
    } failure:^(NSError * _Nonnull error) {
        TEN_LOG(@"%@", error);
    }];
//    [self postWithURL:YMEC_WALLET_BALANCE_URL params:params success:^(NSDictionary *dict) {
//        TEN_LOG(@"%@", dict);
//        YMECTradeModel * ordersModel = [YMECTradeModel mj_objectWithKeyValues:dict[@"content"]];
//        if (isHeaderFresh) {
//            [self.tradesTableView endHeaderRefreshWith:ordersModel.payments];
//        } else {
//            [self.tradesTableView endFooterRefreshWith:ordersModel.payments];
//        }
//        [self.tradesTableView reloadData];
//    } failure:^(NSError *error) {
//        TEN_LOG(@"%@", error);
//    }];
}


@end

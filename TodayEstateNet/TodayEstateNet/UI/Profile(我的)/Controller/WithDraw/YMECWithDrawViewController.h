//
//  YMECWithDrawViewController.h
//  YMEconomicCircle
//
//  Created by HomerLynn on 2018/6/1.
//  Copyright © 2018年 YME. All rights reserved.
//

//#import "YMECBaseViewController.h"
@class YMECPayPasswordPhoneView;

@interface YMECWithDrawViewController : TENBaseViewController

@property (strong, nonatomic) YMECPayPasswordPhoneView * moneyView;
@property (strong, nonatomic) YMECPayPasswordPhoneView * aliPayNumberView;
@property (strong, nonatomic) YMECPayPasswordPhoneView * trueNameView;
@property (weak, nonatomic) IBOutlet UILabel *payAttensionLabel;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;

@end

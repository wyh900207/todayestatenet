//
//  YMECWithDrawViewController.m
//  YMEconomicCircle
//
//  Created by HomerLynn on 2018/6/1.
//  Copyright © 2018年 YME. All rights reserved.
//

#import "YMECWithDrawViewController.h"
#import "YMECPayPasswordPhoneView.h"
#import "TENPaymentRequest.h"
@interface YMECWithDrawViewController ()

@property (weak, nonatomic) IBOutlet UIView *moneyContainerView;
@property (weak, nonatomic) IBOutlet UIView *aliPayNumberContainerView;
@property (weak, nonatomic) IBOutlet UIView *trueNameContainerView;
@property (weak, nonatomic) IBOutlet UIButton *commitBtn;

@property (copy, nonatomic) NSString * currentMoney;
@property (copy, nonatomic) NSString * currentAlipayNumber;
@property (copy, nonatomic) NSString * currentTrueName;

@end

@implementation YMECWithDrawViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupSubviews];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -

- (void)setupSubviews {
    self.navigationItem.title = @"提现";
    [self.view addSubview:self.moneyContainerView];
    [self.view addSubview:self.aliPayNumberContainerView];
    [self.view addSubview:self.trueNameContainerView];
    [self.view addSubview:self.commitBtn];
    [self.commitBtn setBackgroundColor:TENHexColor(TENThemeBlueColor)];
    
    [self.moneyContainerView addSubview:self.moneyView];
    [self.aliPayNumberContainerView addSubview:self.aliPayNumberView];
    [self.trueNameContainerView addSubview:self.trueNameView];
    
    [self.moneyView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.moneyContainerView);
    }];
    [self.aliPayNumberView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.aliPayNumberContainerView);
    }];
    [self.trueNameView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.edges.equalTo(self.trueNameContainerView);
    }];
    
    TENCornerRadius(self.submitButton, 5);
}

#pragma mark -

- (YMECPayPasswordPhoneView *)moneyView {
    if (!_moneyView) {
        _moneyView = [[NSBundle mainBundle] loadNibNamed:@"YMECPayPasswordPhoneView" owner:nil options:nil].firstObject;
        _moneyView.titleLabel.text = @"金额(元)";
        _moneyView.textField.placeholder = @"请输入提现金额";
        _moneyView.textField.keyboardType = UIKeyboardTypeNumberPad;
        __weak typeof(self) wSelf = self;
        _moneyView.textChangeAction = ^(NSString *text) {
            __strong typeof(wSelf) sSelf = wSelf;
            sSelf.currentMoney = text;
        };
    }
    return _moneyView;
}

- (YMECPayPasswordPhoneView *)aliPayNumberView {
    if (!_aliPayNumberView) {
        _aliPayNumberView = [[NSBundle mainBundle] loadNibNamed:@"YMECPayPasswordPhoneView" owner:nil options:nil].firstObject;
        _aliPayNumberView.titleLabel.text = @"支付宝";
        _aliPayNumberView.textField.placeholder = @"请输入您的支付宝账号";
        __weak typeof(self) wSelf = self;
        _aliPayNumberView.textChangeAction = ^(NSString *text) {
            __strong typeof(wSelf) sSelf = wSelf;
            sSelf.currentAlipayNumber = text;
        };
    }
    return _aliPayNumberView;
}

- (YMECPayPasswordPhoneView *)trueNameView {
    if (!_trueNameView) {
        _trueNameView = [[NSBundle mainBundle] loadNibNamed:@"YMECPayPasswordPhoneView" owner:nil options:nil].firstObject;
        _trueNameView.titleLabel.text = @"真实姓名";
        _trueNameView.textField.placeholder = @"请输入您的支付宝真实姓名";
        __weak typeof(self) wSelf = self;
        _trueNameView.textChangeAction = ^(NSString *text) {
            __strong typeof(wSelf) sSelf = wSelf;
            sSelf.currentTrueName = text;
        };
    }
    return _trueNameView;
}

#pragma mark -

- (IBAction)submit:(id)sender {
    if (!self.currentMoney || self.currentMoney.length <= 0 || [self.currentMoney isEqualToString:@"0"]) {
        [NSObject showHudTipStr:@"请输入提现金额"]; return;
    }
    if (!self.currentAlipayNumber || self.currentAlipayNumber.length <= 0) {
        [NSObject showHudTipStr:@"请输入支付宝账号"]; return;
    }
    if (!self.currentTrueName || self.currentTrueName.length <= 0) {
        [NSObject showHudTipStr:@"请输入真实姓名"]; return;
    }
    
    NSDictionary * params = @{@"accountId": kAccountId,
                              @"cash": self.currentMoney,
                              @"loginToken": kLoginToken,
                              @"password": [@"123456" md5String],
                              @"zfbAccount": self.currentAlipayNumber,
                              @"zfbRealName": self.currentTrueName
                              };
    [TENPaymentRequest zfbWithdrawWithParameters:params response:^(NSDictionary * _Nonnull responseObject) {
        TEN_LOG(@"%@", responseObject);
        if ([responseObject[@"code"] integerValue] == 1) {
            [[NSNotificationCenter defaultCenter] postNotificationName:YMEC_PAY_SUCCESS_NOTIFICATION object:nil];
            [self.navigationController popViewControllerAnimated:YES];
        }else{
            [NSObject showHudTipStr:responseObject[@"msg"]];
        }
    } failure:^(NSError * _Nonnull error) {
        TEN_LOG(@"%@", error);
    }];

}

@end

//
//  TENAttendanceViewController.m
//  HouseEconomics
//
//  Created by apple on 2019/2/21.
//  Copyright © 2019  HomerLynn. All rights reserved.
//

#import "TENAttendanceViewController.h"
#import "TENCalendarView.h"

@interface TENAttendanceViewController ()<BMKMapViewDelegate>

@property (nonatomic, strong) BMKMapView *mapView;
@property (nonatomic, strong) NSString *addrString;
@property (nonatomic, strong) TENCalendarView *calendarView;

@end

@implementation TENAttendanceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self locationNow];
    [self setupSubviews];
}

- (void)setupSubviews{
    self.navigationItem.title = @"考勤管理";
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithTitle:@"历史记录" style:UIBarButtonItemStylePlain target:self action:@selector(rightItemAction)];
    rightItem.tintColor = [UIColor whiteColor];
    self.navigationItem.rightBarButtonItem = rightItem;
    self.mapView = [[BMKMapView alloc] init];
    self.mapView.zoomLevel = 15;
    self.mapView.delegate = self;
    [self.view addSubview:self.mapView];
    [self.mapView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.top.equalTo(self.view);
        make.height.equalTo(self.view.mas_height).multipliedBy(0.28);
    }];

    [self.view addSubview:self.calendarView];
    
    [self.calendarView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.top.equalTo(self.mapView.mas_bottom);
        make.height.mas_equalTo(105);
    }];
    
}
- (void)locationNow{
    @weakify(self)
    [[TENLocationManager share] locationJustOneceTimeWith:^(CLLocation *location, BMKLocationReGeocode *rgcData) {
        @strongtify(self)
        NSLog(@"location:%@",location);
        NSLog(@"rgcData:%@",rgcData);
        self.addrString = [NSString stringWithFormat:@"%@%@",rgcData.district,rgcData.street];
        BMKPointAnnotation *annotation = [[BMKPointAnnotation alloc] init];
        self.mapView.centerCoordinate = location.coordinate;
        annotation.coordinate = self.mapView.centerCoordinate;
        [self.mapView addAnnotation:annotation];
        
        
    } faild:^(NSError *error) {
        TEN_LOG(@"无法获取当前位置！");
    }];
}
- (void)rightItemAction{
    //获取签到记录
    
}

- (BMKAnnotationView *)mapView:(BMKMapView *)mapView viewForAnnotation:(id<BMKAnnotation>)annotation{
    BMKPinAnnotationView *newAnnotationView = [[BMKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"myAnnotation"];
    newAnnotationView.annotation = annotation;
    newAnnotationView.image = [UIImage imageNamed:@"paopao.png"];
    [newAnnotationView setSelected:YES animated:YES];
    return newAnnotationView;
}
- (TENCalendarView *)calendarView{
    if (!_calendarView) {
        _calendarView = [[TENCalendarView alloc] initWithFrame:CGRectZero];
    }
    return _calendarView;
}

@end

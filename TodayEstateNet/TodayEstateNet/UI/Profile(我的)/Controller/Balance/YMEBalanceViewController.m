//
//  YMEBalanceViewController.m
//  YMEconomicCircle
//
//  Created by Admin on 2018/5/30.
//  Copyright © 2018年 YME. All rights reserved.
//

#import "YMEBalanceViewController.h"
//#import "YMEAddressBookViewController.h"
#import "YMECWithDrawViewController.h"
#import "YMECRechargeViewController.h"
#import "TENPaymentRequest.h"
@interface YMEBalanceViewController ()
@property (weak, nonatomic) IBOutlet UIView *balanceView;

@property (weak, nonatomic) IBOutlet UIButton *withdrawalBtn;
@property (weak, nonatomic) IBOutlet UIButton *chargeBtn;

@end

@implementation YMEBalanceViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"余额";
    self.view.backgroundColor=[UIColor colorWithRed:248/255.0 green:248/255.0 blue:248/255.0 alpha:1];
    self.withdrawalBtn.layer.borderWidth = 1;
    self.withdrawalBtn.layer.borderColor = YMEC_HEX_COLOR(0xCECECE).CGColor;
    [self fetchBalance];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fetchBalance) name:YMEC_PAY_SUCCESS_NOTIFICATION object:nil];
    [self.view addSubview:self.balanceView];
    [self.view addSubview:self.withdrawalBtn];
    [self.view addSubview:self.chargeBtn];
    [self.chargeBtn setBackgroundColor:TENHexColor(TENThemeBlueColor)];

    
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:YMEC_PAY_SUCCESS_NOTIFICATION object:nil];
}

-(void)rightBtnClick {
    NSLog(@"交易记录");
}

#pragma mark  充值

- (IBAction)topUpBtnClick:(id)sender {
    YMECRechargeViewController * rechargeController = [[YMECRechargeViewController alloc]initWithNibName:@"YMECRechargeViewController" bundle:nil];
    [self.navigationController pushViewController:rechargeController animated:YES];
}

#pragma mark  提现

- (IBAction)withdramalBtnClick:(id)sender {
    YMECWithDrawViewController * withdrawController = [[YMECWithDrawViewController alloc] initWithNibName:@"YMECWithDrawViewController" bundle:nil];
    [self.navigationController pushViewController:withdrawController animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -

/// 获取余额
- (void)fetchBalance {
    NSDictionary * params = @{@"accountId": kAccountId,
                              @"pageNo": @"1",
                              @"pageSize": @"20"
                              };
    @weakify(self)
    [TENPaymentRequest getBalanceAndListParameters:params response:^(NSDictionary * _Nonnull responseObject) {
        @strongtify(self)
        NSDictionary * content = responseObject[@"content"];
        NSNumber * balance = content[@"balance"];
        NSString * balanceString = [NSString stringWithFormat:@"%.2f", balance.doubleValue];
        self.balanceLabel.text = balanceString;
    
    } failure:^(NSError * _Nonnull error) {
        TEN_LOG(@"%@", error);

    }];
}

@end

//
//  YMECPictureCodeViewController.m
//  YMEconomicCircle
//
//  Created by HomerLynn on 2018/5/11.
//  Copyright © 2018年 YME. All rights reserved.
//

#import "YMECPictureCodeViewController.h"

@interface YMECPictureCodeViewController ()

@property (weak, nonatomic) IBOutlet UITextField *codeTextField;
@property (weak, nonatomic) IBOutlet UIWebView *webview;
@property (weak, nonatomic) IBOutlet UIButton *cancleButton;
@property (weak, nonatomic) IBOutlet UIButton *commitBUtton;
@property (weak, nonatomic) IBOutlet UIButton *refreshCodeButton;

@end

@implementation YMECPictureCodeViewController

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    NSString *urlString = [NSString stringWithFormat:@"%@%@%@", @"", @"?phone=86", self.phoneText];
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    
    [self.webview loadRequest:request];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.codeTextField.layer.borderColor = TENHexColor(TENWhiteColor).CGColor;
    self.codeTextField.layer.borderWidth = 1;
    self.codeTextField.layer.masksToBounds = YES;
    [self.codeTextField addTarget:self action:@selector(textChanged:) forControlEvents:UIControlEventEditingChanged];
    
    self.cancleButton.layer.cornerRadius = 5;
    self.cancleButton.layer.borderColor = TENHexColor(TENWhiteColor).CGColor;
    self.cancleButton.layer.borderWidth = 1;
    self.cancleButton.layer.masksToBounds = YES;
    
    self.commitBUtton.layer.cornerRadius = 5;
    self.commitBUtton.layer.borderColor = TENHexColor(TENWhiteColor).CGColor;
    self.commitBUtton.layer.borderWidth = 1;
    self.commitBUtton.layer.masksToBounds = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)goBack:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)commit:(id)sender {
    if (self.commitHandle) self.commitHandle();
}

- (IBAction)refreshCode:(UIButton *)sender {
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@", @"", @"?phone=86", self.phoneText];
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    [self.webview loadRequest:request];
}

#pragma mark -

- (void)textChanged:(UITextField *)textField {
    if (self.textDidChangeHandle) self.textDidChangeHandle(textField.text);
}

@end

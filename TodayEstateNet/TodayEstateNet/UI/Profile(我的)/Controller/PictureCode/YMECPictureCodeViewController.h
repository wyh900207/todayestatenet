//
//  YMECPictureCodeViewController.h
//  YMEconomicCircle
//
//  Created by HomerLynn on 2018/5/11.
//  Copyright © 2018年 YME. All rights reserved.
//

//#import "YMECBaseViewController.h"

@interface YMECPictureCodeViewController : TENBaseViewController

@property (nonatomic, copy) NSString *phoneText;

@property (nonatomic, copy) void (^commitHandle)(void);
@property (nonatomic, copy) void (^textDidChangeHandle)(NSString *text);

@end

//
//  YMECOrdersViewController.m
//  YMEconomicCircle
//
//  Created by HomerLynn on 2018/6/7.
//  Copyright © 2018年 YME. All rights reserved.
//

#import "YMECOrdersViewController.h"
#import "YMECOrderTableViewCell.h"
#import "YMECOrderModel.h"
#import "TENPaymentRequest.h"
@interface YMECOrdersViewController () <UITableViewDelegate, UITableViewDataSource, YMECTableViewRefreshProtocol>

@property (weak, nonatomic) IBOutlet TENTableView * ordersTableView;

@end

@implementation YMECOrdersViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupSubviews];
    [self fetchOrdersWith:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -

- (void)setupSubviews {
    self.navigationItem.title = @"订单信息";
    [self.ordersTableView registerNib:[UINib nibWithNibName:@"YMECOrderTableViewCell" bundle:nil] forCellReuseIdentifier:@"ordersTableViewCell"];
    self.ordersTableView.rowHeight = 150;
    self.ordersTableView.estimatedRowHeight = 150;
    self.ordersTableView.separatorColor = YMEC_COLOR_CLEAR;
    self.ordersTableView.refreshType = TENTableViewRefreshTypeBoth;
    self.ordersTableView.refreshDelegate = self;
    [self.contentView addSubview:self.ordersTableView];
}

#pragma mark - UITableViewDelegate, UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.ordersTableView.dataSourceArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 10.f;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0.01f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UIView * view = [UIView new];
    view.frame = CGRectMake(0, 0, TENScreenWidth, 10);
    view.backgroundColor = YMEC_COLOR_TABLE_BACK;
    return view;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    YMECOrderTableViewCell * cell = [tableView dequeueReusableCellWithIdentifier:@"ordersTableViewCell" forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.model = self.ordersTableView.dataSourceArray[indexPath.section];
    __weak typeof(self) wself = self;
    cell.updateWechatOrderState = ^{
        __strong typeof(wself) sself = wself;
        YMECOrderDetailModel * model = sself.ordersTableView.dataSourceArray[indexPath.section];
        model.orderStatus = @2;
        [self.ordersTableView.dataSourceArray replaceObjectAtIndex:indexPath.section withObject:model];
    };
    cell.updateAliOrderState = ^{
        __strong typeof(wself) sself = wself;
        YMECOrderDetailModel * model = sself.ordersTableView.dataSourceArray[indexPath.section];
        model.orderStatus = @2;
        [self.ordersTableView.dataSourceArray replaceObjectAtIndex:indexPath.section withObject:model];
    };

    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    YMECOrderDetailModel * model = self.ordersTableView.dataSourceArray[indexPath.section];
    return !(model.orderStatus.integerValue == 2);
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewCellEditingStyleDelete;
}

- (NSString *)tableView:(UITableView *)tableView titleForDeleteConfirmationButtonForRowAtIndexPath:(NSIndexPath *)indexPath {
    return @"删除订单";
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    YMECOrderDetailModel * model = self.ordersTableView.dataSourceArray[indexPath.section];
    @weakify(self);
    NSDictionary * params = @{@"id": DL_IS_NULL(model.order_id),
                              @"accountId": kAccountId
                              };
    [TENPaymentRequest deleteUnpayOrderWithParameters:params response:^(NSDictionary * _Nonnull responseObject) {
        @strongtify(self);
        TEN_LOG(@"%@", responseObject);
        [self.ordersTableView.dataSourceArray removeObjectAtIndex:indexPath.section];
        [self.ordersTableView reloadData];
        [NSObject showHudTipStr:@"删除成功"];
    } failure:^(NSError * _Nonnull error) {
        TEN_LOG(@"%@", error);
    }];
//    [self postWithURL:YMEC_PAY_DELETE_UNPAY_ORDER_URL params:params success:^(NSDictionary *dict) {
//        __strong typeof(wself) sself = wself;
//        TEN_LOG(@"%@", dict);
//        [sself.ordersTableView.dataSourceArray removeObjectAtIndex:indexPath.section];
//        [sself.ordersTableView reloadData];
//        [NSObject showHudTipStr:@"删除成功"];
//    } failure:^(NSError *error) {
//        TEN_LOG(@"%@", error);
//    }];
}

#pragma mark - YMECTableViewRefreshProtocol

- (void)headerWillRefresh {
    [self fetchOrdersWith:YES];
}

- (void)footerWillRefresh {
    [self fetchOrdersWith:NO];
}

#pragma mark -

- (void)fetchOrdersWith:(BOOL)isHeaderFresh {
    NSDictionary * params = @{@"accountId": kAccountId,
                              @"id": @"0",
                              @"orderStatus": @"0",         // 1.
                              @"pageNo": [NSString stringWithFormat:@"%lu", self.ordersTableView.pageCount],
                              @"pageSize": [NSString stringWithFormat:@"%lu", self.ordersTableView.pageSize]
                              };
    
    [TENPaymentRequest getAllOrderListsWithParameters:params response:^(NSDictionary * _Nonnull responseObject) {
        TEN_LOG(@"%@", responseObject);
        YMECOrderModel * ordersModel = [YMECOrderModel mj_objectWithKeyValues:responseObject];
            if (isHeaderFresh) {
                [self.ordersTableView endHeaderRefreshWith:ordersModel.content];
            } else {
                [self.ordersTableView endFooterRefreshWith:ordersModel.content];
            }
            [self.ordersTableView reloadData];
    } failure:^(NSError * _Nonnull error) {
        TEN_LOG(@"%@", error);
    }];
//    [self postWithURL:YMEC_PAY_ALL_ORDERS_URL params:params success:^(NSDictionary *dict) {
//        TEN_LOG(@"%@", dict);
//        YMECOrderModel * ordersModel = [YMECOrderModel mj_objectWithKeyValues:dict];
//        if (isHeaderFresh) {
//            [self.ordersTableView endHeaderRefreshWith:ordersModel.content];
//        } else {
//            [self.ordersTableView endFooterRefreshWith:ordersModel.content];
//        }
//        [self.ordersTableView reloadData];
//    } failure:^(NSError *error) {
//        TEN_LOG(@"%@", error);
//        [self.ordersTableView endRefreshWithError];
//    }];
}

@end

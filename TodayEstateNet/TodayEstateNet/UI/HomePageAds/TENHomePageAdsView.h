//
//  TENHomePageAdsView.h
//  TodayEstateNet
//
//  Created by HomerLynn on 2019/2/21.
//  Copyright © 2019 TEN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TENHomePageAdsView : UIView

@property (nonatomic, copy) NSString *adsImageURL;
@property (nonatomic, copy) void (^openHandler)(void);
@property (nonatomic, copy) void (^closeHandler)(void);

@end

//
//  TENHomePageAdsView.m
//  TodayEstateNet
//
//  Created by HomerLynn on 2019/2/21.
//  Copyright © 2019 TEN. All rights reserved.
//

#import "TENHomePageAdsView.h"

@interface TENHomePageAdsView ()

@property (nonatomic, strong) UIButton *adsImageButton;
@property (nonatomic, strong) UIButton *closeButton;

@end

@implementation TENHomePageAdsView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setupSubviews];
    }
    return self;
}

#pragma mark - Init

- (void)setupSubviews {
    self.backgroundColor = [UIColor colorWithHexString:TENBlackColor alpha:0.8];
    
    [self addSubview:self.adsImageButton];
    [self addSubview:self.closeButton];
    
    [self.adsImageButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.centerY.equalTo(self).offset(-40);
        make.width.equalTo(@260);
        make.height.equalTo(@360);
    }];
    [self.closeButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.centerX.equalTo(self);
        make.top.equalTo(self.adsImageButton.mas_bottom).offset(50);
        make.width.height.equalTo(@30);
    }];
}

#pragma mark - Getter & Setter

- (UIButton *)adsImageButton {
    if (!_adsImageButton) {
        _adsImageButton = [UIButton new];
        [_adsImageButton addTarget:self action:@selector(openAds) forControlEvents:UIControlEventTouchUpInside];
    }
    return _adsImageButton;
}

- (UIButton *)closeButton {
    if (!_closeButton) {
        _closeButton = [UIButton new];
        [_closeButton setImage:TENImage(@"home_close_ads") forState:0];
        [_closeButton addTarget:self action:@selector(closeAds) forControlEvents:UIControlEventTouchUpInside];
    }
    return _closeButton;
}

- (void)setAdsImageURL:(NSString *)adsImageURL {
    _adsImageURL = adsImageURL;
    [self.adsImageButton sd_setBackgroundImageWithURL:[NSURL URLWithString:adsImageURL] forState:0];
}

#pragma mark - Private

- (void)openAds {
    if (self.openHandler) {
        self.openHandler();
    }
}

- (void)closeAds {
    if (self.closeHandler) {
        self.closeHandler();
    }
}

@end

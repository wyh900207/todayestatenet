//
//  TENMainViewController.m
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/9/11.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "TENMainViewController.h"
#import "TENNavigationViewController.h"
#import "TENUtilsHeader.h"
// Tabbar
#import "TENHomeViewController.h"
#import "TENSearchDepartmentViewController.h"
#import "TENRecommendViewController.h"
#import "TENMessageViewController.h"
#import "TENProfileViewController.h"

@interface TENMainViewController ()

@end

@implementation TENMainViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // modifi test
    //----------------------------1.6版本-添加颜色设置----------------------------
    // 设置tabBar
    UIColor *backgroundColor = [UIColor colorWithHexString:TENWhiteColor];
    UIColor *foregroundColor = [UIColor colorWithHexString:TENThemeBlueColor];
    
    self.tabBar.barTintColor = backgroundColor;
    self.tabBar.tintColor = foregroundColor;
    //-----------------------------------end-----------------------------------
    
    // 创建数组.
    NSArray *titlesArr = @[@"首页", @"找房", @"推荐", @"消息", @"我的"];
    NSArray *imagesArr = @[@"tabbar_home", @"tabbar_search", @"tabbar_recommend", @"tabbar_message", @"tabbar_profile"];
    
    // 创建对应的控制器放加入TabBarController的数组中.
    self.viewControllers = @[[[TENNavigationViewController alloc] initWithRootViewController:[TENHomeViewController new]],
                             [[TENNavigationViewController alloc] initWithRootViewController:[TENSearchDepartmentViewController new]],
                             [[TENNavigationViewController alloc] initWithRootViewController:[TENRecommendViewController new]],
                             [[TENNavigationViewController alloc] initWithRootViewController:[TENMessageViewController new]],
                             [[TENNavigationViewController alloc] initWithRootViewController:[TENProfileViewController new]]
                             ];
    
    // 遍历将文字和图片放入tabBar的Item中
    for (int i = 0; i < self.viewControllers.count; ++i) {
        //        UIViewController *vc = self.viewControllers[i];
        TENNavigationViewController *vc = self.viewControllers[i];
        
        NSString *notSelectedImage = imagesArr[i];
        NSString *selectedImage = [NSString stringWithFormat:@"%@_hl", imagesArr[i]];
        
        vc.tabBarItem.title = titlesArr[i];
        vc.tabBarItem.image = TENImage(notSelectedImage);
        vc.tabBarItem.selectedImage = TENImage(selectedImage);
        
        /*
        vc.navigationBar.barTintColor = backgroundColor;
        vc.navigationBar.tintColor = foregroundColor;
        vc.navigationBar.titleTextAttributes = @{NSForegroundColorAttributeName: foregroundColor};
         */
    }
    // 状态栏样式设置
    //[[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

@end

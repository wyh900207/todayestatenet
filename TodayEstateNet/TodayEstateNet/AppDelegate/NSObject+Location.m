//
//  NSObject+Location.m
//  TodayEstateNet
//
//  Created by HomerLynn on 2019/2/24.
//  Copyright © 2019 TEN. All rights reserved.
//

#import "NSObject+Location.h"

@implementation NSObject (Location)

// 定位权限是否开启
- (void)locationPliocy {
    @weakify(self);
    [[TENLocationManager share] locationWith:^{
        @strongtify(self);
        [self locationNow];
    } faild:nil];
}

#pragma mark -

- (void)locationNow {
    [[TENLocationManager share] locationJustOneceTimeWith:^(CLLocation *location, BMKLocationReGeocode *rgcData) {
        NSLog(@"location:%@",location);
        NSLog(@"rgcData:%@",rgcData);
        
        NSString *section = [NSString stringWithFormat:@"%@%@",rgcData.district,rgcData.street];
        NSString *city = rgcData.city;
        
        // 保存定位信息
        TEN_UD_SET_VALUE(section, kSectionCode);
        TEN_UD_SET_VALUE(city, kCityName);
        // 判断是否修改`display city name`
        NSString *display = TEN_UD_VALUE(kDisplayCityName);
        if (!display || [display isEqualToString:@""]) {
            TEN_UD_SET_VALUE(city, kDisplayCityName);
        }
    } faild:^(NSError *error) {
        // 定位失败, 不做任何操作. 由`get`方法, 获取默认信息
    }];
}

@end

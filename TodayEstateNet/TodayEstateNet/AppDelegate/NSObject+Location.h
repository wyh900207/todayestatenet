//
//  NSObject+Location.h
//  TodayEstateNet
//
//  Created by HomerLynn on 2019/2/24.
//  Copyright © 2019 TEN. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Location)

// 定位权限是否开启
- (void)locationPliocy;
// 单次定位
- (void)locationNow;

@end

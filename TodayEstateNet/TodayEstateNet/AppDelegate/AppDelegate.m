//
//  AppDelegate.m
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/9/9.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import "AppDelegate.h"
#import "TENUtilsHeader.h"
#import <BMKLocationKit/BMKLocationAuth.h>
#import <BaiduMapKit/BaiduMapAPI_Base/BMKMapManager.h>
// 登录
#import "TENLoginViewController.h"
#import "TENBindingPhoneViewController.h"
#import "AppDelegate+Umeng.h"

@interface AppDelegate ()<BMKLocationAuthDelegate>

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    self.window = [UIWindow new];
    self.window.backgroundColor = [UIColor whiteColor];
    self.window.frame = [UIScreen mainScreen].bounds;
    
    // 友盟
    [self configUmeng];
    
    // 百度地图
    BMKMapManager * mapManager = [[BMKMapManager alloc] init];
#ifdef DEBUG
    // 定位
    [[BMKLocationAuth sharedInstance] checkPermisionWithKey:@"FdMbKDcoGRQ6wZVas1LXe1e4m3UqmxRi" authDelegate:self];
    // 地图
    BOOL ret = [mapManager start:@"FdMbKDcoGRQ6wZVas1LXe1e4m3UqmxRi"  generalDelegate:nil];
    if (!ret) {
        NSLog(@"manager start failed!");
    }
#else
    [[BMKLocationAuth sharedInstance] checkPermisionWithKey:@"FdMbKDcoGRQ6wZVas1LXe1e4m3UqmxRi" authDelegate:self];
    BOOL ret = [mapManager start:@"FdMbKDcoGRQ6wZVas1LXe1e4m3UqmxRi"  generalDelegate:nil];
    if (!ret) {
        NSLog(@"manager start failed!");
    }
#endif
    
    TENMainViewController *mainViewController = [TENMainViewController new];
    self.window.rootViewController = mainViewController;
    
    [self.window makeKeyAndVisible];
    
    // 用来自定义`UserAgent`
    [self customGlobalUserAgent];
    
    return YES;
}

// 设置全局的`UserAgent`
- (void)customGlobalUserAgent {
    NSString *appendingUserAgent = @"jrfwIOS";
    
    WKWebView *webview = [WKWebView new];
    //@weakify(self);
    [webview evaluateJavaScript:@"navigator.userAgent" completionHandler:^(id result, NSError *error) {
        TEN_LOG(@"userAgent :%@", result);
        //@strongtify(self);
        if ([result containsString:appendingUserAgent]) {
            return;
        }
        
        NSString *UA = [result stringByAppendingString:[NSString stringWithFormat:@";%@", appendingUserAgent]];
        
        // 设置global User-Agent
        NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:UA, @"UserAgent", nil];
        [[NSUserDefaults standardUserDefaults] registerDefaults:dictionary];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSURL *baidu = [NSURL URLWithString:@"https://www.baidu.com/"];
        NSURLRequest *request = [NSURLRequest requestWithURL:baidu];
        [webview loadRequest:request];
    }];
}

#if __IPHONE_OS_VERSION_MAX_ALLOWED > 100000
- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey, id> *)options
{
    //6.3的新的API调用，是为了兼容国外平台(例如:新版facebookSDK,VK等)的调用[如果用6.2的api调用会没有回调],对国内平台没有影响。
    BOOL result = [[UMSocialManager defaultManager]  handleOpenURL:url options:options];
    if (!result) {
        // 其他如支付等SDK的回调
    }
    return result;
}
#endif

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    //6.3的新的API调用，是为了兼容国外平台(例如:新版facebookSDK,VK等)的调用[如果用6.2的api调用会没有回调],对国内平台没有影响
    BOOL result = [[UMSocialManager defaultManager] handleOpenURL:url sourceApplication:sourceApplication annotation:annotation];
    if (!result) {
        // 其他如支付等SDK的回调
    }
    return result;
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url
{
    BOOL result = [[UMSocialManager defaultManager] handleOpenURL:url];
    if (!result) {
        // 其他如支付等SDK的回调
    }
    return result;
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


@end

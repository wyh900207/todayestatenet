//
//  AppDelegate+Umeng.h
//  TodayEstateNet
//
//  Created by  HomerLynn on 2018/12/11.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (Umeng)

- (void)configUmeng;

@end

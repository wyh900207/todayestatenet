//
//  AppDelegate.h
//  TodayEstateNet
//
//  Created by HomerLynn on 2018/9/9.
//  Copyright © 2018年 TEN. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TENMainViewController.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
// Tabbar controller
@property (nonatomic, strong) TENMainViewController *mainViewController;

@end


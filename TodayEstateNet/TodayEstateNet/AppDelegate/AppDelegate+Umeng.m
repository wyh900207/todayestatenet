//
//  AppDelegate+Umeng.m
//  TodayEstateNet
//
//  Created by  HomerLynn on 2018/12/11.
//  Copyright © 2018 TEN. All rights reserved.
//

#import "AppDelegate+Umeng.h"
#import <UMSocialCore/UMSocialCore.h>

@implementation AppDelegate (Umeng)

- (void)configUmeng {
    [[UMSocialManager defaultManager] setPlaform:UMSocialPlatformType_WechatSession appKey:@"wxc418f81e88ae591c" appSecret:@"c18ddbc120eb4f6ca7420b053b1ac35a" redirectURL:@"http://mobile.umeng.com/social"];
}

@end
